<?php
use Illuminate\Support\Facades\Mail;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/sendnotification', function () {
    Artisan::call("queue:work --once");
    Artisan::call("queue:restart");
    return;
});

Route::get('/sendmail', function () {
     Mail::raw("test1111", function ($message) {
        $message->from('smtp@teqtop.com', 'Test mail');
        $message->to('sunil.kumar@xcelance.com');
        $message->subject('test email1');
    });

    // check for failures
    if (Mail::failures()) {
        echo "dfhkdjfh";
    }else{
    	echo "send";
    }    
});
//Reoptimized class loader:
Route::get('reoptimize', function() {
    $exitCode = Artisan::call('config:cache');
    $exitCode = Artisan::call('route:clear');
    $exitCode = Artisan::call('view:clear');
    $exitCode = Artisan::call('config:clear');
    return '<h1>Reoptimized class loader</h1>';
});
Route::get('/{any}', function () {
    return view('coreui.homepage');
})->where('any', '.*');

//Auth::routes();
//Route::get('/home', 'HomeController@index')->name('home');

/*
Route::post('register', 'AuthController@register');
Route::post('login', 'AuthController@login');
Route::get('logout', 'AuthController@logout');
Route::post('logout', 'AuthController@logout');
*/