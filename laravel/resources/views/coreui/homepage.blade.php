	<!DOCTYPE html>
<html>
  	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <link rel="icon" href="/favicon.ico">
	    <title>Project Portal</title>

	    <link href="{{ url('assets/css/TextTip.css') }}" rel="stylesheet">
	    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
	    <link href="{{ url('fonts.min.css') }}" rel="stylesheet">
	    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
	    <link rel="preconnect" href="https://fonts.gstatic.com">
		<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	    <script>
	        window.Laravel = {!! json_encode([
	            'csrfToken' => csrf_token(),
	        ]) !!};
	    </script>
  	</head>
  
  	<body>
	    <noscript>
	      	<strong>We're sorry but this app doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
	    </noscript>
	    <div id="app"></div>
	    <!-- built files will be auto injected -->
	    <div class="row hide download-file" id="file_downloader">
	    	<a type="button" id="download_btn_opt" download><img style="max-width: 13px; width: auto;" src="/img/download.png"> Download</a>
	    </div>

	    <script src="{{ asset('js/app.js') }}"></script>
	    <script src="{{ url('assets/js/TextTip.js') }}"></script>
	    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
	    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
	    <script src="{{ url('assets/js/jquery.show-more.js') }}"></script>
	    <script src="{{ url('assets/js/custom.js') }}"></script>
  	</body>
</html>
