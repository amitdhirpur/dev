-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 11, 2021 at 08:24 AM
-- Server version: 5.7.33-0ubuntu0.16.04.1
-- PHP Version: 7.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `deepcheck_live`
--

-- --------------------------------------------------------

--
-- Table structure for table `invoices_approval`
--

CREATE TABLE `invoices_approval` (
  `media_id` int(11) NOT NULL COMMENT 'reference to the media ID for the document',
  `user_id` bigint(20) NOT NULL COMMENT 'User id for refering to the main account number',
  `approval_flow` varchar(255) NOT NULL COMMENT 'Here the name of the loop is stored',
  `email_address` varchar(255) NOT NULL COMMENT 'The person who is approving the invoice is stored here',
  `email_sent` date NOT NULL COMMENT 'The email is added to the field when it is sent out',
  `reminder_sent` int(11) NOT NULL COMMENT 'add number of times an email has been sent.',
  `isApproved` int(11) NOT NULL COMMENT '0=pending review\r\n1=approved\r\n2=not approved\r\n3=error and correction',
  `time_approval` date NOT NULL COMMENT 'What date is the approval or not apprval done on',
  `admin_approved` int(11) NOT NULL COMMENT '0=admin NOT approved\r\n1=admin approved'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
