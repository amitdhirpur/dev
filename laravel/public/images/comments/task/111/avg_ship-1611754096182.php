<?php

defined ('_JEXEC') or die('Restricted access');

/**
 * Shipment plugin for Star Track shipments, like regular postal services
 *
 * @version $Id: star_track.php 9233 2016-06-22 21:13:23Z Milbo $
 * @package VirtueMart
 * @subpackage Plugins - shipment
 * @copyright Copyright (C) 2004-2012 VirtueMart Team - All rights reserved.
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL, see LICENSE.php
 * VirtueMart is free software. This version may have been modified pursuant
 * to the GNU General Public License, and as distributed it includes or
 * is derivative of works licensed under the GNU General Public License or
 * other free or open source software licenses.
 * See /administrator/components/com_virtuemart/COPYRIGHT.php for copyright notices and details.
 *
 * http://virtuemart.org
 * @author Maneesh Garg
 *
 */

if (!class_exists ('vmPSPlugin')) {
	require(JPATH_VM_PLUGINS . DS . 'vmpsplugin.php'); 
}

require_once( dirname(__FILE__)."/star_track/Executables/eServices.php");// Import the StarTrack PHP API - do not modify this file
require_once( dirname(__FILE__)."/star_track/Executables/CustomerConnect.php");

/**
 *
 */
class plgVmShipmentAvg_ship extends vmPSPlugin {

	/**
	 * @param object $subjectplugins/vmshipment/avg_ship
	 * @param array  $config
	 */
	function __construct (& $subject, $config) {

		parent::__construct ($subject, $config);

		$this->_loggable = TRUE;
		$this->_tablepkey = 'id';
		$this->_tableId = 'id';
		$this->tableFields = array_keys ($this->getTableSQLFields ());
		$varsToPush = $this->getVarsToPush ();
		$this->setConfigParameterable ($this->_configTableFieldName, $varsToPush);
		$this->setConvertable(array('orderamount_start','orderamount_stop','shipment_cost','package_fee'));
		//vmdebug('Muh constructed plgVmShipmentSTAR_TRACK',$varsToPush);
	}

	/**
	 * Create the table for this plugin if it does not yet exist.
	 *
	 * @author Maneesh Garg
	 */
	public function getVmPluginCreateTableSQL () {

		return $this->createTableSQL ('Shipment Star Track Table');
	}

	/**
	 * @return array
	 */
	function getTableSQLFields () {

		$SQLfields = array(
			'id'                           => 'int(1) UNSIGNED NOT NULL AUTO_INCREMENT',
			'virtuemart_order_id'          => 'int(11) UNSIGNED',
			'order_number'                 => 'char(32)',
			'virtuemart_shipmentmethod_id' => 'mediumint(1) UNSIGNED',
			'shipment_name'                => 'varchar(5000)',
			'order_weight'                 => 'decimal(10,4)',
			'shipment_weight_unit'         => 'char(3) DEFAULT \'KG\'',
			'shipment_cost'                => 'decimal(10,2)',
			'shipment_package_fee'         => 'decimal(10,2)',
			'tax_id'                       => 'smallint(1)'
		);
		return $SQLfields;
	}

	/**
	 * This method is fired when showing the order details in the frontend.
	 * It displays the shipment-specific data.
	 *
	 * @param integer $virtuemart_order_id The order ID
	 * @param integer $virtuemart_shipmentmethod_id The selected shipment method id
	 * @param string  $shipment_name Shipment Name
	 * @return mixed Null for shipments that aren't active, text (HTML) otherwise
	 * @author Maneesh Garg
	 * @author Max Milbers
	 */
	public function plgVmOnShowOrderFEShipment ($virtuemart_order_id, $virtuemart_shipmentmethod_id, &$shipment_name) {

		$this->onShowOrderFE ($virtuemart_order_id, $virtuemart_shipmentmethod_id, $shipment_name);
	}

	/**
	 * This event is fired after the order has been stored; it gets the shipment method-
	 * specific data.
	 *
	 * @param int    $order_id The order_id being processed
	 * @param object $cart  the cart
	 * @param array  $order The actual order saved in the DB
	 * @return mixed Null when this method was not selected, otherwise true
	 * @author Valerie Isaksen
	 */
	function plgVmConfirmedOrder (VirtueMartCart $cart, $order) {
		session_start();
		$standard_shiping_type = $_SESSION['standard_shiping_type'];
		if (!($method = $this->getVmPluginMethod ($order['details']['BT']->virtuemart_shipmentmethod_id))) {
			return NULL; // Another method was selected, do nothing
		}
		if (!$this->selectedThisElement ($method->shipment_element)) {
			return FALSE;
		}
		$values['virtuemart_order_id'] = $order['details']['BT']->virtuemart_order_id;
		$values['order_number'] = $order['details']['BT']->order_number;
		$values['virtuemart_shipmentmethod_id'] = $order['details']['BT']->virtuemart_shipmentmethod_id;
		$values['shipment_name'] = $this->renderPluginName ($method)."<br>"."<span>".$standard_shiping_type."</span>";
		$values['order_weight'] = $this->getOrderWeight ($cart, $method->weight_unit);
		$values['shipment_weight_unit'] = $method->weight_unit;

		$costs = $this->getCosts($cart,$method,$cart->cartPrices);
	
		if(empty($costs)){
			$values['shipment_cost'] = 0;
			$values['shipment_package_fee'] = 0;
		} else {
			$values['shipment_cost'] = $method->shipment_cost;
			$values['shipment_package_fee'] = $method->package_fee;
		}

		$values['tax_id'] = $method->tax_id;
		$this->storePSPluginInternalData ($values);

		return TRUE;
	}

	/**
	 * This method is fired when showing the order details in the backend.
	 * It displays the shipment-specific data.
	 * NOTE, this plugin should NOT be used to display form fields, since it's called outside
	 * a form! Use plgVmOnUpdateOrderBE() instead!
	 *
	 * @param integer $virtuemart_order_id The order ID
	 * @param integer $virtuemart_shipmentmethod_id The order shipment method ID
	 * @param object  $_shipInfo Object with the properties 'shipment' and 'name'
	 * @return mixed Null for shipments that aren't active, text (HTML) otherwise
	 * @author Valerie Isaksen
	 */
	public function plgVmOnShowOrderBEShipment ($virtuemart_order_id, $virtuemart_shipmentmethod_id) {

		if (!($this->selectedThisByMethodId ($virtuemart_shipmentmethod_id))) {
			return NULL;
		}
		$html = $this->getOrderShipmentHtml ($virtuemart_order_id);
		return $html;
	}

	/**
	 * @param $virtuemart_order_id
	 * @return string
	 */
	function getOrderShipmentHtml ($virtuemart_order_id) {
	
		$db = JFactory::getDBO ();
		$q = 'SELECT * FROM `' . $this->_tablename . '` '
			. 'WHERE `virtuemart_order_id` = ' . $virtuemart_order_id;
		$db->setQuery ($q);
		if (!($shipinfo = $db->loadObject ())) {
			vmWarn (500, $q . " " . $db->getErrorMsg ());
			return '';
		}

		if (!class_exists ('CurrencyDisplay')) {
			require(JPATH_VM_ADMINISTRATOR . DS . 'helpers' . DS . 'currencydisplay.php');
		}

		$currency = CurrencyDisplay::getInstance ();
		$tax = ShopFunctions::getTaxByID ($shipinfo->tax_id);
		$taxDisplay = is_array ($tax) ? $tax['calc_value'] . ' ' . $tax['calc_value_mathop'] : $shipinfo->tax_id;
		$taxDisplay = ($taxDisplay == -1) ? vmText::_ ('COM_VIRTUEMART_PRODUCT_TAX_NONE') : $taxDisplay;

		$html = '<table class="adminlist table">' . "\n";
		$html .= $this->getHtmlHeaderBE ();
		$html .= $this->getHtmlRowBE ('STAR_TRACK_SHIPPING_NAME', $shipinfo->shipment_name);
		$html .= $this->getHtmlRowBE ('STAR_TRACK_WEIGHT', $shipinfo->order_weight . ' ' . ShopFunctions::renderWeightUnit ($shipinfo->shipment_weight_unit));
		$html .= $this->getHtmlRowBE ('STAR_TRACK_COST', $currency->priceDisplay ($shipinfo->shipment_cost));
		$html .= $this->getHtmlRowBE ('STAR_TRACK_PACKAGE_FEE', $currency->priceDisplay ($shipinfo->shipment_package_fee));
		$html .= $this->getHtmlRowBE ('STAR_TRACK_TAX', $taxDisplay);
		$html .= '</table>' . "\n";

		return $html;
	}

	/**
	 * @param VirtueMartCart $cart
	 * @param                $method
	 * @param                $cart_prices
	 * @return int
	 */
	function getCosts (VirtueMartCart $cart, $method, $cart_prices) {
	          
		//echo '<pre>';print_r($cart -> getST());echo '</pre>';
			
		if ($method->free_shipment && $cart_prices['salesPrice'] >= $method->free_shipment) {
			return 0.0;
		} else {
			
			$totalCostExGST = 0; 
			$address = $cart -> getST();
			//print_r($address);
			if($address['address_2'] == '' && $address['zip'] == ''){
				/* $address = array(
					'city' => 'AARONS PASS',
					'zip' => '2850',
					'address_2' => 'NSW'
				); */
				return "please add address";
			}
			//print_r($address['zip']);die;
			
			$db = JFactory::getDBO ();
			$q = 'SELECT * FROM `#__virtuemart_states` '
				. 'WHERE `virtuemart_state_id` = ' . $address['virtuemart_state_id'].'';
			$db->setQuery ($q);
			if (!($shipinfo = $db->loadObject ())) {
				vmWarn ( 500, $q . " " . $db->getErrorMsg () );
				return '';
			}
			
			
			$store = array();
			
			$starting_index = substr($address['zip'], 0, 1);  // Check the zip code for selecting store
			
			switch($starting_index){
				case '2' : 
					$store['address_2']  = 'SMEATON GRANGE';
					$store['zip']  = 2567;
					$store['state']  = 'NSW';
				break;
				case '5' : 
					$store['address_2']  = 'HOLDEN HILL';
					$store['zip']  = 5088;
					$store['state']  = 'SA';
				break;
				case '3':
				case '7':
					$store['address_2']  = 'DANDENONG';
					$store['zip']  = 3175;
					$store['state']  = 'VIC';
				break;
				case '8':
				case '6':
					$store['address_2']  = 'KAWANA';
					$store['zip']  = 4701;
					$store['state']  = 'QLD';
				break;
			}
			
			if(empty($store)){
				$storeinfo = array(
						'address_2' => 'Petrie Terrace',
						'zip' => '4000',
						'address_2' => 'QLD'
				);
				
				
				$q = 'SELECT * FROM `#__statTrack_storCodes` '
					. 'WHERE `ship_to` = ' . $address['zip'];
				$db->setQuery ($q);
				$storeinfo = array($db->loadObject());
				
				if(!empty($storeinfo)){
					if($storeinfo[0]->store_location == 'Brisbane'){
						$store['address_2']  = 'Petrie Terrace';
						$store['zip']  = 4000;
						$store['state']  = 'QLD';
					}else{
						//$store['city']  = 'ROCKHAMPTON';
						$store['address_2']  = 'KAWANA';
						$store['zip']  = 4701;
						$store['state']  = 'QLD';
					}
				}else{
					$store['address_2']  = 'KAWANA';
					$store['zip']  = 4701;
					$store['state']  = 'QLD';
				}
			}
			
			if(empty($store)){
				$store['address_2']  = 'KAWANA';
				$store['zip']  = 4701;
				$store['state']  = 'QLD';
			}
			
////////************ vendor assign according to zip code********/////////	
			session_start();
			$_SESSION['order_for_store'] = $store['address_2'];
			// echo $store['city'];die;
				//assign vendor according to zip code
			if(!empty($store['address_2'])){
				if($store['address_2'] == "KAWANA"){
					$vendor_id_acc_zip_code = "1";
				}if($store['address_2'] == "Petrie Terrace"){
					$vendor_id_acc_zip_code = "2";
				}if($store['address_2'] == "SMEATON GRANGE"){
					$vendor_id_acc_zip_code = "3";
				}if($store['address_2'] == "DANDENONG"){
					$vendor_id_acc_zip_code = "4";
				}if($store['address_2'] == "HOLDEN HILL"){
					$vendor_id_acc_zip_code = "5";
				}
			 	 $cart->vendorId = $vendor_id_acc_zip_code;
			} 

		
////////************ vendor assign according to zip code end********/////////


			$volume = 0;
			$volume_incm = 0;
			$weight = 0;
			$quantity = 0;
			
			$height = 0;
			$length = 0;
			$width = 0;
			
			// echo '<pre>';print_r($cart->products);echo '</pre>';
			//echo '<pre>';print_r($store);print_r($address);echo '</pre>';

			foreach($cart->products as $product){
				$unit = $product->product_lwh_uom;
				switch($unit){
					case 'M':
						$volume += (int)$product->quantity * $product->product_height * $product->product_width * $product->product_length;
						$length += $product->quantity * $product->product_length * 100;
						$width += $product->quantity * $product->product_width * 100;
						$height += $product->quantity * $product->product_height * 100;
					break;
					case 'CM':
						$volume += (int)$product->quantity * $product->product_height/100 * $product->product_width/100 * $product->product_length/100;
						$length += $product->quantity * $product->product_length;
						$width += $product->quantity * $product->product_width;
						$height += $product->quantity * $product->product_height;
					break;
					case 'MM':
						$volume += $product->quantity * ($product->product_height/1000) * ($product->product_width/1000) * ($product->product_length/1000);
						
						$length += $product->quantity * $product->product_length / 10;
						$width += $product->quantity * $product->product_width / 10;
						$height += $product->quantity * $product->product_height / 10;
					break;
					case 'YD':
						$volume += (int)$product->quantity * $product->product_height*0.9144 * $product->product_width*0.9144 * $product->product_length*0.9144;
						$length += $product->quantity * $product->product_length / 91.44;
						$width += $product->quantity * $product->product_width / 91.44;
						$height += $product->quantity * $product->product_height / 91.44;
					break;
					case 'FT':
						$volume += (int)$product->quantity * $product->product_height*0.3048 * $product->product_width*0.3048 * $product->product_length*0.3048;
						$length += $product->quantity * $product->product_length / 30.48;
						$width += $product->quantity * $product->product_width / 30.48;
						$height += $product->quantity * $product->product_height / 30.48;
					break;
					case 'IN':
						$volume += (int)$product->quantity * $product->product_height*0.0254 * $product->product_width*0.0254 * $product->product_length*0.0254;
						$length += $product->quantity * $product->product_length / 2.54;
						$width += $product->quantity * $product->product_width / 2.54;
						$height += $product->quantity * $product->product_height / 2.54;
					break;
					default :
						$volume += (int)$product->quantity * $product->product_height/100 * $product->product_width/100 * $product->product_length/100;
						$length += $product->quantity * $product->product_length;
						$width += $product->quantity * $product->product_width;
						$height += $product->quantity * $product->product_height;
					break;
				}
				
				$quantity += $product->quantity;
				$weight += $product->quantity * $product->product_weight;
			}
			
			session_start();
			//echo $weight;
			//echo '**'.$length;
			if($weight < 22 && ($length < 105)  && ($length*$width*$height / 1000000 ) <= 0.25){
				
				$length = $length<1?1:$length;
				$height = $height<1?1:$height;
				$width = $totalWidth<1?1:$width;
				$totalCostExGST = $this->getCostsFirst($cart, $method, $cart_prices, $store, $weight, $length, $width, $height);
				$_SESSION['standard_shiping_type'] = "AusPost";

				$totalCostExGST1 = (float)$totalCostExGST;
				if(empty($totalCostExGST1) || $totalCostExGST1 == " " || $totalCostExGST1 == "0"){
					$application = JFactory::getApplication();

					$totalCostExGST_starTrack = $this->getStarTrackCost($store, $address, $shipinfo, 1, $weight, $volume);

					//echo "Star Track Price is ".$totalCostExGST_starTrack."<br/>";

					if($totalCostExGST_starTrack == 0){
						$application->enqueueMessage(JText::_("There is some Technical error in API. Please check your shipping address1."), 'error');
						$totalCostExGST = 0;
					}else{
							$_SESSION['standard_shiping_type'] = "StarTrack";
							$totalCostExGST =$totalCostExGST_starTrack;
						}


				}

				// $totalCostExGST = number_format($ausprice, 2);
				//echo "Auspost Price is ".$totalCostExGST."<br/>";
			}else{
				
				$application = JFactory::getApplication();
				
				//$totalCostExGST_starTrack = 1;
				//echo "Star Track Price is ".$totalCostExGST_starTrack."<br/>";
				
				/////get enabled shipping method/////

				$qu = 'SELECT * FROM `#__virtuemart_shipmentmethods` WHERE `virtuemart_shipmentmethod_id` = "9"';
				$db->setQuery ($qu);
				$shipmethodP = $db->loadObject();
				$ship_params = $shipmethodP->shipment_params;
				$exParms = explode("|",$ship_params);
				$exArr = explode("=",$exParms[2]);
				$Ship_methods = explode(",", substr($exArr[1],1,-1));

				foreach($Ship_methods as $ship_enable_methods){ 
					$trmspace = trim($ship_enable_methods); 
					$remove_doubleq =  str_replace('"', '', $trmspace);

					if($remove_doubleq == "aus_post"){ $auspost_flag = 1; }
					elseif($remove_doubleq == "star_track"){ $star_track_flag = 1; }
					elseif($remove_doubleq == "mytoll"){ $mytoll_flag = 1; }
					else{ $auspost_flag = 0; $star_track_flag = 0; $mytoll_flag = 0; }
					
				}

				if($star_track_flag == "1" && empty($mytoll_flag)){

					$application = JFactory::getApplication();
					if($star_track_flag == 1){


						/*$totalCostExGST_starTrack = $this->getStarTrackCost($store, $address, $shipinfo,1, $weight, $volume);
						if($totalCostExGST_starTrack == 0){
							$totalCostExGST_starTrack = 0;
						}else{
							$totalCostExGST_starTrack = $totalCostExGST_starTrack;
						}if($totalCostExGST_starTrack == 0){
						$application->enqueueMessage(JText::_("There is some Technical error in API. Please check your shipping address---uuu."), 'error');
						$totalCostExGST = 0;
						}else{
							$_SESSION['standard_shiping_type'] = "StarTrack";
							$totalCostExGST = $totalCostExGST_starTrack;
						}*/


						//$application = JFactory::getApplication();

					$totalCostExGST_starTrack = $this->getStarTrackCost($store, $address, $shipinfo, 1, $weight, $volume);

					//echo "Star Track Price is ".$totalCostExGST_starTrack."<br/>";

					if($totalCostExGST_starTrack == 0){
						$application->enqueueMessage(JText::_("There is some Technical error in API. Please check your shipping address2."), 'error');
						$totalCostExGST = 0;
					}else{
							$_SESSION['standard_shiping_type'] = "StarTrack";
							$totalCostExGST =$totalCostExGST_starTrack;
						}
						
					} 

					
				}
				

				if($mytoll_flag == "1" && empty($star_track_flag)){
					$application = JFactory::getApplication();
					if(substr($p_sku, 0,2)=="BU"){
						$totalCostExGST_Toll = $this->getOversizeBullBarTollCost($store, $address, $shipinfo, 1, $product->product_weight, $length, $width, $height, $volume);

					}else{
						$totalCostExGST_Toll = $this->getTollCost($store, $address, $shipinfo, 1, $product->product_weight, $length, $width, $height, $volume);
					}
					if($totalCostExGST_Toll == 0){
						
						$totalCostExGST_Toll = 0;
					}else{
						$totalCostExGST_Toll = $totalCostExGST_Toll;
					} 
					if($totalCostExGST_Toll == 0){
						$application->enqueueMessage(JText::_("There is some Technical error in API. Please check your shipping address3."), 'error');
						$totalCostExGST = 0;
					}else{
						$_SESSION['standard_shiping_type'] = "Toll";
						$totalCostExGST = $totalCostExGST_Toll;
					}
				}
				if( $star_track_flag == 1 && $mytoll_flag == 1){

				$application = JFactory::getApplication();
			
				$totalCostExGST_starTrack = $this->getStarTrackCost($store, $address, $shipinfo, $quantity, $weight, $volume);
					//echo "Star Track Price is ".$totalCostExGST_starTrack."<br/>";
					if(substr($p_sku, 0,2)=="BU"){
						$totalCostExGST_Toll = $this->getOversizeBullBarTollCost($store, $address, $shipinfo, 1, $product->product_weight, $length, $width, $height, $volume);

					}else{
						$totalCostExGST_Toll = $this->getTollCost($store, $address, $shipinfo, 1, $product->product_weight, $length, $width, $height, $volume);
					}
					//echo "Toll Price is ".$totalCostExGST_Toll."<br/>";
					
					if($totalCostExGST_starTrack == 0 && $totalCostExGST_Toll == 0){
					$application->enqueueMessage(JText::_("There is some Technical error in API. Please check your shipping address4."), 'error');
					$totalCostExGST = 0;
					}else{
						if($totalCostExGST_starTrack != 0 && $totalCostExGST_Toll != 0){
							if($totalCostExGST_starTrack < $totalCostExGST_Toll){
								$_SESSION['standard_shiping_type'] = "StarTrack";
							}else{
								$_SESSION['standard_shiping_type'] = "Toll";
							}
							$totalCostExGST = min($totalCostExGST_starTrack , $totalCostExGST_Toll);
						}else{
							if($totalCostExGST_starTrack > $totalCostExGST_Toll){
								$_SESSION['standard_shiping_type'] = "StarTrack";
							}else{
								$_SESSION['standard_shiping_type'] = "Toll";
							}
							$totalCostExGST = max($totalCostExGST_starTrack , $totalCostExGST_Toll);
						}
					}
					
				}if( $star_track_flag == 0 && $mytoll_flag == 0){
					$application->enqueueMessage(JText::_("There is some Technical error in API. Please check your shipping address5."), 'error');
					$totalCostExGST = 0;
				}
				
				
				
			}
			
			
			// Add 20% to total shipping price.
			$totalCostExGST = $totalCostExGST + $totalCostExGST*0.2; 

			$totalCostExGST = (float)$totalCostExGST;
			//$totalCostExGST = number_format($totalCostExGST, 2);
			
			return $totalCostExGST;
			//return 3 + $method->shipment_cost + $method->package_fee;
		}
	}
	
	
	/* Calculate Star Track Price */
	function getStarTrackCost($store, $address, $shipinfo, $quantity, $weight, $volume){
		
		// Get the parameters required for a connection to eServices
		$oConnect = new ConnectDetails();
		$connection = $oConnect->getConnectDetails();

		//Create the request, as per Request Schema described in eServices - Usage Guide.xls
		$parameters = array(
					'header' => array(
										'source' => 'TEAM',
										'accountNo' => '10057108',
										'userAccessKey' => $connection['userAccessKey']
									 ),
					'senderLocation' => array(
										'suburb' => $store['address_2'],
										'postCode' => $store['zip'],
										'state' => $store['state']		// Must be upper case
									 ),
					'receiverLocation' => array(
										'suburb' => strtoupper($address['address_2']),
										'postCode' => $address['zip'],
										'state' => $shipinfo->state	// Must be upper case
									 ),
					'serviceCode' => strtoupper("EXP"),					// Must be upper case
					'noOfItems' => $quantity,
					'weight' => $weight,
					'volume' =>$volume,
					'includeTransitWarranty' => 0,
					'includeFuelSurcharge' => 1
					);
							
		//print_r($parameters);

		// Transit Warranty Value can be present only if it is non-null, otherwise a fault occurs

		$transitWarrantyValue = 100;
		if ($transitWarrantyValue != "")
		{
			$parameters += array('transitWarrantyValue' => $transitWarrantyValue);						// Append Transit Warranty Value
		}

		// NOTE: Applications *must* validate all parameters passed (data type, mandatory fields non-null), as described in Readme.pdf,
		// or alerts are generated at StarTrack. Validation is omitted in this sample for reasons of clarity.

		$request = array('parameters' => $parameters);

		// Invoke StarTrack eServices

		try
		{
			$oC = new STEeService();
			//$oC = new startrackexpress\eservices\STEeService();	
			// *** If PHP V5.3 or later, uncomment this line and remove the line above ***
			
			$response = $oC->invokeWebService( $connection, 'calculateCost', $request );
//echo '<pre>';print_r($response);die;
			
			
			// $response is as per Response Schema
			// described in eServices - Usage Guide.xls.
			// Returned value is a stdClass object.
			// Faults to be handled as appropriate.<br />

			$totalCostExGST_starTrack = $response->cost + $response->fuelSurcharge + $response->transitWarrantyCharge;
		}
		catch (SoapFault $e)
		{
			//print_r($e);
			$application = JFactory::getApplication();

			// Add a message to the message queue
			//$application->enqueueMessage(JText::_("There is some Technical error in Star track. Please check your shipping address."), 'error');
			 /*  echo "<p>" . $e->detail->fault->fs_severity . "<br />";
			echo $e->faultstring . "<br />";
			echo $e->detail->fault->fs_message . "<br />";
			echo $e->detail->fault->fs_category . "<br />";
			echo $e->faultcode . "<br />" . "</p>";
			exit($e->detail->fault->fs_timestamp); */ 
			//	Or if there is a higher caller:    throw new SoapFault($e->faultcode, $e->faultstring, $e->faultactor, $e->detail, $e->_name, $e->headerfault);
			$totalCostExGST_starTrack = 0;
		}
		return $totalCostExGST_starTrack;
	}
	
	
	/* Calculate Toll Price */
	function getTollCost($store, $address, $shipinfo, $quantity, $weight, $length, $width, $height, $volume){
	
		if($shipinfo->state_3_code == 'SOA'){$shipinfo->state_3_code = 'SA';}
		//if($shipinfo->state_3_code == 'NSW'){$shipinfo->state_3_code = 'NW';}
		
		$date_s = date('Y-m-d\TH:i:s');
		//echo DATE_ATOM;
		//echo $date_s."---".$store['city']."---".$store['state']."---".$store['zip'].'----'.strtoupper($address['city']).'---'.$shipinfo->state_3_code.'---'.$address['zip'].'---'.date('Y-m-d', strtotime('+1 day')).'---'.$quantity.'---weight'.round($weight).'---length'.round($length).'---width'.round($width).'---height'.round($height).'---volume'.$volume;

			
		$input_xml = '<?xml version="1.0"?>  
			<TollRateRequest>
			<Header>
			<Sender>212008</Sender>
			<Receiver>Toll</Receiver>
			<DocumentType>TollRateRequest</DocumentType>
			<DocumentID>212008</DocumentID>
			<DateTimeStamp>'.$date_s.'+1000</DateTimeStamp>
			</Header>
			<RateRequest>
			<CarrierID>TNQX</CarrierID>
			<AccountDetail>
			<AccountCode>212008</AccountCode>
			</AccountDetail>
			<SenderLocality>
			<Suburb>'.$store['address_2'].'</Suburb>
			<State>'.$store['state'].'</State>
			<Postcode>'.$store['zip'].'</Postcode>
			<Country>AU</Country>
			</SenderLocality>
			<ReceiverLocality>
			<Suburb>'.strtoupper($address['address_2']).'</Suburb>
			<State>'.$shipinfo->state_3_code.'</State>
			<Postcode>'.$address['zip'].'</Postcode>
			<Country>AU</Country>
			</ReceiverLocality>
			<ServiceDetail>
			<DespatchDate>'.date('Y-m-d', strtotime('+1 day')).'</DespatchDate>
			<ServiceID>G</ServiceID>
			<ServiceMode>Road</ServiceMode>
			</ServiceDetail>
			<Items>
			<NumberOfItems>'.$quantity.'</NumberOfItems>
			<Weight>'.round($weight).'</Weight>
			<Length>'.round($length).'</Length>
			<Width>'.round($width).'</Width>
			<Height>'.round($height).'</Height>
			<CubicMetres>'.$volume.'</CubicMetres>
			</Items>
			</RateRequest>
			</TollRateRequest>';

		$curl = curl_init();
		$opts = array(
			CURLOPT_URL             => 'https://online.toll.com.au/trackandtrace/b2b',
			CURLOPT_USERPWD         => 'DOBINSONS:NOSNIBOD2',
			CURLOPT_RETURNTRANSFER  => true,
			CURLOPT_CUSTOMREQUEST   => 'POST',
			CURLOPT_POST            => 1,
			CURLOPT_POSTFIELDS      => $input_xml,
			CURLOPT_HTTPHEADER  	=> array('Content-Type: text/xml','Content-Length: ' . strlen($input_xml))
		);

		// Set curl options
		curl_setopt_array($curl, $opts);

		// Get the results
		$result = curl_exec($curl);

		// Close resource
		curl_close($curl);
		
		//echo '<pre>';print_r($result);
		//convert the XML result into array
		$array_data = json_decode(json_encode(simplexml_load_string($result)), true);
		//print_r($array_data);
				
		if(empty($array_data['Error'])){
			$totalCostExGST_Toll = $array_data['RateResponse']['TotalChargeAmount'];
		}else{
			$totalCostExGST_Toll = 0;
		}
		return $totalCostExGST_Toll;
	}

	/* Calculate Toll Price */
	function getOversizeBullBarTollCost($store, $address, $shipinfo, $quantity, $weight, $length, $width, $height, $volume){
	
		if($shipinfo->state_3_code == 'SOA'){$shipinfo->state_3_code = 'SA';}
		//if($shipinfo->state_3_code == 'NSW'){$shipinfo->state_3_code = 'NW';}
		
		$date_s = date('Y-m-d\TH:i:s');
		//echo DATE_ATOM;
		//echo $date_s."---".$store['city']."---".$store['state']."---".$store['zip'].'----'.strtoupper($address['city']).'---'.$shipinfo->state_3_code.'---'.$address['zip'].'---'.date('Y-m-d', strtotime('+1 day')).'---'.$quantity.'---weight'.round($weight).'---length'.round($length).'---width'.round($width).'---height'.round($height).'---volume'.$volume;

			
		$input_xml = '<?xml version="1.0"?>  
			<TollRateRequest>
			<Header>
			<Sender>212008</Sender>
			<Receiver>Toll</Receiver>
			<DocumentType>TollRateRequest</DocumentType>
			<DocumentID>212008</DocumentID>
			<DateTimeStamp>'.$date_s.'+1000</DateTimeStamp>
			</Header>
			<RateRequest>
			<CarrierID>TNQX</CarrierID>
			<AccountDetail>
			<AccountCode>212008</AccountCode>
			</AccountDetail>
			<SenderLocality>
			<Suburb>'.$store['address_2'].'</Suburb>
			<State>'.$store['state'].'</State>
			<Postcode>'.$store['zip'].'</Postcode>
			<Country>AU</Country>
			</SenderLocality>
			<ReceiverLocality>
			<Suburb>'.strtoupper($address['address_2']).'</Suburb>
			<State>'.$shipinfo->state_3_code.'</State>
			<Postcode>'.$address['zip'].'</Postcode>
			<Country>AU</Country>
			</ReceiverLocality>
			<ServiceDetail>
			<DespatchDate>'.date('Y-m-d', strtotime('+1 day')).'</DespatchDate>
			<ServiceID>G</ServiceID>
			<ServiceMode>Road</ServiceMode>
			</ServiceDetail>
			<Items>
			<NumberOfItems>'.$quantity.'</NumberOfItems>
			<Weight>'.round($weight).'</Weight>
			<Length>'.round($length).'</Length>
			<Width>'.round($width).'</Width>
			<Height>'.round($height).'</Height>
			<CommodityCode>8</CommodityCode>
			<CubicMetres>'.$volume.'</CubicMetres>
			</Items>
			</RateRequest>
			</TollRateRequest>';

		$curl = curl_init();
		$opts = array(
			CURLOPT_URL             => 'https://online.toll.com.au/trackandtrace/b2b',
			CURLOPT_USERPWD         => 'DOBINSONS:NOSNIBOD2',
			CURLOPT_RETURNTRANSFER  => true,
			CURLOPT_CUSTOMREQUEST   => 'POST',
			CURLOPT_POST            => 1,
			CURLOPT_POSTFIELDS      => $input_xml,
			CURLOPT_HTTPHEADER  	=> array('Content-Type: text/xml','Content-Length: ' . strlen($input_xml))
		);

		// Set curl options
		curl_setopt_array($curl, $opts);

		// Get the results
		$result = curl_exec($curl);

		// Close resource
		curl_close($curl);
		
		//echo '<pre>';print_r($result);
		//convert the XML result into array
		$array_data = json_decode(json_encode(simplexml_load_string($result)), true);
		//print_r($array_data);
				
		if(empty($array_data['Error'])){
			$totalCostExGST_Toll = $array_data['RateResponse']['TotalChargeAmount'];
		}else{
			$totalCostExGST_Toll = 0;
		}
		return $totalCostExGST_Toll;
	}
	
	// Get the shipping Cost with AusPost API
	function getCostsFirst(VirtueMartCart $cart, $method, $cart_prices, $store, $weight, $length, $width, $height) {
	
		#print 'Debug Line '.__LINE__.' getCostsFirst called<pre>'; print_r (''); print "</pre><br />\n";
		//LWH	
		$asvol=$method->asvol;
		//declare
		$Order_Length ='';
		$TotalQuantity ='';
		$Order_WeightKG ='';
		$OrderVolume ='';
		$Order_Height ='';
		$asdebug ='1';
		$free ='';
		$taxid = $method->tax_id;
		//DEBUG
		$asdebug=$method->asdebug;
		$asdebug2=$method->asdebug2;
		//MESSAGES	
		$asmessages=$method->asmessages;
		$errorlabel=$method->errorlabel;
		//$asdebug ='1';
		//FREE	
		$free=$method->free;
		##$salesprice = $cart_prices['salesPrice'];
		$salesprice = $this->getCartAmount($cart_prices);//use native function from vmpsplugin
		if($asdebug==1){
			print 'Debug Line '.__LINE__.' $cart->auspost_return<pre>'; print_r ($cart->auspost_return); print "</pre><br />\n";
			print 'Debug Line '.__LINE__.' $salesprice<pre>'; print_r ($salesprice); print "</pre><br />\n";
			print 'Debug Line '.__LINE__.' $free<pre>'; print_r ($free); print "</pre><br />\n";
		}
	
		//then test free price
		if(($salesprice >= $free)&&($free)){// free bug fix (if no $free) v1.1.9 28.12.2012
			$this->isfree = '1'; 
			$cart->auspost_return = 0.00;
			if($asdebug==1){
				print "Debug Line ".__LINE__." Free calculated for ". ($method->shipment_name)."<br />\n";
			}
			if($asdebug2==1){
				print "Free shipping calculated for $". (number_format($salesprice,2))."<br />\n";
			}
			if(isset($asmessages)&&($asmessages==1)){
				$app = JFactory::getApplication();
				$app->set( '_messageQueue', '' ); //clear messages
				vmInfo($method->freelabel);			
			}
			return '0.00';

		}else{//continue method
	
			$this->isfree = '0';//reset isfree
	
	
			//Postcode of the pick-up address (e.g. 3015)
			// start sender pstcode changes
			if($asdebug2==1){
				print 'Same as Billto is set to: '.$cart->STsameAsBT.' <br />';
			}
			//Destination Postcode
			//GJC 28.01.2017 new $address call
			if($cart->STsameAsBT == 0){
				$type = ($cart->ST == 0 ) ? 'BT' : 'ST';
			} else {
				$type = 'BT';
			}
			$address = $cart -> getST();
			if($asdebug==1){
				print 'Same as Billto is set to: '.$cart->STsameAsBT.' <br />';
				print 'Debug Line '.__LINE__.' $address<pre>'; print_r ($address); print "</pre><br />\n"; 
			}
			if(isset($_POST['zipcode'])) {
				$address['zip'] = $_POST['zipcode'] ; 
			} 
			
			if (isset($address['zip'])) {
				
				$Order_Destination_Postcode = $address['zip'];
				##$Order_Destination_Postcode = 6001;
				
				if($asdebug2==1){
					print 'Destination Postcode: '.$Order_Destination_Postcode.' <br />';
				}
				$dest_postcode_full = strtoupper(str_replace(' ', '', $Order_Destination_Postcode));// remove whitespace - is full code
				if($asdebug==1){
					print '<span class="debug">Debug Line '.__LINE__.' $dest_postcode_full<pre>'; print_r ($dest_postcode_full); print "</pre><br /></span>\n";
				}
				$dest_postcode_outwardxx = substr($dest_postcode_full, 0, 4);// take 1st 4 characters
				if($asdebug==1){
					print '<span class="debug">Debug Line '.__LINE__.' $dest_postcode_outwardxx<pre>'; print_r ($dest_postcode_outwardxx); print "</pre><br /></span>\n";
				}
				$dest_postcode_outwardx = substr($dest_postcode_full, 0, 3);// take 1st 3 characters
				if($asdebug==1){
					print '<span class="debug">Debug Line '.__LINE__.' $dest_postcode_outwardx<pre>'; print_r ($dest_postcode_outwardx); print "</pre><br /></span>\n";
				}
					
				$dest_postcode_outward = substr($dest_postcode_full, 0, 2);//take 1st 2 characters
				if($asdebug==1){
					print '<span class="debug">Debug Line '.__LINE__.' $dest_postcode_outward<pre>'; print_r ($dest_postcode_outward); print "</pre><br /></span>\n";
				}
					
				$dest_postcode_area = substr($dest_postcode_full, 0, 1);//take 1st 2 characters
				if($asdebug==1){
					print '<span class="debug">Debug Line '.__LINE__.' $dest_postcode_area<pre>'; print_r ($dest_postcode_area); print "</pre><br /></span>\n";
				}

				$dest_postcode_matches = array( 
					'outwardxx' => $dest_postcode_outwardxx,
					'outwardx' => $dest_postcode_outwardx,
					'outward' => $dest_postcode_outward, 
					'area' => $dest_postcode_area
				);//form the array

				if($asdebug==1){
					print '<span class="debug">Debug Line '.__LINE__.' $dest_postcode_matches<pre>'; print_r ($dest_postcode_matches); print "</pre><br /></span>\n"; 
				}
				#die();
				$spostcode = array();
				$spostcode[1] = $method->spostcode;
				$spostcode[2] = $method->spostcode2;
				$spostcode[3] = $method->spostcode3;
				$spostcode[4] = $method->spostcode4;
				$spostcode[5] = $method->spostcode5;
				if($asdebug==1){
					print 'Debug Line '.__LINE__.' $spostcode<pre>'; print_r ($spostcode); print "</pre><br />\n";
				}	
				$data_list = array();
				$data_list[2] = $method->vmzones2;
				$data_list[3] = $method->vmzones3;
				$data_list[4] = $method->vmzones4;
				$data_list[5] = $method->vmzones5;
				
				if($asdebug==1){
					print 'Debug Line '.__LINE__.' $data_list<pre>'; print_r ($data_list); print "</pre><br />\n";
				}
				$zone = 0;
				foreach($data_list as $zone_id => $postcodes) {
					if($asdebug==1){
						print '<span class="debug">Debug Line '.__LINE__.' $zone_id <pre>'; print_r ($zone_id); print "</pre><br /></span>\n";       
					} 
					# $postcodes = $postcode_zone;
					if($asdebug==1){
						print '<span class="debug">Debug Line '.__LINE__.' $postcodes <pre>'; print_r ($postcodes); print "</pre><br /></span>\n";       
					}              

					$postcodes = explode('*',$postcodes);//split codes by asterisk
					$postcodes = array_fill_keys($postcodes,'');//from them into array keys

					if($asdebug==1){
						print '<span class="debug">Debug Line '.__LINE__.' $postcodes <pre>'; print_r ($postcodes); print "</pre><br /></span>\n";
					}
	
	
					//outwardxx
					if(isset($postcodes[$dest_postcode_matches[outwardxx]])) {
						$zone = $zone_id;
						if($asdebug==1){
							print '<span class="debug">Debug Line found on outwardxx line '.__LINE__.' $zone_id <pre>'; print_r ($zone_id); print "</pre><br /></span>\n" ;    
						}

						if($asdebug2==1){
							print "<span class='debug'>Postcode found in Stage 1 (4 digit mask): ".$zone_id." - ".$zone_id."<br /></span>\n";       
						}
						break(1);
					}//outwardxx

					if(isset($postcodes[$dest_postcode_matches[outwardx]])) {
						$zone = $zone_id;
						if($asdebug==1){
							print '<span class="debug">Debug Line found on outwardx line '.__LINE__.' $zone_id <pre>'; print_r ($zone_id); print "</pre><br /></span>\n" ;    
						}

						if($asdebug2==1){
							print "<span class='debug'>Postcode found in Stage 2 (3 digit mask): ".$zone_id." - ".$zone_id."<br /></span>\n";       
						}
						break(1);
					}//outwardx

					if(!$zone) {
					   if(isset($postcodes[$dest_postcode_matches[outward]])) {
							$zone = $zone_id;

							if($asdebug==1){
								print '<span class="debug">Debug Line found on outward line '.__LINE__.' $zone_id <pre>'; print_r ($zone_id); print "</pre><br /></span>\n";       
							}
							if($asdebug2==1){
								print "<span class='debug'>Postcode found in Stage 3 (2 digit mask): ".$zone_id." - ".$zone_id."<br /></span>\n";       
							}
							break(1);
						}//outward
					}//no outward


					if(!$zone) {
						if(isset($postcodes[$dest_postcode_matches[area]])) {
							$zone = $zone_id;
							if($asdebug==1){
								print '<span class="debug">Debug Line found on area line '.__LINE__.' $zone_id <pre>'; print_r ($zone_id); print "</pre><br /></span>\n";      
							}
							if($asdebug2==1){
								print "<span class='debug'>Postcode found in Stage 4 (1 digit mask): ".$zone_id." - ".$zone_id."<br /></span>\n";
							}
							break(1);
						}//area
					}//no area

				}//foreach
				#die();
				if($zone > 0){
					$Order_Pickup_Postcode = $spostcode[$zone_id];
				}else {
					$Order_Pickup_Postcode = $spostcode[1];
				}
	
				//$Order_Pickup_Postcode = $store['zip'];
				//echo $Order_Pickup_Postcode."---".$store['zip'];
				if($asdebug==1){
					print '<span class="debug">Debug Line found sender postcode line '.__LINE__.' <pre>'; print_r ($Order_Pickup_Postcode); print "</pre><br /></span>\n";       
				}
				if($asdebug2==1){
					print "<span class='debug'>Sender Postcode found : ".$Order_Pickup_Postcode."<br /></span>\n";
				}
			} else {
				//no zip in address data normally occurs only, when it is removed from the form by the shopowner
			   $Order_Destination_Postcode = 'XXX';
			}
			#die();
			// end sender postcode changes

			//Get country code
			if(!function_exists('getCountryCode')){
				function getCountryCode($virtuemart_country_id ) {

					$db = JFactory::getDBO();
					$q = 'SELECT country_2_code FROM `#__virtuemart_countries` '
						. 'WHERE `virtuemart_country_id` = ' . $virtuemart_country_id;
						
					
					$db->setQuery($q);
					$country_code = $db->loadResult();
					return $country_code;
				}
			}
		if(!isset($address['virtuemart_country_id'])){$address['virtuemart_country_id'] = 13;}
			$country_code = getCountryCode($address['virtuemart_country_id']);
			if($asdebug==1){
				print 'Debug Line '.__LINE__.' $country_code<pre>'; print_r ($country_code); print "</pre><br />\n";
			}
			#print 'Debug Line '.__LINE__.' $cart <pre>'; print_r ($cart); print "</pre><br />\n"; 
			//size calculation
			$i = 0;
			foreach ($cart->products as $product) {
				if($asdebug==1){
					#print 'Debug Line '.__LINE__.' $product <pre>'; print_r ($product); print "</pre><br />\n";   
				}
				if($product->quantity){#only if array contains values
						
					#$Order_WeightKG .= $product->product_weight*$product->quantity.',';
					
					$TotalQuantity .= $product->quantity.',' ; 
					//$OrderVolume = $this->_getVolume($cart); //new function	
					$OrderVolume = $volume; //new function	
					
					$Order_Length .= $product->product_length.',' ;
					$Order_Width .= $product->product_width.',' ; 
					$Order_Height .= $product->product_height.',' ; 
				}
			} 
			$Order_Length = explode(',',$Order_Length);
			$Order_Length = array_sum($Order_Length);
			$Order_Length = number_format($Order_Length,2);
			
			$Order_Width = explode(',',$Order_Width);
			$Order_Width = array_sum($Order_Width);
			$Order_Width = number_format($Order_Width,2);
			
			$TotalQuantity = explode(',',$TotalQuantity);
			$TotalQuantity = array_sum($TotalQuantity);
			$TotalQuantity = number_format($TotalQuantity,2);
			
			$Order_Height = explode(',',$Order_Height);
			$Order_Height = array_sum($Order_Height);
			$Order_Height = number_format($Order_Height,2);//cm
			$Order_Heightmm = number_format($Order_Height,2)*10;//mm
			if($asdebug==1){
				print 'Debug Line '.__LINE__.' $OrderVolume <pre>'; print_r ($OrderVolume); print "</pre><br />\n";
				print 'Debug Line '.__LINE__.' $Order_Length <pre>'; print_r ($Order_Length); print "</pre><br />\n";
				print 'Debug Line '.__LINE__.' $too_long <pre>'; print_r ($too_long); print "</pre><br />\n";
				print 'Debug Line '.__LINE__.' $Order_Width <pre>'; print_r ($Order_Width); print "</pre><br />\n";
				print 'Debug Line '.__LINE__.' $Order_Height <pre>'; print_r ($Order_Height); print "</pre><br />\n";
				print 'Debug Line '.__LINE__.' $Order_Heightmm <pre>'; print_r ($Order_Heightmm); print "</pre><br />\n";
				print 'Debug Line '.__LINE__.' $TotalQuantity <pre>'; print_r ($TotalQuantity); print "</pre><br />\n";
			}
			//$Order_WeightKG = substr($Order_WeightKG,'',-1);//removed 25.06.2012 caused NULL weight

			#$Order_WeightKG = explode(',',$Order_WeightKG);
			if($asdebug==1){
				#print 'Debug Line '.__LINE__.' $Order_WeightKG <pre>'; print_r ($Order_WeightKG); print "</pre><br />\n";
			}
			#die();

			//Change below to $Order_WeightKG = array_sum($Order_WeightKG); if using J2.6/VM2.6 - test!!!  =
			#$Order_WeightKG = array_sum($Order_WeightKG);
			//$Order_WeightKG = $this->getOrderWeight($cart, 'KG');// use native func vmsplugin jan 2014
			$Order_WeightKG = $weight;// use native func vmsplugin jan 2014
			//OVERWEIGHT
			$Over_WeightKG = 0;
			if($method->overweight == 1 && $Order_WeightKG >22){
				$Over_WeightKG = $Order_WeightKG - 22;
				$Order_WeightKG = 22;

				if($asdebug==1){
					print 'Debug Line '.__LINE__.' Overweight is set: Weight is set to 22KG: $Over_Weight <pre>'; print_r ($Over_WeightKG); print "</pre><br />\n";
				}
			}
			$Order_WeightG = $Order_WeightKG*1000; // in grammes
			if($asdebug==1){
				print 'Debug Line '.__LINE__.' $Order_Length <pre>'; print_r ($Order_Length); print "</pre><br />\n";
				print 'Debug Line '.__LINE__.' $Order_WeightKG <pre>'; print_r ($Order_WeightKG); print "</pre><br />\n";
				print 'Debug Line '.__LINE__.' $Order_WeightG <pre>'; print_r ($Order_WeightG); print "</pre><br />\n";
			}

			#$TotalQuantity = explode(',',$TotalQuantity);

			#$TotalQuantity = array_sum($TotalQuantity);
			if($asdebug==1){
				#print 'Debug Line '.__LINE__.' $TotalQuantity <pre>'; print_r ($TotalQuantity); print "</pre><br />\n";
			}


			//size calculation
			if(!$OrderVolume){
				
				 $OrderVolume = 1000 * $TotalQuantity * $Order_WeightKG; 
				 if($asdebug2==1){
					print 'No measurements - dummy values used. <br />';
				 }
			}
		
			$cart->orderVolume = $OrderVolume;
	
			//$cal_lengths = number_format(pow($orderVolume, 1/3),2);
			$cal_lengths = $length;
			$cal_widths = $width;
			$cal_heights = $height;
			if($asdebug2==1){
				print 'Quantity is '.$TotalQuantity.' <br />';
				print 'Volume is '.$orderVolume.'cm<sup>3</sup> <br />';
				#print 'Length is '.$Order_Length.'cm <br />';
				#print 'Width is '.$Order_Width.'cm <br />';
				#print 'Height is '.$Order_Height.'cm <br />';
				print 'Calculated lengths are '.$cal_lengths.'cm per side <br />';
			}

			$shiptype=$method->shiptype = 0;
			$shipmethod=$method->shipmethod = 0;
			$packtype=$method->packtype = 1;
			if($asdebug==1){
				print 'Debug Line '.__LINE__.' $shipmethod '; print_r ($shipmethod); print "<br />\n";
				print 'Debug Line '.__LINE__.' $shiptype '; print_r ($shiptype); print "<br />\n";
				print 'Debug Line '.__LINE__.' $packtype '; print_r ($packtype); print "<br />\n";
			}


			if($packtype==1){//parcel
				if($shiptype==0){//domestic
					switch ((string)$shipmethod) {
						case "AUS_PARCEL_REGULAR":
							$link = 'https://digitalapi.auspost.com.au/postage/parcel/domestic/calculate.xml?from_postcode='.$Order_Pickup_Postcode.'&to_postcode='.$Order_Destination_Postcode.'&length='.$cal_lengths.'&height='.$cal_heights.'&width='.$cal_widths.'&service_code=AUS_PARCEL_REGULAR&weight='.$Order_WeightKG;
							break;
						case "AUS_PARCEL_REGULAR_SATCHEL_500G":
							$link = 'https://digitalapi.auspost.com.au/postage/parcel/domestic/calculate.xml?from_postcode='.$Order_Pickup_Postcode.'&to_postcode='.$Order_Destination_Postcode.'&length='.$cal_lengths.'&height='.$cal_heights.'&width='.$cal_widths.'&service_code=AUS_PARCEL_REGULAR_SATCHEL_500G&weight='.$Order_WeightKG;
							break;
						case "AUS_PARCEL_REGULAR_SATCHEL_3KG":
							$link = 'https://digitalapi.auspost.com.au/postage/parcel/domestic/calculate.xml?from_postcode='.$Order_Pickup_Postcode.'&to_postcode='.$Order_Destination_Postcode.'&length='.$cal_lengths.'&height='.$cal_heights.'&width='.$cal_widths.'&service_code=AUS_PARCEL_REGULAR_SATCHEL_3KG&weight='.$Order_WeightKG;
							break;
						case "AUS_PARCEL_REGULAR_SATCHEL_5KG":
							$link = 'https://digitalapi.auspost.com.au/postage/parcel/domestic/calculate.xml?from_postcode='.$Order_Pickup_Postcode.'&to_postcode='.$Order_Destination_Postcode.'&length='.$cal_lengths.'&height='.$cal_heights.'&width='.$cal_widths.'&service_code=AUS_PARCEL_REGULAR_SATCHEL_5KG&weight='.$Order_WeightKG;
							break;
						case "AUS_PARCEL_EXPRESS":
							$link = 'https://digitalapi.auspost.com.au/postage/parcel/domestic/calculate.xml?from_postcode='.$Order_Pickup_Postcode.'&to_postcode='.$Order_Destination_Postcode.'&length='.$cal_lengths.'&height='.$cal_heights.'&width='.$cal_widths.'&service_code=AUS_PARCEL_EXPRESS&weight='.$Order_WeightKG;
							break;
							
						case "AUS_PARCEL_EXPRESS_SATCHEL_500G":
							$link = 'https://digitalapi.auspost.com.au/postage/parcel/domestic/calculate.xml?from_postcode='.$Order_Pickup_Postcode.'&to_postcode='.$Order_Destination_Postcode.'&length='.$cal_lengths.'&height='.$cal_heights.'&width='.$cal_widths.'&service_code=AUS_PARCEL_EXPRESS_SATCHEL_500G&weight='.$Order_WeightKG;
							break;
						case "AUS_PARCEL_EXPRESS_SATCHEL_3KG":
							$link = 'https://digitalapi.auspost.com.au/postage/parcel/domestic/calculate.xml?from_postcode='.$Order_Pickup_Postcode.'&to_postcode='.$Order_Destination_Postcode.'&length='.$cal_lengths.'&height='.$cal_heights.'&width='.$cal_widths.'&service_code=AUS_PARCEL_EXPRESS_SATCHEL_3KG&weight='.$Order_WeightKG;
							break;
						case "AUS_PARCEL_EXPRESS_SATCHEL_5KG":
							 $link = 'https://digitalapi.auspost.com.au/postage/parcel/domestic/calculate.xml?from_postcode='.$Order_Pickup_Postcode.'&to_postcode='.$Order_Destination_Postcode.'&length='.$cal_lengths.'&height='.$cal_heights.'&width='.$cal_widths.'&service_code=AUS_PARCEL_EXPRESS_SATCHEL_5KG&weight='.$Order_WeightKG;
							break;

						default://cheapest 
						
						$link = 'https://digitalapi.auspost.com.au/postage/parcel/domestic/service.xml?from_postcode='.$Order_Pickup_Postcode.'&to_postcode='.$Order_Destination_Postcode.'&length='.$cal_lengths.'&height='.$cal_heights.'&width='.$cal_widths.'&weight='.$Order_WeightKG; 
					}
					
				}else{//international parcels
					switch ($shipmethod) {
						case "INT_PARCEL_AIR_OWN_PACKAGING":
							$link = 'https://digitalapi.auspost.com.au/postage/letter/international/calculate.xml?country_code='.$country_code.'&service_code=INT_PARCEL_AIR_OWN_PACKAGING&weight='.$Order_WeightG;
							break;
						case "INT_PARCEL_STD_OWN_PACKAGING":
							$link = 'https://digitalapi.auspost.com.au/postage/letter/international/calculate.xml?country_code='.$country_code.'&service_code=INT_PARCEL_STD_OWN_PACKAGING&weight='.$Order_WeightG;
							break;
						case "INT_PARCEL_EXP_OWN_PACKAGING":
							$link = 'https://digitalapi.auspost.com.au/postage/letter/international/calculate.xml?country_code='.$country_code.'&service_code=INT_PARCEL_EXP_OWN_PACKAGING&weight='.$Order_WeightG;
							break;
						case "INT_PARCEL_COR_OWN_PACKAGING":
							$link = 'https://digitalapi.auspost.com.au/postage/letter/international/calculate.xml?country_code='.$country_code.'&service_code=INT_PARCEL_COR_OWN_PACKAGING&weight='.$Order_WeightG;
							break;
						case "INT_PARCEL_SEA_OWN_PACKAGING":
							$link = 'https://digitalapi.auspost.com.au/postage/letter/international/calculate.xml?country_code='.$country_code.'&service_code=INT_PARCEL_SEA_OWN_PACKAGING&weight='.$Order_WeightG;
							break;
						default://cheapest 
						$link = 'https://digitalapi.auspost.com.au/postage/parcel/international/service.xml?country_code='.$country_code.'&weight='.$Order_WeightKG;
					}	
				}
				
			}else{//packtype is letter
				if($shiptype==0){//domestic
					switch ($shipmethod) {
							
							case "AUS_LETTER_REGULAR_LARGE":
								$link = 'https://digitalapi.auspost.com.au/postage/letter/domestic/calculate.xml?from_postcode='.$Order_Pickup_Postcode.'&to_postcode='.$Order_Destination_Postcode.'&length=200&thickness='.$Order_Heightmm.'&width=200&service_code=AUS_LETTER_REGULAR_LARGE&weight='.$Order_WeightG;
								break;
							case "AUS_LETTER_EXPRESS_SMALL":
								$link = 'https://digitalapi.auspost.com.au/postage/letter/domestic/calculate.xml?from_postcode='.$Order_Pickup_Postcode.'&to_postcode='.$Order_Destination_Postcode.'&length=200&thickness='.$Order_Heightmm.'&width=200&service_code=AUS_LETTER_EXPRESS_SMALL&weight='.$Order_WeightG;
								break;
							default://cheapest 
								$link = 'https://digitalapi.auspost.com.au/postage/letter/domestic/service.xml?from_postcode='.$Order_Pickup_Postcode.'&to_postcode='.$Order_Destination_Postcode.'&length=200&thickness='.$Order_Heightmm.'&width=200&weight='.$Order_WeightG;
								#$link = 'https://digitalapi.auspost.com.au/postage/letter/domestic/service.xml?from_postcode='.$Order_Pickup_Postcode.'&to_postcode='.$Order_Destination_Postcode.'&length=20&thickness=5&width=20&weight=40';
					}//end switch
				}else{//international  $shiptype 1 letters
					switch ($shipmethod) {
						case "INT_LETTER_AIR_OWN_PACKAGING_LIGHT":
							$link = 'https://digitalapi.auspost.com.au/postage/letter/international/calculate.xml?country_code='.$country_code.'&service_code=INT_LETTER_AIR_OWN_PACKAGING_LIGHT&weight='.$Order_WeightG;
							break;
						case "INT_LETTER_REG_LARGE":
							$link = 'https://digitalapi.auspost.com.au/postage/letter/international/calculate.xml?country_code='.$country_code.'&service_code=INT_LETTER_REG_LARGE&weight='.$Order_WeightG;
							break;
						case "INT_LETTER_REG_SMALL":
							$link = 'https://digitalapi.auspost.com.au/postage/letter/international/calculate.xml?country_code='.$country_code.'&service_code=INT_LETTER_REG_SMALL&weight='.$Order_WeightG;
							break;

						default://cheapest 
							$link = 'https://digitalapi.auspost.com.au/postage/letter/international/service.xml?country_code='.$country_code.'&weight='.$Order_WeightG;
					}
				}
			}//end packtype
	
	

			// Collect variables into the query URI for auspost.com.au
			if(!function_exists('get_auspost_api')){
				function get_auspost_api($url,$apikey,$asdebug){

					if($asdebug==1){

						echo 'Curl: ', function_exists('curl_version') ? 'Enabled<br />' : 'Disabled<br />';
						$fp = fopen(dirname(__FILE__).'/curl_log.txt', 'w');
						if ($fp !== false) {
							print "Opened the log file without errors<br />";
							$line = date( 'Y-m-d H:i:s' ) ."\n\n";

							fwrite( $fp, $line );
						}
					}
					$crl = curl_init();
					$timeout = 30;
					curl_setopt ($crl, CURLOPT_HTTPHEADER, array("accept: application/json",
   "cache-control: no-cache","AUTH-KEY:$apikey"));
					curl_setopt ($crl, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt ($crl, CURLOPT_URL, $url);
					curl_setopt ($crl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt ($crl, CURLOPT_CONNECTTIMEOUT, $timeout);
					if($asdebug==1){
						echo "cURL debug on\n\n";	
						curl_setopt($crl, CURLOPT_FOLLOWLOCATION, TRUE);
						curl_setopt($crl, CURLOPT_VERBOSE, true);
						curl_setopt($crl, CURLOPT_STDERR, $fp);
					}
						
					$ret = curl_exec($crl);
					  
					if($asdebug==1){
						echo "\nVerbose information:\n<pre>", !rewind($fp), htmlspecialchars(stream_get_contents($fp)), "</pre>\n";


						#echo "\nVerbose information:\n<pre>", htmlspecialchars($verboseLog), "</pre>\n";
						
						$info = curl_getinfo($crl);
						print 'Debug Line '.__LINE__.' $info <pre>'; print_r ($info); print "</pre><br />\n";		
					}
						
					curl_close($crl);
					if($asdebug==1){
						fwrite($fp, "\n\n###########################################\n\n");
						fclose($fp);
					}

					return $ret;
				}
			}

			if($method->apfail){
				$link = 'http://www.xaswerxxx.com';
				if($asdebug==1){
					print '<br />Debug Line '.__LINE__.' Test fail on! <br />';
				}	
			}
			if($asdebug==1){
				print '<br />Debug Line '.__LINE__.' $link <br />'; print_r ($link); print "<br />\n";
			}

			// echo $method->idcode;
			//$auspost_xml = get_auspost_api($link,$method->idcode,$asdebug);
			// echo $link;echo '<pre>'; print_r($method); echo $asdebug; echo '</pre>';
	
			$auspost_xml = get_auspost_api($link,$method->auspost_key,$asdebug);
	

			if($asdebug==1){
				if($packtype==1){
					$unitht = "Height"; 
				}else{
					$unitht = "Thickness";  
				}
			
				if($shiptype==0){
				
					echo 'Australia Post assesses charges for parcels according to their actual or cubic weight equivalent, whichever is greater. Weights are rounded up to the nearest kilogram in all calculations.<br />Dimension restrictions apply to all parcels, including maximum length and girth.<br />See <a href="http://auspost.com.au/personal/parcel-dimensions.html" target="_BLANK">Parcel Dimensions</a><br />The calculation I use is for each type of product LxWxHxQuantity = Volume<br />Add the volumes of the different articles together.<br />Divide by 2500 to get the total length in cm.<br/>If any article is longer than 105cm that length is used. (will immediately fail the request).<br />Use a standard 20cm x 20cm height and width.<br (>the dimensions are only to find an approximation for the max. length and girth allowed.<br />The fee is only calculated from the weight.<br /><br />The calculator can return either the cheapest option or a chosen method.<br /><br />';
				
					echo '<b>Data Sent : </b><br />Sender Postcode : '.$Order_Pickup_Postcode.'<br />Receive Postcode : '.$Order_Destination_Postcode.'<br />Length : '.$Order_Length.'cm<br />'.$unitht.' : '.$Order_Height.'cm<br />Width : 20cm<br />Weight : '.$Order_WeightKG.'kg<br />Quantity : '.$TotalQuantity.'<br /><br />';
				}else{
					echo 'Australia International Post assesses charges for parcels according to their actual or cubic weight equivalent, whichever is greater. Weights are rounded up to the nearest kilogram in all calculations.<br />Dimension restrictions apply to all parcels, including maximum length and girth.<br />See <a href="http://auspost.com.au/personal/parcel-dimensions.html" target="_BLANK">Parcel Dimensions</a><br />The calculation I use is for each type of product LxWxHxQuantity = Volume<br />Add the volumes of the different articles together.<br />Divide by 2500 to get the total length in cm.<br/>If any article is longer than 105cm that length is used. (will immediately fail the request).<br />Use a standard 20cm x 20cm height and width.<br (>the dimensions are only to find an approximation for the max. length and girth allowed.<br />The fee is only calculated from the weight.<br /><br />The calculator can return either the cheapest option or a chosen method.<br /><br />';
					echo '<b>Data Sent : </b><br />Sender Postcode : '.$Order_Pickup_Postcode.'<br />Dest Country :  '.$country_code.'<br />Weight : '.$Order_WeightKG.'kg<br />Quantity : '.$TotalQuantity.'<br /><br />';
				}
			}
			//echo output for test purposes
			#echo '<pre>'.$auspost_xml.'</pre>'; 

			#die();
			$xml = simplexml_load_string($auspost_xml);
			// print_r($xml);
			
			if($asdebug==1){
				print 'Debug Line '.__LINE__.' $xml <pre>'; print_r ($xml); print "</pre><br />\n";
			}
			$error =((string)$xml->errorMessage);
			if($asdebug==1){
				print 'Debug Line '.__LINE__.' $error <pre>'; print_r ($error); print "</pre><br />\n";
			}
			//no error
			if((!$error)&&($xml)) {//25.02.2013 no error AND AusPost returns
				//set postcode that was queried
				$session =& JFactory::getSession();	
				$session->set( 'auspost_postcode', $Order_Destination_Postcode );
				$session->set( 'auspost_spostcode', $Order_Pickup_Postcode );
				if($asdebug==1){
					echo '<b>AusPost returns : </b><br />';	
				}
				if ($shipmethod == '0'){//$shipmethod is services - cheapest
					if($xml->service){
						
						foreach ($xml->service as $service) {
							$returnprice[] = ((string)$service->price).'|'.((string)$service->name);
							
							if($asdebug==1){
								echo 'Service : '.$service->name.'<br />';
								echo 'Price : $'.$service->price.'<br /><br />';
							}
						}
					}
				
					sort($returnprice, SORT_NUMERIC);

					// Get Australia Post charge value separate to 'charge='
					
					$returnArray = explode('|',$returnprice[0]);
					$returncharge = $returnArray[0];
					$returnname = $returnArray[1];
					$GLOBALS['servicename'] = null;
					$GLOBALS['servicename'] = $returnname;    //globalize variable 
					
				}else{//$shipmethod is specified
					if($asdebug==1){
						echo 'Service : '.((string)$xml->service).'<br />';
						echo 'Price : $'.((string)$xml->total_cost).'<br /><br />';
					}
					$returncharge = ((string)$xml->total_cost);
					$returnname = ((string)$xml->service); 
					$GLOBALS['servicename'] = null;
					
				}

				//OVERWEIGHT
				if($method->overweight == 1 && $Over_WeightKG > 0){
					$returnchargeold = $returncharge;
					$cost_kg = $returncharge / 22;
					$extra_cost = $Over_WeightKG * $cost_kg;
					$returncharge = $returncharge + $extra_cost; 
					if($asdebug==1){
						print 'Debug Line '.__LINE__.' Overweight enabled : new cost = ((22kg cost / 22kg ) X Over weight) + 22KG cost <br /> (('.$returnchargeold.' / 22) X '.$Over_WeightKG.') + '.$returnchargeold.' = '.$returncharge.' <pre>'; print_r ($returncharge); print "</pre><br />\n";

					}
				}
				
				if($asdebug==1){	
					print 'Debug Line '.__LINE__.' $returncharge <pre>'; print_r ($returncharge); print "</pre><br />\n";
					print 'Debug Line '.__LINE__.' $returnname <pre>'; print_r ($returnname); print "</pre><br />\n";
					print 'Debug Line '.__LINE__.' $GLOBALS[servicename] <pre>'; print_r ($GLOBALS['servicename']); print "</pre><br />\n";
				}			
				if($asdebug2==1){
					print 'AusPost returns $'.number_format($returncharge,2).' incl. GST<br />';
					print 'Service: '.$returnname.' <br />';
				}
				
				if(isset($asmessages)&&($asmessages==1)){
					$app = JFactory::getApplication();
					$app->set( '_messageQueue', '' ); //clear messages
					if(!$cart->auspost_warned){
						vmInfo("auspost.com.au delivery service: ".$returnname);			
					}
					$cart->auspost_warned = 1;
				}
				
			
				$Total_Shipping_Handlingb4 = $returncharge;
				if($asdebug==1){
					print 'Debug Line '.__LINE__.' AusPost returned $Total_Shipping_Handlingb4 <pre>'; print_r ($Total_Shipping_Handlingb4); print "</pre><br />\n";
				}
	
				//signature
				if($shiptype==0 && $method->sod ==1){//domestic
					$Total_Shipping_Handlingb4 = $Total_Shipping_Handlingb4 + $method->sodfee;
					if($asdebug==1){
						print 'Debug Line '.__LINE__.' Sig fee added ($'.$method->sodfee.') - $Total_Shipping_Handlingb4 <pre>'; print_r ($Total_Shipping_Handlingb4); print "</pre><br />\n";
					}
				}elseif($method->isod ==1){
					$Total_Shipping_Handlingb4 = $Total_Shipping_Handlingb4 + $method->isodfee;
					if($asdebug==1){
						print 'Debug Line '.__LINE__.' Int Sig fee added ($'.$method->isodfee.') - $Total_Shipping_Handlingb4 <pre>'; print_r ($Total_Shipping_Handlingb4); print "</pre><br />\n";
					}
				}
					
				//signature
				
				//insurance
				if($shiptype==0 && $method->ins_incl ==1){//domestic and ins
					$insurance = $this->getInsurance($cart_prices,$method); //local method
					if($asdebug==1){
						print 'Debug Line '.__LINE__.' $insurance <pre>'; print_r ($insurance); print "</pre><br />\n";	
					}
					$Total_Shipping_Handlingb4 = $Total_Shipping_Handlingb4 + $insurance;
					if($asdebug==1){
						print 'Debug Line '.__LINE__.' Insurance fee added ($'.$insurance.') - $Total_Shipping_Handlingb4 <pre>'; print_r ($Total_Shipping_Handlingb4); print "</pre><br />\n";
					}
				}
				//insurance
	
				//Fee for packaging and handling, added to the delivery costs returned by auspost.com.au
				if ($method->fee > 0){
					if ($method->apply ==1){
						$Order_Handling_Fee = $method->fee*$TotalQuantity;
						if($asdebug==1){
							print 'Debug Line '.__LINE__.' Handling Fee x quantity added ($'.$method->fee*$TotalQuantity.') - $Total_Shipping_Handlingb4 <pre>'; print_r ($Order_Handling_Fee); print "</pre><br />\n";
						}
					}else {
						$Order_Handling_Fee = $method->fee;
						if($asdebug==1){
							print 'Debug Line '.__LINE__.' Handling Fee one time added (§'.$method->fee.') - $Total_Shipping_Handlingb4 <pre>'; print_r ($Order_Handling_Fee); print "</pre><br />\n";
						}
					}
					$Total_Shipping_Handlingb4 = $Total_Shipping_Handlingb4 + $Order_Handling_Fee;
					if($asdebug==1 && $method->fee > 0 ){	 
						print 'Debug Line '.__LINE__.' <b>Shipping Cost is now <pre>'; print_r ($Total_Shipping_Handlingb4); print "</b></pre><br />\n";
					}			
				}
			
				//END Fee for packaging and handling, added to the delivery costs returned by auspost.com.au

				//Discount subtracted from the delivery costs returned by auspost.com.au
				if ($method->discount > 0 && $salesprice >= $method->discount2){
					$costb4 = 'auspostb4cost'.$method->virtuemart_shipmentmethod_id;
					$cart->$costb4 = $Total_Shipping_Handlingb4;
					$Order_Discount = $this->_discount($Total_Shipping_Handlingb4, $method);//call _discount function		
					$Total_Shipping_Handlingb4 = $Total_Shipping_Handlingb4 - $Order_Discount;
					if($Total_Shipping_Handlingb4 < 0){
						$Total_Shipping_Handlingb4 = 0;
					}
					if($asdebug==1 ){
						print 'Debug Line '.__LINE__.' <b>B4 Shipping Cost <pre>'; print_r ($GLOBALS['beforediscount']); print "</b></pre><br />\n";	 
						print 'Debug Line '.__LINE__.' <b>Shipping Cost is now <pre>'; print_r ($Total_Shipping_Handlingb4); print "</b></pre><br />\n";
					}
					if($asdebug2==1){
						print 'Discount applied - Cart greater than $'.$method->discount2.' - $'.number_format($Order_Discount,2).'<br />';
						print 'Shipping now: $'.$Total_Shipping_Handlingb4.' <br />';
					}
				}else{
					if($asdebug==1 ){
						print 'Debug Line '.__LINE__.' <b>B4 Shipping Cost <pre>'; print_r ($GLOBALS['beforediscount']); print "</b></pre><br />\n";	 
						print 'Debug Line '.__LINE__.' <b>Shipping Cost is now <pre>'; print_r ($Total_Shipping_Handlingb4); print "</b></pre><br />\n";
					}
					if($asdebug2==1){
						print 'No discount applied<br />';
						if($method->discount > 0){
							print 'Cart less than discount trigger: $'.$method->discount2.' <br />';
						}else{
							print 'No discount set<br />';
						}
					}
				}
			
				//END Discount subtracted from the delivery costs returned by auspost.com.au


				//tax handling 12.02.2013
				
	
				if(($taxid !='-1') &&($returncharge)){
					$db = JFactory::getDBO ();
					
					$q = 'SELECT * FROM #__virtuemart_calcs WHERE `virtuemart_calc_id`="' . $taxid . '" ';
					$db->setQuery ($q);
					$taxrule = $db->loadObject ();
					
					if($asdebug==1){
						print 'Debug Line '.__LINE__.' $Total_Shipping_Handlingb4 <pre>'; print_r ($Total_Shipping_Handlingb4); print "</pre><br />\n";
						print 'Debug Line '.__LINE__.' $taxrule <pre>'; print_r ($taxrule); print "</pre><br />\n";
					}					
					
					// $Total_Shipping_Handling = $Total_Shipping_Handlingb4 + (($taxrule->calc_value* $Total_Shipping_Handlingb4)/100);
					$Total_Shipping_Handling = $Total_Shipping_Handlingb4;
				
				}else{
				
					$Total_Shipping_Handling = $Total_Shipping_Handlingb4;
				}
				if($asdebug==1){	
					print 'Debug Line after tax handling '.__LINE__.' $Total_Shipping_Handling <pre>'; print_r ($Total_Shipping_Handling); print "</pre><br />\n";
				}
			}else{//Auspost returned nothing or error
				#print 'Debug Line '.__LINE__.' $error <pre>'; print_r ($error); print "</pre><br />\n";	
				$app = JFactory::getApplication();
				$app->set( '_messageQueue', '' ); //clear messages
				if($error){
					$app = JFactory::getApplication();
					$app->set( '_messageQueue', '' ); //clear messages
					#if(!$cart->auspost_warned){
					if($errorlabel) {
					vmWarn("auspost.com.au : ".$errorlabel);
					}else{
						//filter warnings 14.09.2016
						if($error != 'Please enter a valid To postcode.' && $error != 'Please enter To postcode.') {
							vmWarn("auspost.com.au : ".$error);
						}
					}
					#}
					$cart->auspost_warned = 1;
					$Total_Shipping_Handlingb4 = 'xxx|'.$error;
					$Total_Shipping_Handling = 'xxx|'.$error;
				}else{
					$app = JFactory::getApplication();
					$app->set( '_messageQueue', '' ); //clear messages
					#if(!$cart->auspost_warned){
					vmWarn('AusPost didn\'t respond - please try again.');
					#}
					$cart->auspost_warned = 1;
				}
				$Total_Shipping_Handlingb4 = 'xxx|'.$error;
				$Total_Shipping_Handling = 'xxx|'.$error;
			}
			
			if($asdebug==1){	
				print 'Debug Line '.__LINE__.' $Total_Shipping_Handlingb4 <pre>'; print_r ($Total_Shipping_Handlingb4); print "</pre><br />\n";
			}	
			//push return into cart
			// $cart->auspost_return = $Total_Shipping_Handlingb4;
			$cart->auspost_return = $Total_Shipping_Handling;
			// echo 'Ands1__'. $Total_Shipping_Handling;
			
			return $Total_Shipping_Handling."-------";
		}//free else
    } 

private function _getVolume($cart){
	
		foreach ($cart->products as $product) {
	if($asdebug==1){
#print 'Debug Line '.__LINE__.' $product <pre>'; print_r ($product); print "</pre><br />\n";   
}
		  if($product->quantity){#only if array contains values

		$OrderVolume  .= ($product->product_width*$product->product_length*$product->product_height)*$product->quantity.',';
			}
		}
		$OrderVolume = explode(',',$OrderVolume);
		$OrderVolume = array_sum($OrderVolume)/1000000;
		$OrderVolume = number_format($OrderVolume,3);
if($asdebug==1){
print 'Debug Line '.__LINE__.' $OrderVolume <pre>'; print_r ($OrderVolume); print "</pre><br />\n";
}
		return $OrderVolume;
	}
	
	protected function getPluginHtml ($plugin, $selectedPlugin, $pluginSalesPrice) {
		
		$pluginmethod_id = $this->_idName;
		$pluginName = $this->_psType . '_name';
		if ($selectedPlugin == $plugin->$pluginmethod_id) {
			$checked = 'checked="checked"';
		} else {
			$checked = '';
		}

		if (!class_exists ('CurrencyDisplay')) {
			require(VMPATH_ADMIN . DS . 'helpers' . DS . 'currencydisplay.php');
		}
		$currency = CurrencyDisplay::getInstance ();
		$costDisplay = "";
		if ($pluginSalesPrice) {
			$costDisplay = $currency->priceDisplay( $pluginSalesPrice );
			$t = vmText::_( 'COM_VIRTUEMART_PLUGIN_COST_DISPLAY' );
			if(strpos($t,'/')!==FALSE){
				list($discount, $fee) = explode( '/', vmText::_( 'COM_VIRTUEMART_PLUGIN_COST_DISPLAY' ) );
				if($pluginSalesPrice>=0) {
					$costDisplay = '<span class="'.$this->_type.'_cost fee"> ('.$costDisplay.")</span>";
				} else if($pluginSalesPrice<0) {
					$costDisplay = '<span class="'.$this->_type.'_cost discount"> ('.$discount.' -'.$costDisplay.")</span>";
				}
			} else {
				$costDisplay = '<span class="'.$this->_type.'_cost fee"> ('.$t.' +'.$costDisplay.")</span>";
			}
		}
		$dynUpdate='';
		if( VmConfig::get('oncheckout_ajax',false)) {
			//$url = JRoute::_('index.php?option=com_virtuemart&view=cart&task=updatecart&'. $this->_idName. '='.$plugin->$pluginmethod_id );
			$dynUpdate=' data-dynamic-update="1" ';
		}
		$html = '<input type="radio"'.$dynUpdate.' name="' . $pluginmethod_id . '" id="' . $this->_psType . '_id_' . $plugin->$pluginmethod_id . '"   value="' . $plugin->$pluginmethod_id . '" ' . $checked . ">\n"
			. '<label for="' . $this->_psType . '_id_' . $plugin->$pluginmethod_id . '">' . '<span class="' . $this->_type . '">' . $plugin->$pluginName . $costDisplay . "</span></label>\n";
		return $html;
	}

	/**
	 * @param \VirtueMartCart $cart
	 * @param int             $method
	 * @param array           $cart_prices
	 * @return bool
	 */
	protected function checkConditions ($cart, $method, $cart_prices) {
		//echo '<pre>';print_r($method);die;
		//print_r($cart->BT);
		//print_r($cart_prices);
		static $result = array();

		if($cart->STsameAsBT == 0){
			$type = ($cart->ST == 0 ) ? 'BT' : 'ST';
		} else {
			$type = 'BT';
		}

		$address = $cart -> getST();

		if(!is_array($address)) $address = array();
		if(isset($cart_prices['salesPrice'])){
			$hashSalesPrice = $cart_prices['salesPrice'];
		} else {
			$hashSalesPrice = '';
		}
		
		if(empty($address['virtuemart_country_id'])) $address['virtuemart_country_id'] = 13;
		if(empty($address['zip'])) $address['zip'] = 0;

		$hash = $method->virtuemart_shipmentmethod_id.$type.$address['virtuemart_country_id'].'_'.$address['zip'].'_'.$hashSalesPrice;

		if(isset($result[$hash])){
			return $result[$hash];
		}

		$this->convert ($method);

		if($this->_toConvert){
			$this->convertToVendorCurrency($method);
		}

		$orderWeight = $this->getOrderWeight ($cart, $method->weight_unit);

		$countries = array();
		if (!empty($method->countries)) {
			if (!is_array ($method->countries)) {
				$countries[0] = $method->countries;
			} else {
				$countries = $method->countries;
			}
		}
		
		$weight_cond = $this->testRange($orderWeight,$method,'weight_start','weight_stop','weight');

		$nbproducts_cond = $this->_nbproductsCond ($cart, $method);

		if(isset($cart_prices['salesPrice'])){
			$orderamount_cond = $this->testRange($cart_prices['salesPrice'],$method,'orderamount_start','orderamount_stop','order amount');
		} else {
			$orderamount_cond = FALSE;
		}

		$userFieldsModel =VmModel::getModel('Userfields');
		if ($userFieldsModel->fieldPublished('zip', $type)){
			if (!isset($address['zip'])) {
				$address['zip'] = '';
			}
			$zip_cond = $this->testRange($address['zip'],$method,'zip_start','zip_stop','zip');
		} else {
			$zip_cond = true;
		}

		if ($userFieldsModel->fieldPublished('virtuemart_country_id', $type)){

			if (!isset($address['virtuemart_country_id'])) {
				$address['virtuemart_country_id'] = 0;
			}

			if (in_array ($address['virtuemart_country_id'], $countries) || count ($countries) == 0) {

				//vmdebug('checkConditions '.$method->shipment_name.' fit ',$weight_cond,(int)$zip_cond,$nbproducts_cond,$orderamount_cond);
				vmdebug('shipmentmethod '.$method->shipment_name.' = TRUE for variable virtuemart_country_id = '.$address['virtuemart_country_id'].', Reason: Countries in rule '.implode($countries,', ').' or none set');
				$country_cond = true;
			}
			else{
				vmdebug('shipmentmethod '.$method->shipment_name.' = FALSE for variable virtuemart_country_id = '.$address['virtuemart_country_id'].', Reason: Country '.implode($countries,', ').' does not fit');
				$country_cond = false;
			}
		} else {
			vmdebug('shipmentmethod '.$method->shipment_name.' = TRUE for variable virtuemart_country_id, Reason: no boundary conditions set');
			$country_cond = true;
		}

		$cat_cond = true;
		if($method->categories or $method->blocking_categories){
			if($method->categories)$cat_cond = false;
			//vmdebug('hmm, my value',$method);
			//if at least one product is  in a certain category, display this shipment
			if(!is_array($method->categories)) $method->categories = array($method->categories);
			if(!is_array($method->blocking_categories)) $method->blocking_categories = array($method->blocking_categories);
			//Gather used cats
			foreach($cart->products as $product){
				if(array_intersect($product->categories,$method->categories)){
					$cat_cond = true;
					//break;
				}
				if(array_intersect($product->categories,$method->blocking_categories)){
					$cat_cond = false;
					break;
				}
			}
			//if all products in a certain category, display the shipment
			//if a product has a certain category, DO NOT display the shipment
		}

		$allconditions = (int) $weight_cond + (int)$zip_cond + (int)$nbproducts_cond + (int)$orderamount_cond + (int)$country_cond + (int)$cat_cond;
		if($allconditions === 6){
			$result[$hash] = true;
			return TRUE;
		} else {
			$result[$hash] = false;
			//vmdebug('checkConditions '.$method->shipment_name.' does not fit ',(int)$weight_cond,(int)$zip_cond,(int)$nbproducts_cond,(int)$orderamount_cond,(int)$country_cond);
			return FALSE;
		}

		$result[$hash] = false;
		return FALSE;
	}

	/**
	 * @param $method
	 */
	function convert (&$method) {
		
		//$method->weight_start = (float) $method->weight_start;
		//$method->weight_stop = (float) $method->weight_stop;
		$method->orderamount_start =  (float)str_replace(',','.',$method->orderamount_start);
		$method->orderamount_stop =   (float)str_replace(',','.',$method->orderamount_stop);
		$method->zip_start = (int)$method->zip_start;
		$method->zip_stop = (int)$method->zip_stop;
		$method->nbproducts_start = (int)$method->nbproducts_start;
		$method->nbproducts_stop = (int)$method->nbproducts_stop;
		$method->free_shipment = (float)str_replace(',','.',$method->free_shipment);
	}

	/**
	 * @param $cart
	 * @param $method
	 * @return bool
	 */
	private function _nbproductsCond ($cart, $method) {

		if (empty($method->nbproducts_start) and empty($method->nbproducts_stop)) {
			//vmdebug('_nbproductsCond',$method);
			return true;
		}

		$nbproducts = 0;
		foreach ($cart->products as $product) {
			$nbproducts += $product->quantity;
		}

		if ($nbproducts) {

			$nbproducts_cond = $this->testRange($nbproducts,$method,'nbproducts_start','nbproducts_stop','products quantity');

		} else {
			$nbproducts_cond = false;
		}

		return $nbproducts_cond;
	}


	private function testRange($value, $method, $floor, $ceiling,$name){
		
		$cond = true;
		if(!empty($method->$floor) and !empty($method->$ceiling)){
			$cond = (($value >= $method->$floor AND $value <= $method->$ceiling));
			if(!$cond){
				$result = 'FALSE';
				$reason = 'is NOT within Range of the condition from '.$method->$floor.' to '.$method->$ceiling;
			} else {
				$result = 'TRUE';
				$reason = 'is within Range of the condition from '.$method->$floor.' to '.$method->$ceiling;
			}
		} else if(!empty($method->$floor)){
			$cond = ($value >= $method->$floor);
			if(!$cond){
				$result = 'FALSE';
				$reason = 'is not at least '.$method->$floor;
			} else {
				$result = 'TRUE';
				$reason = 'is over min limit '.$method->$floor;
			}
		} else if(!empty($method->$ceiling)){
			$cond = ($value <= $method->$ceiling);
			if(!$cond){
				$result = 'FALSE';
				$reason = 'is over '.$method->$ceiling;
			} else {
				$result = 'TRUE';
				$reason = 'is lower than the set '.$method->$ceiling;
			}
		} else {
			$result = 'TRUE';
			$reason = 'no boundary conditions set';
		}

		vmdebug('shipmentmethod '.$method->shipment_name.' = '.$result.' for variable '.$name.' = '.$value.' Reason: '.$reason);
		return $cond;
	}


	function plgVmOnProductDisplayShipment($product, &$productDisplayShipments){
		
		if ($this->getPluginMethods($product->virtuemart_vendor_id) === 0) {

			return FALSE;
		}
		if (!class_exists('VirtueMartCart'))
			require(VMPATH_SITE . DS . 'helpers' . DS . 'cart.php');

		$html = '';
		if (!class_exists('CurrencyDisplay'))
			require(JPATH_VM_ADMINISTRATOR . DS . 'helpers' . DS . 'currencydisplay.php');
		$currency = CurrencyDisplay::getInstance();

		if(!isset($cart)){
			$cart = VirtueMartCart::getCart();
			$cart->products['virtual'] = $product;
			$cart->_productAdded = true;
			$cart->prepareCartData();
		}
		
		
		
		foreach ($this->methods as $this->_currentMethod) {
			
			if($this->_currentMethod->show_on_pdetails){

				if($this->checkConditions($cart,$this->_currentMethod,$cart->cartPrices,$product)){

					$product->prices['shipmentPrice'] = $this->getCosts($cart,$this->_currentMethod,$cart->cartPrices);

					if(isset($product->prices['VatTax']) and count($product->prices['VatTax'])>0){
						reset($product->prices['VatTax']);
						$rule = current($product->prices['VatTax']);
						if(isset($rule[1])){
							$product->prices['shipmentTax'] = $product->prices['shipmentPrice'] * $rule[1]/100.0;
							$product->prices['shipmentPrice'] = $product->prices['shipmentPrice'] * (1 + $rule[1]/100.0);
						}
					}

					$html = $this->renderByLayout( 'default', array("method" => $this->_currentMethod, "cart" => $cart,"product" => $product,"currency" => $currency) );
				}
			}
		}
		unset($cart->products['virtual']);
		$cart->_productAdded = true;
		$cart->prepareCartData();

		$productDisplayShipments[] = $html;

	}

	/**
	 * Create the table for this plugin if it does not yet exist.
	 * This functions checks if the called plugin is active one.
	 * When yes it is calling the standard method to create the tables
	 *
	 * @author Maneesh Garg
	 *
	 */
	function plgVmOnStoreInstallShipmentPluginTable ($jplugin_id) {

		return $this->onStoreInstallPluginTable ($jplugin_id);
	}

	/**
	 * @param VirtueMartCart $cart
	 * @return null
	 */
	public function plgVmOnSelectCheckShipment (VirtueMartCart &$cart) {
		return $this->OnSelectCheck ($cart);
	}

	/**
	 * plgVmDisplayListFE
	 * This event is fired to display the pluginmethods in the cart (edit shipment/payment) for example
	 *
	 * @param object  $cart Cart object
	 * @param integer $selected ID of the method selected
	 * @return boolean True on success, false on failures, null when this plugin was not selected.
	 * On errors, JError::raiseWarning (or JError::raiseError) must be used to set a message.
	 *
	 * @author Valerie Isaksen
	 * @author Max Milbers
	 */
	public function plgVmDisplayListFEShipment (VirtueMartCart $cart, $selected = 0, &$htmlIn) {
		
		return $this->displayListFE ($cart, $selected, $htmlIn);
	}

	/**
	 * @param VirtueMartCart $cart
	 * @param array          $cart_prices
	 * @param                $cart_prices_name
	 * @return bool|null
	 */
	public function plgVmOnSelectedCalculatePriceShipment (VirtueMartCart $cart, array &$cart_prices, &$cart_prices_name) {
		
		return $this->onSelectedCalculatePrice ($cart, $cart_prices, $cart_prices_name);
	}

	/**
	 * plgVmOnCheckAutomaticSelected
	 * Checks how many plugins are available. If only one, the user will not have the choice. Enter edit_xxx page
	 * The plugin must check first if it is the correct type
	 *
	 * @author Valerie Isaksen
	 * @param VirtueMartCart cart: the cart object
	 * @return null if no plugin was found, 0 if more then one plugin was found,  virtuemart_xxx_id if only one plugin is found
	 *
	 */
	function plgVmOnCheckAutomaticSelectedShipment (VirtueMartCart $cart, array $cart_prices, &$shipCounter) {
				
		if ($shipCounter > 1) {
			return 0;
		}

		return $this->onCheckAutomaticSelected ($cart, $cart_prices, $shipCounter);
	}

	/**
	 * This method is fired when showing when priting an Order
	 * It displays the the payment method-specific data.
	 *
	 * @param integer $_virtuemart_order_id The order ID
	 * @param integer $method_id  method used for this order
	 * @return mixed Null when for payment methods that were not selected, text (HTML) otherwise
	 * @author Valerie Isaksen
	 */
	function plgVmonShowOrderPrint ($order_number, $method_id) {
		return $this->onShowOrderPrint ($order_number, $method_id);
	}

	function plgVmDeclarePluginParamsShipment ($name, $id, &$dataOld) {
		return $this->declarePluginParams ('shipment', $name, $id, $dataOld);
	}

	function plgVmDeclarePluginParamsShipmentVM3 (&$data) {
		
		return $this->declarePluginParams ('shipment', $data);
	}


	/**
	 * @author Max Milbers
	 * @param $data
	 * @param $table
	 * @return bool
	 */
	function plgVmSetOnTablePluginShipment(&$data,&$table){
		
		$name = $data['shipment_element'];
		$id = $data['shipment_jplugin_id'];

		if (!empty($this->_psType) and !$this->selectedThis ($this->_psType, $name, $id)) {
			return FALSE;
		} else {
			$tCon = array('weight_start','weight_stop','orderamount_start','orderamount_stop','shipment_cost','package_fee');
			foreach($tCon as $f){
				if(!empty($data[$f])){
					$data[$f] = str_replace(array(',',' '),array('.',''),$data[$f]);
				}
			}

			$data['nbproducts_start'] = (int) $data['nbproducts_start'];
			$data['nbproducts_stop'] = (int) $data['nbproducts_stop'];

			//Reasonable tests:
			if(!empty($data['zip_start']) and !empty($data['zip_stop']) and (int)$data['zip_start']>=(int)$data['zip_stop']){
				vmWarn('VMSHIPMENT_STAR_TRACK_ZIP_CONDITION_WRONG');
			}
			if(!empty($data['weight_start']) and !empty($data['weight_stop']) and (float)$data['weight_start']>=(float)$data['weight_stop']){
				vmWarn('VMSHIPMENT_STAR_TRACK_WEIGHT_CONDITION_WRONG');
			}

			if(!empty($data['orderamount_start']) and !empty($data['orderamount_stop']) and (float)$data['orderamount_start']>=(float)$data['orderamount_stop']){
				vmWarn('VMSHIPMENT_STAR_TRACK_AMOUNT_CONDITION_WRONG');
			}

			if(!empty($data['nbproducts_start']) and !empty($data['nbproducts_stop']) and (float)$data['nbproducts_start']>=(float)$data['nbproducts_stop']){
				vmWarn('VMSHIPMENT_STAR_TRACK_NBPRODUCTS_CONDITION_WRONG');
			}

			$data['show_on_pdetails'] = (int) $data['show_on_pdetails'];
			return $this->setOnTablePluginParams ($name, $id, $table);
		}
	}
	
	
	/* Custum Function to fetch Shipping Price on Product details Page */
	function plgVmOnProductCalculateShipment ($product, &$productDisplayShipments){
				die('function');
		$address['address_2'] = $_POST['address_2'];
		$address['zip'] = $_POST['zipcode'];
		//$product->product_weight;
		
			$cart = VirtueMartCart::getCart();
			$cart->products['virtual'] = $product;
			$cart->_productAdded = true;
			$cart->prepareCartData();
			
			$totalCostExGST = 0; 
			
			if($address['address_2'] == '' && $address['zip'] == ''){
				/* $address = array(
					'city' => 'AARONS PASS',
					'zip' => '2850',
					'address_2' => 'NSW'
				); */
				return "please add address";
			}
			
			$db = JFactory::getDBO ();
			$q = 'SELECT * FROM `#__statTrack_postCodes` '
				. 'WHERE `postcode` = "' . $address['zip'].'" and `suburb` = "' . $address['address_2'].'"';
			
			$db->setQuery($q);
			
			if (!($shipinfo = $db->loadObject ())) {
				//vmWarn ( 500, $q . " " . $db->getErrorMsg () );
				vmWarn (  "It seems that your Suburb/City does not match the Post Code you have provide, please try again" );
				return '';
			}
			
			/////get enabled shipping method/////

			$qu = 'SELECT * FROM `#__virtuemart_shipmentmethods` WHERE `virtuemart_shipmentmethod_id` = "9"';
			$db->setQuery ($qu);
			$shipmethodP = $db->loadObject();
			$ship_params = $shipmethodP->shipment_params;
			$exParms = explode("|",$ship_params);
			$exArr = explode("=",$exParms[2]);
			$Ship_methods = explode(",", substr($exArr[1],1,-1));


			///////////////////////////////////////////
			$store = array();
			
			$starting_index = substr($address['zip'], 0, 1);  // Check the zip code for selecting store
			
			switch($starting_index){
				case '2' : 
					$store['address_2']  = 'SMEATON GRANGE';
					$store['zip']  = 2567;
					$store['state']  = 'NSW';
				break;
				case '5' : 
					$store['address_2']  = 'HOLDEN HILL';
					$store['zip']  = 5088;
					$store['state']  = 'SA';
				break;
				case '3':
				case '7':
					$store['address_2']  = 'DANDENONG';
					$store['zip']  = 3175;
					$store['state']  = 'VIC';
				break;
				case '8':
				case '6':
					$store['address_2']  = 'KAWANA';
					$store['zip']  = 4701;
					$store['state']  = 'QLD';
				break;
			}
			
			if(empty($store)){
				$storeinfo = array(
						'address_2' => 'Petrie Terrace',
						'zip' => '4000',
						'address_2' => 'QLD'
				);
				
				
				$q = 'SELECT * FROM `#__statTrack_storCodes` '
					. 'WHERE `ship_to` = ' . $address['zip'];
				
				$db->setQuery ($q);
				$storeinfo = array($db->loadObject());
				
				if(!empty($storeinfo)){
					if($storeinfo['store_location'] == 'BRISBANE'){
						$store['address_2']  = 'Petrie Terrace';
						$store['zip']  = 4000;
						$store['state']  = 'QLD';
					}else{
						//$store['city']  = 'ROCKHAMPTON';
						$store['address_2']  = 'KAWANA';
						$store['zip']  = 4701;
						$store['state']  = 'QLD';
					}
				}else{
					$store['address_2']  = 'KAWANA';
					$store['zip']  = 4701;
					$store['state']  = 'QLD';
				}
			}
			
			if(empty($store)){
				$store['address_2']  = 'KAWANA';
				$store['zip']  = 4701;
				$store['state']  = 'QLD';
			}
			
			
			
			$unit = $product->product_lwh_uom;
			switch($unit){
				case 'M':
					$volume = (int)$product->quantity * $product->product_height * $product->product_width * $product->product_length;
					$length = $product->quantity * $product->product_length * 100;
					$width = $product->quantity * $product->product_width * 100;
					$height = $product->quantity * $product->product_height * 100;
				break;
				case 'CM':
					$volume = (int)$product->quantity * $product->product_height/100 * $product->product_width/100 * $product->product_length/100;
					$length = $product->quantity * $product->product_length;
					$width = $product->quantity * $product->product_width;
					$height = $product->quantity * $product->product_height;
				break;
				case 'MM':
					$volume = $product->quantity * ($product->product_height/1000) * ($product->product_width/1000) * ($product->product_length/1000);
					
					$length = $product->quantity * $product->product_length / 10;
					$width = $product->quantity * $product->product_width / 10;
					$height = $product->quantity * $product->product_height / 10;
				break;
				case 'YD':
					$volume = (int)$product->quantity * $product->product_height*0.9144 * $product->product_width*0.9144 * $product->product_length*0.9144;
					$length = $product->quantity * $product->product_length / 91.44;
					$width = $product->quantity * $product->product_width / 91.44;
					$height = $product->quantity * $product->product_height / 91.44;
				break;
				case 'FT':
					$volume = (int)$product->quantity * $product->product_height*0.3048 * $product->product_width*0.3048 * $product->product_length*0.3048;
					$length = $product->quantity * $product->product_length / 30.48;
					$width = $product->quantity * $product->product_width / 30.48;
					$height = $product->quantity * $product->product_height / 30.48;
				break;
				case 'IN':
					$volume = (int)$product->quantity * $product->product_height*0.0254 * $product->product_width*0.0254 * $product->product_length*0.0254;
					$length = $product->quantity * $product->product_length / 2.54;
					$width = $product->quantity * $product->product_width / 2.54;
					$height = $product->quantity * $product->product_height / 2.54;
				break;
				default :
					$volume = (int)$product->quantity * $product->product_height/100 * $product->product_width/100 * $product->product_length/100;
					$length = $product->quantity * $product->product_length;
					$width = $product->quantity * $product->product_width;
					$height = $product->quantity * $product->product_height;
				break;
			} 
			
			//echo "volume->".$volume."length->".$length."width->".$width."height->".$height."weight->".$product->product_weight;
			
			$shipinfo->state_3_code = $shipinfo->state;

			//get sku number
			 $find_underscore = preg_match('/_/', $product->product_sku);
        	if($find_underscore){
        		$product_sku = explode("_",$product->product_sku);
        		$p_sku = $product_sku[1];
        	}
        	foreach($Ship_methods as $ship_enable_methods){ 
				$trmspace = trim($ship_enable_methods); 
				$remove_doubleq =  str_replace('"', '', $trmspace);

				if($remove_doubleq == "aus_post"){ $auspost_flag = 1; }
				elseif($remove_doubleq == "star_track"){ $star_track_flag = 1; }
				elseif($remove_doubleq == "mytoll"){ $mytoll_flag = 1; }
				else{ $auspost_flag = 0; $star_track_flag = 0; $mytoll_flag = 0; }
				
			} //echo "auspost".$auspost_flag."<br>--star--".$star_track_flag."<br>--toll--".$mytoll_flag.'<br>';
			//if aus post and my toll  disabled then use star track 
			if(empty($auspost_flag) && $star_track_flag == 1 && empty($mytoll_flag)){
				$application = JFactory::getApplication();
				if($star_track_flag == 1){
					$totalCostExGST_starTrack = $this->getStarTrackCost($store, $address, $shipinfo, 1, $product->product_weight, $volume);
					if($totalCostExGST_starTrack == 0){
						$application->enqueueMessage(JText::_("There is some Technical error in API. Please check your shipping address6."), 'error');
						$totalCostExGST = 0;
					}else{
						$totalCostExGST = $totalCostExGST_starTrack;
					}
					//echo "Only StarTrack".$totalCostExGST;
				} // closing elseif($star_track_flag == 1)		
			}

			//if aus post and  star track  disabled then use mytoll
			if(empty($auspost_flag) && empty($star_track_flag) && $mytoll_flag==1){

				$application = JFactory::getApplication();

				if($mytoll_flag == 1){
					if(substr($p_sku, 0,2)=="BU"){
						$totalCostExGST_Toll = $this->getOversizeBullBarTollCost($store, $address, $shipinfo, 1, $product->product_weight, $length, $width, $height, $volume);

					}else{
						$totalCostExGST_Toll = $this->getTollCost($store, $address, $shipinfo, 1, $product->product_weight, $length, $width, $height, $volume);
					}

					if($totalCostExGST_Toll == 0){
						$application->enqueueMessage(JText::_("There is some Technical error in API. Please check your shipping address7."), 'error');
						$totalCostExGST = 0;
					}else{
						$totalCostExGST = $totalCostExGST_Toll;
					} //echo "<br/>Toll Price is ".$totalCostExGST_Toll."<br/>";		
			} //close if $mytoll_flag
		}


			//if aus post is disabled then use star track and my toll
			if(empty($auspost_flag)&& $star_track_flag == 1 && $mytoll_flag == 1){

				$application = JFactory::getApplication();
			
				$totalCostExGST_starTrack = $this->getStarTrackCost($store, $address, $shipinfo, 1, $product->product_weight, $volume);
					//echo "Star Track Price is ".$totalCostExGST_starTrack."<br/>";
					if(substr($p_sku, 0,2)=="BU"){
						$totalCostExGST_Toll = $this->getOversizeBullBarTollCost($store, $address, $shipinfo, 1, $product->product_weight, $length, $width, $height, $volume);

					}else{
						$totalCostExGST_Toll = $this->getTollCost($store, $address, $shipinfo, 1, $product->product_weight, $length, $width, $height, $volume);
					}

					//echo "Toll Price is ".$totalCostExGST_Toll."<br/>";
					if($totalCostExGST_starTrack == 0 && $totalCostExGST_Toll == 0){
						$application->enqueueMessage(JText::_("There is some Technical error in API. Please check your shipping address8."), 'error');
						$totalCostExGST = 0;
					}else{
						if($totalCostExGST_starTrack != 0 && $totalCostExGST_Toll != 0){
							$totalCostExGST = min($totalCostExGST_starTrack , $totalCostExGST_Toll);
							
						}else{
							$totalCostExGST = max($totalCostExGST_starTrack , $totalCostExGST_Toll);
							
						}
					}
			}
			//if aus post is disabled then use star track and my toll
			if(empty($auspost_flag)&& $star_track_flag == 1 && empty($mytoll_flag)){
					$totalCostExGST_starTrack = $this->getStarTrackCost($store, $address, $shipinfo, 1, $product->product_weight, $volume);
					if($totalCostExGST_starTrack == 0){
						$application->enqueueMessage(JText::_("There is some Technical error in API. Please check your shipping address9."), 'error');
						$totalCostExGST = 0;
					}else{
						$totalCostExGST = $totalCostExGST_starTrack;
					}
					//echo $totalCostExGST;
				} // closing if(empty($auspost_flag)&& $star_track_flag == 1 && empty($mytoll_flag)){

				//auspost is enalbed but not others
				if($auspost_flag == 1 && empty($star_track_flag) && empty($mytoll_flag)){
					$userModel = VmModel::getModel('user');
					$vmuser = $userModel->getCurrentUser();
					$vmgroup = $vmuser->shopper_groups;

					if($vmgroup[0] == 1){
						$db = JFactory::getDbo();
						$query3 =  "SELECT `qvp13_virtuemart_shipmentmethods`.*, `qvp13_extensions`.*, `qvp13_virtuemart_shipmentmethod_shoppergroups`.`virtuemart_shoppergroup_id`, `qvp13_virtuemart_shipmentmethods_en_gb`.`shipment_name`, `qvp13_virtuemart_shipmentmethods_en_gb`.`shipment_desc` FROM `qvp13_virtuemart_shipmentmethods`, `qvp13_extensions`, `qvp13_virtuemart_shipmentmethod_shoppergroups`, `qvp13_virtuemart_shipmentmethods_en_gb` where `qvp13_virtuemart_shipmentmethods`.virtuemart_shipmentmethod_id=9 and `qvp13_extensions`.extension_id=10458 and `qvp13_virtuemart_shipmentmethod_shoppergroups`.virtuemart_shipmentmethod_id =`qvp13_virtuemart_shipmentmethods`.virtuemart_shipmentmethod_id and `qvp13_virtuemart_shipmentmethod_shoppergroups`.virtuemart_shoppergroup_id=1 and `qvp13_virtuemart_shipmentmethods_en_gb`.virtuemart_shipmentmethod_id = 9";
						$db->setQuery($query3);				
						$this->methods = $db->loadObjectList();
						//shipping method params
						$shipment_params = $this->methods[0]->shipment_params;
						$shipmentArr = explode("|", $this->methods[0]->shipment_params);
						$methodsArr = (array)$this->methods[0];

						foreach ($shipmentArr as $key => $value) {
							$exArr = explode("=", $value); 
							if(!empty($exArr[0])){
								$methodsArr[$exArr[0]] = trim($exArr[1], '"');
								$this->methods[0] = (object)$methodsArr;
							}
						}
						//get weight parameter
						$db = JFactory::getDbo();
						$query3 =  "SELECT `qvp13_virtuemart_shipmentmethods`.shipment_params FROM `qvp13_virtuemart_shipmentmethods` where `qvp13_virtuemart_shipmentmethods`.virtuemart_shipmentmethod_id=8";
						$db->setQuery($query3);				
						$weightres= $db->loadObject();
						$shipmentArrw = explode("|", $weightres->shipment_params);
						foreach ($shipmentArrw as $key => $value) {
							$exArr = explode("=", $value);
							if($exArr[0] == "zip_start"|| $exArr[0] == "zip_stop"){
								$methodsArr[$exArr[0]] = trim($exArr[1], '"');
								$this->methods[0] = (object)$methodsArr;
							}
						}
						$methodsArr['converted'] = '1';
						$this->methods[0] = (object)$methodsArr;
					}

					
				////////////////
				foreach ($this->methods as $this->_currentMethod) {

					if($this->_currentMethod->show_on_pdetails){
						//if($this->checkConditions($cart,$this->_currentMethod,$cart->cartPrices,$product)){
							$totalLength = $totalLength<1?1:$totalLength;
							$totalheight = $totalheight<1?1:$totalheight;
							$totalWidth = $totalWidth<1?1:$totalWidth;
							$ausprice = $this->getCostsFirst($cart,$this->_currentMethod,$cart->cartPrices, $store, $product->product_weight, $length, $width, $height);
							// $ausprice = $this->getCostsFirst($cart,$this->_currentMethod,$cart->cartPrices, $store, $product->product_weight, 1,1,1);
						//}
					}
				}
				
				$totalCostExGST = number_format($ausprice, 2);

			}

			///////if Aus post, star track  is enabled
			if($auspost_flag == 1 && $star_track_flag == 1 && empty($mytoll_flag)){			
			if($product->product_weight < 22 && $length < 105){ 
				/////////////////////////////////////////////////////
					$userModel = VmModel::getModel('user');
					$vmuser = $userModel->getCurrentUser();
					$vmgroup = $vmuser->shopper_groups;

					if($vmgroup[0] == 1){
						$db = JFactory::getDbo();
						$query3 =  "SELECT `qvp13_virtuemart_shipmentmethods`.*, `qvp13_extensions`.*, `qvp13_virtuemart_shipmentmethod_shoppergroups`.`virtuemart_shoppergroup_id`, `qvp13_virtuemart_shipmentmethods_en_gb`.`shipment_name`, `qvp13_virtuemart_shipmentmethods_en_gb`.`shipment_desc` FROM `qvp13_virtuemart_shipmentmethods`, `qvp13_extensions`, `qvp13_virtuemart_shipmentmethod_shoppergroups`, `qvp13_virtuemart_shipmentmethods_en_gb` where `qvp13_virtuemart_shipmentmethods`.virtuemart_shipmentmethod_id=9 and `qvp13_extensions`.extension_id=10458 and `qvp13_virtuemart_shipmentmethod_shoppergroups`.virtuemart_shipmentmethod_id =`qvp13_virtuemart_shipmentmethods`.virtuemart_shipmentmethod_id and `qvp13_virtuemart_shipmentmethod_shoppergroups`.virtuemart_shoppergroup_id=1 and `qvp13_virtuemart_shipmentmethods_en_gb`.virtuemart_shipmentmethod_id = 9";
						$db->setQuery($query3);				
						$this->methods = $db->loadObjectList();
						//shipping method params
						$shipment_params = $this->methods[0]->shipment_params;
						$shipmentArr = explode("|", $this->methods[0]->shipment_params);
						$methodsArr = (array)$this->methods[0];

						foreach ($shipmentArr as $key => $value) {
							$exArr = explode("=", $value); 
							if(!empty($exArr[0])){
								$methodsArr[$exArr[0]] = trim($exArr[1], '"');
								$this->methods[0] = (object)$methodsArr;
							}
						}
						//get weight parameter
						$db = JFactory::getDbo();
						$query3 =  "SELECT `qvp13_virtuemart_shipmentmethods`.shipment_params FROM `qvp13_virtuemart_shipmentmethods` where `qvp13_virtuemart_shipmentmethods`.virtuemart_shipmentmethod_id=8";
						$db->setQuery($query3);				
						$weightres= $db->loadObject();
						$shipmentArrw = explode("|", $weightres->shipment_params);
						foreach ($shipmentArrw as $key => $value) {
							$exArr = explode("=", $value);
							if($exArr[0] == "zip_start"|| $exArr[0] == "zip_stop"){
								$methodsArr[$exArr[0]] = trim($exArr[1], '"');
								$this->methods[0] = (object)$methodsArr;
							}
						}
						$methodsArr['converted'] = '1';
						$this->methods[0] = (object)$methodsArr;
					}

					
				//////////////////////////////////////////////////////
				foreach ($this->methods as $this->_currentMethod) {

					if($this->_currentMethod->show_on_pdetails){
						//if($this->checkConditions($cart,$this->_currentMethod,$cart->cartPrices,$product)){
							$totalLength = $totalLength<1?1:$totalLength;
							$totalheight = $totalheight<1?1:$totalheight;
							$totalWidth = $totalWidth<1?1:$totalWidth;
							$ausprice = $this->getCostsFirst($cart,$this->_currentMethod,$cart->cartPrices, $store, $product->product_weight, $length, $width, $height);
							// $ausprice = $this->getCostsFirst($cart,$this->_currentMethod,$cart->cartPrices, $store, $product->product_weight, 1,1,1);
						//}
					}
				}
				
				//echo $ausprice;
				//$totalCostExGST = number_format($ausprice, 2);
				$totalCostExGST = (float)$ausprice;

				if(empty($totalCostExGST) || $totalCostExGST == " " || $totalCostExGST == "0"){
					$application = JFactory::getApplication();

					$totalCostExGST_starTrack = $this->getStarTrackCost($store, $address, $shipinfo, 1, $product->product_weight, $volume);

					//echo "Star Track Price is ".$totalCostExGST_starTrack."<br/>";

					if($totalCostExGST_starTrack == 0){
						$application->enqueueMessage(JText::_("There is some Technical error in API. Please check your shipping address10."), 'error');
						$totalCostExGST = 0;
					}else{
							$totalCostExGST =$totalCostExGST_starTrack;
						}


				}
				// echo "Auspost Price is1 ".$totalCostExGST."<br/>";
			}else{

				$application = JFactory::getApplication();

				$totalCostExGST_starTrack = $this->getStarTrackCost($store, $address, $shipinfo, 1, $product->product_weight, $volume);

				//echo "Star Track Price is ".$totalCostExGST_starTrack."<br/>";

				if($totalCostExGST_starTrack == 0){
					$application->enqueueMessage(JText::_("There is some Technical error in API. Please check your shipping address11."), 'error');
					$totalCostExGST = 0;
				}else{
						$totalCostExGST =$totalCostExGST_starTrack;
					}
				
			}

		}//closing of if($auspost_flag == 1 && $star_track_flag == 1 && empty($mytoll_flag))

		///////if Aus post, my toll  is enabled
			if($auspost_flag == 1 && empty($star_track_flag) && $mytoll_flag==1){			
			if($product->product_weight < 22 && $length < 105){ 
				/////////////////////////////////////////////////////
					$userModel = VmModel::getModel('user');
					$vmuser = $userModel->getCurrentUser();
					$vmgroup = $vmuser->shopper_groups;

					if($vmgroup[0] == 1){
						$db = JFactory::getDbo();
						$query3 =  "SELECT `qvp13_virtuemart_shipmentmethods`.*, `qvp13_extensions`.*, `qvp13_virtuemart_shipmentmethod_shoppergroups`.`virtuemart_shoppergroup_id`, `qvp13_virtuemart_shipmentmethods_en_gb`.`shipment_name`, `qvp13_virtuemart_shipmentmethods_en_gb`.`shipment_desc` FROM `qvp13_virtuemart_shipmentmethods`, `qvp13_extensions`, `qvp13_virtuemart_shipmentmethod_shoppergroups`, `qvp13_virtuemart_shipmentmethods_en_gb` where `qvp13_virtuemart_shipmentmethods`.virtuemart_shipmentmethod_id=9 and `qvp13_extensions`.extension_id=10458 and `qvp13_virtuemart_shipmentmethod_shoppergroups`.virtuemart_shipmentmethod_id =`qvp13_virtuemart_shipmentmethods`.virtuemart_shipmentmethod_id and `qvp13_virtuemart_shipmentmethod_shoppergroups`.virtuemart_shoppergroup_id=1 and `qvp13_virtuemart_shipmentmethods_en_gb`.virtuemart_shipmentmethod_id = 9";
						$db->setQuery($query3);				
						$this->methods = $db->loadObjectList();
						//shipping method params
						$shipment_params = $this->methods[0]->shipment_params;
						$shipmentArr = explode("|", $this->methods[0]->shipment_params);
						$methodsArr = (array)$this->methods[0];

						foreach ($shipmentArr as $key => $value) {
							$exArr = explode("=", $value); 
							if(!empty($exArr[0])){
								$methodsArr[$exArr[0]] = trim($exArr[1], '"');
								$this->methods[0] = (object)$methodsArr;
							}
						}
						//get weight parameter
						$db = JFactory::getDbo();
						$query3 =  "SELECT `qvp13_virtuemart_shipmentmethods`.shipment_params FROM `qvp13_virtuemart_shipmentmethods` where `qvp13_virtuemart_shipmentmethods`.virtuemart_shipmentmethod_id=8";
						$db->setQuery($query3);				
						$weightres= $db->loadObject();
						$shipmentArrw = explode("|", $weightres->shipment_params);
						foreach ($shipmentArrw as $key => $value) {
							$exArr = explode("=", $value);
							if($exArr[0] == "zip_start"|| $exArr[0] == "zip_stop"){
								$methodsArr[$exArr[0]] = trim($exArr[1], '"');
								$this->methods[0] = (object)$methodsArr;
							}
						}
						$methodsArr['converted'] = '1';
						$this->methods[0] = (object)$methodsArr;
					}

					
				//////////////////////////////////////////////////////
				foreach ($this->methods as $this->_currentMethod) {

					if($this->_currentMethod->show_on_pdetails){
						//if($this->checkConditions($cart,$this->_currentMethod,$cart->cartPrices,$product)){
							$totalLength = $totalLength<1?1:$totalLength;
							$totalheight = $totalheight<1?1:$totalheight;
							$totalWidth = $totalWidth<1?1:$totalWidth;
							$ausprice = $this->getCostsFirst($cart,$this->_currentMethod,$cart->cartPrices, $store, $product->product_weight, $length, $width, $height);
							// $ausprice = $this->getCostsFirst($cart,$this->_currentMethod,$cart->cartPrices, $store, $product->product_weight, 1,1,1);
						//}
					}
				}
				
				//$totalCostExGST = number_format($ausprice, 2);
				$totalCostExGST = (float)$ausprice;
				if(empty($totalCostExGST) || $totalCostExGST == " " || $totalCostExGST == "0"){
					$application = JFactory::getApplication();

						if(substr($p_sku, 0,2)=="BU"){
								$totalCostExGST_Toll = $this->getOversizeBullBarTollCost($store, $address, $shipinfo, 1, $product->product_weight, $length, $width, $height, $volume);

							}else{
								$totalCostExGST_Toll = $this->getTollCost($store, $address, $shipinfo, 1, $product->product_weight, $length, $width, $height, $volume);
							}
						//echo "Toll Price is ".$totalCostExGST_Toll."<br/>";

						if($totalCostExGST_Toll == 0){
							$application->enqueueMessage(JText::_("There is some Technical error in API. Please check your shipping address12."), 'error');
							$totalCostExGST = 0;
						}else{
							$totalCostExGST = $totalCostExGST_Toll;
						}	
				}

				//echo "Auspost Price is2 ".$totalCostExGST."<br/>";
			}else{

				$application = JFactory::getApplication();

				if(substr($p_sku, 0,2)=="BU"){
						$totalCostExGST_Toll = $this->getOversizeBullBarTollCost($store, $address, $shipinfo, 1, $product->product_weight, $length, $width, $height, $volume);

					}else{
						$totalCostExGST_Toll = $this->getTollCost($store, $address, $shipinfo, 1, $product->product_weight, $length, $width, $height, $volume);
					}
				//echo "Toll Price is ".$totalCostExGST_Toll."<br/>";

				if($totalCostExGST_Toll == 0){
					$application->enqueueMessage(JText::_("There is some Technical error in API. Please check your shipping address13."), 'error');
					$totalCostExGST = 0;
				}else{
					$totalCostExGST = $totalCostExGST_Toll;
				}			
			}

		}//closing of if($auspost_flag == 1 && empty($star_track_flag) && $mytoll_flag==1)
		
			///////if Aus post, star track and my toll is enabled
			if($auspost_flag == 1 && $star_track_flag == 1 && $mytoll_flag == 1){			
			if($product->product_weight < 22 && $length < 105){ 
				/////////////////////////////////////////////////////
					$userModel = VmModel::getModel('user');
					$vmuser = $userModel->getCurrentUser();
					$vmgroup = $vmuser->shopper_groups;

					if($vmgroup[0] == 1){
						$db = JFactory::getDbo();
						$query3 =  "SELECT `qvp13_virtuemart_shipmentmethods`.*, `qvp13_extensions`.*, `qvp13_virtuemart_shipmentmethod_shoppergroups`.`virtuemart_shoppergroup_id`, `qvp13_virtuemart_shipmentmethods_en_gb`.`shipment_name`, `qvp13_virtuemart_shipmentmethods_en_gb`.`shipment_desc` FROM `qvp13_virtuemart_shipmentmethods`, `qvp13_extensions`, `qvp13_virtuemart_shipmentmethod_shoppergroups`, `qvp13_virtuemart_shipmentmethods_en_gb` where `qvp13_virtuemart_shipmentmethods`.virtuemart_shipmentmethod_id=9 and `qvp13_extensions`.extension_id=10458 and `qvp13_virtuemart_shipmentmethod_shoppergroups`.virtuemart_shipmentmethod_id =`qvp13_virtuemart_shipmentmethods`.virtuemart_shipmentmethod_id and `qvp13_virtuemart_shipmentmethod_shoppergroups`.virtuemart_shoppergroup_id=1 and `qvp13_virtuemart_shipmentmethods_en_gb`.virtuemart_shipmentmethod_id = 9";
						$db->setQuery($query3);				
						$this->methods = $db->loadObjectList();
						//shipping method params
						$shipment_params = $this->methods[0]->shipment_params;
						$shipmentArr = explode("|", $this->methods[0]->shipment_params);
						$methodsArr = (array)$this->methods[0];

						foreach ($shipmentArr as $key => $value) {
							$exArr = explode("=", $value); 
							if(!empty($exArr[0])){
								$methodsArr[$exArr[0]] = trim($exArr[1], '"');
								$this->methods[0] = (object)$methodsArr;
							}
						}
						//get weight parameter
						$db = JFactory::getDbo();
						$query3 =  "SELECT `qvp13_virtuemart_shipmentmethods`.shipment_params FROM `qvp13_virtuemart_shipmentmethods` where `qvp13_virtuemart_shipmentmethods`.virtuemart_shipmentmethod_id=8";
						$db->setQuery($query3);				
						$weightres= $db->loadObject();
						$shipmentArrw = explode("|", $weightres->shipment_params);
						foreach ($shipmentArrw as $key => $value) {
							$exArr = explode("=", $value);
							if($exArr[0] == "zip_start"|| $exArr[0] == "zip_stop"){
								$methodsArr[$exArr[0]] = trim($exArr[1], '"');
								$this->methods[0] = (object)$methodsArr;
							}
						}
						$methodsArr['converted'] = '1';
						$this->methods[0] = (object)$methodsArr;
					}

					
				//////////////////////////////////////////////////////
				foreach ($this->methods as $this->_currentMethod) {

					if($this->_currentMethod->show_on_pdetails){
						//if($this->checkConditions($cart,$this->_currentMethod,$cart->cartPrices,$product)){
							$totalLength = $totalLength<1?1:$totalLength;
							$totalheight = $totalheight<1?1:$totalheight;
							$totalWidth = $totalWidth<1?1:$totalWidth;
							$ausprice = $this->getCostsFirst($cart,$this->_currentMethod,$cart->cartPrices, $store, $product->product_weight, $length, $width, $height);
							// $ausprice = $this->getCostsFirst($cart,$this->_currentMethod,$cart->cartPrices, $store, $product->product_weight, 1,1,1);
						//}
					}
				}
				
				//$totalCostExGST = number_format($ausprice, 2);
				$totalCostExGST = (float)$ausprice;
				if(empty($totalCostExGST) || $totalCostExGST == " " || $totalCostExGST == "0"){
							$application = JFactory::getApplication();

						$totalCostExGST_starTrack = $this->getStarTrackCost($store, $address, $shipinfo, 1, $product->product_weight, $volume);
						//echo "Star Track Price is ".$totalCostExGST_starTrack."<br/>";
						
						if(substr($p_sku, 0,2)=="BU"){
								$totalCostExGST_Toll = $this->getOversizeBullBarTollCost($store, $address, $shipinfo, 1, $product->product_weight, $length, $width, $height, $volume);

							}else{
								$totalCostExGST_Toll = $this->getTollCost($store, $address, $shipinfo, 1, $product->product_weight, $length, $width, $height, $volume);
							}
						//echo "Toll Price is ".$totalCostExGST_Toll."<br/>";

						
						if($totalCostExGST_starTrack == 0 && $totalCostExGST_Toll == 0){
							$application->enqueueMessage(JText::_("There is some Technical error in API. Please check your shipping address14."), 'error');
							$totalCostExGST = 0;
						}else{
							if($totalCostExGST_starTrack != 0 && $totalCostExGST_Toll != 0){
								$totalCostExGST = min($totalCostExGST_starTrack , $totalCostExGST_Toll);
								
							}else{
								$totalCostExGST = max($totalCostExGST_starTrack , $totalCostExGST_Toll);
								
							}
						}
				}

				// echo "Auspost Price is3 ".$totalCostExGST."<br/>";
			}else{
				$application = JFactory::getApplication();

				$totalCostExGST_starTrack = $this->getStarTrackCost($store, $address, $shipinfo, 1, $product->product_weight, $volume);
				//echo "Star Track Price is ".$totalCostExGST_starTrack."<br/>";
				
				if(substr($p_sku, 0,2)=="BU"){
						$totalCostExGST_Toll = $this->getOversizeBullBarTollCost($store, $address, $shipinfo, 1, $product->product_weight, $length, $width, $height, $volume);

					}else{
						$totalCostExGST_Toll = $this->getTollCost($store, $address, $shipinfo, 1, $product->product_weight, $length, $width, $height, $volume);
					}
				//echo "Toll Price is ".$totalCostExGST_Toll."<br/>";

				
				if($totalCostExGST_starTrack == 0 && $totalCostExGST_Toll == 0){
					$application->enqueueMessage(JText::_("There is some Technical error in API. Please check your shipping address33."), 'error');
					$totalCostExGST = 0;
				}else{
					if($totalCostExGST_starTrack != 0 && $totalCostExGST_Toll != 0){
						$totalCostExGST = min($totalCostExGST_starTrack , $totalCostExGST_Toll);
						
					}else{
						$totalCostExGST = max($totalCostExGST_starTrack , $totalCostExGST_Toll);
						
					}
				}
			}

		}//closing of if($auspost_flag==1 && $star_track_flag == 1 && $mytoll_flag == 1)
		


			
			$method = $this->_currentMethod;

			$taxid = $method->tax_id;
			$taxid = 1;
			if(($taxid !='-1') &&($totalCostExGST)){ 
				$db = JFactory::getDBO ();
				
				$q = 'SELECT * FROM #__virtuemart_calcs WHERE `virtuemart_calc_id`="' . $taxid . '" ';
				$db->setQuery ($q);
				$taxrule = $db->loadObject ();

				$totalCostExGST = $totalCostExGST + (($taxrule->calc_value* $totalCostExGST)/100);
			
			}
			$totalCostExGST = $totalCostExGST + $totalCostExGST*0.2;

		    return $totalCostExGST;

	}

	

}

// No closing tag
