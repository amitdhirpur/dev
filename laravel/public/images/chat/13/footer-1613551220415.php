</div>

<?php if (!is_page_template( 'faitron-contact.php' )) {
    get_template_part('/template-parts/newsletter/newsletter-blog', '');
}
?>
<footer>

    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-xs-4">
                <a href="/" class="logo">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
                         id="Layer_2" x="0px" y="0px" viewBox="0 0 154.2 36.3"
                         style="enable-background:new 0 0 154.2 36.3;" xml:space="preserve">
<style type="text/css">
    .st1 {
        fill: #24AA57;
    }

    .st0 {
        fill: #fff !important;
    }
</style>
                        <g>
                            <g>
                                <path class="st0"
                                      d="M1.9,0h17.4c1,0,1.9,0.8,1.9,1.9v1.5c0,1-0.8,1.9-1.9,1.9H6.1v9.9h11.6c1,0,1.9,0.8,1.9,1.9v1.5    c0,1-0.8,1.9-1.9,1.9H6.1v13.3c0,1-0.8,1.9-1.9,1.9H1.9c-1,0-1.9-0.8-1.9-1.9V1.9C0,0.8,0.8,0,1.9,0z"/>
                                <path class="st0"
                                      d="M34.2,36.3c-5.8,0-9.5-2.9-9.5-8.1s3.7-8.1,9.5-8.1c3.3,0,6.1,1.2,6.9,3.4v-4.2c0-3.3-1.6-4.9-4.9-4.9    c-2.4,0-3.9,0.8-4.5,2.4c-0.3,0.6-0.9,1-1.6,1h-2.4c-1.2,0-2.1-1.1-1.7-2.3c1.2-3.7,4.6-5.9,10.2-5.9c7.2,0,10.7,3.8,10.7,9.7    v12.9c0,0.5,0,1,0.1,1.5c0.1,1.1-0.7,2-1.8,2H43c-0.9,0-1.7-0.7-1.8-1.6c-0.1-0.4-0.1-0.9-0.1-1.3C40.4,34.8,37.9,36.3,34.2,36.3z     M41.1,28.2c0-2.5-1.8-3.7-5.3-3.7c-3.5,0-5.3,1.2-5.3,3.7s1.8,3.7,5.3,3.7C39.3,31.9,41.1,30.7,41.1,28.2z"/>
                                <path class="st0"
                                      d="M55.1,0h2c1,0,1.9,0.8,1.9,1.9v1.8c0,1-0.8,1.9-1.9,1.9h-2c-1,0-1.9-0.8-1.9-1.9V1.9C53.2,0.8,54,0,55.1,0z     M55.1,10.2h2c1,0,1.9,0.8,1.9,1.9v21.7c0,1-0.8,1.9-1.9,1.9h-2c-1,0-1.9-0.8-1.9-1.9V12.1C53.2,11,54,10.2,55.1,10.2z"/>
                                <path class="st0"
                                      d="M73,27.7c0,2.9,0.9,3.6,4,3.6h0.7c1,0,1.9,0.8,1.9,1.9v1c0,1-0.8,1.9-1.8,1.9c-0.6,0-1.3,0-1.8,0    c-6.2,0-8.8-1.8-8.8-7.2V15h-1.7c-1,0-1.9-0.8-1.9-1.9v-1c0-1,0.8-1.9,1.9-1.9h1.7V4.8c0-1,0.8-1.9,1.9-1.9h2c1,0,1.9,0.8,1.9,1.9    v5.4h4.7c1,0,1.9,0.8,1.9,1.9v1c0,1-0.8,1.9-1.9,1.9H73V27.7z"/>
                                <path class="st0"
                                      d="M90.3,22.4v11.4c0,1-0.8,1.9-1.9,1.9h-2c-1,0-1.9-0.8-1.9-1.9V12.1c0-1,0.8-1.9,1.9-1.9h2    c1,0,1.9,0.8,1.9,1.9v2.6c0.9-3.4,3-5.1,6.9-5.1c0.1,0,0.1,0,0.2,0c1,0,1.9,0.9,1.9,1.9l0,1c0,1-0.9,1.9-1.9,1.8    c-0.1,0-0.3,0-0.4,0C92.7,14.4,90.3,17.1,90.3,22.4z"/>
                                <path class="st0"
                                      d="M152.3,35.7h-2c-1,0-1.9-0.8-1.9-1.9V19.7c0-3.6-1.5-5.4-4.3-5.4c-3.4,0-5.9,2.4-5.9,7.1v12.3    c0,1-0.8,1.9-1.9,1.9h-2c-1,0-1.9-0.8-1.9-1.9V12.1c0-1,0.8-1.9,1.9-1.9h2c1,0,1.9,0.8,1.9,1.9v2.5c1-3,3.6-5,7.3-5    c5.6,0,8.7,3.6,8.7,10v14.2C154.2,34.9,153.3,35.7,152.3,35.7z"/>
                            </g>
                            <g>
                                <g>
                                    <g>
                                        <g>
                                            <path class="st1"
                                                  d="M114.5,22.7L114.5,22.7c-1.2,0-2.2-1-2.2-2.2V9c0-1.2,1-2.2,2.2-2.2l0,0c1.2,0,2.2,1,2.2,2.2v11.5       C116.8,21.7,115.8,22.7,114.5,22.7z"/>
                                        </g>
                                    </g>
                                </g>
                                <path class="st1"
                                      d="M122.5,11.9c-1-0.7-2.4-0.5-3.2,0.5c-0.7,1-0.5,2.4,0.5,3.2c2.3,1.7,3.6,4.3,3.6,7.1c0,4.9-4,8.8-8.8,8.8    c-4.9,0-8.8-4-8.8-8.8c0-2.8,1.4-5.5,3.7-7.2c1-0.7,1.3-2.1,0.5-3.1c-0.7-1-2.1-1.3-3.1-0.5c-3.5,2.5-5.6,6.6-5.6,10.9    c0,7.4,6,13.3,13.3,13.3c7.4,0,13.3-6,13.3-13.3C127.8,18.4,125.8,14.4,122.5,11.9z"/>
                            </g>
                        </g>
</svg>
                </a>
            </div>
            <div class="col-sm-3 col-xs-8">
                <?php if (has_nav_menu('social')) : ?>
                    <?php get_template_part('template-parts/navigation/navigation', 'social'); ?>
                <?php endif; ?>
            </div>
            <div class="col-sm-5 col-xs-12">
                <style>
                    .copyright a {
                        color: white;
                        padding-left: 15px;
                    }
                </style>

                <div class="copyright">© Faitron Ltd., 2019 
				    <?php 
           
                        
                        if($GLOBALS['q_config']['language'] =="en")
                        {?>
                      <a class="impressum" href="http://www.faitron.com/impressum/"> Impressum</a>
                      <a class="impressum" href="http://www.faitron.com/terms/"> Terms</a>
					  <a class="impressum" href="http://www.faitron.com/privacy/"> Privacy</a>
                       <?php }
                        if($GLOBALS['q_config']['language'] =="de")
                        {?>
                       <a class="impressum" href="http://www.faitron.com/de/impressum/"> Impressum</a>
					   <a class="impressum" href="http://www.faitron.com/de/terms/"> Bedingungen</a>
					   <a class="impressum" href="http://www.faitron.com/de/privacy/"> Privatsphäre</a>
                        
                       <?php } ?>
					   
				
				</div>
            </div>
        </div>
    </div>

<?php //echo get_permalink();  ?>



</footer>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBpmQUo0Qs_t2l4geos82BReRMo8sjJWko"></script>

<?php wp_footer(); ?>
<?php get_template_part('/modules/contact-fotm', ''); ?>
</body>
</html>

<script>
$(document).ready(function(){
  if(location.href == 'https://www.faitron.com/contact/#wpcf7-f10690-o1'){
    window.location.href = "https://www.faitron.com/thankyou/";
  }
   /* $("#slick-slider").ready(function(){
      if ($(window).width() <= 600) {  

             

       }  
     });  */
  

 var qtranslate=localStorage.getItem("qtranslate");
   $( "#qtranslate option" ).each(function() {
       if($(this).val() == qtranslate){
	   $(this).attr("selected","");
	 }
	});
   if(window.location.href == window.location.origin+"/de/")
   {
    localStorage.setItem("qtranslate", window.location.origin+"/de/");
       $( "#qtranslate option" ).each(function() {
           if($(this).val() == window.location.origin+"/de/"){
           $(this).attr("selected","");
         }
        });
   }
   if(window.location.href == window.location.origin+"/")
   {
    localStorage.setItem("qtranslate", window.location.origin+"/en/");
    $( "#qtranslate option" ).each(function() {
           if($(this).val() == window.location.origin+"/en/"){
           $(this).attr("selected","");
         }
        });
   }

$("#click9").click(function() {
    $('html,body').animate({
        scrollTop: $("#onclick5").offset().top},
        3000);
});
$("#buy_now").click(function() {
    window.location.href="https://heatsbox.myshopify.com/products/worlds-smartest-heating-lunchbox";
});
$("#Jetzt").click(function() {
    window.location.href="https://heatsbox.myshopify.com/products/smart-heatsbox-app-gesteuert";
});
       /* $(".banner_outer").on("click" ,function(){
                scrolled=scrolled+600;
        
                $("body").animate({
                        scrollTop:  scrolled
                   });

            });*/
            $('#qtranslate').on('change', function() {
               var qtranslate_value =$(this).val();
              // alert(qtranslate_value)
		        var url=window.location.origin+"/faitron";
            var value=window.location.pathname;
           // var value=val_url.split("/");
           // var orgval=value[0]+"//"+value[1]+value[2]+"/";
           //alert(value);
          // return false;
            if(qtranslate_value && qtranslate_value == "de"){
               value = value.replace("/faitron/de", "");
               value = value.replace("/faitron", "");
               value = value.replace("/faitron/de", "");
               //alert(url+qtranslate_value+value)
                 window.location.href=url+"/"+qtranslate_value+value;
            }
            else
            {
              value = value.replace("/faitron/en", "");
              value = value.replace("/faitron/de", "");
              value = value.replace("/faitron", "");
              // alert(url+qtranslate_value+value)
                window.location.href=url+"/"+qtranslate_value+value;
            }
		   
		    localStorage.setItem("qtranslate", qtranslate_value);
		    $(this).addClass("qtranslate_add");
		   });
		    
		   
});
$.get("https://api.ipdata.co?api-key=test", function (response) {
    var html=JSON.stringify(response, null, 4);
    var obj = JSON.parse(html);
}, "jsonp");
</script>


<?php    
function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
             $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
           $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
 $PublicIP = get_client_ip(); 
 $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$PublicIP)); 
 $country=$ip_data->geoplugin_countryCode;
 
if(($country == "CH" || $country == "DE" || $country == "AT") && ($GLOBALS['q_config']['language'] =="en"))
{?>
<script>
var target=window.location.origin;
//window.location.href=target+"/de/";
if(localStorage.getItem("qtranslate")=== null){
  localStorage.setItem("qtranslate", target+"/de/");
  window.location.href=target+"/de/";
 }
</script>

<?php }
 
?> 
<?php $language = get_locale();
 ?>
<!-- 
<script>
jQuery(".custom_sucess1").click(function(){
	var lang = "<?php echo $language; ?>";
	if(lang="de_DE"){
		window.location.href="https://heatsbox.myshopify.com/products/smart-heatsbox-app-gesteuert";         
    }

});
</script> -->



