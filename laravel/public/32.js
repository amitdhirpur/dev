(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[32],{

/***/ "../coreui/src/views/leaves/Leave.vue":
/*!********************************************!*\
  !*** ../coreui/src/views/leaves/Leave.vue ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Leave_vue_vue_type_template_id_139c59ee___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Leave.vue?vue&type=template&id=139c59ee& */ "../coreui/src/views/leaves/Leave.vue?vue&type=template&id=139c59ee&");
/* harmony import */ var _Leave_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Leave.vue?vue&type=script&lang=js& */ "../coreui/src/views/leaves/Leave.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Leave_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Leave_vue_vue_type_template_id_139c59ee___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Leave_vue_vue_type_template_id_139c59ee___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/leaves/Leave.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/leaves/Leave.vue?vue&type=script&lang=js&":
/*!*********************************************************************!*\
  !*** ../coreui/src/views/leaves/Leave.vue?vue&type=script&lang=js& ***!
  \*********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Leave_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Leave.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/leaves/Leave.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Leave_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/leaves/Leave.vue?vue&type=template&id=139c59ee&":
/*!***************************************************************************!*\
  !*** ../coreui/src/views/leaves/Leave.vue?vue&type=template&id=139c59ee& ***!
  \***************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Leave_vue_vue_type_template_id_139c59ee___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Leave.vue?vue&type=template&id=139c59ee& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/leaves/Leave.vue?vue&type=template&id=139c59ee&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Leave_vue_vue_type_template_id_139c59ee___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Leave_vue_vue_type_template_id_139c59ee___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/leaves/Leave.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/leaves/Leave.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "../coreui/node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Leave',
  data: function data() {
    return {
      name: '',
      profile: '',
      from_date: '',
      to_date: '',
      _id: '',
      email: '',
      date_format: '',
      subject: '',
      description: '',
      status: '',
      show: false,
      loading: false,
      permission: false,
      avtar: '/img/user-avtar.jpg',
      loader: '/img/loader.gif'
    };
  },
  methods: {
    goBack: function goBack() {
      this.$router.back();
    },
    showOptions: function showOptions() {
      this.show = true;
    },
    dateFormat: function dateFormat(val) {
      var value = "";

      if (val) {
        value = moment__WEBPACK_IMPORTED_MODULE_1___default()(val).format(this.date_format);
      }

      return value;
    },
    getStatus: function getStatus() {
      var _cls = '',
          _name = '';

      if (this.status == '1') {
        _cls = 'success';
        _name = 'Approved';
      } else if (this.status == '2') {
        _cls = 'danger';
        _name = 'Rejected';
      } else {
        _cls = 'warning';
        _name = 'Pending';
      }

      return '<span class="badge badge-' + _cls + '">' + _name + '</span>';
    },
    setStatus: function setStatus() {
      if (this.status) {
        var that = this;
        this.loading = true;
        var formdata = new FormData();
        formdata.append('id', this._id);
        formdata.append('status', this.status);
        axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/leaves/edit?token=' + localStorage.getItem("api_token"), formdata).then(function (response) {
          that.loading = false;
          that.show = false; // if(response.data.status == 'success') {
          // 	this.$swal('Success', 'Successfully update status', 'success');
          // } else {
          // 	this.$swal('Warning!', 'Unable to update status', 'error');
          // }
        });
      }
    }
  },
  mounted: function mounted() {
    var self = this;
    this.loading = true;
    axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/leaves/view/' + self.$route.params.id + '?token=' + localStorage.getItem("api_token")).then(function (response) {
      self.loading = false;
      self.date_format = response.data.date_format.value;
      var leave = response.data.leave;
      self.name = leave.name;
      self.profile = leave.profile;
      self.from_date = leave.start_date;
      self.to_date = leave.end_date;
      self._id = leave.id;
      self.subject = leave.subject;
      self.description = leave.description;
      self.status = leave.status;
      self.permission = response.data.permission;
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/leaves/Leave.vue?vue&type=template&id=139c59ee&":
/*!*********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/leaves/Leave.vue?vue&type=template&id=139c59ee& ***!
  \*********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", lg: "12" } },
        [
          _c(
            "CCardHeader",
            {
              staticClass:
                "mb-1 d-flex justify-content-between align-items-center"
            },
            [
              _c("h4", { staticClass: "mb-0" }, [
                _vm._v("Leave Request From: " + _vm._s(_vm.name))
              ]),
              _vm._v(" "),
              _c(
                "CButton",
                { attrs: { color: "primary" }, on: { click: _vm.goBack } },
                [_vm._v("Back")]
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "CRow",
            { staticClass: "user-view-sec" },
            [
              _c(
                "CCol",
                { attrs: { col: "5" } },
                [
                  _c(
                    "CCard",
                    [
                      _c("CCardBody", [
                        _c("div", { staticClass: "user-profile-pic" }, [
                          _vm.profile
                            ? _c("img", { attrs: { src: _vm.profile } })
                            : _c("img", { attrs: { src: _vm.avtar } })
                        ])
                      ])
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "CCol",
                { attrs: { col: "7" } },
                [
                  _c(
                    "CCard",
                    [
                      _c(
                        "CCardBody",
                        [
                          _c("CCardHeader", [_vm._v("Leave Information")]),
                          _vm._v(" "),
                          _c("div", { staticClass: "user-info-section" }, [
                            _vm.from_date
                              ? _c("div", { staticClass: "inner-sec" }, [
                                  _c("span", [_vm._v("From Date:")]),
                                  _vm._v(" "),
                                  _c("strong", [
                                    _vm._v(
                                      _vm._s(_vm.dateFormat(this.from_date))
                                    )
                                  ])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.to_date
                              ? _c("div", { staticClass: "inner-sec" }, [
                                  _c("span", [_vm._v("To Date:")]),
                                  _vm._v(" "),
                                  _c("strong", [
                                    _vm._v(_vm._s(_vm.dateFormat(this.to_date)))
                                  ])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.subject
                              ? _c("div", { staticClass: "inner-sec" }, [
                                  _c("span", [_vm._v("Subject:")]),
                                  _vm._v(" "),
                                  _c("strong", [_vm._v(_vm._s(_vm.subject))])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.description
                              ? _c("div", { staticClass: "inner-sec" }, [
                                  _c("span", [_vm._v("Reason:")]),
                                  _vm._v(" "),
                                  _c("strong", [
                                    _vm._v(_vm._s(_vm.description))
                                  ])
                                ])
                              : _vm._e(),
                            _vm._v(" "),
                            _vm.status
                              ? _c("div", { staticClass: "inner-sec d-flex" }, [
                                  _c("span", [_vm._v("Status:")]),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    { staticClass: "leave-status" },
                                    [
                                      _vm.show
                                        ? _c(
                                            "CRow",
                                            [
                                              _c("CCol", [
                                                _c(
                                                  "select",
                                                  {
                                                    directives: [
                                                      {
                                                        name: "model",
                                                        rawName: "v-model",
                                                        value: _vm.status,
                                                        expression: "status"
                                                      }
                                                    ],
                                                    staticClass: "form-control",
                                                    on: {
                                                      change: [
                                                        function($event) {
                                                          var $$selectedVal = Array.prototype.filter
                                                            .call(
                                                              $event.target
                                                                .options,
                                                              function(o) {
                                                                return o.selected
                                                              }
                                                            )
                                                            .map(function(o) {
                                                              var val =
                                                                "_value" in o
                                                                  ? o._value
                                                                  : o.value
                                                              return val
                                                            })
                                                          _vm.status = $event
                                                            .target.multiple
                                                            ? $$selectedVal
                                                            : $$selectedVal[0]
                                                        },
                                                        _vm.setStatus
                                                      ]
                                                    }
                                                  },
                                                  [
                                                    _c(
                                                      "option",
                                                      { attrs: { value: "" } },
                                                      [
                                                        _vm._v(
                                                          "--Select Status --"
                                                        )
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "option",
                                                      { attrs: { value: "0" } },
                                                      [_vm._v("Pending")]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "option",
                                                      { attrs: { value: "1" } },
                                                      [_vm._v("Approve")]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "option",
                                                      { attrs: { value: "2" } },
                                                      [_vm._v("Reject")]
                                                    )
                                                  ]
                                                )
                                              ])
                                            ],
                                            1
                                          )
                                        : _c(
                                            "div",
                                            {
                                              staticClass:
                                                "d-flex justify-content-between"
                                            },
                                            [
                                              _c("span", {
                                                domProps: {
                                                  innerHTML: _vm._s(
                                                    _vm.getStatus()
                                                  )
                                                }
                                              }),
                                              _vm._v(" "),
                                              _vm.permission
                                                ? _c(
                                                    "span",
                                                    {
                                                      staticClass: "pointer",
                                                      on: {
                                                        click: _vm.showOptions
                                                      }
                                                    },
                                                    [
                                                      _c("CIcon", {
                                                        attrs: {
                                                          name: "cil-pencil"
                                                        }
                                                      })
                                                    ],
                                                    1
                                                  )
                                                : _vm._e()
                                            ]
                                          )
                                    ],
                                    1
                                  )
                                ])
                              : _vm._e()
                          ])
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _vm.loading
        ? _c("div", { staticClass: "loader-image" }, [
            _c("img", { attrs: { src: _vm.loader } })
          ])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);