(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[28],{

/***/ "../coreui/src/views/drive/Files.vue":
/*!*******************************************!*\
  !*** ../coreui/src/views/drive/Files.vue ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Files_vue_vue_type_template_id_8cbe8a10___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Files.vue?vue&type=template&id=8cbe8a10& */ "../coreui/src/views/drive/Files.vue?vue&type=template&id=8cbe8a10&");
/* harmony import */ var _Files_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Files.vue?vue&type=script&lang=js& */ "../coreui/src/views/drive/Files.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Files_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Files_vue_vue_type_template_id_8cbe8a10___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Files_vue_vue_type_template_id_8cbe8a10___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/drive/Files.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/drive/Files.vue?vue&type=script&lang=js&":
/*!********************************************************************!*\
  !*** ../coreui/src/views/drive/Files.vue?vue&type=script&lang=js& ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Files_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Files.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/drive/Files.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Files_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/drive/Files.vue?vue&type=template&id=8cbe8a10&":
/*!**************************************************************************!*\
  !*** ../coreui/src/views/drive/Files.vue?vue&type=template&id=8cbe8a10& ***!
  \**************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Files_vue_vue_type_template_id_8cbe8a10___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Files.vue?vue&type=template&id=8cbe8a10& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/drive/Files.vue?vue&type=template&id=8cbe8a10&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Files_vue_vue_type_template_id_8cbe8a10___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Files_vue_vue_type_template_id_8cbe8a10___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/drive/Files.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/drive/Files.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "../coreui/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vue_image_lightbox__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-image-lightbox */ "../coreui/node_modules/vue-image-lightbox/dist/vue-image-lightbox.min.js");
/* harmony import */ var vue_image_lightbox__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vue_image_lightbox__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _errors_validation_errors_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/errors/validation-errors.vue */ "../coreui/src/views/errors/validation-errors.vue");
/* harmony import */ var _drive_FileProgress_vue__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/drive/FileProgress.vue */ "../coreui/src/views/drive/FileProgress.vue");
/* harmony import */ var _drive_DownloadProgressBar_vue__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/drive/DownloadProgressBar.vue */ "../coreui/src/views/drive/DownloadProgressBar.vue");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! vuex */ "../coreui/node_modules/vuex/dist/vuex.esm.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
__webpack_require__(/*! vue-image-lightbox/dist/vue-image-lightbox.min.css */ "../coreui/node_modules/vue-image-lightbox/dist/vue-image-lightbox.min.css");







/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'DriveFiles',
  data: function data() {
    return {
      files: '',
      progressWidth: 0,
      downloadProgressWidth: 0,
      search: '',
      loader: '/img/loader.gif',
      filePath: {
        "zip": "https://team.teqtop.com/img/zip-file.png",
        "docx": "https://team.teqtop.com/img/doc-file.png",
        "folder": "https://team.teqtop.com/img/folder.png",
        "mp4": "https://team.teqtop.com/img/video-file.png",
        "txt": "https://team.teqtop.com/img/txt-file.png",
        "psd": "https://team.teqtop.com/img/psd-icon.png",
        "pdf": "https://team.teqtop.com/img/pdf-icon.png",
        "file": "https://team.teqtop.com/img/file.png"
      },
      folders: [],
      placement: 'bottom',
      folderName: '',
      parentFolder: '',
      darkModal: false,
      dangerModal: false,
      isActive: false,
      renameModal: false,
      is_loading: false,
      targetId: '',
      isAdmin: null,
      Auth_id: null,
      media: [],
      imageType: ['jpg', 'jpeg', 'gif', 'png', 'tiff', 'bmp'],
      num: 0,
      renameIsFolder: '',
      renameFolderName: '',
      oldFolderName: '',
      renameId: ''
    };
  },
  components: {
    LightBox: vue_image_lightbox__WEBPACK_IMPORTED_MODULE_2___default.a,
    validationError: _errors_validation_errors_vue__WEBPACK_IMPORTED_MODULE_3__["default"],
    progressBar: _drive_FileProgress_vue__WEBPACK_IMPORTED_MODULE_4__["default"],
    DownloadProgressBar: _drive_DownloadProgressBar_vue__WEBPACK_IMPORTED_MODULE_5__["default"]
  },
  computed: _objectSpread(_objectSpread(_objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_6__["mapGetters"])(["errors"])), Object(vuex__WEBPACK_IMPORTED_MODULE_6__["mapGetters"])(["downloadBtn"])), Object(vuex__WEBPACK_IMPORTED_MODULE_6__["mapState"])(['percentComplete', 'downloadPercentComplete', 'kbps'])), {}, {
    progressUpgrade: function progressUpgrade() {
      return this.progressWidth;
    },
    downloadProgressUpgrade: function downloadProgressUpgrade() {
      return this.downloadProgressWidth;
    }
  }),
  methods: {
    validationError: function validationError() {
      var self = this;
      var error = {};

      if (!self.folderName) {
        error.folderName = [];
        error.folderName[0] = "This Field is required!";
      }

      if (Object.keys(error).length) {
        self.$store.commit("setErrors", error);
        return true;
      }

      self.$store.commit("setErrors", {});
    },
    formPostAjax: function formPostAjax(files) {
      if (files) {
        this.files = files;
        document.querySelector('.drive-upload .outer-drag-drop').style.display = "none";
      }

      var self = this;
      self.progressWidth = 0;
      var formData = new FormData();

      for (var i = 0; i < this.files.length; i++) {
        var file = this.files[i];
        formData.append('files[' + i + ']', file);
      }

      formData.append('isFile', true);
      var path = this.$route.fullPath;
      var lastSegment = path.substring(path.lastIndexOf('/') + 1);
      formData.append('parent', lastSegment);
      self.$store.commit('setProgressBarModal', true);
      var config = {
        onUploadProgress: function onUploadProgress(progressEvent) {
          var percentCompleted = Math.round(progressEvent.loaded * 100 / progressEvent.total);
          self.progressWidth = percentCompleted;
          self.$store.commit('setPercentComplete', percentCompleted);

          if (percentCompleted === 100) {
            setTimeout(function () {
              self.$store.commit('setProgressBarModal', false);
              self.$store.commit('setPercentComplete', 0);
            }, 1000);
          }
        }
      };
      axios__WEBPACK_IMPORTED_MODULE_1___default.a.post('/api/drives/add?token=' + localStorage.getItem("api_token"), formData, config).then(function (response) {
        self.folders = response.data.driveFolders;
        self.media = response.data.test;

        var _targetEl = document.querySelector('.outer-drag-drop');

        if (_targetEl.style.display == "flex") {
          _targetEl.style.display = 'none';
        }
      });
    },
    getDrives: function getDrives() {
      var self = this;
      self.is_loading = true;
      var _path = self.$route.path;
      axios__WEBPACK_IMPORTED_MODULE_1___default.a.get('/api' + _path + '?token=' + localStorage.getItem("api_token")).then(function (response) {
        self.folders = response.data.driveFolders;
        self.media = response.data.test;
        self.is_loading = false;
        self.parentFolder = response.data.parentFolder;
        self.targetId = response.data.parentFolder.id;
        self.Auth_id = localStorage.getItem("user");
        self.isAdmin = localStorage.getItem("type");
      });
    },
    handleFileUpload: function handleFileUpload(event) {
      this.files = event.target.files;
      this.formPostAjax();
    },
    createFolder: function createFolder() {
      var self = this;

      if (this.validationError()) {
        return;
      }

      self.$store.commit("setErrors", {});
      self.is_loading = true;
      var _current_path = this.$route.path;

      var _form = document.getElementById('createFolder');

      var _folder = this.folderName;

      var is_folder = _form.querySelector('input[type="hidden"]').value;

      var lastSegment = _current_path.substring(_current_path.lastIndexOf('/') + 1);

      var formData = new FormData();
      formData.append('_folder', _folder);
      formData.append('is_folder', is_folder);
      formData.append('current_path', _current_path);
      formData.append('parent', lastSegment);
      axios__WEBPACK_IMPORTED_MODULE_1___default.a.post('/api/drives/folder/add?token=' + localStorage.getItem("api_token"), formData).then(function (response) {
        if (response.data.errors) {
          var error = {};
          self.is_loading = false;
          error.folderName = [];
          error.folderName[0] = response.data.errors;
          self.$store.commit("setErrors", error);
        } else {
          self.is_loading = false;
          self.darkModal = false;
          self.folders = response.data.driveFolders;
          self.media = response.data.test;
        }
      });
    },
    doubleClick: function doubleClick(link) {
      var self = this;

      var _type = event.currentTarget.getAttribute('data-type');

      var _path = event.currentTarget.getAttribute('data-path');

      var _name = event.currentTarget.getAttribute('data-name');

      if (_type != null && (_type == "pdf" || _type == 'zip' || _type == "docx" || _type == "txt")) {
        self.onClick(_path, _name, _type);
      }

      this.$router.push({
        path: link
      });
    },
    onClick: function onClick(link, name, ext) {
      link = document.location.origin + link;
      axios__WEBPACK_IMPORTED_MODULE_1___default()({
        url: link,
        method: 'GET',
        responseType: 'blob'
      }).then(function (response) {
        var fileURL = window.URL.createObjectURL(new Blob([response.data]));
        var fileLink = document.createElement('a');
        fileLink.href = fileURL;
        fileLink.setAttribute('download', name);
        document.body.appendChild(fileLink);
        fileLink.click();
      });
    },
    renameFolderModal: function renameFolderModal(id, isFolder, name) {
      this.$store.commit("setErrors", {});
      this.renameModal = true;
      this.renameIsFolder = isFolder;
      this.renameId = id;
      this.renameFolderName = name;
      this.oldFolderName = name;
    },
    renameFolderOrFile: function renameFolderOrFile() {
      var self = this;
      var _current_path = this.$route.path;

      var lastSegment = _current_path.substring(_current_path.lastIndexOf('/') + 1);

      var formData = new FormData();
      formData.append('isFolder', self.renameIsFolder);
      formData.append('oldFolderName', self.oldFolderName);
      formData.append('renameFolderName', self.renameFolderName);
      formData.append('parent', lastSegment);
      formData.append('id', self.renameId);
      axios__WEBPACK_IMPORTED_MODULE_1___default.a.post('/api/drives/folder/rename', formData).then(function (response) {
        if (response.data.errors) {
          var error = {};
          error.folderName = [];
          error.folderName[0] = response.data.errors;
          self.$store.commit("setErrors", error);
        } else {
          self.folders = response.data.driveFolders;
          self.media = response.data.test;
          self.renameModal = false;
        }
      });
    },
    deleteFolderOrFile: function deleteFolderOrFile(id) {
      var self = this;
      self.is_loading = true;
      var _current_path = this.$route.path;

      var lastSegment = _current_path.substring(_current_path.lastIndexOf('/') + 1);

      var formData = new FormData();
      formData.append('parent', lastSegment);
      formData.append('id', id);
      axios__WEBPACK_IMPORTED_MODULE_1___default.a.post('/api/drives/folder/delete?token=' + localStorage.getItem("api_token"), formData).then(function (response) {
        self.folders = response.data.driveFolders;
        self.media = response.data.test;
        self.is_loading = false;
      });
    },
    openGallery: function openGallery(index) {
      this.$refs.lightbox.showImage(index);
    },
    downloadZip: function downloadZip(path, name, type) {
      var self = this;
      self.$store.commit('setDownloadProgressBarModal', true);
      var formData = new FormData();
      formData.append('path', path);
      formData.append('type', type);
      formData.append('name', name);
      axios__WEBPACK_IMPORTED_MODULE_1___default.a.post('/api/drives/zip-files?token=' + localStorage.getItem("api_token"), formData).then(function (response) {
        var start = new Date().getTime();

        if (response.data.error) {
          self.dangerModal = true;
          self.is_loading = false;
          self.$store.commit('setDownloadProgressBarModal', false);
        } else {
          self.is_loading = false;
          var link = "https://dev.team.teqtop.com" + response.data;
          var config = {
            onDownloadProgress: function onDownloadProgress(progressEvent) {
              var percentCompleted = Math.round(progressEvent.loaded * 100 / progressEvent.total);
              var end = new Date().getTime();
              var duration = (end - start) / 1000;
              var bps = progressEvent.loaded / duration;
              var kbps = bps / 1024;
              kbps = Math.floor(kbps);
              var time = progressEvent.total / progressEvent.loaded / bps;
              var seconds = time % 60;
              var minutes = time / 60;
              self.downloadProgressWidth = percentCompleted;
              var completed = new Object();
              completed.percentCompleted = percentCompleted;
              completed.kbps = kbps; // completed.minutes = minutes;         

              self.$store.commit('setDownloadPercentComplete', completed); // completed.seconds = seconds;

              if (percentCompleted === 100) {
                setTimeout(function () {
                  self.$store.commit('setDownloadProgressBarModal', false);
                }, 1000);
              }
            }
          };
          axios__WEBPACK_IMPORTED_MODULE_1___default.a.get(link, config).then(function () {
            window.location = link;
          });
        }
      });
    },
    closeModal: function closeModal() {
      var self = this;
      self.$store.commit('setDownloadBtn', false);
    },
    openModal: function openModal() {
      var self = this;
      self.$store.commit('setDownloadBtn', true);
      self.$nextTick(function () {
        var _src = "";

        if (event.target.className == "dropdown-item") {
          _src = event.target.parentElement.parentElement.parentElement.nextElementSibling.getAttribute('src');
        } else {
          _src = event.currentTarget.getAttribute('src');
        }

        document.querySelector('.download-file a').setAttribute('download', '');
        document.querySelector('.download-file a').setAttribute('href', _src);
      });
    },
    currentVal: function currentVal() {
      setTimeout(function () {
        var _src = document.querySelector('.vue-lb-modal-image') || document.querySelector('.vue-lb-content .vue-lb-figure video source');

        _src = _src.getAttribute('src');
        document.querySelector('.download-file a').setAttribute('download', '');
        document.querySelector('.download-file a').setAttribute('href', _src);
      }, 800);
    },
    goBack: function goBack() {
      this.$router.back();
    },
    driveSearch: function driveSearch() {
      var self = this;
      self.is_loading = true;
      var path = this.$route.fullPath;
      var lastSegment = path.substring(path.lastIndexOf('/') + 1);
      var formdata = new FormData();
      formdata.append('search', this.search);
      formdata.append('parent', lastSegment);
      axios__WEBPACK_IMPORTED_MODULE_1___default.a.post('/api/drives/search-drive', formdata).then(function (response) {
        self.is_loading = false;
        self.folders = response.data.driveFolders;
        self.media = response.data.test;
      });
    },
    handleDragFileUpload: function handleDragFileUpload(event) {
      this.files = event.target.files;
      this.formPostAjax();
    },
    allowDragDrop: function allowDragDrop() {
      document.querySelector('.outer-drag-drop').style.display = "flex";
    },
    dragleave: function dragleave() {
      document.querySelector('.outer-drag-drop').style.display = "none";
    },
    adddarkModal: function adddarkModal() {
      this.darkModal = true;
      this.$store.commit("setErrors", {});
    }
  },
  mounted: function mounted() {
    var _this = this;

    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _this.getDrives();

            case 1:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/drive/Files.vue?vue&type=template&id=8cbe8a10&":
/*!********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/drive/Files.vue?vue&type=template&id=8cbe8a10& ***!
  \********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "wrapper" },
    [
      _c(
        "CRow",
        [
          _c(
            "CCol",
            { attrs: { col: "12", xl: "12" } },
            [
              _c(
                "div",
                {
                  staticClass: "outer-drag-drop",
                  staticStyle: { display: "none" },
                  on: { dragleave: _vm.dragleave }
                },
                [
                  _c("div", { staticClass: "inner-drop" }, [
                    _c("h2", [_vm._v("Upload file or image")]),
                    _vm._v(" "),
                    _c("p", [_vm._v("Add using drag'n'drop")]),
                    _vm._v(" "),
                    _c("form", [
                      _c("input", {
                        attrs: { type: "file", multiple: "multiple" },
                        on: { change: _vm.handleDragFileUpload }
                      })
                    ])
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "transition",
                { attrs: { name: "slide" } },
                [
                  _c(
                    "CCard",
                    [
                      _c(
                        "CCardHeader",
                        { staticClass: "d-flex justify-content-between" },
                        [
                          _c("div", { staticClass: "my-drive" }, [
                            _c("h4", [_vm._v("My Drive :-")]),
                            _c("p", [_vm._v(_vm._s(_vm.parentFolder.name))])
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "dropdown-back-btn" },
                            [
                              _c("CInput", {
                                staticClass: "mb-0",
                                attrs: { Placeholder: "Filter and search" },
                                on: { input: _vm.driveSearch },
                                model: {
                                  value: _vm.search,
                                  callback: function($$v) {
                                    _vm.search = $$v
                                  },
                                  expression: "search"
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "back-btn-outer" },
                                [
                                  _c(
                                    "CButton",
                                    {
                                      attrs: { color: "primary" },
                                      on: { click: _vm.goBack }
                                    },
                                    [_vm._v("Back")]
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "CDropdown",
                                {
                                  staticClass: "dropdown-text",
                                  attrs: {
                                    "toggler-text": "Add",
                                    color: "primary"
                                  }
                                },
                                [
                                  _c(
                                    "form",
                                    {
                                      staticClass: "file-outer",
                                      attrs: { id: "formPostAjax" }
                                    },
                                    [
                                      _c("input", {
                                        attrs: {
                                          type: "file",
                                          multiple: "multiple"
                                        },
                                        on: { change: _vm.handleFileUpload }
                                      }),
                                      _vm._v(" "),
                                      _c("p", [_vm._v("File")])
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "CButton",
                                    {
                                      staticClass: "mr-1",
                                      on: { click: _vm.adddarkModal }
                                    },
                                    [
                                      _vm._v(
                                        "\n                      Folder\n              "
                                      )
                                    ]
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "CCardBody",
                        { on: { dragover: _vm.allowDragDrop } },
                        [
                          _vm.folders.length
                            ? _c(
                                "CRow",
                                _vm._l(_vm.folders, function(folder, index) {
                                  return _c(
                                    "CCol",
                                    {
                                      key: folder.id,
                                      staticClass: "mb-4",
                                      attrs: { col: "2", xl: "2", data: folder }
                                    },
                                    [
                                      _c(
                                        "div",
                                        {
                                          key: index,
                                          staticClass: "drive-box",
                                          attrs: {
                                            "data-path": folder.path,
                                            "data-name": folder.name,
                                            "data-type": folder.type
                                          },
                                          on: {
                                            dblclick: function($event) {
                                              return _vm.doubleClick(
                                                folder.link
                                              )
                                            }
                                          }
                                        },
                                        [
                                          _c(
                                            "div",
                                            { staticClass: "togle-btn" },
                                            [
                                              _c(
                                                "CDropdown",
                                                {
                                                  staticClass:
                                                    "mb-1 d-inline-block",
                                                  attrs: {
                                                    "toggler-text": "",
                                                    color: ""
                                                  }
                                                },
                                                [
                                                  folder.isFile == "true" &&
                                                  (folder.type == "jpg" ||
                                                    folder.type == "png" ||
                                                    folder.type == "jpeg" ||
                                                    folder.type == "gif" ||
                                                    folder.type == "tiff" ||
                                                    folder.type == "bmp")
                                                    ? _c(
                                                        "CDropdownItem",
                                                        {
                                                          on: {
                                                            click: function(
                                                              $event
                                                            ) {
                                                              return _vm.openGallery(
                                                                folder.keyVal
                                                              )
                                                            }
                                                          }
                                                        },
                                                        [_vm._v("Open")]
                                                      )
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  folder.isFolder == "true"
                                                    ? _c(
                                                        "CDropdownItem",
                                                        {
                                                          on: {
                                                            click: function(
                                                              $event
                                                            ) {
                                                              return _vm.doubleClick(
                                                                folder.link
                                                              )
                                                            }
                                                          }
                                                        },
                                                        [_vm._v("Open")]
                                                      )
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  folder.isFolder == "true"
                                                    ? _c(
                                                        "CDropdownItem",
                                                        {
                                                          on: {
                                                            click: function(
                                                              $event
                                                            ) {
                                                              return _vm.downloadZip(
                                                                folder.path,
                                                                folder.name,
                                                                folder.type
                                                              )
                                                            }
                                                          }
                                                        },
                                                        [_vm._v("Download")]
                                                      )
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  folder.isFile == "true"
                                                    ? _c(
                                                        "CDropdownItem",
                                                        {
                                                          on: {
                                                            click: function(
                                                              $event
                                                            ) {
                                                              return _vm.onClick(
                                                                folder.path,
                                                                folder.name,
                                                                folder.type
                                                              )
                                                            }
                                                          }
                                                        },
                                                        [_vm._v("Download")]
                                                      )
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  _vm.isAdmin == "true" ||
                                                  folder.user_id == _vm.Auth_id
                                                    ? _c(
                                                        "CDropdownItem",
                                                        {
                                                          on: {
                                                            click: function(
                                                              $event
                                                            ) {
                                                              return _vm.renameFolderModal(
                                                                folder.id,
                                                                folder.isFolder,
                                                                folder.name
                                                              )
                                                            }
                                                          }
                                                        },
                                                        [_vm._v("Rename")]
                                                      )
                                                    : _vm._e(),
                                                  _vm._v(" "),
                                                  _vm.isAdmin == "true" ||
                                                  folder.user_id == _vm.Auth_id
                                                    ? _c(
                                                        "CDropdownItem",
                                                        {
                                                          on: {
                                                            click: function(
                                                              $event
                                                            ) {
                                                              return _vm.deleteFolderOrFile(
                                                                folder.id
                                                              )
                                                            }
                                                          }
                                                        },
                                                        [_vm._v("Delete")]
                                                      )
                                                    : _vm._e()
                                                ],
                                                1
                                              )
                                            ],
                                            1
                                          ),
                                          _vm._v(" "),
                                          folder.isFolder == "true"
                                            ? _c("img", {
                                                attrs: {
                                                  src: _vm.filePath.folder
                                                }
                                              })
                                            : folder.isFile == "true" &&
                                              folder.type == "zip"
                                            ? _c("img", {
                                                attrs: { src: _vm.filePath.zip }
                                              })
                                            : folder.isFile == "true" &&
                                              folder.type == "docx"
                                            ? _c("img", {
                                                attrs: {
                                                  src: _vm.filePath.docx
                                                }
                                              })
                                            : folder.isFile == "true" &&
                                              folder.type == "pdf"
                                            ? _c("img", {
                                                attrs: { src: _vm.filePath.pdf }
                                              })
                                            : folder.isFile == "true" &&
                                              folder.type == "txt"
                                            ? _c("img", {
                                                attrs: { src: _vm.filePath.txt }
                                              })
                                            : folder.isFile == "true" &&
                                              folder.type == "psd"
                                            ? _c("img", {
                                                attrs: { src: _vm.filePath.psd }
                                              })
                                            : folder.type == "mp4"
                                            ? _c("img", {
                                                attrs: {
                                                  src: _vm.filePath.mp4
                                                },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.openGallery(
                                                      folder.keyVal
                                                    )
                                                  }
                                                }
                                              })
                                            : folder.isFile == "true" &&
                                              (folder.type == "jpg" ||
                                                folder.type == "png" ||
                                                folder.type == "jpeg" ||
                                                folder.type == "gif" ||
                                                folder.type == "tiff" ||
                                                folder.type == "bmp")
                                            ? _c("img", {
                                                attrs: { src: folder.path },
                                                on: {
                                                  click: function($event) {
                                                    return _vm.openGallery(
                                                      folder.keyVal
                                                    )
                                                  }
                                                }
                                              })
                                            : _c("img", {
                                                attrs: {
                                                  src: _vm.filePath.file
                                                }
                                              }),
                                          _vm._v(" "),
                                          _c(
                                            "p",
                                            {
                                              directives: [
                                                {
                                                  name: "c-tooltip",
                                                  rawName: "v-c-tooltip.hover",
                                                  value: {
                                                    content: "" + folder.name,
                                                    placement: _vm.placement
                                                  },
                                                  expression:
                                                    "{ content: `${folder.name}`, placement }",
                                                  modifiers: { hover: true }
                                                }
                                              ]
                                            },
                                            [_vm._v(_vm._s(folder.name))]
                                          )
                                        ]
                                      )
                                    ]
                                  )
                                }),
                                1
                              )
                            : _c("CRow", { staticClass: "no-file-folder" }, [
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "disk-folder-list-no-data-inner-message"
                                  },
                                  [
                                    _vm._v(
                                      "\n          There are no files or folders in this folder\n        "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  {
                                    staticClass:
                                      "disk-folder-list-no-data-inner-variable"
                                  },
                                  [
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "drive-folder-outer disk-folder-list-no-data-inner-create-file",
                                        attrs: {
                                          id:
                                            "disk-folder-list-no-data-upload-file"
                                        }
                                      },
                                      [
                                        _c("CIcon", {
                                          attrs: { name: "cil-file" }
                                        }),
                                        _vm._v("Create file\n\n            "),
                                        _c(
                                          "form",
                                          {
                                            staticClass:
                                              "formPostAjax file-outer"
                                          },
                                          [
                                            _c("input", {
                                              attrs: {
                                                type: "file",
                                                multiple: "multiple"
                                              },
                                              on: {
                                                change: _vm.handleFileUpload
                                              }
                                            })
                                          ]
                                        )
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "drive-folder-outer disk-folder-list-no-data-inner-create-folder",
                                        attrs: {
                                          id:
                                            "disk-folder-list-no-data-create-folder"
                                        },
                                        on: {
                                          click: function($event) {
                                            _vm.darkModal = true
                                          }
                                        }
                                      },
                                      [
                                        _c("CIcon", {
                                          attrs: { name: "cil-folder" }
                                        }),
                                        _vm._v("Create folder\n          ")
                                      ],
                                      1
                                    )
                                  ]
                                )
                              ])
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _vm.media.length
                ? _c("LightBox", {
                    ref: "lightbox",
                    attrs: {
                      media: _vm.media,
                      "show-caption": false,
                      "show-light-box": false,
                      "show-thumbs": false,
                      "show-footer-count": false
                    },
                    on: {
                      onClosed: _vm.closeModal,
                      onOpened: _vm.openModal,
                      onImageChanged: _vm.currentVal
                    }
                  })
                : _vm._e()
            ],
            1
          ),
          _vm._v(" "),
          _vm.downloadBtn == true
            ? _c("div", { staticClass: "download-file" }, [
                _c(
                  "a",
                  { attrs: { type: "button" } },
                  [
                    _c("CIcon", { attrs: { name: "cil-cloud-download" } }),
                    _vm._v(" Download")
                  ],
                  1
                )
              ])
            : _vm._e()
        ],
        1
      ),
      _vm._v(" "),
      _c("progressBar", {
        attrs: {
          uploadPercentage: _vm.percentComplete,
          progressUpgrade: _vm.progressUpgrade
        }
      }),
      _vm._v(" "),
      _c("DownloadProgressBar", {
        attrs: {
          downloadUploadPercentage: _vm.downloadPercentComplete,
          downloadProgressUpgrade: _vm.downloadProgressUpgrade,
          kbps: _vm.kbps
        }
      }),
      _vm._v(" "),
      _c(
        "CModal",
        {
          staticClass: "drive-folder",
          attrs: {
            show: _vm.darkModal,
            "no-close-on-backdrop": true,
            centered: true,
            title: "Modal title 2",
            size: "lg",
            color: "dark"
          },
          on: {
            "update:show": function($event) {
              _vm.darkModal = $event
            }
          },
          scopedSlots: _vm._u([
            {
              key: "header",
              fn: function() {
                return [
                  _c("h6", { staticClass: "modal-title" }, [
                    _vm._v("Create Folder")
                  ]),
                  _vm._v(" "),
                  _c("CButtonClose", {
                    staticClass: "text-white",
                    on: {
                      click: function($event) {
                        _vm.darkModal = false
                      }
                    }
                  })
                ]
              },
              proxy: true
            },
            {
              key: "footer",
              fn: function() {
                return [
                  _c(
                    "CButton",
                    {
                      attrs: { color: "primary" },
                      on: { click: _vm.createFolder }
                    },
                    [_vm._v("Create")]
                  ),
                  _vm._v(" "),
                  _c(
                    "CButton",
                    {
                      attrs: { color: "danger" },
                      on: {
                        click: function($event) {
                          _vm.darkModal = false
                        }
                      }
                    },
                    [_vm._v("Close")]
                  )
                ]
              },
              proxy: true
            }
          ])
        },
        [
          _c("form", { attrs: { id: "createFolder" } }, [
            _c(
              "label",
              { staticClass: "w-100" },
              [
                _c("span", [_vm._v("*")]),
                _vm._v("Name\n       "),
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.folderName,
                      expression: "folderName"
                    }
                  ],
                  staticClass: "form-control",
                  attrs: { type: "text" },
                  domProps: { value: _vm.folderName },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.folderName = $event.target.value
                    }
                  }
                }),
                _vm._v(" "),
                _vm.errors.folderName
                  ? _c("validation-error", {
                      attrs: { error: _vm.errors.folderName[0] }
                    })
                  : _vm._e()
              ],
              1
            ),
            _vm._v(" "),
            _c("input", {
              attrs: { type: "hidden", name: "isFolder", value: "true" }
            })
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "CModal",
        {
          staticClass: "no-zip-popup",
          attrs: { color: "danger", show: _vm.dangerModal },
          on: {
            "update:show": function($event) {
              _vm.dangerModal = $event
            }
          }
        },
        [_vm._v("\n   There are no files or folders!\n  ")]
      ),
      _vm._v(" "),
      _c(
        "CModal",
        {
          staticClass: "drive-upload",
          attrs: {
            title: "Rename Folder or File",
            color: "info",
            centered: true,
            show: _vm.renameModal
          },
          on: {
            "update:show": function($event) {
              _vm.renameModal = $event
            }
          },
          scopedSlots: _vm._u([
            {
              key: "footer",
              fn: function() {
                return [
                  _c(
                    "CButton",
                    {
                      attrs: { color: "secondary" },
                      on: { click: _vm.renameFolderOrFile }
                    },
                    [_vm._v("Save")]
                  )
                ]
              },
              proxy: true
            }
          ])
        },
        [
          _c("div", { staticClass: "rename-outer" }, [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.renameFolderName,
                  expression: "renameFolderName"
                }
              ],
              staticClass: "form-control",
              attrs: { type: "text" },
              domProps: { value: _vm.renameFolderName },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.renameFolderName = $event.target.value
                }
              }
            })
          ]),
          _vm._v(" "),
          _vm.errors.folderName
            ? _c("validation-error", {
                attrs: { error: _vm.errors.folderName[0] }
              })
            : _vm._e()
        ],
        1
      ),
      _vm._v(" "),
      _vm.is_loading
        ? _c("div", { staticClass: "loader-image" }, [
            _c("img", { attrs: { src: _vm.loader } })
          ])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);