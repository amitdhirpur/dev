(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[17],{

/***/ "../coreui/src/views/errors/validation-errors.vue":
/*!********************************************************!*\
  !*** ../coreui/src/views/errors/validation-errors.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _validation_errors_vue_vue_type_template_id_473119a5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./validation-errors.vue?vue&type=template&id=473119a5& */ "../coreui/src/views/errors/validation-errors.vue?vue&type=template&id=473119a5&");
/* harmony import */ var _validation_errors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./validation-errors.vue?vue&type=script&lang=js& */ "../coreui/src/views/errors/validation-errors.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _validation_errors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _validation_errors_vue_vue_type_template_id_473119a5___WEBPACK_IMPORTED_MODULE_0__["render"],
  _validation_errors_vue_vue_type_template_id_473119a5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/errors/validation-errors.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/errors/validation-errors.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ../coreui/src/views/errors/validation-errors.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./validation-errors.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/errors/validation-errors.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/errors/validation-errors.vue?vue&type=template&id=473119a5&":
/*!***************************************************************************************!*\
  !*** ../coreui/src/views/errors/validation-errors.vue?vue&type=template&id=473119a5& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_template_id_473119a5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./validation-errors.vue?vue&type=template&id=473119a5& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/errors/validation-errors.vue?vue&type=template&id=473119a5&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_template_id_473119a5___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_template_id_473119a5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "../coreui/src/views/projects/EditProject.vue":
/*!****************************************************!*\
  !*** ../coreui/src/views/projects/EditProject.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EditProject_vue_vue_type_template_id_2caa3104___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EditProject.vue?vue&type=template&id=2caa3104& */ "../coreui/src/views/projects/EditProject.vue?vue&type=template&id=2caa3104&");
/* harmony import */ var _EditProject_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EditProject.vue?vue&type=script&lang=js& */ "../coreui/src/views/projects/EditProject.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EditProject_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EditProject_vue_vue_type_template_id_2caa3104___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EditProject_vue_vue_type_template_id_2caa3104___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/projects/EditProject.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/projects/EditProject.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ../coreui/src/views/projects/EditProject.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_EditProject_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./EditProject.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/projects/EditProject.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_EditProject_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/projects/EditProject.vue?vue&type=template&id=2caa3104&":
/*!***********************************************************************************!*\
  !*** ../coreui/src/views/projects/EditProject.vue?vue&type=template&id=2caa3104& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_EditProject_vue_vue_type_template_id_2caa3104___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./EditProject.vue?vue&type=template&id=2caa3104& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/projects/EditProject.vue?vue&type=template&id=2caa3104&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_EditProject_vue_vue_type_template_id_2caa3104___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_EditProject_vue_vue_type_template_id_2caa3104___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/errors/validation-errors.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/errors/validation-errors.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    error: {
      type: [Number, String, Array, Object],
      "default": 'Unknown'
    }
  },
  data: function data() {
    return {
      test: 'helo'
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/projects/EditProject.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/projects/EditProject.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _errors_validation_errors_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/errors/validation-errors.vue */ "../coreui/src/views/errors/validation-errors.vue");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "../coreui/node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var vue2_editor__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue2-editor */ "../coreui/node_modules/vue2-editor/dist/vue2-editor.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'EditProject',
  props: {
    caption: {
      type: String,
      "default": 'Project id'
    }
  },
  data: function data() {
    return {
      name: '',
      client: '',
      url: '',
      portal: '',
      profile: '',
      description: '',
      loader: '/img/loader.gif',
      is_loading: false,
      access_detail: '',
      showMessage: false,
      message: '',
      dismissSecs: 7,
      dismissCountDown: 0,
      showDismissibleAlert: false
    };
  },
  components: {
    validationError: _errors_validation_errors_vue__WEBPACK_IMPORTED_MODULE_1__["default"],
    VueEditor: vue2_editor__WEBPACK_IMPORTED_MODULE_3__["VueEditor"]
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapGetters"])(["errors"])),
  methods: {
    validationError: function validationError() {
      var self = this;
      var error = {};

      if (!self.name) {
        error.name = [];
        error.name[0] = "This Field is required!";
      }

      if (!self.client) {
        error.client = [];
        error.client[0] = "This Field is required!";
      }

      if (!self.url) {
        error.url = [];
        error.url[0] = "This Field is required!";
      }

      if (!self.profile) {
        error.profile = [];
        error.profile[0] = "This Field is required!";
      }

      if (!self.portal) {
        error.portal = [];
        error.portal[0] = "This Field is required!";
      }

      if (Object.keys(error).length) {
        self.$store.commit("setErrors", error);
        return true;
      }

      self.$store.commit("setErrors", {});
    },
    goBack: function goBack() {
      this.$router.back();
    },
    update: function update() {
      var self = this;

      if (self.validationError()) {
        return;
      }

      this.is_loading = true;
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/projects/' + self.$route.params.id + '?token=' + localStorage.getItem("api_token"), {
        _method: 'PUT',
        name: self.name,
        client: self.client,
        url: self.url,
        portal: self.portal,
        profile: self.profile,
        description: self.description,
        access_detail: self.access_detail
      }).then(function (response) {
        self.is_loading = false;

        if (response) {
          self.message = 'Project updated Successfully.';
          self.showAlert();
        }

        self.$store.dispatch('topFunction');
      });
    },
    countDownChanged: function countDownChanged(dismissCountDown) {
      this.dismissCountDown = dismissCountDown;
    },
    showAlert: function showAlert() {
      this.dismissCountDown = this.dismissSecs;
    }
  },
  mounted: function mounted() {
    this.$store.commit("setErrors", {});
    var self = this;
    axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/projects/' + self.$route.params.id + '/edit?token=' + localStorage.getItem("api_token")).then(function (response) {
      self.name = response.data.name;
      self.client = response.data.client;
      self.url = response.data.url;
      self.portal = response.data.portal;
      self.profile = response.data.profile;
      self.description = response.data.description;
      self.access_detail = response.data.access_detail;
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/errors/validation-errors.vue?vue&type=template&id=473119a5&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/errors/validation-errors.vue?vue&type=template&id=473119a5& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "form-group", attrs: { role: "group" } }, [
    _vm.error
      ? _c("span", { staticClass: "error" }, [_vm._v(_vm._s(_vm.error))])
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/projects/EditProject.vue?vue&type=template&id=2caa3104&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/projects/EditProject.vue?vue&type=template&id=2caa3104& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", lg: "12" } },
        [
          _c(
            "CCard",
            { attrs: { "no-header": "" } },
            [
              _c(
                "CCardBody",
                [
                  _c(
                    "CForm",
                    [
                      _c("template", { slot: "header" }, [
                        _vm._v(
                          "\n            Edit Project id:  " +
                            _vm._s(_vm.$route.params.id) +
                            "\n          "
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "CAlert",
                        {
                          attrs: {
                            show: _vm.dismissCountDown,
                            color: "primary",
                            fade: ""
                          },
                          on: {
                            "update:show": function($event) {
                              _vm.dismissCountDown = $event
                            }
                          }
                        },
                        [
                          _vm._v(
                            "\n            (" +
                              _vm._s(_vm.dismissCountDown) +
                              ") " +
                              _vm._s(_vm.message) +
                              "\n          "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c("CInput", {
                        attrs: {
                          type: "text",
                          label: "Name *",
                          placeholder: "Name"
                        },
                        model: {
                          value: _vm.name,
                          callback: function($$v) {
                            _vm.name = $$v
                          },
                          expression: "name"
                        }
                      }),
                      _vm._v(" "),
                      _vm.errors.name
                        ? _c("validation-error", {
                            attrs: { error: _vm.errors.name[0] }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      _c("CInput", {
                        attrs: {
                          type: "text",
                          label: "Client *",
                          placeholder: "Client"
                        },
                        model: {
                          value: _vm.client,
                          callback: function($$v) {
                            _vm.client = $$v
                          },
                          expression: "client"
                        }
                      }),
                      _vm._v(" "),
                      _vm.errors.client
                        ? _c("validation-error", {
                            attrs: { error: _vm.errors.client[0] }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      _c("CInput", {
                        attrs: {
                          type: "url",
                          label: "Url *",
                          placeholder: "Url"
                        },
                        model: {
                          value: _vm.url,
                          callback: function($$v) {
                            _vm.url = $$v
                          },
                          expression: "url"
                        }
                      }),
                      _vm._v(" "),
                      _vm.errors.url
                        ? _c("validation-error", {
                            attrs: { error: _vm.errors.url[0] }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      _c("CInput", {
                        attrs: {
                          type: "text",
                          label: "Portal *",
                          placeholder: "Portal"
                        },
                        model: {
                          value: _vm.portal,
                          callback: function($$v) {
                            _vm.portal = $$v
                          },
                          expression: "portal"
                        }
                      }),
                      _vm._v(" "),
                      _vm.errors.portal
                        ? _c("validation-error", {
                            attrs: { error: _vm.errors.portal[0] }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      _c("CInput", {
                        attrs: {
                          type: "text",
                          label: "Profile *",
                          placeholder: "Profile"
                        },
                        model: {
                          value: _vm.profile,
                          callback: function($$v) {
                            _vm.profile = $$v
                          },
                          expression: "profile"
                        }
                      }),
                      _vm._v(" "),
                      _vm.errors.profile
                        ? _c("validation-error", {
                            attrs: { error: _vm.errors.profile[0] }
                          })
                        : _vm._e(),
                      _vm._v(" "),
                      _c("CTextarea", {
                        attrs: {
                          type: "text",
                          label: "Description",
                          rows: "50"
                        },
                        model: {
                          value: _vm.description,
                          callback: function($$v) {
                            _vm.description = $$v
                          },
                          expression: "description"
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        {
                          staticClass: "mt-3",
                          attrs: { color: "primary" },
                          on: {
                            click: function($event) {
                              return _vm.update()
                            }
                          }
                        },
                        [_vm._v("Save")]
                      ),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        {
                          staticClass: "mt-3",
                          attrs: { color: "primary" },
                          on: { click: _vm.goBack }
                        },
                        [_vm._v("Back")]
                      )
                    ],
                    2
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _vm.is_loading
        ? _c("div", { staticClass: "loader-image" }, [
            _c("img", { attrs: { src: _vm.loader } })
          ])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);