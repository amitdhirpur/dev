(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[34],{

/***/ "../coreui/src/views/logs/Logs.vue":
/*!*****************************************!*\
  !*** ../coreui/src/views/logs/Logs.vue ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Logs_vue_vue_type_template_id_0d606095___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Logs.vue?vue&type=template&id=0d606095& */ "../coreui/src/views/logs/Logs.vue?vue&type=template&id=0d606095&");
/* harmony import */ var _Logs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Logs.vue?vue&type=script&lang=js& */ "../coreui/src/views/logs/Logs.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Logs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Logs_vue_vue_type_template_id_0d606095___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Logs_vue_vue_type_template_id_0d606095___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/logs/Logs.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/logs/Logs.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ../coreui/src/views/logs/Logs.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Logs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Logs.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/logs/Logs.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Logs_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/logs/Logs.vue?vue&type=template&id=0d606095&":
/*!************************************************************************!*\
  !*** ../coreui/src/views/logs/Logs.vue?vue&type=template&id=0d606095& ***!
  \************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Logs_vue_vue_type_template_id_0d606095___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Logs.vue?vue&type=template&id=0d606095& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/logs/Logs.vue?vue&type=template&id=0d606095&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Logs_vue_vue_type_template_id_0d606095___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Logs_vue_vue_type_template_id_0d606095___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/logs/Logs.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/logs/Logs.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "../coreui/node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Logs',
  data: function data() {
    return {
      logs: [],
      fields: ['user', 'project', 'component', 'created_at'],
      currentPage: 1,
      perPage: 10,
      totalRows: 0,
      loader: '/img/loader.gif',
      is_loading: false,
      date_format: ''
    };
  },
  methods: {
    goBack: function goBack() {
      this.$router.back();
    },
    dateFormat: function dateFormat($date) {
      var value = "";

      if ($date) {
        value = moment__WEBPACK_IMPORTED_MODULE_1___default()($date).format(this.date_format);
      }

      return value;
    },
    getLogs: function getLogs() {
      var self = this;
      this.is_loading = true;
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/logs?token=' + localStorage.getItem("api_token")).then(function (response) {
        self.is_loading = false;
        self.logs = response.data;
      })["catch"](function (error) {
        self.is_loading = false;
        console.log(error);
        self.$router.push({
          path: '/login'
        });
      });
    },
    gotoProject: function gotoProject(_id) {
      var link = "/projects/".concat(_id);
      this.$router.push({
        path: link
      });
    }
  },
  mounted: function mounted() {
    this.getLogs();
    this.date_format = localStorage.getItem('date_format');
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/logs/Logs.vue?vue&type=template&id=0d606095&":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/logs/Logs.vue?vue&type=template&id=0d606095& ***!
  \******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", xl: "12" } },
        [
          _c(
            "transition",
            { attrs: { name: "slide" } },
            [
              _c(
                "CCard",
                [
                  _c("CCardHeader", [
                    _c(
                      "h4",
                      [
                        _vm._v("Logs "),
                        _c(
                          "CButton",
                          {
                            staticClass: "float-right",
                            attrs: { color: "secondary" },
                            on: { click: _vm.goBack }
                          },
                          [_vm._v("Back")]
                        )
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "CCardBody",
                    { staticClass: "logs-table" },
                    [
                      _c("CDataTable", {
                        attrs: {
                          hover: "",
                          striped: "",
                          items: _vm.logs,
                          fields: _vm.fields,
                          "items-per-page": 10,
                          pagination: "",
                          tableFilter: "",
                          sorter: ""
                        },
                        scopedSlots: _vm._u([
                          {
                            key: "user",
                            fn: function(ref) {
                              var item = ref.item
                              return [
                                _c("td", [
                                  _vm._v(
                                    "\n              " +
                                      _vm._s(item.user) +
                                      "\n            "
                                  )
                                ])
                              ]
                            }
                          },
                          {
                            key: "project",
                            fn: function(ref) {
                              var item = ref.item
                              return [
                                _c("td", [
                                  _c(
                                    "a",
                                    {
                                      on: {
                                        click: function($event) {
                                          return _vm.gotoProject(
                                            item.project_id
                                          )
                                        }
                                      }
                                    },
                                    [_vm._v(" " + _vm._s(item.project_name))]
                                  )
                                ])
                              ]
                            }
                          },
                          {
                            key: "component",
                            fn: function(ref) {
                              var item = ref.item
                              return [
                                _c("td", [
                                  _vm._v(
                                    "\n              " +
                                      _vm._s(item.component) +
                                      "\n            "
                                  )
                                ])
                              ]
                            }
                          },
                          {
                            key: "created_at",
                            fn: function(ref) {
                              var item = ref.item
                              return [
                                _c("td", [
                                  _vm._v(
                                    "\n              " +
                                      _vm._s(_vm.dateFormat(item.created_at)) +
                                      "\n            "
                                  )
                                ])
                              ]
                            }
                          }
                        ])
                      })
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _vm.is_loading
        ? _c("div", { staticClass: "loader-image" }, [
            _c("img", { attrs: { src: _vm.loader } })
          ])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);