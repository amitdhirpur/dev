(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[57],{

/***/ "../coreui/src/views/tasks/Tasks.vue":
/*!*******************************************!*\
  !*** ../coreui/src/views/tasks/Tasks.vue ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Tasks_vue_vue_type_template_id_5c4f74da___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Tasks.vue?vue&type=template&id=5c4f74da& */ "../coreui/src/views/tasks/Tasks.vue?vue&type=template&id=5c4f74da&");
/* harmony import */ var _Tasks_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Tasks.vue?vue&type=script&lang=js& */ "../coreui/src/views/tasks/Tasks.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var vue_multiselect_dist_vue_multiselect_min_css_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-multiselect/dist/vue-multiselect.min.css?vue&type=style&index=0&lang=css& */ "../coreui/node_modules/vue-multiselect/dist/vue-multiselect.min.css?vue&type=style&index=0&lang=css&");
/* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Tasks_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Tasks_vue_vue_type_template_id_5c4f74da___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Tasks_vue_vue_type_template_id_5c4f74da___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/tasks/Tasks.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/tasks/Tasks.vue?vue&type=script&lang=js&":
/*!********************************************************************!*\
  !*** ../coreui/src/views/tasks/Tasks.vue?vue&type=script&lang=js& ***!
  \********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Tasks_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Tasks.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/tasks/Tasks.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Tasks_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/tasks/Tasks.vue?vue&type=template&id=5c4f74da&":
/*!**************************************************************************!*\
  !*** ../coreui/src/views/tasks/Tasks.vue?vue&type=template&id=5c4f74da& ***!
  \**************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Tasks_vue_vue_type_template_id_5c4f74da___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Tasks.vue?vue&type=template&id=5c4f74da& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/tasks/Tasks.vue?vue&type=template&id=5c4f74da&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Tasks_vue_vue_type_template_id_5c4f74da___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Tasks_vue_vue_type_template_id_5c4f74da___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/tasks/Tasks.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/tasks/Tasks.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "../coreui/node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vue_multiselect__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-multiselect */ "../coreui/node_modules/vue-multiselect/dist/vue-multiselect.min.js");
/* harmony import */ var vue_multiselect__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vue_multiselect__WEBPACK_IMPORTED_MODULE_2__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Tasks',
  data: function data() {
    return {
      tasks: [],
      listing: [],
      date_format: '',
      users: [],
      is_admin: false,
      current_user: '',
      currentPage: 1,
      perPage: 10,
      message: '',
      projectName: '',
      dismissSecs: 7,
      dismissCountDown: 0,
      showDismissibleAlert: false,
      filterPriority: '',
      filterStatus: '',
      filterUsers: '',
      bulkAction: '',
      checked: [],
      loader: '/img/loader.gif',
      is_loading: false
    };
  },
  components: {
    Multiselect: vue_multiselect__WEBPACK_IMPORTED_MODULE_2___default.a
  },
  methods: {
    getParams: function getParams() {
      var queryString = window.location.search,
          urlParams = new URLSearchParams(queryString);
      return urlParams;
    },
    checked_active: function checked_active(fld) {
      var val = [fld.value];

      if (fld.checked) {
        this.checked.push(fld.value);
      } else {
        this.checked = this.checked.filter(function (item) {
          return !val.includes(item);
        });
      }

      this.bulkAction = '';

      if (this.checked.length > 0) {
        document.getElementById('bulkAction').removeAttribute('disabled');
      } else {
        document.getElementById('bulkAction').setAttribute('disabled', '');
      }
    },
    getProjName: function getProjName(id) {
      var that = this;
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/tasks/project-name/' + id + '?token=' + localStorage.getItem("api_token")).then(function (response) {
        that.projectName = response.data;
      });
    },
    getUsers: function getUsers() {
      var that = this;
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/report-users?token=' + localStorage.getItem("api_token")).then(function (response) {
        that.users = response.data;
      });
    },
    getTasks: function getTasks() {
      var that = this; // list all the tasks according to the user authorizations.

      var urlParams = this.getParams();
      var url = '/api/tasks?token=' + localStorage.getItem("api_token");

      if (urlParams.get('project')) {
        url = url + '&project=' + atob(urlParams.get('project'));
        this.getProjName(atob(urlParams.get('project')));
      }

      if (urlParams.get('type')) {
        url = url + '&type=' + atob(urlParams.get('type'));
      } // filter data properties.


      if (this.filterPriority) {
        url = url + '&priority=' + this.filterPriority;
      }

      if (this.filterStatus) {
        url = url + '&status=' + this.filterStatus;
      }

      if (this.filterUsers) {
        url = url + '&users=' + this.filterUsers;
      }

      jQuery('#tasks_table').DataTable({
        "lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
        destroy: true,
        processing: true,
        "order": [],
        serverSide: true,
        ajax: url,
        columns: [// {data: 'DT_RowIndex' },
        {
          data: 'sno',
          name: 'sno',
          render: function render(data, type, full, meta) {
            return '<div role="group" class="form-group"><input type="checkbox" class="check-task" value="' + full.id + '"></div>';
          },
          orderable: false,
          searchable: false
        }, {
          data: 'name',
          name: 'name',
          render: function render(data, type, full, meta) {
            var _cls = 'secondary';

            if (full.priority == '1') {
              _cls = 'warning';
            } else if (full.priority == '2') {
              _cls = 'danger';
            }

            var btn = '<div class="first-element"><span class="badge badge-' + _cls + '"><span class="ele-val">1</span></span>';
            btn = btn + '<a data="' + full.id + '" class="name-task">' + full.name + '</a>';

            if (full.status == '1') {
              btn = btn + '<a class="text-success"> <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" role="img" class="c-icon" data-v-6a0024b2=""><path d="M9.392 17.919l-6.53-6.122 1.026-1.094 5.47 5.128 10.736-10.736 1.061 1.061-11.764 11.764z"></path></svg></a>';
            }

            btn = btn + '</div>';
            return btn;
          }
        }, {
          data: 'created_at',
          name: 'created_at',
          render: function render(data, type, full, meta) {
            return that.dateFormat(full.created_at);
          }
        }, {
          data: 'deadline',
          name: 'deadline',
          render: function render(data, type, full, meta) {
            if (full.deadline) {
              return that.dateFormat(full.deadline);
            } else {
              return '';
            }
          }
        }, {
          data: 'created_by'
        }, {
          data: 'responsible_person'
        }, {
          data: 'project'
        }, {
          data: 'action',
          name: 'action',
          render: function render(data, type, full, meta) {
            var btns = '';

            if (full.deleted_at) {
              btns = '<b>Trashed</b>';
            } else {
              var _inner = '<a data="' + full.id + '" class="view-item-icon dropdown-item"><img src="/img/view-icon.png" alt="view-icn"/> View</a>';

              if (that.shouldDisplay(full)) {
                _inner = _inner + '<a data="' + full.id + '" class="edit-item-icon dropdown-item text-primary"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" role="img" class="c-icon"><path d="M22.029 1.971c-0.754-0.754-1.795-1.22-2.944-1.22s-2.191 0.466-2.944 1.22l-12.773 12.773-2.258 6.657c-0.040 0.113-0.063 0.243-0.063 0.378 0 0.323 0.132 0.616 0.344 0.827l0.004 0.004c0.211 0.212 0.503 0.344 0.826 0.344h0c0.136-0 0.266-0.023 0.388-0.066l-0.008 0.003 6.657-2.258 12.773-12.773c0.754-0.754 1.22-1.795 1.22-2.944s-0.466-2.191-1.22-2.944v0zM8.443 19.325l-5.702 1.934 1.934-5.702 9.785-9.785 3.767 3.767zM20.969 6.799l-1.68 1.68-3.767-3.767 1.68-1.68c0.482-0.483 1.149-0.783 1.886-0.783 1.471 0 2.664 1.193 2.664 2.664 0 0.737-0.299 1.404-0.783 1.886l-0 0z"></path></svg> Edit</a>';
                _inner = _inner + '<a data="' + full.id + '" class="delete-item-icon dropdown-item text-danger"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" role="img" class="c-icon"><path d="M4.5 22.125c-0 0.003-0 0.006-0 0.008 0 0.613 0.494 1.11 1.105 1.116h12.79c0.612-0.006 1.105-0.504 1.105-1.117 0-0.003-0-0.006-0-0.009v0-15h-15zM6 8.625h12v13.125h-12z"></path><path d="M7.875 10.125h1.5v9.375h-1.5v-9.375z"></path><path d="M11.25 10.125h1.5v9.375h-1.5v-9.375z"></path><path d="M14.625 10.125h1.5v9.375h-1.5v-9.375z"></path><path d="M15.375 4.125v-2.25c0-0.631-0.445-1.125-1.013-1.125h-4.725c-0.568 0-1.013 0.494-1.013 1.125v2.25h-5.625v1.5h18v-1.5zM10.125 2.25h3.75v1.875h-3.75z"></path></svg> Delete</a> ';
              }

              _inner = _inner + '<a data="' + full.id + '" class="driver-item-icon dropdown-item text-info"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" role="img" class="c-icon"><path d="M22.125 22.125h-20.25c-0.621-0.001-1.124-0.504-1.125-1.125v-18c0.001-0.621 0.504-1.124 1.125-1.125h8.75c0.001 0 0.001 0 0.002 0 0.465 0 0.863 0.282 1.034 0.685l0.003 0.007 0.961 2.308h9.5c0.621 0.001 1.124 0.504 1.125 1.125v15c-0.001 0.621-0.504 1.124-1.125 1.125h-0zM2.25 20.625h19.5v-14.25h-10.125l-1.25-3h-8.125z"></path></svg> Drive</a>';
              btns = '<div class="custom-list-dropdown"><div class="dropdown"><p class="dropdown-toggle pointer text-center" id="listDroper' + full.id + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="/img/drop-icon.svg" /></p><div class="dropdown-menu" aria-labelledby="listDroper' + full.id + '">' + _inner + '</div></div></div>';
            }

            return btns;
          },
          orderable: false,
          searchable: false
        }],
        "language": {
          "processing": '<div class="loader-image"><img src="' + that.loader + '"></div>'
        },
        "dom": '<"top table-search-flds d-flex align-items-center justify-content-between"fl>rt<"bottom table-paginater"ip><"clear">'
      }); // trigger click functions.

      jQuery(document).on('change', '.check-task', function () {
        that.checked_active(this);
      });
      jQuery(document).on('click', '.view-item-icon, .name-task', function () {
        var id = this.getAttribute('data');
        that.viewTask(id);
      });
      jQuery(document).on('click', '.edit-item-icon', function () {
        var id = this.getAttribute('data');
        that.editTask(id);
      });
      jQuery(document).on('click', '.delete-item-icon', function () {
        var id = this.getAttribute('data');
        that.deleteTask(id);
      });
      jQuery(document).on('click', '.driver-item-icon', function () {
        var id = this.getAttribute('data');
        that.taskDrive(id);
      });
      jQuery(document).on('click', '.redirect-project', function () {
        var id = this.getAttribute('data-id');
        that.redirectProject(id);
      });
    },
    changeColor: function changeColor(value) {
      // change color of span.
      var color = "secondary";

      if (value == '1') {
        color = 'warning';
      } else if (value == '2') {
        color = 'danger';
      }

      return color;
    },
    shouldDisplay: function shouldDisplay(item) {
      return this.is_admin == "true" || JSON.parse(this.current_user) == item.user;
    },
    showAlert: function showAlert() {
      this.dismissCountDown = this.dismissSecs;
    },
    addTask: function addTask() {
      var _path = "/tasks/add";
      var _projectQuery = this.$route.query.project;

      if (_projectQuery) {
        _path += "?project=" + _projectQuery;
      }

      this.$router.push({
        path: _path
      });
    },
    dateFormat: function dateFormat($date) {
      // set date format.
      return moment__WEBPACK_IMPORTED_MODULE_1___default()($date).format(this.date_format);
    },
    goToProject: function goToProject() {
      var urlParams = this.getParams();

      var _id = atob(urlParams.get('project')),
          link = "/projects/".concat(_id);

      this.$router.push({
        path: link
      });
    },
    viewTask: function viewTask(id) {
      // push url to see task details
      var _id = btoa(id.toString());

      var link = "/tasks/".concat(_id);
      this.$router.push({
        path: link
      });
    },
    editTask: function editTask(id) {
      // push url to edit task.
      var _id = btoa(id.toString());

      var link = "/tasks/".concat(_id, "/edit");
      this.$router.push({
        path: link
      });
    },
    bulkActions: function bulkActions() {
      var that = this;
      var action = 'Tasks are ' + event.target.selectedOptions[0].innerText;

      if (this.bulkAction) {
        var formdata = new FormData();
        formdata.append('ids', this.checked);
        formdata.append('action', this.bulkAction);
        axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/tasks/bulk-action?token=' + localStorage.getItem("api_token"), formdata).then(function (response) {
          that.checked = [];
          that.bulkAction = '';
          document.getElementById('bulkAction').setAttribute('disabled', '');
          jQuery('#tasks_table').DataTable().destroy();
          that.getTasks();

          if (response.data.status == 'warning') {
            that.message = "You don't have permissions to delete some tasks.";
          } else {
            that.message = action;
          }

          that.showAlert();
        });
      }
    },
    deleteTask: function deleteTask(id) {
      var _this = this;

      // delete task from table.
      var that = this;
      this.$swal.fire({
        title: 'Are you sure?',
        text: 'You can\'t revert your action',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
        showCloseButton: true,
        showLoaderOnConfirm: true
      }).then(function (result) {
        if (result.value) {
          axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/tasks/delete/' + id + '?token=' + localStorage.getItem("api_token")).then(function (response) {
            jQuery('#tasks_table').DataTable().destroy();
            that.getTasks();
            that.message = 'Task Moved To Trash.';
            that.showAlert();
            that.$swal('Deleted', 'Your task is added to trash.', 'success');
          })["catch"](function (error) {
            that.message = 'Something went wrong.';
            that.showAlert();
          });
        } else {
          _this.$swal('Cancelled', 'Your task is still intact.', 'info');
        }
      });
    },
    filterData: function filterData() {
      if (this.filterUsers || this.filterUsers == '') {
        localStorage.setItem('taskfilUser', this.filterUsers);
      }

      if (this.filterPriority || this.filterPriority == '') {
        localStorage.setItem('taskfilPriority', this.filterPriority);
      }

      if (this.filterStatus || this.filterStatus == '') {
        localStorage.setItem('taskfilStatus', this.filterStatus);
      }

      jQuery('#tasks_table').DataTable().destroy();
      this.getTasks();
    },
    resetFilter: function resetFilter() {
      this.filterPriority = '', this.filterStatus = '', this.filterUsers = '';
      localStorage.removeItem('taskfilUser');
      localStorage.removeItem('taskfilPriority');
      localStorage.removeItem('taskfilStatus');
      jQuery('#tasks_table').DataTable().destroy();
      this.getTasks();
    },
    taskDrive: function taskDrive(driveId) {
      var _id = btoa(driveId.toString());

      var link = "/drives/".concat(_id);
      this.$router.push({
        path: link
      });
    },
    redirectProject: function redirectProject(_id) {
      var link = "/projects/".concat(_id);
      this.$router.push({
        path: link
      });
    }
  },
  mounted: function mounted() {
    if (localStorage.getItem("taskfilPriority")) {
      this.filterPriority = localStorage.getItem("taskfilPriority");
    }

    if (localStorage.getItem("taskfilStatus")) {
      this.filterStatus = localStorage.getItem("taskfilStatus");
    }

    if (localStorage.getItem("taskfilUser")) {
      this.filterUsers = localStorage.getItem("taskfilUser");
    }

    this.is_admin = localStorage.getItem("type");
    this.current_user = localStorage.getItem("user");
    this.date_format = localStorage.getItem("date_format");
    console.log(localStorage.getItem("api_token"));

    if (localStorage.getItem("api_token")) {
      this.getTasks();
      this.getUsers();
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/tasks/Tasks.vue?vue&type=template&id=5c4f74da&":
/*!********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/tasks/Tasks.vue?vue&type=template&id=5c4f74da& ***!
  \********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", xl: "12" } },
        [
          _c(
            "transition",
            { attrs: { name: "slide" } },
            [
              _c(
                "CCard",
                [
                  _c(
                    "CCardBody",
                    [
                      _c(
                        "h4",
                        { staticClass: "main-title-task" },
                        [
                          _c("span", [_vm._v("Tasks")]),
                          _vm.projectName
                            ? _c(
                                "span",
                                {
                                  staticClass: "project-name pointer",
                                  on: { click: _vm.goToProject }
                                },
                                [
                                  _vm._v(
                                    "(Project: " +
                                      _vm._s(this.projectName) +
                                      ")"
                                  )
                                ]
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _c(
                            "CButton",
                            {
                              staticClass: "float-right",
                              attrs: { color: "primary" },
                              on: {
                                click: function($event) {
                                  return _vm.addTask()
                                }
                              }
                            },
                            [
                              _c("CIcon", { attrs: { name: "cil-plus" } }),
                              _vm._v(" Add Task")
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "CAlert",
                        {
                          attrs: {
                            show: _vm.dismissCountDown,
                            color: "success",
                            fade: ""
                          },
                          on: {
                            "update:show": function($event) {
                              _vm.dismissCountDown = $event
                            }
                          }
                        },
                        [
                          _vm._v(
                            "\n\t\t\t              \t" +
                              _vm._s(_vm.message) +
                              "\n\t\t\t            "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "CRow",
                        { staticClass: "table-filters mb-3" },
                        [
                          _vm.is_admin == "true"
                            ? _c("CCol", [
                                _c(
                                  "select",
                                  {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.filterUsers,
                                        expression: "filterUsers"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    on: {
                                      change: [
                                        function($event) {
                                          var $$selectedVal = Array.prototype.filter
                                            .call(
                                              $event.target.options,
                                              function(o) {
                                                return o.selected
                                              }
                                            )
                                            .map(function(o) {
                                              var val =
                                                "_value" in o
                                                  ? o._value
                                                  : o.value
                                              return val
                                            })
                                          _vm.filterUsers = $event.target
                                            .multiple
                                            ? $$selectedVal
                                            : $$selectedVal[0]
                                        },
                                        _vm.filterData
                                      ]
                                    }
                                  },
                                  [
                                    _c("option", { attrs: { value: "" } }, [
                                      _vm._v("--Select User --")
                                    ]),
                                    _vm._v(" "),
                                    _vm._l(_vm.users, function(user) {
                                      return _c(
                                        "option",
                                        { domProps: { value: user.id } },
                                        [_vm._v(_vm._s(user.name))]
                                      )
                                    })
                                  ],
                                  2
                                )
                              ])
                            : _vm._e(),
                          _vm._v(" "),
                          _c("CCol", [
                            _c(
                              "select",
                              {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.filterPriority,
                                    expression: "filterPriority"
                                  }
                                ],
                                staticClass: "form-control",
                                on: {
                                  change: [
                                    function($event) {
                                      var $$selectedVal = Array.prototype.filter
                                        .call($event.target.options, function(
                                          o
                                        ) {
                                          return o.selected
                                        })
                                        .map(function(o) {
                                          var val =
                                            "_value" in o ? o._value : o.value
                                          return val
                                        })
                                      _vm.filterPriority = $event.target
                                        .multiple
                                        ? $$selectedVal
                                        : $$selectedVal[0]
                                    },
                                    _vm.filterData
                                  ]
                                }
                              },
                              [
                                _c("option", { attrs: { value: "" } }, [
                                  _vm._v("--Select Priority --")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "0" } }, [
                                  _vm._v("Low")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "1" } }, [
                                  _vm._v("Medium")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "2" } }, [
                                  _vm._v("High")
                                ])
                              ]
                            )
                          ]),
                          _vm._v(" "),
                          _c("CCol", [
                            _c(
                              "select",
                              {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.filterStatus,
                                    expression: "filterStatus"
                                  }
                                ],
                                staticClass: "form-control",
                                on: {
                                  change: [
                                    function($event) {
                                      var $$selectedVal = Array.prototype.filter
                                        .call($event.target.options, function(
                                          o
                                        ) {
                                          return o.selected
                                        })
                                        .map(function(o) {
                                          var val =
                                            "_value" in o ? o._value : o.value
                                          return val
                                        })
                                      _vm.filterStatus = $event.target.multiple
                                        ? $$selectedVal
                                        : $$selectedVal[0]
                                    },
                                    _vm.filterData
                                  ]
                                }
                              },
                              [
                                _c("option", { attrs: { value: "" } }, [
                                  _vm._v("--Select Status --")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "0" } }, [
                                  _vm._v("Pending")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "1" } }, [
                                  _vm._v("Complete")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "2" } }, [
                                  _vm._v("On Going")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "3" } }, [
                                  _vm._v("Due")
                                ])
                              ]
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "CButton",
                            {
                              staticClass: "float-right",
                              attrs: { color: "primary" },
                              on: {
                                click: function($event) {
                                  return _vm.resetFilter()
                                }
                              }
                            },
                            [_vm._v("Reset")]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "table-responsive cust-tesks-table" },
                        [
                          _c(
                            "table",
                            {
                              staticClass:
                                "table table-manage-user table-striped",
                              attrs: { id: "tasks_table" }
                            },
                            [
                              _c("thead", [
                                _c("tr", [
                                  _c("th", { attrs: { scope: "col" } }, [
                                    _c("div", [_vm._v("S.No")])
                                  ]),
                                  _vm._v(" "),
                                  _c("th", { attrs: { scope: "col" } }, [
                                    _vm._v("Name")
                                  ]),
                                  _vm._v(" "),
                                  _c("th", { attrs: { scope: "col" } }, [
                                    _vm._v("Created On")
                                  ]),
                                  _vm._v(" "),
                                  _c("th", { attrs: { scope: "col" } }, [
                                    _vm._v("Deadline")
                                  ]),
                                  _vm._v(" "),
                                  _c("th", { attrs: { scope: "col" } }, [
                                    _vm._v("Created By")
                                  ]),
                                  _vm._v(" "),
                                  _c("th", { attrs: { scope: "col" } }, [
                                    _vm._v("Responsible Person")
                                  ]),
                                  _vm._v(" "),
                                  _c("th", { attrs: { scope: "col" } }, [
                                    _vm._v("Project")
                                  ]),
                                  _vm._v(" "),
                                  _c("th", { attrs: { scope: "col" } }, [
                                    _vm._v("Action")
                                  ])
                                ])
                              ]),
                              _vm._v(" "),
                              _c("tbody")
                            ]
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "bottom-row-filter" },
                        [
                          _c(
                            "CRow",
                            [
                              _c("CCol", { attrs: { col: "5" } }, [
                                _c(
                                  "select",
                                  {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.bulkAction,
                                        expression: "bulkAction"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: { id: "bulkAction", disabled: "" },
                                    on: {
                                      change: [
                                        function($event) {
                                          var $$selectedVal = Array.prototype.filter
                                            .call(
                                              $event.target.options,
                                              function(o) {
                                                return o.selected
                                              }
                                            )
                                            .map(function(o) {
                                              var val =
                                                "_value" in o
                                                  ? o._value
                                                  : o.value
                                              return val
                                            })
                                          _vm.bulkAction = $event.target
                                            .multiple
                                            ? $$selectedVal
                                            : $$selectedVal[0]
                                        },
                                        _vm.bulkActions
                                      ]
                                    }
                                  },
                                  [
                                    _c("option", { attrs: { value: "" } }, [
                                      _vm._v("--Select Status --")
                                    ]),
                                    _vm._v(" "),
                                    _c("option", { attrs: { value: "0" } }, [
                                      _vm._v("Pending")
                                    ]),
                                    _vm._v(" "),
                                    _c("option", { attrs: { value: "1" } }, [
                                      _vm._v("Complete")
                                    ]),
                                    _vm._v(" "),
                                    _c("option", { attrs: { value: "2" } }, [
                                      _vm._v("On Going")
                                    ]),
                                    _vm._v(" "),
                                    _c("option", { attrs: { value: "3" } }, [
                                      _vm._v("Due")
                                    ]),
                                    _vm._v(" "),
                                    _c(
                                      "option",
                                      { attrs: { value: "delete" } },
                                      [_vm._v("Delete")]
                                    )
                                  ]
                                )
                              ])
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);