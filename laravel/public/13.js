(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[13],{

/***/ "../coreui/src/views/employees/validation-errors.vue":
/*!***********************************************************!*\
  !*** ../coreui/src/views/employees/validation-errors.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _validation_errors_vue_vue_type_template_id_2961bf1b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./validation-errors.vue?vue&type=template&id=2961bf1b& */ "../coreui/src/views/employees/validation-errors.vue?vue&type=template&id=2961bf1b&");
/* harmony import */ var _validation_errors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./validation-errors.vue?vue&type=script&lang=js& */ "../coreui/src/views/employees/validation-errors.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _validation_errors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _validation_errors_vue_vue_type_template_id_2961bf1b___WEBPACK_IMPORTED_MODULE_0__["render"],
  _validation_errors_vue_vue_type_template_id_2961bf1b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/employees/validation-errors.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/employees/validation-errors.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ../coreui/src/views/employees/validation-errors.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./validation-errors.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/validation-errors.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/employees/validation-errors.vue?vue&type=template&id=2961bf1b&":
/*!******************************************************************************************!*\
  !*** ../coreui/src/views/employees/validation-errors.vue?vue&type=template&id=2961bf1b& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_template_id_2961bf1b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./validation-errors.vue?vue&type=template&id=2961bf1b& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/validation-errors.vue?vue&type=template&id=2961bf1b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_template_id_2961bf1b___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_template_id_2961bf1b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "../coreui/src/views/user/User.vue":
/*!*****************************************!*\
  !*** ../coreui/src/views/user/User.vue ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _User_vue_vue_type_template_id_3089dbd6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./User.vue?vue&type=template&id=3089dbd6& */ "../coreui/src/views/user/User.vue?vue&type=template&id=3089dbd6&");
/* harmony import */ var _User_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./User.vue?vue&type=script&lang=js& */ "../coreui/src/views/user/User.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _User_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _User_vue_vue_type_template_id_3089dbd6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _User_vue_vue_type_template_id_3089dbd6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/user/User.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/user/User.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ../coreui/src/views/user/User.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_User_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./User.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/user/User.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_User_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/user/User.vue?vue&type=template&id=3089dbd6&":
/*!************************************************************************!*\
  !*** ../coreui/src/views/user/User.vue?vue&type=template&id=3089dbd6& ***!
  \************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_User_vue_vue_type_template_id_3089dbd6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./User.vue?vue&type=template&id=3089dbd6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/user/User.vue?vue&type=template&id=3089dbd6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_User_vue_vue_type_template_id_3089dbd6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_User_vue_vue_type_template_id_3089dbd6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/validation-errors.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/employees/validation-errors.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    error: {
      type: [Number, String, Array, Object],
      "default": 'Unknown'
    }
  },
  data: function data() {
    return {
      test: 'helo'
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/user/User.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/user/User.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _employees_validation_errors_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/employees/validation-errors.vue */ "../coreui/src/views/employees/validation-errors.vue");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "../coreui/node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! devextreme-vue/html-editor */ "../coreui/node_modules/devextreme-vue/html-editor.js");
/* harmony import */ var devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_3__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'EditUser',
  props: {
    caption: {
      type: String,
      "default": 'User id'
    }
  },
  data: function data() {
    var _ref;

    return _ref = {
      tabs: ['Calculator', 'Shopping cart', 'Charts'],
      activeTab: 1,
      name: '',
      email: '',
      contact_number: '',
      alternate_number: '',
      additional_info: '',
      position: '',
      department: '',
      dof: '',
      profile: '',
      avtar: '/img/user-avtar.jpg',
      showMessage: false,
      message: '',
      dismissSecs: 7,
      dismissCountDown: 0,
      showDismissibleAlert: false,
      current_password: '',
      new_password: '',
      new_confirm_password: ''
    }, _defineProperty(_ref, "message", ''), _defineProperty(_ref, "current_address", ''), _defineProperty(_ref, "permanent_address", ''), _defineProperty(_ref, "showPermanentAddress", false), _ref;
  },
  components: {
    validationError: _employees_validation_errors_vue__WEBPACK_IMPORTED_MODULE_1__["default"],
    DxHtmlEditor: devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_3__["DxHtmlEditor"]
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapGetters"])(["errors"])),
  methods: {
    goBack: function goBack() {
      this.$router.go(-1); // this.$router.replace({path: '/users'})
    },
    validationError: function validationError() {
      var self = this;
      var error = {};

      if (!self.current_password) {
        error.current_password = [];
        error.current_password[0] = "This Field is required!";
      }

      if (!self.new_password) {
        error.new_password = [];
        error.new_password[0] = "This Field is required!";
      }

      if (!self.new_confirm_password) {
        error.new_confirm_password = [];
        error.new_confirm_password[0] = "This Field is required!";
      }

      if (Object.keys(error).length) {
        self.$store.commit("setErrors", error);
        return true;
      }

      self.$store.commit("setErrors", {});
      return false;
    },
    profileError: function profileError() {
      var self = this;
      var error = {};

      if (!self.name) {
        error.name = [];
        error.name[0] = "This Field is required!";
      }

      if (!self.email) {
        error.email = [];
        error.email[0] = "This Field is required!";
      }

      if (!self.contact_number) {
        error.contact_number = [];
        error.contact_number[0] = "This Field is required!";
      }

      if (!self.dof) {
        error.dof = [];
        error.dof[0] = "This Field is required!";
      }

      if (Object.keys(error).length) {
        self.$store.commit("setErrors", error);
        return true;
      }

      self.$store.commit("setErrors", {});
      return false;
    },
    update: function update() {
      var self = this;

      if (self.profileError()) {
        return;
      }

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/user/profile/' + self.$route.params.id + '/update?token=' + localStorage.getItem("api_token"), {
        name: self.name,
        email: self.email,
        contact_no: self.contact_number,
        alternate_no: self.alternate_number,
        birth_date: self.dof,
        additional_info: self.additional_info,
        current_address: self.current_address,
        permanent_address: self.permanent_address
      }).then(function (response) {
        self.message = 'Successfully updated Profile Information.';
        self.showAlert();
        self.$store.commit('setUser', response.data);
        self.$store.dispatch('topFunction');
      });
    },
    updatePassword: function updatePassword() {
      var self = this;

      if (self.validationError()) {
        return;
      }

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/user/password/' + self.$route.params.id + '/update?token=' + localStorage.getItem("api_token"), {
        current_password: self.current_password,
        new_password: self.new_password,
        new_confirm_password: self.new_confirm_password
      }).then(function (response) {
        if (response) {
          self.message = 'Successfully updated password.';
          self.showAlert();
        }
      });
    },
    countDownChanged: function countDownChanged(dismissCountDown) {
      this.dismissCountDown = dismissCountDown;
    },
    showAlert: function showAlert() {
      this.dismissCountDown = this.dismissSecs;
    },
    selectFile: function selectFile(event) {
      var _this = this;

      var reader = new FileReader();
      var file = event.target.files[0];

      reader.onloadend = function () {
        _this.profile = reader.result;
      };

      reader.readAsDataURL(file);
      this.image = event.target.files[0];
    },
    formSubmit: function formSubmit(e) {
      e.preventDefault();
      var self = this;
      var config = {
        headers: {
          'content-type': 'multipart/form-data'
        }
      };
      var formData = new FormData();
      formData.append('file', self.image);
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/users/' + self.$route.params.id + '/save?token=' + localStorage.getItem("api_token"), formData, config).then(function (response) {
        self.message = 'Successfully updated Profile Pic.';
        self.showAlert();
        self.$store.commit('setUser', response.data);
      })["catch"](function (error) {
        currentObj.output = error;
      });
    }
  },
  mounted: function mounted() {
    var self = this;
    self.$store.commit("setErrors", {});
    axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/user?token=' + localStorage.getItem("api_token")).then(function (response) {
      self.name = response.data.user.name;
      self.email = response.data.user.email;
      self.contact_number = response.data.user.contact_no;
      self.profile = response.data.user.profile;
      self.alternate_number = response.data.user.alternate_no;
      self.position = response.data.user.position;
      self.department = response.data.user.department;
      self.additional_info = response.data.user.additional_info;
      self.current_address = response.data.user.current_address;
      self.permanent_address = response.data.user.permanent_address;
      self.dof = response.data.user.birth_date.split(' ')[0];

      if (response.data.user.roles == 'admin' || response.data.user.roles == 'HR') {
        self.showPermanentAddress = true;
      }

      console.log(response);
    })["catch"](function (error) {
      console.log(error); // self.$router.push({ path: '/login' });
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/validation-errors.vue?vue&type=template&id=2961bf1b&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/employees/validation-errors.vue?vue&type=template&id=2961bf1b& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "form-group", attrs: { role: "group" } }, [
    _vm.error
      ? _c("span", { staticClass: "error" }, [_vm._v(_vm._s(_vm.error))])
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/user/User.vue?vue&type=template&id=3089dbd6&":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/user/User.vue?vue&type=template&id=3089dbd6& ***!
  \******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { xs: "12", lg: "12" } },
        [
          _c(
            "CCard",
            [
              _c("CCardHeader", { staticClass: "task-heading mb-1" }, [
                _vm.name
                  ? _c(
                      "h3",
                      {
                        staticClass:
                          "d-flex justify-content-between align-items-center"
                      },
                      [
                        _vm._v(
                          "\n          Edit User : " +
                            _vm._s(_vm.name) +
                            "\n          "
                        ),
                        _c(
                          "div",
                          { staticClass: "btns-header" },
                          [
                            _c(
                              "CButton",
                              {
                                attrs: { color: "secondary" },
                                on: { click: _vm.goBack }
                              },
                              [_vm._v("Back")]
                            )
                          ],
                          1
                        )
                      ]
                    )
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("CCardBody", [
                _c(
                  "div",
                  { staticClass: "custom-tabs-sec" },
                  [
                    _c(
                      "CTabs",
                      { attrs: { variant: "pills", horizontal: "" } },
                      [
                        _c(
                          "CTab",
                          { attrs: { title: "Basic Info", active: "" } },
                          [
                            _c(
                              "CForm",
                              [
                                _c(
                                  "CAlert",
                                  {
                                    attrs: {
                                      show: _vm.dismissCountDown,
                                      color: "primary",
                                      fade: ""
                                    },
                                    on: {
                                      "update:show": function($event) {
                                        _vm.dismissCountDown = $event
                                      }
                                    }
                                  },
                                  [
                                    _vm._v(
                                      "\n                    " +
                                        _vm._s(_vm.message) +
                                        "\n                  "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c("CInput", {
                                  attrs: {
                                    type: "text",
                                    label: "Name",
                                    placeholder: "Name"
                                  },
                                  model: {
                                    value: _vm.name,
                                    callback: function($$v) {
                                      _vm.name = $$v
                                    },
                                    expression: "name"
                                  }
                                }),
                                _vm._v(" "),
                                _vm.errors.name
                                  ? _c("validation-error", {
                                      attrs: { error: _vm.errors.name[0] }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("CInput", {
                                  attrs: {
                                    type: "text",
                                    label: "Email",
                                    placeholder: "Email",
                                    readonly: ""
                                  },
                                  model: {
                                    value: _vm.email,
                                    callback: function($$v) {
                                      _vm.email = $$v
                                    },
                                    expression: "email"
                                  }
                                }),
                                _vm._v(" "),
                                _vm.errors.email
                                  ? _c("validation-error", {
                                      attrs: { error: _vm.errors.email[0] }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("CInput", {
                                  attrs: {
                                    type: "number",
                                    multiple: "",
                                    label: "Contact Number",
                                    placeholder: "Contact Number"
                                  },
                                  model: {
                                    value: _vm.contact_number,
                                    callback: function($$v) {
                                      _vm.contact_number = $$v
                                    },
                                    expression: "contact_number"
                                  }
                                }),
                                _vm._v(" "),
                                _vm.errors.contact_number
                                  ? _c("validation-error", {
                                      attrs: {
                                        error: _vm.errors.contact_number[0]
                                      }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("CInput", {
                                  attrs: {
                                    type: "number",
                                    multiple: "",
                                    label: "Alternate Number",
                                    placeholder: "Alternate Number"
                                  },
                                  model: {
                                    value: _vm.alternate_number,
                                    callback: function($$v) {
                                      _vm.alternate_number = $$v
                                    },
                                    expression: "alternate_number"
                                  }
                                }),
                                _vm._v(" "),
                                _vm.errors.alternate_number
                                  ? _c("validation-error", {
                                      attrs: {
                                        error: _vm.errors.alternate_number[0]
                                      }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("CInput", {
                                  attrs: {
                                    label: "Date of Birth",
                                    placeholder: "Department",
                                    type: "date"
                                  },
                                  model: {
                                    value: _vm.dof,
                                    callback: function($$v) {
                                      _vm.dof = $$v
                                    },
                                    expression: "dof"
                                  }
                                }),
                                _vm._v(" "),
                                _vm.errors.dof
                                  ? _c("validation-error", {
                                      attrs: { error: _vm.errors.dof[0] }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("CTextarea", {
                                  attrs: {
                                    type: "text",
                                    label: "Current Address",
                                    rows: "5"
                                  },
                                  model: {
                                    value: _vm.current_address,
                                    callback: function($$v) {
                                      _vm.current_address = $$v
                                    },
                                    expression: "current_address"
                                  }
                                }),
                                _vm._v(" "),
                                _vm.showPermanentAddress == true
                                  ? _c("CTextarea", {
                                      attrs: {
                                        type: "text",
                                        label: "Permanent Address",
                                        rows: "5"
                                      },
                                      model: {
                                        value: _vm.permanent_address,
                                        callback: function($$v) {
                                          _vm.permanent_address = $$v
                                        },
                                        expression: "permanent_address"
                                      }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("CTextarea", {
                                  attrs: {
                                    type: "text",
                                    label: "Additional Info",
                                    rows: "5"
                                  },
                                  model: {
                                    value: _vm.additional_info,
                                    callback: function($$v) {
                                      _vm.additional_info = $$v
                                    },
                                    expression: "additional_info"
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "CButton",
                                  {
                                    attrs: { color: "primary" },
                                    on: {
                                      click: function($event) {
                                        return _vm.update()
                                      }
                                    }
                                  },
                                  [_vm._v("Save")]
                                )
                              ],
                              1
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("CTab", { attrs: { title: "Profile Pic" } }, [
                          _c(
                            "form",
                            {
                              attrs: { enctype: "multipart/form-data" },
                              on: { submit: _vm.formSubmit }
                            },
                            [
                              _c(
                                "CAlert",
                                {
                                  attrs: {
                                    show: _vm.dismissCountDown,
                                    color: "primary",
                                    fade: ""
                                  },
                                  on: {
                                    "update:show": function($event) {
                                      _vm.dismissCountDown = $event
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                    " +
                                      _vm._s(_vm.message) +
                                      "\n                  "
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _vm.profile
                                ? _c("img", {
                                    staticClass: "add-profile",
                                    attrs: { src: _vm.profile }
                                  })
                                : _c("img", {
                                    staticClass: "add-profile",
                                    attrs: { src: _vm.avtar }
                                  }),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass: "form-group",
                                  attrs: { role: "group" }
                                },
                                [
                                  _c("label", [_vm._v("Profile Picture")]),
                                  _vm._v(" "),
                                  _c("input", {
                                    staticClass: "form-control",
                                    attrs: { type: "file" },
                                    on: { change: _vm.selectFile }
                                  })
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "CButton",
                                { attrs: { color: "primary", type: "submit" } },
                                [_vm._v("Save")]
                              )
                            ],
                            1
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "CTab",
                          { attrs: { title: "Password" } },
                          [
                            _c(
                              "CForm",
                              [
                                _c(
                                  "CAlert",
                                  {
                                    attrs: {
                                      show: _vm.dismissCountDown,
                                      color: "primary",
                                      fade: ""
                                    },
                                    on: {
                                      "update:show": function($event) {
                                        _vm.dismissCountDown = $event
                                      }
                                    }
                                  },
                                  [
                                    _vm._v(
                                      "\n                    " +
                                        _vm._s(_vm.message) +
                                        "\n                  "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c("CInput", {
                                  attrs: {
                                    type: "password",
                                    label: "Current Password",
                                    placeholder: "Current Password"
                                  },
                                  model: {
                                    value: _vm.current_password,
                                    callback: function($$v) {
                                      _vm.current_password = $$v
                                    },
                                    expression: "current_password"
                                  }
                                }),
                                _vm._v(" "),
                                _vm.errors.current_password
                                  ? _c("validation-error", {
                                      attrs: {
                                        error: _vm.errors.current_password[0]
                                      }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("CInput", {
                                  attrs: {
                                    type: "password",
                                    label: "New Password",
                                    placeholder: "New Password"
                                  },
                                  model: {
                                    value: _vm.new_password,
                                    callback: function($$v) {
                                      _vm.new_password = $$v
                                    },
                                    expression: "new_password"
                                  }
                                }),
                                _vm._v(" "),
                                _vm.errors.new_password
                                  ? _c("validation-error", {
                                      attrs: {
                                        error: _vm.errors.new_password[0]
                                      }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("CInput", {
                                  attrs: {
                                    type: "password",
                                    label: "Confirm New Password",
                                    placeholder: "Confirm New Password"
                                  },
                                  model: {
                                    value: _vm.new_confirm_password,
                                    callback: function($$v) {
                                      _vm.new_confirm_password = $$v
                                    },
                                    expression: "new_confirm_password"
                                  }
                                }),
                                _vm._v(" "),
                                _vm.errors.new_confirm_password
                                  ? _c("validation-error", {
                                      attrs: {
                                        error:
                                          _vm.errors.new_confirm_password[0]
                                      }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c(
                                  "CButton",
                                  {
                                    attrs: { color: "primary" },
                                    on: {
                                      click: function($event) {
                                        return _vm.updatePassword()
                                      }
                                    }
                                  },
                                  [_vm._v("Save")]
                                )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ])
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);