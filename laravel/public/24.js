(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[24],{

/***/ "../coreui/src/views/chat/Chat.vue":
/*!*****************************************!*\
  !*** ../coreui/src/views/chat/Chat.vue ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Chat_vue_vue_type_template_id_6f1c9b96___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Chat.vue?vue&type=template&id=6f1c9b96& */ "../coreui/src/views/chat/Chat.vue?vue&type=template&id=6f1c9b96&");
/* harmony import */ var _Chat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Chat.vue?vue&type=script&lang=js& */ "../coreui/src/views/chat/Chat.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Chat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Chat_vue_vue_type_template_id_6f1c9b96___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Chat_vue_vue_type_template_id_6f1c9b96___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/chat/Chat.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/chat/Chat.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ../coreui/src/views/chat/Chat.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Chat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Chat.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/chat/Chat.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Chat_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/chat/Chat.vue?vue&type=template&id=6f1c9b96&":
/*!************************************************************************!*\
  !*** ../coreui/src/views/chat/Chat.vue?vue&type=template&id=6f1c9b96& ***!
  \************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Chat_vue_vue_type_template_id_6f1c9b96___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Chat.vue?vue&type=template&id=6f1c9b96& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/chat/Chat.vue?vue&type=template&id=6f1c9b96&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Chat_vue_vue_type_template_id_6f1c9b96___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Chat_vue_vue_type_template_id_6f1c9b96___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/chat/Chat.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/chat/Chat.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "../coreui/node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "../coreui/node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! devextreme-vue/html-editor */ "../coreui/node_modules/devextreme-vue/html-editor.js");
/* harmony import */ var devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var devextreme_vue_text_area__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! devextreme-vue/text-area */ "../coreui/node_modules/devextreme-vue/text-area.js");
/* harmony import */ var devextreme_vue_text_area__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(devextreme_vue_text_area__WEBPACK_IMPORTED_MODULE_4__);
function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Inbox',
  data: function data() {
    return {
      users: [],
      chats: [],
      files: [],
      notifications: [],
      name: '',
      _id: '',
      _messageId: '',
      role: '',
      profile: '',
      search: '',
      time_format: '',
      date_format: '',
      logged: '',
      _interval: '',
      message: '',
      avtar: '/img/user-avtar.jpg',
      loader: '/img/loader.gif',
      is_admin: false,
      pauseInterval: false,
      edited: false,
      inboxLoader: false,
      placeholderShow: true,
      loading: false,
      inboxloading: false,
      started_chat: false,
      close: false,
      textarea: ''
    };
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapState"])({
    authProfile: function authProfile(state) {
      return state.authProfile;
    }
  })), Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapGetters"])(["userLoggedin"])),
  components: {
    DxHtmlEditor: devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_3__["DxHtmlEditor"],
    DxTextArea: devextreme_vue_text_area__WEBPACK_IMPORTED_MODULE_4__["DxTextArea"]
  },
  methods: {
    onEnterKey: function onEnterKey(e) {
      var self = this;

      if (e.which == 13 && !e.shiftKey) {
        this.addMessage();
        e.preventDefault();
      }
    },
    searchUser: function searchUser() {
      if (this.search !== '') {
        this.close = true;
      }

      this.listUsers();
    },
    closeSearch: function closeSearch() {
      this.close = false;
      this.search = '';
      this.listUsers();
    },
    dateFormat: function dateFormat(value) {
      var date = moment__WEBPACK_IMPORTED_MODULE_1___default()(value).format('MMMM D, Y'),
          val = '',
          time = moment__WEBPACK_IMPORTED_MODULE_1___default()(value).format(this.time_format);

      if (moment__WEBPACK_IMPORTED_MODULE_1___default()().diff(date, 'days') === 0) {
        val = 'Today, ' + time;
      } else if (moment__WEBPACK_IMPORTED_MODULE_1___default()().diff(date, 'days') === 1) {
        val = 'Yesterday, ' + time;
      } else {
        val = moment__WEBPACK_IMPORTED_MODULE_1___default()(value).format(this.date_format);
      }

      return val;
    },
    geToDivvBottom: function geToDivvBottom() {
      setTimeout(function () {
        jQuery("#scrollBottom").scrollTop(jQuery("#scrollBottom")[0].scrollHeight);
      }, 100);
    },
    isValidImageURL: function isValidImageURL(str) {
      if (typeof str !== 'string') return false;
      return !!str.match(/\w+\.(jpg|jpeg|gif|png|tiff|bmp|webm)$/gi);
    },
    getParams: function getParams() {
      var queryString = window.location.search,
          urlParams = new URLSearchParams(queryString);
      return urlParams;
    },
    addClassActive: function addClassActive() {
      var div = jQuery('.chat-left-section .info-div'),
          _cls = '.custUsr' + this._id;

      var active = document.querySelector(_cls);

      for (var i = div.length - 1; i >= 0; i--) {
        if (div[i] == active) {
          active.classList.add('active');
        } else {
          div[i].classList.remove('active');
        }
      }
    },
    getValidFileName: function getValidFileName(file) {
      if (typeof file == 'string') {
        var val = file.split('/');
        return val.slice(-1).pop();
      }
    },
    getNotified: function getNotified(data) {
      for (var i = 0; i < data.length; i++) {
        var _cls = '.chatNotify' + data[i].user_id;

        var div = document.querySelector(_cls);
        div.classList.remove('hide');
        div.innerText = data[i].count;
      }
    },
    getLoggedIn: function getLoggedIn(data) {
      for (var i = 0; i < data.length; i++) {
        var _cls = '.userLogin' + data[i].id;

        var div = document.querySelector(_cls);

        if (data[i].user_loggedIn) {
          div.classList.add('onlineuser');
          div.classList.remove('offlineuser');
        } else {
          div.classList.remove('onlineuser');
          div.classList.add('offlineuser');
        }
      }
    },
    listUsers: function listUsers() {
      var that = this;
      this.loading = true;
      var url = '/api/chat/users?token=' + localStorage.getItem("api_token");

      if (this.search) {
        url = url + '&search=' + this.search;
      }

      var urlParams = this.getParams();

      if (urlParams.get('user')) {
        url = url + '&user=' + urlParams.get('user');
      }

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(url).then(function (response) {
        that.loading = false;

        if (_typeof(response.data.users) == 'object') {
          that.users = Object.values(response.data.users);
        } else {
          that.users = response.data.users;
        }

        setTimeout(function () {
          that.getLoggedIn(that.users);
        }, 500);
        setTimeout(function () {
          that.getNotified(response.data.notify);
        }, 500);
      })["catch"](function (error) {
        that.loading = false;
        console.log('here');
      });
    },
    realtimeChat: function realtimeChat() {
      var that = this;
      this._interval = setInterval(function () {
        if (!that.pauseInterval) {
          var url = '/api/chat/get-chat?token=' + localStorage.getItem("api_token") + '&user=' + that._id;

          axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(url).then(function (response) {
            that.chats = response.data.chats;
            var $messages = document.querySelector('#scrollBottom');

            if ($messages != null) {
              var newMessage = $messages.lastElementChild;
              var newMessageStyles = getComputedStyle(newMessage);
              var newMessageMargin = parseInt(newMessageStyles.marginBottom);
              var newMessageHeight = newMessage.offsetHeight + newMessageMargin;
              var visible_height = $messages.offsetHeight;
              var containerHeight = $messages.scrollHeight;
              var scrollOffset = $messages.scrollTop + visible_height;

              if (containerHeight - newMessageHeight <= scrollOffset) {
                if (containerHeight - newMessageHeight <= scrollOffset) {
                  $messages.scrollTop = $messages.scrollHeight;
                }
              }

              if (_typeof(response.data.users) == 'object') {
                that.users = Object.values(response.data.users);
              } else {
                that.users = response.data.users;
              }

              setTimeout(function () {
                that.getLoggedIn(that.users);
              }, 500);
              that.getNotified(response.data.notify);
            }
          });
        }

        var _route = that.$router.currentRoute.path;
        _route = _route.split("/");
        _route = _route.filter(function (el) {
          return el != "";
        });

        if (_route[0] != 'inbox') {
          clearInterval(that._interval);
        }
      }, 1500);
    },
    listChat: function listChat(user) {
      var that = this;
      this.name = user.name;
      this.profile = user.profile;
      this.role = user.menuroles;
      this._id = user.id;
      this.started_chat = true;
      this.inboxLoader = true;
      this.addClassActive();
      var div = document.querySelector('.chatNotify' + this._id);
      div.classList.add('hide');
      div.innerText = '';

      if (this._interval) {
        clearInterval(this._interval);
      }

      var url = '/api/chat?token=' + localStorage.getItem("api_token") + '&user=' + user.id;
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(url).then(function (response) {
        that.inboxLoader = false;
        that.chats = response.data.chats;
        that.time_format = response.data.time;
        that.geToDivvBottom();
        setTimeout(function () {
          that.realtimeChat();
        }, 2000);
      });
    },
    addMessage: function addMessage() {
      console.log('sdfsf  --  ' + this.message);
      var that = this;
      var formdata = new FormData();
      var valid = false;
      this.pauseInterval = true;
      formdata.append('user', this._id);

      if (this.files.length > 0) {
        valid = true;

        for (var i = 0; i < this.files.length; i++) {
          formdata.append('files[' + i + ']', this.files[i]);
        }
      } else {
        if (this.message) {
          valid = true;
        }

        formdata.append('message', this.message);
      }

      var div = document.querySelector('.chatNotify' + this._id);
      div.classList.add('hide');
      div.innerText = '';

      if (valid) {
        this.inboxloading = true;
        axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/chat/message?token=' + localStorage.getItem("api_token"), formdata).then(function (response) {
          that.inboxloading = false;
          that.chats = response.data.chats;
          that.message = '';
          that.files = [];
          that.geToDivvBottom();
          setTimeout(function () {
            that.pauseInterval = false;
          }, 2000);
        });
      }
    },
    editMessage: function editMessage(chat) {
      this.message = chat.message;
      this._messageId = chat.id;
      this.edited = true;
    },
    updateMessage: function updateMessage() {
      this.pauseInterval = true;
      var that = this;
      var formdata = new FormData();
      var valid = false;
      formdata.append('id', this._messageId);
      formdata.append('user', this._id);
      formdata.append('message', this.message);

      if (this.message) {
        valid = true;
      }

      var div = document.querySelector('.chatNotify' + this._id);
      div.classList.add('hide');
      div.innerText = '';

      if (valid) {
        this.inboxloading = true;
        axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/chat/edit?token=' + localStorage.getItem("api_token"), formdata).then(function (response) {
          that.inboxloading = false;
          that.edited = false;
          that.chats = response.data.chats;
          that.message = '';
          that._messageId = '';
          that.geToDivvBottom();
          setTimeout(function () {
            that.pauseInterval = false;
          }, 2000);
        });
      }
    },
    deleteMessage: function deleteMessage(id) {
      this.pauseInterval = true;
      var that = this;
      this.$swal({
        title: 'Are you sure?',
        text: 'You can\'t revert your action',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
        showCloseButton: true,
        showLoaderOnConfirm: true
      }).then(function (result) {
        if (result.value) {
          var url = '/api/chat/delete/' + id + '/' + that._id + '?token=' + localStorage.getItem("api_token");
          axios__WEBPACK_IMPORTED_MODULE_0___default.a.get(url).then(function (response) {
            that.chats = response.data.chats;
            setTimeout(function () {
              that.pauseInterval = false;
            }, 2000);
          });
        }
      });
    },
    cancelEdit: function cancelEdit() {
      this.message = '';
      this.files = [];
      this.edited = false;
    },
    enterClicked: function enterClicked() {
      console.log('sdfsd');
    },
    allowDragDrop: function allowDragDrop() {
      document.querySelector('.outer-drag-drop').style.display = "flex";
    },
    dragleave: function dragleave() {
      document.querySelector('.outer-drag-drop').style.display = "none";
    },
    handleDragFileUpload: function handleDragFileUpload() {
      this.files = event.target.files;
      this.dragleave();
      this.addMessage();
    }
  },
  mounted: function mounted() {
    this.logged = parseInt(localStorage.getItem('user'));
    this.is_admin = JSON.parse(localStorage.getItem('type'));
    this.date_format = localStorage.getItem('date_format');
    this.listUsers();
    var self = this;
    jQuery(document).on('keydown', ".ql-editor.dx-htmleditor-content", function (e) {
      var event = e;
      setTimeout(function () {
        self.onEnterKey(event);
      }, 10);
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/chat/Chat.vue?vue&type=template&id=6f1c9b96&":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/chat/Chat.vue?vue&type=template&id=6f1c9b96& ***!
  \******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "chat-section" },
    [
      _c(
        "CRow",
        { staticClass: "remove-chatrow-padding" },
        [
          _c(
            "CCol",
            { attrs: { col: "3", lg: "3" } },
            [
              _c(
                "CCard",
                { staticClass: "h-100" },
                [
                  _c("CCardBody", { staticClass: "p-0" }, [
                    _c(
                      "div",
                      { staticClass: "chat-left-section" },
                      [
                        _c(
                          "CCardHeader",
                          {
                            staticClass:
                              "d-flex justify-content-between align-items-center"
                          },
                          [
                            _c("h4", { staticClass: "mb-0" }, [
                              _vm._v("Chat List")
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c("CRow", { staticClass: "m-0" }, [
                          _c(
                            "div",
                            { staticClass: "chat-search-box w-100 pl-1 pr-1" },
                            [
                              _c("CInput", {
                                attrs: {
                                  type: "text",
                                  placeholder: "Search ..."
                                },
                                on: { input: _vm.searchUser },
                                model: {
                                  value: _vm.search,
                                  callback: function($$v) {
                                    _vm.search = $$v
                                  },
                                  expression: "search"
                                }
                              }),
                              _vm._v(" "),
                              _vm.close
                                ? _c(
                                    "div",
                                    {
                                      staticClass: "global-search-close",
                                      on: { click: _vm.closeSearch }
                                    },
                                    [_c("CIcon", { attrs: { name: "cil-x" } })],
                                    1
                                  )
                                : _vm._e()
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _vm.users.length > 0
                            ? _c(
                                "div",
                                { staticClass: "info-section w-100" },
                                _vm._l(_vm.users, function(user) {
                                  return _c(
                                    "div",
                                    {
                                      class:
                                        "info-div pointer custUsr" + user.id,
                                      on: {
                                        click: function($event) {
                                          return _vm.listChat(user)
                                        }
                                      }
                                    },
                                    [
                                      _c("CCol", { attrs: { col: "3" } }, [
                                        _c(
                                          "div",
                                          { staticClass: "small-rounded-img" },
                                          [
                                            user.profile
                                              ? _c("img", {
                                                  staticClass: "c-avatar-img",
                                                  attrs: { src: user.profile }
                                                })
                                              : _c("img", {
                                                  attrs: { src: _vm.avtar }
                                                })
                                          ]
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c(
                                        "CCol",
                                        {
                                          staticClass: "pl-0",
                                          attrs: { col: "9" }
                                        },
                                        [
                                          _c(
                                            "p",
                                            { staticClass: "user-chat-left" },
                                            [
                                              _vm._v(_vm._s(user.name) + " "),
                                              _c("CIcon", {
                                                class:
                                                  "user_logindiv offlineuser userLogin" +
                                                  user.id,
                                                attrs: { name: "cil-circle" }
                                              })
                                            ],
                                            1
                                          ),
                                          _vm._v(" "),
                                          user.menuroles == "admin"
                                            ? _c(
                                                "p",
                                                {
                                                  staticClass: "pos-name-chat"
                                                },
                                                [_vm._v(" Director")]
                                              )
                                            : _c(
                                                "p",
                                                {
                                                  staticClass: "pos-name-chat"
                                                },
                                                [
                                                  _vm._v(
                                                    " " + _vm._s(user.menuroles)
                                                  )
                                                ]
                                              ),
                                          _vm._v(" "),
                                          _c(
                                            "span",
                                            {
                                              class:
                                                "hide user-notify-chat chatNotify" +
                                                user.id
                                            },
                                            [_vm._v("1")]
                                          )
                                        ]
                                      )
                                    ],
                                    1
                                  )
                                }),
                                0
                              )
                            : _c("div", { staticClass: "chat-no-user" }, [
                                _vm._v(
                                  "\n\t\t\t        \t\t\tUsers not found.\n\t\t\t        \t\t"
                                )
                              ])
                        ])
                      ],
                      1
                    )
                  ])
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "CCol",
            { attrs: { col: "9", lg: "9" } },
            [
              _c(
                "CCard",
                { staticClass: "h-100" },
                [
                  _c("CCardBody", { staticClass: "p-0" }, [
                    _vm.started_chat
                      ? _c(
                          "div",
                          { staticClass: "chat-right-section" },
                          [
                            _c(
                              "CCardHeader",
                              {
                                staticClass:
                                  "d-flex justify-content-between align-items-center"
                              },
                              [
                                _c("div", { staticClass: "outer-top-chat" }, [
                                  _c(
                                    "div",
                                    { staticClass: "small-rounded-img" },
                                    [
                                      _vm.profile
                                        ? _c("img", {
                                            attrs: { src: _vm.profile }
                                          })
                                        : _c("img", {
                                            attrs: { src: _vm.avtar }
                                          })
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("h4", { staticClass: "mb-0" }, [
                                    _c("p", { staticClass: "user-name-chat" }, [
                                      _vm._v(_vm._s(this.name))
                                    ]),
                                    _vm._v(" "),
                                    _vm.role == "admin"
                                      ? _c(
                                          "p",
                                          { staticClass: "pos-name-chat" },
                                          [_vm._v(" Director")]
                                        )
                                      : _c(
                                          "p",
                                          { staticClass: "pos-name-chat" },
                                          [_vm._v(" " + _vm._s(this.role))]
                                        )
                                  ])
                                ])
                              ]
                            ),
                            _vm._v(" "),
                            _c("CRow", { staticClass: "m-0 pl-1 pr-1" }, [
                              _c(
                                "div",
                                {
                                  staticClass: "chat-outer-module w-100",
                                  attrs: { id: "scrollBottom" },
                                  on: {
                                    dragover: function($event) {
                                      return _vm.allowDragDrop()
                                    }
                                  }
                                },
                                [
                                  _c(
                                    "div",
                                    {
                                      staticClass:
                                        "outer-drag-drop chat-dropper",
                                      staticStyle: { display: "none" },
                                      on: {
                                        dragleave: function($event) {
                                          return _vm.dragleave()
                                        }
                                      }
                                    },
                                    [
                                      _c("div", { staticClass: "inner-drop" }, [
                                        _c("h2", [
                                          _vm._v("Upload file or image")
                                        ]),
                                        _vm._v(" "),
                                        _c("p", [
                                          _vm._v("Add using drag'n'drop")
                                        ]),
                                        _vm._v(" "),
                                        _c(
                                          "form",
                                          [
                                            _c("CInput", {
                                              staticClass: "file-chat-input",
                                              attrs: {
                                                type: "file",
                                                multiple: ""
                                              },
                                              on: {
                                                change: _vm.handleDragFileUpload
                                              }
                                            })
                                          ],
                                          1
                                        )
                                      ])
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _vm._l(_vm.chats.slice().reverse(), function(
                                    chat,
                                    key
                                  ) {
                                    return _vm.chats.length > 0
                                      ? _c(
                                          "div",
                                          { staticClass: "chat-inner-module" },
                                          [
                                            _vm.logged != chat.user_id &&
                                            chat.type == "message"
                                              ? _c(
                                                  "div",
                                                  {
                                                    staticClass:
                                                      "chat-text-outer"
                                                  },
                                                  [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "chat-text-inner"
                                                      },
                                                      [
                                                        _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "small-rounded-img"
                                                          },
                                                          [
                                                            _vm.profile
                                                              ? _c("img", {
                                                                  attrs: {
                                                                    src:
                                                                      _vm.profile
                                                                  }
                                                                })
                                                              : _c("img", {
                                                                  attrs: {
                                                                    src:
                                                                      _vm.avtar
                                                                  }
                                                                })
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "text-chat"
                                                          },
                                                          [
                                                            _c("p", {
                                                              domProps: {
                                                                innerHTML: _vm._s(
                                                                  chat.message
                                                                )
                                                              }
                                                            }),
                                                            _vm._v(" "),
                                                            _c(
                                                              "div",
                                                              {
                                                                staticClass:
                                                                  "time-chat"
                                                              },
                                                              [
                                                                _c("p", [
                                                                  _vm._v(
                                                                    _vm._s(
                                                                      _vm.dateFormat(
                                                                        chat.created_at
                                                                      )
                                                                    )
                                                                  )
                                                                ])
                                                              ]
                                                            )
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                )
                                              : _vm._e(),
                                            _vm._v(" "),
                                            _vm.logged == chat.user_id &&
                                            chat.type == "message"
                                              ? _c(
                                                  "div",
                                                  {
                                                    staticClass:
                                                      "chat-text-outer my-chat-outer"
                                                  },
                                                  [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "chat-text-inner"
                                                      },
                                                      [
                                                        _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "text-chat"
                                                          },
                                                          [
                                                            _c("p", {
                                                              domProps: {
                                                                innerHTML: _vm._s(
                                                                  chat.message
                                                                )
                                                              }
                                                            }),
                                                            _vm._v(" "),
                                                            _c(
                                                              "div",
                                                              {
                                                                staticClass:
                                                                  "time-chat d-flex justify-content-between align-items-center"
                                                              },
                                                              [
                                                                _c(
                                                                  "CDropdown",
                                                                  {
                                                                    staticClass:
                                                                      "custom-menu-btn chat-edit-btn",
                                                                    attrs: {
                                                                      "toggler-text":
                                                                        "More"
                                                                    }
                                                                  },
                                                                  [
                                                                    _c(
                                                                      "CDropdownItem",
                                                                      {
                                                                        on: {
                                                                          click: function(
                                                                            $event
                                                                          ) {
                                                                            return _vm.editMessage(
                                                                              chat
                                                                            )
                                                                          }
                                                                        }
                                                                      },
                                                                      [
                                                                        _c(
                                                                          "CIcon",
                                                                          {
                                                                            attrs: {
                                                                              name:
                                                                                "cil-pencil"
                                                                            }
                                                                          }
                                                                        ),
                                                                        _vm._v(
                                                                          " Edit"
                                                                        )
                                                                      ],
                                                                      1
                                                                    ),
                                                                    _vm._v(" "),
                                                                    _c(
                                                                      "CDropdownItem",
                                                                      {
                                                                        on: {
                                                                          click: function(
                                                                            $event
                                                                          ) {
                                                                            return _vm.deleteMessage(
                                                                              chat.id
                                                                            )
                                                                          }
                                                                        }
                                                                      },
                                                                      [
                                                                        _c(
                                                                          "CIcon",
                                                                          {
                                                                            attrs: {
                                                                              name:
                                                                                "cil-trash"
                                                                            }
                                                                          }
                                                                        ),
                                                                        _vm._v(
                                                                          " Delete"
                                                                        )
                                                                      ],
                                                                      1
                                                                    )
                                                                  ],
                                                                  1
                                                                ),
                                                                _vm._v(" "),
                                                                _c("p", [
                                                                  _vm._v(
                                                                    _vm._s(
                                                                      _vm.dateFormat(
                                                                        chat.created_at
                                                                      )
                                                                    )
                                                                  )
                                                                ])
                                                              ],
                                                              1
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "small-rounded-img"
                                                          },
                                                          [
                                                            _vm.authProfile
                                                              ? _c("img", {
                                                                  attrs: {
                                                                    src:
                                                                      _vm.authProfile
                                                                  }
                                                                })
                                                              : _c("img", {
                                                                  attrs: {
                                                                    src:
                                                                      _vm.avtar
                                                                  }
                                                                })
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                )
                                              : _vm._e(),
                                            _vm._v(" "),
                                            _vm.logged == chat.user_id &&
                                            chat.type == "file"
                                              ? _c(
                                                  "div",
                                                  {
                                                    staticClass:
                                                      "chat-text-outer my-chat-outer"
                                                  },
                                                  [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "chat-text-inner"
                                                      },
                                                      [
                                                        _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "text-chat"
                                                          },
                                                          [
                                                            _vm.isValidImageURL(
                                                              chat.message
                                                            )
                                                              ? _c("img", {
                                                                  attrs: {
                                                                    src:
                                                                      chat.message
                                                                  }
                                                                })
                                                              : _c(
                                                                  "a",
                                                                  {
                                                                    staticClass:
                                                                      "file-zip-chat pointer"
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      _vm._s(
                                                                        _vm.getValidFileName(
                                                                          chat.message
                                                                        )
                                                                      )
                                                                    )
                                                                  ]
                                                                ),
                                                            _vm._v(" "),
                                                            _c("p", [
                                                              _c(
                                                                "a",
                                                                {
                                                                  staticClass:
                                                                    "download-img-chat",
                                                                  attrs: {
                                                                    href:
                                                                      chat.message,
                                                                    download: ""
                                                                  }
                                                                },
                                                                [
                                                                  _vm._v(
                                                                    "Download"
                                                                  )
                                                                ]
                                                              )
                                                            ]),
                                                            _vm._v(" "),
                                                            _c(
                                                              "div",
                                                              {
                                                                staticClass:
                                                                  "time-chat d-flex justify-content-between align-items-center"
                                                              },
                                                              [
                                                                _c(
                                                                  "CDropdown",
                                                                  {
                                                                    staticClass:
                                                                      "custom-menu-btn chat-edit-btn",
                                                                    attrs: {
                                                                      "toggler-text":
                                                                        "More"
                                                                    }
                                                                  },
                                                                  [
                                                                    _c(
                                                                      "CDropdownItem",
                                                                      {
                                                                        on: {
                                                                          click: function(
                                                                            $event
                                                                          ) {
                                                                            return _vm.deleteMessage(
                                                                              chat.id
                                                                            )
                                                                          }
                                                                        }
                                                                      },
                                                                      [
                                                                        _c(
                                                                          "CIcon",
                                                                          {
                                                                            attrs: {
                                                                              name:
                                                                                "cil-trash"
                                                                            }
                                                                          }
                                                                        ),
                                                                        _vm._v(
                                                                          " Delete"
                                                                        )
                                                                      ],
                                                                      1
                                                                    )
                                                                  ],
                                                                  1
                                                                ),
                                                                _vm._v(" "),
                                                                _c("p", [
                                                                  _vm._v(
                                                                    _vm._s(
                                                                      _vm.dateFormat(
                                                                        chat.created_at
                                                                      )
                                                                    )
                                                                  )
                                                                ])
                                                              ],
                                                              1
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "small-rounded-img"
                                                          },
                                                          [
                                                            _vm.authProfile
                                                              ? _c("img", {
                                                                  attrs: {
                                                                    src:
                                                                      _vm.authProfile
                                                                  }
                                                                })
                                                              : _c("img", {
                                                                  attrs: {
                                                                    src:
                                                                      _vm.avtar
                                                                  }
                                                                })
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                )
                                              : _vm._e(),
                                            _vm._v(" "),
                                            _vm.logged != chat.user_id &&
                                            chat.type == "file"
                                              ? _c(
                                                  "div",
                                                  {
                                                    staticClass:
                                                      "chat-text-outer"
                                                  },
                                                  [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "chat-text-inner"
                                                      },
                                                      [
                                                        _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "small-rounded-img"
                                                          },
                                                          [
                                                            _vm.profile
                                                              ? _c("img", {
                                                                  attrs: {
                                                                    src:
                                                                      _vm.profile
                                                                  }
                                                                })
                                                              : _c("img", {
                                                                  attrs: {
                                                                    src:
                                                                      _vm.avtar
                                                                  }
                                                                })
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "text-chat"
                                                          },
                                                          [
                                                            _vm.isValidImageURL(
                                                              chat.message
                                                            )
                                                              ? _c("img", {
                                                                  attrs: {
                                                                    src:
                                                                      chat.message
                                                                  }
                                                                })
                                                              : _c(
                                                                  "a",
                                                                  {
                                                                    staticClass:
                                                                      "file-zip-chat pointer"
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      _vm._s(
                                                                        _vm.getValidFileName(
                                                                          chat.message
                                                                        )
                                                                      )
                                                                    )
                                                                  ]
                                                                ),
                                                            _vm._v(" "),
                                                            _c("p", [
                                                              _c(
                                                                "a",
                                                                {
                                                                  staticClass:
                                                                    "download-img-chat",
                                                                  attrs: {
                                                                    href:
                                                                      chat.message,
                                                                    download: ""
                                                                  }
                                                                },
                                                                [
                                                                  _vm._v(
                                                                    "Download"
                                                                  )
                                                                ]
                                                              )
                                                            ]),
                                                            _vm._v(" "),
                                                            _c(
                                                              "div",
                                                              {
                                                                staticClass:
                                                                  "time-chat d-flex justify-content-between align-items-center"
                                                              },
                                                              [
                                                                _c("p", [
                                                                  _vm._v(
                                                                    _vm._s(
                                                                      _vm.dateFormat(
                                                                        chat.created_at
                                                                      )
                                                                    )
                                                                  )
                                                                ])
                                                              ]
                                                            )
                                                          ]
                                                        )
                                                      ]
                                                    )
                                                  ]
                                                )
                                              : _vm._e()
                                          ]
                                        )
                                      : _c(
                                          "div",
                                          {
                                            staticClass: "no-chat-message w-100"
                                          },
                                          [
                                            _c(
                                              "p",
                                              { staticClass: "no-message-txt" },
                                              [_vm._v("No messages.")]
                                            )
                                          ]
                                        )
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    { staticClass: "chat-message-field" },
                                    [
                                      _c("DxHtmlEditor", {
                                        ref: "messageEditor",
                                        attrs: {
                                          height: "70px",
                                          "value-type": "html",
                                          placeholder:
                                            "Send a message and drag 'n' drop file."
                                        },
                                        model: {
                                          value: _vm.message,
                                          callback: function($$v) {
                                            _vm.message = $$v
                                          },
                                          expression: "message"
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c(
                                        "div",
                                        { staticClass: "input-icons" },
                                        [
                                          _vm.inboxloading
                                            ? _c(
                                                "div",
                                                {
                                                  staticClass: "message-loader"
                                                },
                                                [
                                                  _c("img", {
                                                    attrs: { src: _vm.loader }
                                                  })
                                                ]
                                              )
                                            : _vm._e()
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _vm.edited
                                        ? _c(
                                            "CButton",
                                            {
                                              staticClass:
                                                "btn btn-chat btn-chat-send",
                                              attrs: { color: "primary" },
                                              on: { click: _vm.updateMessage }
                                            },
                                            [_vm._v("Edit")]
                                          )
                                        : _c(
                                            "CButton",
                                            {
                                              staticClass:
                                                "btn btn-chat btn-chat-send",
                                              attrs: { color: "primary" },
                                              on: { click: _vm.addMessage }
                                            },
                                            [_vm._v("Send")]
                                          ),
                                      _vm._v(" "),
                                      _c(
                                        "CButton",
                                        {
                                          staticClass:
                                            "btn btn-chat btn-chat-cancel",
                                          attrs: { color: "primary" },
                                          on: { click: _vm.cancelEdit }
                                        },
                                        [_vm._v("Cancel")]
                                      )
                                    ],
                                    1
                                  )
                                ],
                                2
                              ),
                              _vm._v(" "),
                              _vm.inboxLoader
                                ? _c(
                                    "div",
                                    { staticClass: "inbox-loader-image" },
                                    [_c("img", { attrs: { src: _vm.loader } })]
                                  )
                                : _vm._e()
                            ])
                          ],
                          1
                        )
                      : _c("div", { staticClass: "chat-no-data" }, [
                          _c("p", [
                            _c("img", {
                              attrs: { src: "/assets/images/icon-chat.png" }
                            }),
                            _vm._v(" Start conversation.")
                          ])
                        ])
                  ])
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _vm.loading
        ? _c("div", { staticClass: "loader-image" }, [
            _c("img", { attrs: { src: _vm.loader } })
          ])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);