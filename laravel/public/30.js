(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[30],{

/***/ "../coreui/src/views/employees/Employees.vue":
/*!***************************************************!*\
  !*** ../coreui/src/views/employees/Employees.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Employees_vue_vue_type_template_id_836eef7e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Employees.vue?vue&type=template&id=836eef7e& */ "../coreui/src/views/employees/Employees.vue?vue&type=template&id=836eef7e&");
/* harmony import */ var _Employees_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Employees.vue?vue&type=script&lang=js& */ "../coreui/src/views/employees/Employees.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Employees_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Employees_vue_vue_type_template_id_836eef7e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Employees_vue_vue_type_template_id_836eef7e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/employees/Employees.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/employees/Employees.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ../coreui/src/views/employees/Employees.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Employees_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Employees.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/Employees.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Employees_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/employees/Employees.vue?vue&type=template&id=836eef7e&":
/*!**********************************************************************************!*\
  !*** ../coreui/src/views/employees/Employees.vue?vue&type=template&id=836eef7e& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Employees_vue_vue_type_template_id_836eef7e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Employees.vue?vue&type=template&id=836eef7e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/Employees.vue?vue&type=template&id=836eef7e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Employees_vue_vue_type_template_id_836eef7e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Employees_vue_vue_type_template_id_836eef7e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/Employees.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/employees/Employees.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "../coreui/node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Employees',
  data: function data() {
    return {
      items: [],
      currentPage: 1,
      perPage: 5,
      totalRows: 0,
      date_format: '',
      you: null,
      is_admin: false,
      message: '',
      loader: '/img/loader.gif',
      is_loading: false,
      showMessage: false,
      dismissSecs: 7,
      dismissCountDown: 0,
      showDismissibleAlert: false
    };
  },
  paginationProps: {
    align: 'center',
    doubleArrows: false,
    previousButtonHtml: 'prev',
    nextButtonHtml: 'next'
  },
  methods: {
    userLink: function userLink(id) {
      return "/employees/".concat(id.toString());
    },
    editLink: function editLink(id) {
      return "/employees/".concat(id.toString(), "/edit");
    },
    showUser: function showUser(id) {
      var userLink = this.userLink(id);
      this.$router.push({
        path: userLink
      });
    },
    editUser: function editUser(id) {
      var editLink = this.editLink(id);
      this.$router.push({
        path: editLink
      });
    },
    deleteUser: function deleteUser(uid) {
      var _this = this;

      var self = this;
      var id = uid;
      this.$swal.fire({
        title: 'Are you sure?',
        text: 'You can\'t revert your action',
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes',
        cancelButtonText: 'No',
        showCloseButton: true,
        showLoaderOnConfirm: true
      }).then(function (result) {
        if (result.value) {
          axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/users/' + id + '?token=' + localStorage.getItem("api_token"), {
            _method: 'DELETE'
          }).then(function (response) {
            self.message = 'Successfully deleted user.';
            self.showAlert();
            jQuery('#users_table').DataTable().destroy();
            self.getUsers();
          })["catch"](function (error) {
            console.log(error);
            self.$router.push({
              path: '/login'
            });
          });
        } else {
          _this.$swal('Cancelled', 'Your user is still intact.', 'info');
        }
      });
    },
    countDownChanged: function countDownChanged(dismissCountDown) {
      this.dismissCountDown = dismissCountDown;
    },
    showAlert: function showAlert() {
      this.dismissCountDown = this.dismissSecs;
    },
    dateFormat: function dateFormat($date) {
      // set date format.
      return moment__WEBPACK_IMPORTED_MODULE_1___default()($date).format(this.date_format);
    },
    getUsers: function getUsers() {
      var that = this;
      jQuery('#users_table').DataTable({
        "lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]],
        "order": [],
        processing: true,
        serverSide: true,
        ajax: '/api/users?token=' + localStorage.getItem("api_token"),
        columns: [// {data: 'DT_RowIndex' },
        {
          data: 'employee_id'
        }, // {data: 'name'},
        {
          data: 'name',
          name: 'name',
          render: function render(data, type, full, meta) {
            return '<a data="' + full.id + '" class="view-item-icon">' + full.name + '</a>';
          }
        }, {
          data: 'registered',
          name: 'registered',
          render: function render(data, type, full, meta) {
            return that.dateFormat(full.registered);
          }
        }, {
          data: 'roles'
        }, {
          data: 'status'
        }, {
          data: 'action',
          name: 'action',
          render: function render(data, type, full, meta) {
            var btn = '';

            if (full.deleted_at) {
              btn = '<b>Trashed</b>';
            } else {
              var _inner = '<a data="' + full.id + '" class="view-item-icon dropdown-item"><img src="/img/view-icon.png" alt="view-icn"/> View</a>';

              _inner = _inner + '<a data="' + full.id + '" class="edit-item-icon dropdown-item text-primary"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" role="img" class="c-icon"><path d="M22.029 1.971c-0.754-0.754-1.795-1.22-2.944-1.22s-2.191 0.466-2.944 1.22l-12.773 12.773-2.258 6.657c-0.040 0.113-0.063 0.243-0.063 0.378 0 0.323 0.132 0.616 0.344 0.827l0.004 0.004c0.211 0.212 0.503 0.344 0.826 0.344h0c0.136-0 0.266-0.023 0.388-0.066l-0.008 0.003 6.657-2.258 12.773-12.773c0.754-0.754 1.22-1.795 1.22-2.944s-0.466-2.191-1.22-2.944v0zM8.443 19.325l-5.702 1.934 1.934-5.702 9.785-9.785 3.767 3.767zM20.969 6.799l-1.68 1.68-3.767-3.767 1.68-1.68c0.482-0.483 1.149-0.783 1.886-0.783 1.471 0 2.664 1.193 2.664 2.664 0 0.737-0.299 1.404-0.783 1.886l-0 0z"></path></svg> Edit</a>';

              if (full.you !== full.id) {
                _inner = _inner + '<a data="' + full.id + '" class="delete-item-icon dropdown-item text-danger"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" role="img" class="c-icon"><path d="M4.5 22.125c-0 0.003-0 0.006-0 0.008 0 0.613 0.494 1.11 1.105 1.116h12.79c0.612-0.006 1.105-0.504 1.105-1.117 0-0.003-0-0.006-0-0.009v0-15h-15zM6 8.625h12v13.125h-12z"></path><path d="M7.875 10.125h1.5v9.375h-1.5v-9.375z"></path><path d="M11.25 10.125h1.5v9.375h-1.5v-9.375z"></path><path d="M14.625 10.125h1.5v9.375h-1.5v-9.375z"></path><path d="M15.375 4.125v-2.25c0-0.631-0.445-1.125-1.013-1.125h-4.725c-0.568 0-1.013 0.494-1.013 1.125v2.25h-5.625v1.5h18v-1.5zM10.125 2.25h3.75v1.875h-3.75z"></path></svg> Delete</a>';
                _inner = _inner + '<a data="' + full.id + '" class="download-item-icon dropdown-item text-info"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" role="img" class="c-icon" data-v-6a0024b2=""><path d="M12.75 20.379v-10.573h-1.5v10.573l-2.432-2.432-1.061 1.061 4.243 4.243 4.243-4.243-1.061-1.061-2.432 2.432z"></path><path d="M18.75 7.555c0-3.722-3.028-6.75-6.75-6.75s-6.75 3.028-6.75 6.75c-2.485 0-4.5 2.015-4.5 4.5s2.015 4.5 4.5 4.5h3.75v-1.5h-3.75c-1.657 0-3-1.343-3-3s1.343-3 3-3v0h1.5v-1.5c0-2.899 2.351-5.25 5.25-5.25s5.25 2.351 5.25 5.25v0 1.5h1.5c1.657 0 3 1.343 3 3s-1.343 3-3 3v0h-3.75v1.5h3.75c2.485 0 4.5-2.015 4.5-4.5s-2.015-4.5-4.5-4.5v0z"></path></svg> Documents</a>';
              }

              btn = '<div class="custom-list-dropdown"><div class="dropdown"><p class="dropdown-toggle pointer text-center" id="listusrs' + full.id + '" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="/img/drop-icon.svg" /></p><div class="dropdown-menu" aria-labelledby="listusrs' + full.id + '">' + _inner + '</div></div></div>';
            }

            return btn;
          },
          orderable: false,
          searchable: false
        }],
        "language": {
          "processing": '<div class="loader-image"><img src="' + that.loader + '"></div>'
        },
        "dom": '<"top table-search-flds d-flex align-items-center justify-content-between"fl>rt<"bottom table-paginater"ip><"clear">'
      }); // trigger click functions.

      jQuery(document).on('click', '.view-item-icon', function () {
        var id = this.getAttribute('data');
        that.showUser(id);
      });
      jQuery(document).on('click', '.edit-item-icon', function () {
        var id = this.getAttribute('data');
        that.editUser(id);
      });
      jQuery(document).on('click', '.delete-item-icon', function () {
        var id = this.getAttribute('data');
        that.deleteUser(id);
      });
      jQuery(document).on('click', '.download-item-icon', function () {
        var id = this.getAttribute('data');
        that.downloadZip(id);
      });
    },
    createUser: function createUser() {
      this.$router.push({
        path: 'employees/create'
      });
    },
    downloadZip: function downloadZip(id) {
      var self = this;
      self.is_loading = true;
      var formData = new FormData();
      formData.append('id', id);
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/users/zip-files', formData).then(function (response) {
        if (response.data.error) {
          self.$swal('Warning', 'User has no files.', 'warning');
          self.is_loading = false;
        } else {
          self.is_loading = false;

          if (response.data) {
            window.location = response.data;
          } else {
            self.$swal('Warning', 'User has no files.', 'warning');
          }
        }
      });
    }
  },
  mounted: function mounted() {
    this.date_format = localStorage.getItem("date_format");

    if (localStorage.getItem("api_token")) {
      this.getUsers();
    }

    this.is_admin = JSON.parse(localStorage.getItem('type'));
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/Employees.vue?vue&type=template&id=836eef7e&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/employees/Employees.vue?vue&type=template&id=836eef7e& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", xl: "12" } },
        [
          _c(
            "transition",
            { attrs: { name: "slide" } },
            [
              _c(
                "CCard",
                [
                  _c(
                    "CCardHeader",
                    {
                      staticClass:
                        "d-flex justify-content-between align-items-center"
                    },
                    [
                      _c("h4", { staticClass: "mb-0" }, [_vm._v("Employees")]),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        {
                          attrs: { color: "primary" },
                          on: {
                            click: function($event) {
                              return _vm.createUser()
                            }
                          }
                        },
                        [
                          _c("CIcon", { attrs: { name: "cil-plus" } }),
                          _vm._v(" Create Employee")
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "CCardBody",
                    [
                      _c(
                        "CAlert",
                        {
                          attrs: {
                            show: _vm.dismissCountDown,
                            color: "primary",
                            fade: ""
                          },
                          on: {
                            "update:show": function($event) {
                              _vm.dismissCountDown = $event
                            }
                          }
                        },
                        [
                          _vm._v(
                            "\n\t\t\t\t\t\t" +
                              _vm._s(_vm.message) +
                              "\n\t\t\t\t\t"
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "table-responsive" }, [
                        _c(
                          "table",
                          {
                            staticClass:
                              "table table-manage-user table-striped",
                            attrs: { id: "users_table" }
                          },
                          [
                            _c("thead", [
                              _c("tr", [
                                _c("th", { attrs: { scope: "col" } }, [
                                  _vm._v("Employee ID")
                                ]),
                                _vm._v(" "),
                                _c("th", { attrs: { scope: "col" } }, [
                                  _vm._v("Name")
                                ]),
                                _vm._v(" "),
                                _c("th", { attrs: { scope: "col" } }, [
                                  _vm._v("Registered")
                                ]),
                                _vm._v(" "),
                                _c("th", { attrs: { scope: "col" } }, [
                                  _vm._v("Roles")
                                ]),
                                _vm._v(" "),
                                _c("th", { attrs: { scope: "col" } }, [
                                  _vm._v("Status")
                                ]),
                                _vm._v(" "),
                                _c("th", { attrs: { scope: "col" } }, [
                                  _vm._v("Action")
                                ])
                              ])
                            ]),
                            _vm._v(" "),
                            _c("tbody")
                          ]
                        )
                      ])
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _vm.is_loading
        ? _c("div", { staticClass: "loader-image" }, [
            _c("img", { attrs: { src: _vm.loader } })
          ])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);