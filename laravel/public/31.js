(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[31],{

/***/ "../coreui/src/views/leaves/EditLeave.vue":
/*!************************************************!*\
  !*** ../coreui/src/views/leaves/EditLeave.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EditLeave_vue_vue_type_template_id_e86fe0f8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EditLeave.vue?vue&type=template&id=e86fe0f8& */ "../coreui/src/views/leaves/EditLeave.vue?vue&type=template&id=e86fe0f8&");
/* harmony import */ var _EditLeave_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EditLeave.vue?vue&type=script&lang=js& */ "../coreui/src/views/leaves/EditLeave.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EditLeave_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EditLeave_vue_vue_type_template_id_e86fe0f8___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EditLeave_vue_vue_type_template_id_e86fe0f8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/leaves/EditLeave.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/leaves/EditLeave.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ../coreui/src/views/leaves/EditLeave.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_EditLeave_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./EditLeave.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/leaves/EditLeave.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_EditLeave_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/leaves/EditLeave.vue?vue&type=template&id=e86fe0f8&":
/*!*******************************************************************************!*\
  !*** ../coreui/src/views/leaves/EditLeave.vue?vue&type=template&id=e86fe0f8& ***!
  \*******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_EditLeave_vue_vue_type_template_id_e86fe0f8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./EditLeave.vue?vue&type=template&id=e86fe0f8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/leaves/EditLeave.vue?vue&type=template&id=e86fe0f8&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_EditLeave_vue_vue_type_template_id_e86fe0f8___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_EditLeave_vue_vue_type_template_id_e86fe0f8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/leaves/EditLeave.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/leaves/EditLeave.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Edittask',
  data: function data() {
    return {
      alertClass: false,
      is_loading: false,
      loader: '/img/loader.gif',
      status: '',
      reason: '',
      message: '',
      dismissCountDown: 0
    };
  },
  components: {},
  methods: {
    mailFeedback: function mailFeedback() {
      var self = this;
      self.is_loading = true;

      var _id = atob(self.$route.params.id);

      var formData = new FormData();
      formData.append('id', _id);
      formData.append('status', self.status);
      formData.append('reason', self.reason);
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/leaves/edit', formData).then(function (response) {
        self.is_loading = false;
      });
    },
    goBack: function goBack() {
      this.$router.back();
    }
  },
  mounted: function mounted() {
    var self = this;
    this.loading = true;

    var _id = atob(self.$route.params.id);

    axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/leaves/view/' + _id + '?token=' + localStorage.getItem("api_token")).then(function (response) {
      self.loading = false;
      var leave = response.data.leave;
      self.status = leave.status;
      self.reason = leave.if_reason;
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/leaves/EditLeave.vue?vue&type=template&id=e86fe0f8&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/leaves/EditLeave.vue?vue&type=template&id=e86fe0f8& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", lg: "12" } },
        [
          _c(
            "CCard",
            { attrs: { "no-header": "" } },
            [
              _c(
                "CCardBody",
                [
                  _c(
                    "h3",
                    [
                      _vm._v(" Edit Leave - "),
                      _c(
                        "CButton",
                        {
                          staticClass: "float-right",
                          attrs: { color: "primary" },
                          on: { click: _vm.goBack }
                        },
                        [_vm._v("Back")]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "CAlert",
                    {
                      attrs: {
                        show: _vm.dismissCountDown,
                        color: _vm.alertClass ? "success" : "danger",
                        fade: ""
                      },
                      on: {
                        "update:show": function($event) {
                          _vm.dismissCountDown = $event
                        }
                      }
                    },
                    [
                      _vm._v(
                        "\n\t\t            \t(" +
                          _vm._s(_vm.dismissCountDown) +
                          ") " +
                          _vm._s(_vm.message) +
                          "\n\t\t          \t"
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "CForm",
                    {
                      attrs: { method: "POST" },
                      on: {
                        submit: function($event) {
                          $event.preventDefault()
                          return _vm.mailFeedback($event)
                        }
                      }
                    },
                    [
                      _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "col-md-12" }, [
                          _c("div", { staticClass: "form-group" }, [
                            _c("label", [_vm._v("Status *")]),
                            _vm._v(" "),
                            _c(
                              "select",
                              {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.status,
                                    expression: "status"
                                  }
                                ],
                                staticClass: "form-control",
                                on: {
                                  change: function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.status = $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  }
                                }
                              },
                              [
                                _c("option", { attrs: { value: "" } }, [
                                  _vm._v("--Select Status --")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "0" } }, [
                                  _vm._v("Pending")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "1" } }, [
                                  _vm._v("Approve")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "2" } }, [
                                  _vm._v("Reject")
                                ])
                              ]
                            )
                          ])
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "col-md-12" },
                          [
                            _c("CTextarea", {
                              attrs: {
                                type: "text",
                                label: "Reason",
                                rows: "10",
                                placeholder: "Reason"
                              },
                              model: {
                                value: _vm.reason,
                                callback: function($$v) {
                                  _vm.reason = $$v
                                },
                                expression: "reason"
                              }
                            })
                          ],
                          1
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        {
                          staticClass: "mt-3",
                          attrs: { color: "primary", type: "submit" }
                        },
                        [_vm._v("Send")]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _vm.is_loading
        ? _c("div", { staticClass: "loader-image" }, [
            _c("img", { attrs: { src: _vm.loader } })
          ])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);