
function createEditor(id) {
	console.log(ClassicEditor)
	ClassicEditor.create( document.getElementById( id ) )
}


jQuery(document).on('click', '.show-img-full img', function() {
	var div = document.getElementById('file_downloader');
	div.classList.remove('hide');

});

jQuery(document).on('click', '.viewer-button.viewer-close', function() {
	var div = document.getElementById('file_downloader');
	div.classList.add('hide');
});

// download file on button click.
jQuery(document).on('click', '#download_btn_opt', function() {
	var canv = document.querySelector('.viewer-in .viewer-canvas');
	var url = canv.querySelector('img').src;

	this.href = url;
});


jQuery(document).on('click', '.viewer-container.viewer-backdrop', function() {
	const that = this;
	setTimeout(function() {
		if(!that.classList.contains('viewer-in')) {
			var div = document.getElementById('file_downloader');
			div.classList.add('hide');
		}
	}, 400);
});

var hide = true;
jQuery('body').on("click", function () {
    if (hide) $('.drive-box').removeClass('drive-active');
    hide = true;
});
// add and remove .active
jQuery('body').on('click', '.drive-box', function () {
    var self = jQuery(this);
    if (self.hasClass('drive-active')) {
        jQuery('.drive-box').removeClass('drive-active');
        return false;
    }
    jQuery('.drive-box').removeClass('drive-active');
    self.toggleClass('drive-active');
    hide = false;
});
// add and remove .active
jQuery('body').on('dragover', '.drive-upload .modal-content', function () {
	 jQuery('.drive-upload .outer-drag-drop').css('display','flex')

});

/// insertion image/docs from dropzone
jQuery(document).ready(function(){
	jQuery(document).on("click",".dz-details",function(){
		 var that = this;
		 var image_src = "";
		 var image_format = "";
		 var file_name = "";
		 var $txt = jQuery(document).find(".dx-htmleditor-content");
		 image_src = jQuery(this).parent().find(".dz-image").find("img").attr("data-uri");
		 image_format = jQuery(this).parent().find(".dz-image").find("img").attr("data-format");
		 file_name = jQuery(this).parent().find(".dz-image").find("img").attr("data-file_name");

		 if(image_src != undefined){
		 	/*if(image_format == "image"){
    		 var clas = '.dx-image-format';
    		}else{
    		  var clas = '.dx-link-format';
    		} 
  		     document.querySelector(clas).click();*/
  		    if(image_format == "image"){
    		   jQuery(this).parent().parent().parent().parent().parent().find(".dx-image-format").click();
    		}else{
    		   jQuery(this).parent().parent().parent().parent().parent().find(".dx-link-format").click();
    		} 

  		     setTimeout(function(){
				   var copyText1 = jQuery(".dx-popup-content").find("input")[0];
				   copyText1.value = "";
				   copyText1.focus();
				   document.execCommand('insertText', false, image_src);
				   if(image_format != "image"){

				   	 var copyText2 = jQuery(".dx-popup-content").find("input")[1];
				   	 copyText2.value = "";
				     copyText2.focus();
				     document.execCommand('insertText', false, file_name);
                     
				   }
  		     },100)
  		 }    
	})
})
jQuery(document).on('click', '.comment-section-detail a', function(e) { 
	e.preventDefault(); 
	window.open(this.href, '_blank');
})
// createEditor('customEditor');
