(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[9],{

/***/ "../coreui/src/views/Dashboard.vue":
/*!*****************************************!*\
  !*** ../coreui/src/views/Dashboard.vue ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Dashboard_vue_vue_type_template_id_78f2734c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=template&id=78f2734c& */ "../coreui/src/views/Dashboard.vue?vue&type=template&id=78f2734c&");
/* harmony import */ var _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=script&lang=js& */ "../coreui/src/views/Dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Dashboard_vue_vue_type_template_id_78f2734c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Dashboard_vue_vue_type_template_id_78f2734c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/Dashboard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/Dashboard.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ../coreui/src/views/Dashboard.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/Dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/Dashboard.vue?vue&type=template&id=78f2734c&":
/*!************************************************************************!*\
  !*** ../coreui/src/views/Dashboard.vue?vue&type=template&id=78f2734c& ***!
  \************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_78f2734c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=template&id=78f2734c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/Dashboard.vue?vue&type=template&id=78f2734c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_78f2734c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_78f2734c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "../coreui/src/views/dashboard/DashboardSidebar.vue":
/*!**********************************************************!*\
  !*** ../coreui/src/views/dashboard/DashboardSidebar.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _DashboardSidebar_vue_vue_type_template_id_ad75a826___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./DashboardSidebar.vue?vue&type=template&id=ad75a826& */ "../coreui/src/views/dashboard/DashboardSidebar.vue?vue&type=template&id=ad75a826&");
/* harmony import */ var _DashboardSidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./DashboardSidebar.vue?vue&type=script&lang=js& */ "../coreui/src/views/dashboard/DashboardSidebar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _DashboardSidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _DashboardSidebar_vue_vue_type_template_id_ad75a826___WEBPACK_IMPORTED_MODULE_0__["render"],
  _DashboardSidebar_vue_vue_type_template_id_ad75a826___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/dashboard/DashboardSidebar.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/dashboard/DashboardSidebar.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ../coreui/src/views/dashboard/DashboardSidebar.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardSidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./DashboardSidebar.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/dashboard/DashboardSidebar.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardSidebar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/dashboard/DashboardSidebar.vue?vue&type=template&id=ad75a826&":
/*!*****************************************************************************************!*\
  !*** ../coreui/src/views/dashboard/DashboardSidebar.vue?vue&type=template&id=ad75a826& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardSidebar_vue_vue_type_template_id_ad75a826___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./DashboardSidebar.vue?vue&type=template&id=ad75a826& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/dashboard/DashboardSidebar.vue?vue&type=template&id=ad75a826&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardSidebar_vue_vue_type_template_id_ad75a826___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_DashboardSidebar_vue_vue_type_template_id_ad75a826___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/Dashboard.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/Dashboard.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "../coreui/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "../coreui/node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ "../coreui/node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! devextreme-vue/html-editor */ "../coreui/node_modules/devextreme-vue/html-editor.js");
/* harmony import */ var devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _dashboard_DashboardSidebar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./dashboard/DashboardSidebar */ "../coreui/src/views/dashboard/DashboardSidebar.vue");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! firebase/app */ "../coreui/node_modules/firebase/app/dist/index.esm.js");
/* harmony import */ var firebase_messaging__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! firebase/messaging */ "../coreui/node_modules/firebase/messaging/dist/index.esm.js");
/* harmony import */ var vue2_dropzone__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! vue2-dropzone */ "../coreui/node_modules/vue2-dropzone/dist/vue2Dropzone.js");
/* harmony import */ var vue2_dropzone__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(vue2_dropzone__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! vue2-dropzone/dist/vue2Dropzone.min.css */ "../coreui/node_modules/vue2-dropzone/dist/vue2Dropzone.min.css");
/* harmony import */ var vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(vue2_dropzone_dist_vue2Dropzone_min_css__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var viewerjs_dist_viewer_css__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! viewerjs/dist/viewer.css */ "../coreui/node_modules/viewerjs/dist/viewer.css");
/* harmony import */ var viewerjs_dist_viewer_css__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(viewerjs_dist_viewer_css__WEBPACK_IMPORTED_MODULE_10__);
/* harmony import */ var v_viewer__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! v-viewer */ "../coreui/node_modules/v-viewer/dist/v-viewer.js");
/* harmony import */ var v_viewer__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(v_viewer__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var vue_popperjs__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! vue-popperjs */ "../coreui/node_modules/vue-popperjs/dist/vue-popper.min.js");
/* harmony import */ var vue_popperjs__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(vue_popperjs__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var vue_popperjs_dist_vue_popper_css__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! vue-popperjs/dist/vue-popper.css */ "../coreui/node_modules/vue-popperjs/dist/vue-popper.css");
/* harmony import */ var vue_popperjs_dist_vue_popper_css__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(vue_popperjs_dist_vue_popper_css__WEBPACK_IMPORTED_MODULE_13__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





var uuid = __webpack_require__(/*! uuid */ "../coreui/node_modules/uuid/dist/esm-browser/index.js");








vue__WEBPACK_IMPORTED_MODULE_1___default.a.use(v_viewer__WEBPACK_IMPORTED_MODULE_11___default.a);


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Dashboard',
  components: {
    DxHtmlEditor: devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_4__["DxHtmlEditor"],
    DxToolbar: devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_4__["DxToolbar"],
    DxItem: devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_4__["DxItem"],
    DashboardSidebar: _dashboard_DashboardSidebar__WEBPACK_IMPORTED_MODULE_5__["default"],
    vueDropzone: vue2_dropzone__WEBPACK_IMPORTED_MODULE_8___default.a,
    popper: vue_popperjs__WEBPACK_IMPORTED_MODULE_12___default.a
  },
  data: function data() {
    return {
      feed: '',
      _current: '',
      latestComment: '',
      moreComment: false,
      targetComment: [],
      _ttEl: '',
      quoteText: '',
      edit_feed: '',
      projects: [],
      quoteTextTime: 0,
      likeCount: 0,
      page: 1,
      feed_ct: 0,
      commentConunt: 0,
      comment_editor: [],
      edit_name: [],
      edit_files: [],
      _ky: '0',
      user_name: '',
      user_profile: '',
      search: '',
      date_format: '',
      time_for: '',
      avtar: '/img/user-avtar.jpg',
      loader: '/img/loader.gif',
      feeds: [],
      tasks: [],
      user: [],
      files: [],
      total_comment: 0,
      mentions: [],
      sidebar_data: [],
      is_shown: false,
      is_edit: false,
      latest: false,
      is_loading: false,
      is_admin: false,
      has_permission: false,
      birthdays: [],
      messaging: firebase_app__WEBPACK_IMPORTED_MODULE_6__["default"].messaging(),
      show_editor: false,
      dropzoneOptions: {
        url: "https://httpbin.org/post",
        thumbnailWidth: 150,
        thumbnailHeight: 150,
        addRemoveLinks: true,
        dictDefaultMessage: "<p class='text-default dropbox-def-txt'><img src=\"/img/cloud-download.png\" /> Upload Images or Files Here.</p>"
      },
      images: [],
      loading: false,
      paginator: 1
    };
  },
  methods: {
    showEditor: function showEditor() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                if (_this.show_editor) {
                  _this.show_editor = false;
                } else {
                  _this.show_editor = true;
                }

                _this.feed = "";
                _this.images = [];
                _this.edit_name = [];
                _this.is_edit = false;
                _this.edit_files = [];
                _this.edit_feed = '';

                if (_this.$refs.imgDropZone) {
                  _this.$refs.imgDropZone.removeAllFiles();
                }

                _this.setArrFields();

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    showCommentEditor: function showCommentEditor(k) {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var i, _field, _editor;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                for (i = 0; i < _this2.feeds.length; i++) {
                  document.querySelector('.commentfiels-' + i).classList.remove('hide');
                  document.querySelector('.commenteditor-' + i).classList.add('hide');
                  document.querySelector('.cust-dropzone-' + i).classList.add('hide');
                  _this2.comment_editor[i] = "";
                }

                _this2.images = [];

                _this2.$refs.commDropZone[k].removeAllFiles();

                _field = document.querySelector('.commentfiels-' + k), _editor = document.querySelector('.commenteditor-' + k);

                _field.classList.add('hide');

                _editor.classList.remove('hide');

              case 6:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    hideCommentEditor: function hideCommentEditor(k) {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var _field, _editor;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _this3.comment_editor[k] = [];
                _this3.images = [];

                _this3.$refs.commDropZone[k].removeAllFiles();

                _field = document.querySelector('.commentfiels-' + k), _editor = document.querySelector('.commenteditor-' + k);

                _field.classList.remove('hide');

                _editor.classList.add('hide');

              case 6:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    selectOption: function selectOption(e, type) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var div, clas;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                div = document.querySelector('.editor-module'), clas = '.dx-' + type + '-format';
                document.querySelector(clas).click();

              case 2:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }))();
    },
    isValidImageURL: function isValidImageURL(str) {
      if (typeof str !== 'string') return false;
      return !!str.match(/\w+\.(jpg|jpeg|gif|png|tiff|bmp|webp)$/gi);
    },
    getValidName: function getValidName(file) {
      var val = file.split('/');
      return val.slice(-1).pop();
    },
    makeEdit: function makeEdit(item, k) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }))();
    },
    deleteComment: function deleteComment(id, comp, k) {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        var that;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                that = _this4;
                _this4._ttEl = event.target;

                _this4.$swal.fire({
                  title: 'Are you sure?',
                  text: 'You can\'t revert your action',
                  type: 'warning',
                  showCancelButton: true,
                  confirmButtonText: 'Yes',
                  cancelButtonText: 'No',
                  showCloseButton: true,
                  showLoaderOnConfirm: true
                }).then(function (result) {
                  if (result.value) {
                    that.is_loading = true;
                    var formdata = new FormData();
                    formdata.append('id', id);
                    formdata.append('component_id', comp);
                    formdata.append('component', 'feed');
                    formdata.append('pager', that.page);
                    axios__WEBPACK_IMPORTED_MODULE_2___default.a.post('/api/comment/delete?token=' + localStorage.getItem("api_token"), formdata).then(function (response) {
                      jQuery(that._ttEl).parents().eq(4).remove();
                      that.is_loading = false; // that.feeds = response.data.feeds.feeds;
                      // that.feed_ct = response.data.feeds.counts;

                      that.setArrFields();
                    });
                  } else {
                    _this4.$swal('Cancelled', 'Your comment is still intact.', 'info');
                  }
                });

              case 3:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6);
      }))();
    },
    shouldDisplay: function shouldDisplay(item) {
      return this.is_admin || this._current == item.user_id;
    },
    shouldDisplayComm: function shouldDisplayComm(item) {
      return this.is_admin || this._current == item.user;
    },
    showCommDropbox: function showCommDropbox(k) {
      for (var i = 0; i < this.feeds.length; i++) {
        document.querySelector('.cust-dropzone-' + i).classList.add('hide');
      }

      var _fld = document.querySelector('.cust-dropzone-' + k);

      _fld.classList.remove('hide');
    },
    selectCommOption: function selectCommOption(k, type) {
      var div = document.querySelector('.editor-mod-' + k),
          clas = '.dx-' + type + '-format';
      div.querySelector(clas).click();
    },
    loadMore: function loadMore() {
      var _this5 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _this5.page = _this5.page + 1;

                _this5.makeSearch('more');

              case 2:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7);
      }))();
    },
    dateFormat: function dateFormat(value) {
      var date = moment__WEBPACK_IMPORTED_MODULE_3___default()(value).format('MMMM D, Y'),
          val = '',
          time = moment__WEBPACK_IMPORTED_MODULE_3___default()(value).format(this.time_for);

      if (moment__WEBPACK_IMPORTED_MODULE_3___default()().diff(date, 'days') === 0) {
        val = 'Today, ' + time;
      } else if (moment__WEBPACK_IMPORTED_MODULE_3___default()().diff(date, 'days') === 1) {
        val = 'Yesterday, ' + time;
      } else {
        val = moment__WEBPACK_IMPORTED_MODULE_3___default()(value).format(this.date_format);
      }

      return val;
    },
    showDropbox: function showDropbox(e) {
      var _this6 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee8() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                if (_this6.is_shown) {
                  _this6.is_shown = false;
                } else {
                  _this6.is_shown = true;
                }

              case 1:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8);
      }))();
    },
    selectDashTrigger: function selectDashTrigger(e) {
      var text = window.getSelection().toString();

      if (text) {
        var _tar = e.target.closest('.listing-section');

        if (typeof _tar !== 'undefined' && _tar !== null) {
          if (_tar.getAttribute('data')) {
            this._ky = _tar.getAttribute('data');
          }
        }

        var _cls = '.make-quote-' + this._ky;

        if (e.target.closest(_cls) !== null) {
          this.quoteText = text;
          this.quoteTextTime = 0;
          var tooltip = new TextTip({
            scope: _cls,
            iconFormat: 'font',
            buttons: [{
              title: 'quote',
              icon: "/img/quote.png",
              callback: this.makeDashQuote
            }]
          });
        }
      }
    },
    makeDashQuote: function makeDashQuote() {
      if (this.quoteTextTime == 0) {
        var ind = parseInt(this._ky);
        var doc = '<blockquote>' + this.quoteText + '</blockquote><div><br /></div>';
        var comment = typeof this.comment_editor[ind] !== 'undefined' ? this.comment_editor[ind].concat(doc) : '';
        this.showCommentEditor(ind);
        this.comment_editor[ind] = comment;
        this.is_loading = true;
        this.quoteTextTime = 1;
        this.is_loading = false;
        this.$refs.commDropZone[ind].$el.focus();
      }
    },
    editableFiles: function editableFiles(value) {
      this.edit_name = [];

      if (value) {
        var files = JSON.parse(value);
        this.edit_files = files;

        for (var a = 0; a < files.length; a++) {
          var val = files[a].split('/'),
              obj = {};
          obj.name = val[val.length - 1];
          obj.file = files[a];
          this.edit_name.push(obj);
        }
      }
    },
    removeIndex: function removeIndex(file) {
      var _this7 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee9() {
        var that;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee9$(_context9) {
          while (1) {
            switch (_context9.prev = _context9.next) {
              case 0:
                that = _this7;

                _this7.edit_files.forEach(function (item, i) {
                  if (item === file) {
                    that.edit_files.splice(i, 1);

                    for (var a = 0; a < that.edit_name.length; a++) {
                      if (that.edit_name[a].file === file) {
                        that.edit_name.splice(a, 1);
                      }
                    }
                  }
                });

              case 2:
              case "end":
                return _context9.stop();
            }
          }
        }, _callee9);
      }))();
    },
    afterComplete: function afterComplete(upload) {
      var _this8 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee10() {
        var imageName, that, file, extension, _id, data_file, formdata;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee10$(_context10) {
          while (1) {
            switch (_context10.prev = _context10.next) {
              case 0:
                imageName = uuid.v1();
                _this8.isLoading = true;
                that = _this8;

                try {
                  file = upload; // that.images.push(file);

                  extension = file.name.split('.').pop().toLowerCase();
                  _id = file.previewElement.parentElement.parentElement.getElementsByClassName("vue-dropzone")[0].getAttribute("data_id");
                  data_file = file;
                  that.is_loading = true;
                  formdata = new FormData();
                  formdata.append('data_file', data_file);
                  formdata.append('extension', extension);
                  formdata.append('_comp', "feed");
                  formdata.append('_id', _id);
                  formdata.append('_file_name', file.name);
                  formdata.append('format', file.type.split('/').shift().toLowerCase());
                  axios__WEBPACK_IMPORTED_MODULE_2___default.a.post('/api/add_pic?token=' + localStorage.getItem("api_token"), formdata).then(function (response) {
                    that.images.push(response.data.src);
                    that.is_loading = false;
                    upload.previewElement.getElementsByClassName("dz-image")[0].children[0].setAttribute("data-uri", response.data.src);
                    upload.previewElement.getElementsByClassName("dz-image")[0].children[0].setAttribute("data-format", response.data.format);
                    upload.previewElement.getElementsByClassName("dz-image")[0].children[0].setAttribute("data-file_name", response.data.file_name); // jQuery(".dz-image").last().find("img").attr("data-uri",response.data.src);
                  })["catch"](function (error) {
                    console.log("Unable to process.");
                  });
                } catch (error) {
                  console.log(error);
                }

              case 4:
              case "end":
                return _context10.stop();
            }
          }
        }, _callee10);
      }))();
    },
    removeThisFile: function removeThisFile(file) {
      var _this9 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee11() {
        var _filePath, that, _id;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee11$(_context11) {
          while (1) {
            switch (_context11.prev = _context11.next) {
              case 0:
                _filePath = event.target.parentElement.querySelector('img').getAttribute('data-uri');
                that = _this9, _id = file.upload.uuid;

                _this9.images.forEach(function (item, i) {
                  if (item === _filePath) {
                    that.images.splice(i, 1);
                  }
                });

              case 3:
              case "end":
                return _context11.stop();
            }
          }
        }, _callee11);
      }))();
    },
    makeSearch: function makeSearch(_type) {
      var _this10 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee12() {
        var that, formdata;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee12$(_context12) {
          while (1) {
            switch (_context12.prev = _context12.next) {
              case 0:
                that = _this10;
                formdata = new FormData();
                _this10.is_loading = true;
                formdata.append('search', _this10.search);
                formdata.append('page', _this10.page);
                axios__WEBPACK_IMPORTED_MODULE_2___default.a.post('/api/feeds/search?token=' + localStorage.getItem("api_token"), formdata).then(function (response) {
                  that.is_loading = false;
                  var data = response.data;
                  that.feeds = data.feeds;
                  that.feed_ct = data.feeds.length;
                  that.setArrFields();
                });

              case 6:
              case "end":
                return _context12.stop();
            }
          }
        }, _callee12);
      }))();
    },
    emptySearch: function emptySearch() {
      var _this11 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee13() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee13$(_context13) {
          while (1) {
            switch (_context13.prev = _context13.next) {
              case 0:
                _this11.search = '';

                _this11.makeSearch('empty');

              case 2:
              case "end":
                return _context13.stop();
            }
          }
        }, _callee13);
      }))();
    },
    parseVal: function parseVal(comment) {
      var parser = new DOMParser();
      var htmlDoc = parser.parseFromString(comment, "text/html");
      var value = htmlDoc.getElementsByClassName('dx-mention');
      var data = [];

      for (var i = 0; i < value.length; i++) {
        data[i] = value[i].getAttribute('data-id');
      }

      return data;
    },
    addComment: function addComment(k, id) {
      var _this12 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee14() {
        var that, formdata, comment, mentions, i, file;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee14$(_context14) {
          while (1) {
            switch (_context14.prev = _context14.next) {
              case 0:
                that = _this12;
                _this12._ttEl = event.target;
                _this12.is_loading = true;
                formdata = new FormData();
                comment = _this12.comment_editor[k];
                mentions = _this12.parseVal(comment);
                formdata.append('component_id', id);
                formdata.append('component', 'feed');
                formdata.append('comment', JSON.stringify(comment));
                formdata.append('mentioned', mentions);
                formdata.append('pager', _this12.page);

                for (i = 0; i < _this12.images.length; i++) {
                  file = _this12.images[i];
                  formdata.append('files[' + i + ']', file);
                }

                axios__WEBPACK_IMPORTED_MODULE_2___default.a.post('/api/comment/add?token=' + localStorage.getItem("api_token"), formdata).then(function (response) {
                  that.is_loading = false;
                  that.latestComment = response.data.latestComment;
                  setTimeout(function () {
                    jQuery(that._ttEl).parents().eq(3).find('.comment-section-values').append(jQuery('#record-latest-comment').html());
                    that.latestComment = "";
                  }, 100); // that.feeds = response.data.feeds.feeds;
                  // that.feed_ct = response.data.feeds.counts;

                  that.images = [];
                  that.$refs.commDropZone[k].removeAllFiles();
                  that.comment_editor = [];
                  that.hideCommentEditor(k);
                  that.setArrFields();
                });

              case 13:
              case "end":
                return _context14.stop();
            }
          }
        }, _callee14);
      }))();
    },
    makeLike: function makeLike(_id, component) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee15() {
        var e, formdata;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee15$(_context15) {
          while (1) {
            switch (_context15.prev = _context15.next) {
              case 0:
                e = event.target;
                formdata = new FormData();
                formdata.append('component_id', _id);
                formdata.append('component', component);
                axios__WEBPACK_IMPORTED_MODULE_2___default.a.post('/api/like?token=' + localStorage.getItem("api_token"), formdata).then(function (response) {
                  if (response.data.status == 'success') {
                    if (e.tagName.toLowerCase() === "a") {
                      e.classList.add('active');
                      e.querySelector('span').innerText = response.data.counts;
                    } else {
                      e.closest('a').classList.add('active');
                      e.closest('a').querySelector('span').innerText = response.data.counts;
                    }
                  } else {
                    if (e.tagName.toLowerCase() === "a") {
                      e.classList.remove('active');
                      e.querySelector('span').innerText = response.data.counts;
                    } else {
                      e.closest('a').classList.remove('active');
                      e.closest('a').querySelector('span').innerText = response.data.counts;
                    }
                  }

                  e.parentElement.parentElement.parentElement.getElementsByClassName("like_box_count")[0].innerText = response.data.counts;
                  e.parentElement.parentElement.parentElement.getElementsByClassName("likeuser-section-values")[0].innerHTML = response.data.likefeed_users;
                });

              case 5:
              case "end":
                return _context15.stop();
            }
          }
        }, _callee15);
      }))();
    },
    addFeed: function addFeed() {
      var _this13 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee16() {
        var that, formdata, i, file;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee16$(_context16) {
          while (1) {
            switch (_context16.prev = _context16.next) {
              case 0:
                that = _this13;
                _this13.is_loading = true;
                formdata = new FormData();
                formdata.append('feed', _this13.feed);
                formdata.append('page', _this13.page);

                for (i = 0; i < _this13.images.length; i++) {
                  file = _this13.images[i];
                  formdata.append('files[' + i + ']', file);
                }

                axios__WEBPACK_IMPORTED_MODULE_2___default.a.post('/api/feeds/add?token=' + localStorage.getItem("api_token"), formdata).then(function (response) {
                  that.is_loading = false;
                  var data = response.data.values;

                  if (response.data.status == 'success') {
                    that.feeds = data.feeds;
                    that.feed_ct = data.feed_ct;
                    that.showEditor();
                    that.addHoverClass();
                    that.addHoverWrapperClass();
                  }
                });

              case 7:
              case "end":
                return _context16.stop();
            }
          }
        }, _callee16);
      }))();
    },
    editFeedVal: function editFeedVal() {
      var that = this;
      this.is_loading = true;
      var formdata = new FormData();
      formdata.append('feed', this.feed);
      formdata.append('page', this.page);

      for (var j = 0; j < this.edit_files.length; j++) {
        var file = this.edit_files[j];
        formdata.append('old_files[' + j + ']', file);
      }

      for (var i = 0; i < this.images.length; i++) {
        var _file = this.images[i];
        formdata.append('files[' + i + ']', _file);
      }

      axios__WEBPACK_IMPORTED_MODULE_2___default.a.post('/api/feeds/edit/' + this.edit_feed + '?token=' + localStorage.getItem("api_token"), formdata).then(function (response) {
        that.is_loading = false;

        if (response.data.status == 'success') {
          var data = response.data.values;
          that.feeds = data.feeds;
          that.feed_ct = data.feed_ct;
          that.showEditor();
          that.addHoverClass();
          that.addHoverWrapperClass();
        }
      });
    },
    editFeed: function editFeed(item) {
      this.show_editor = true;
      this.is_edit = true;
      this.feed = item.description;
      this.edit_feed = item.id;
      this.editableFiles(item.files);
    },
    deleteFeed: function deleteFeed(_id) {
      var _this14 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee17() {
        var that, formdata;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee17$(_context17) {
          while (1) {
            switch (_context17.prev = _context17.next) {
              case 0:
                that = _this14;
                _this14.is_loading = true;
                formdata = new FormData();
                formdata.append('id', _id);
                formdata.append('page', _this14.page);
                axios__WEBPACK_IMPORTED_MODULE_2___default.a.post('/api/feeds/delete?token=' + localStorage.getItem("api_token"), formdata).then(function (response) {
                  that.is_loading = false;
                  var data = response.data;
                  that.feeds = data.feeds;
                  that.feed_ct = data.feed_ct;
                  that.setArrFields();
                });

              case 6:
              case "end":
                return _context17.stop();
            }
          }
        }, _callee17);
      }))();
    },
    listFeeds: function listFeeds() {
      var that = this;
      this.is_loading = true;
      axios__WEBPACK_IMPORTED_MODULE_2___default.a.get('/api/feeds?token=' + localStorage.getItem("api_token")).then(function (response) {
        that.is_loading = false;
        var data = response.data;
        that.feeds = data.feeds;
        that.tasks = data.tasks;
        that.time_for = data.time;
        that.feed_ct = data.feed_ct;
        that.has_permission = data.permission;
        that.birthdays = data.sidebar_data.birthdays;
        that.projects = data.projects;
        that.user_name = data.user_name;
        that.sidebar_data = data.sidebar_data;
        that.mentions = [{
          dataSource: data.users,
          searchExpr: 'text',
          displayExpr: 'text',
          valueExpr: 'id'
        }];

        if (data.profile) {
          that.user_profile = data.profile.profile;
        }

        that.setArrFields();
      });
    },
    setArrFields: function setArrFields() {
      for (var i = 0; i < this.feeds.length; i++) {
        this.comment_editor.push('');
      }
    },
    firebaseTrigger: function firebaseTrigger() {
      var messaging = this.messaging;

      if ('serviceWorker' in navigator) {
        navigator.serviceWorker.register("/assets/js/firebase/firebase-messaging-sw.js").then(function (registration) {
          messaging.useServiceWorker(registration);
          messaging.requestPermission().then(function () {
            // console.log('Notification permission granted.');
            messaging.getToken().then(function (currentToken) {
              if (currentToken) {
                console.log(currentToken);
                var user_id = localStorage.getItem("user"),
                    formdata = new FormData();
                formdata.append('device_token', currentToken);
                formdata.append('user_id', user_id); // set token for notifications for logged in user.

                axios__WEBPACK_IMPORTED_MODULE_2___default.a.post('/api/notify-token?token=' + localStorage.getItem("api_token"), formdata).then(function (response) {
                  localStorage.setItem("check_device_token", true);
                });
              } else {
                console.log('No Instance ID token available. Request permission to generate one.');
              }
            })["catch"](function (err) {
              console.log('An error occurred while retrieving token. ', err);
            });
          })["catch"](function (err) {
            console.log('Unable to get permission to notify.', err);
          });
        });
      }
    },
    uncheckNotifications: function uncheckNotifications() {
      var self = this;
      var _url = '/api/notifications/feedUncheck?countVal=0';
      axios__WEBPACK_IMPORTED_MODULE_2___default.a.get(_url).then(function (response) {
        var span_div = document.createElement('span');
        span_div.className = 'feed_countt';
        span_div.innerHTML = response.data.totalCount;

        if (document.getElementsByClassName("feed_countt").length > 0) {
          document.getElementsByClassName("feed_countt")[0].remove();
        }

        if (response.data.totalCount != 0) {
          document.getElementsByClassName("c-sidebar-nav-item")[0].childNodes[0].appendChild(span_div);
        }
      });
    },
    addHoverClass: function addHoverClass() {
      setTimeout(function () {
        jQuery(document).ready(function () {
          jQuery('.feeds-listing').each(function () {
            var array = [];
            jQuery(this).find('.description-sec img').each(function (index, value) {
              array.push(value.src);
            });
            jQuery(this).find('.show-img-full img').each(function (index, value) {
              if (array.indexOf(value.src) >= 0) {
                jQuery(this).addClass('hover-active');
              } else {
                jQuery(this).removeClass('hover-active');
              }
            });
          });
        });
      }, 2000);
    },
    addHoverWrapperClass: function addHoverWrapperClass() {
      setTimeout(function () {
        jQuery(document).ready(function () {
          jQuery('.wrapper-comment').each(function () {
            var wrapperArray = [];
            jQuery(this).find('.comment-section-detail img').each(function (index, value) {
              wrapperArray.push(value.src);
            });
            jQuery(this).find('.viewer-outer img').each(function (index, value) {
              if (wrapperArray.indexOf(value.src) >= 0) {
                jQuery(this).addClass('hover-active');
              } else {
                jQuery(this).removeClass('hover-active');
              }
            });
          });
        });
      }, 2000);
    },
    viewMore: function viewMore(feedId) {
      this._ttEl = event.target;
      var that = this;
      this.loading = true;
      var formdata = new FormData();
      var paginator = parseInt(jQuery(that._ttEl.parentElement.parentElement).find('.pervious-comm-btn a').attr('data-paginator')) + 1;
      formdata.append('component_id', feedId);
      formdata.append('component', 'feed');
      formdata.append('pager', paginator);
      jQuery(that._ttEl.parentElement.parentElement).find('.pervious-comm-btn a').attr('data-paginator', paginator);
      axios__WEBPACK_IMPORTED_MODULE_2___default.a.post('/api/comment/feeds-more?token=' + localStorage.getItem("api_token"), formdata).then(function (response) {
        that.moreComment = true;
        that.targetComment = response.data.comments;
        that.loading = false;
        setTimeout(function () {
          var _count = that.totalComment(jQuery(that._ttEl.parentElement.parentElement).find('.pervious-comm-btn a').attr('data-count'));

          jQuery(that._ttEl.parentElement.parentElement).find('.pervious-comm-btn a').attr('data-count', _count);
          jQuery(that._ttEl.parentElement.parentElement).find('.pervious-comm-btn span').html("(".concat(_count, ")"));
          jQuery(that._ttEl).parents().eq(1).find('.view-more').html(jQuery('#previous-comment').html());
          that.moreComment = false;
          that.targetComment = [];
        }, 50);
      });
    },
    totalComment: function totalComment(comment) {
      var totalC = comment;

      if (totalC > 0) {
        if (totalC >= 3) {
          this.total_comment = totalC - 3;
          totalC = totalC - 3;
        }

        if (comment < 3) {
          this.total_comment = totalC - totalC;
          totalC = totalC - totalC;
        }

        return totalC;
      } else {
        this.total_comment = 0;
        return 0;
      }
    }
  },
  mounted: function mounted() {
    var _this15 = this;

    var self = this;
    this.uncheckNotifications();

    if (localStorage.getItem("api_token")) {
      this.listFeeds();
      this._current = JSON.parse(localStorage.getItem('user'));
      this.is_admin = JSON.parse(localStorage.getItem('type'));
      this.date_format = localStorage.getItem('date_format');
      document.addEventListener('mouseup', function (event) {
        _this15.selectDashTrigger(event);
      });
    }

    if (!localStorage.getItem("check_device_token")) {
      if (firebase_app__WEBPACK_IMPORTED_MODULE_6__["default"].messaging.isSupported()) {
        this.firebaseTrigger();
      } else {
        this.$swal('Alert!', 'Your browser do not support firebase messaging', 'error');
      }
    }

    this.addHoverClass();
    this.addHoverWrapperClass();
    jQuery(document).on('click', '.driver-item-icon', function () {
      var componentId = this.getAttribute('data-component');
      var id = this.getAttribute('data');
      self.deleteComment(id, componentId);
    });
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/dashboard/DashboardSidebar.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/dashboard/DashboardSidebar.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ "../coreui/node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'DashboardSidebar',
  props: ['data', 'permissions', 'birthdays', 'avtar', 'projects', 'tasks'],
  methods: {
    taskUrl: function taskUrl(component) {
      var link = "/tasks?type=".concat(btoa(component));
      this.$router.push({
        path: link
      });
    },
    userUrl: function userUrl() {
      this.$router.push({
        path: '/users'
      });
    },
    goToProject: function goToProject(_id) {
      this.$router.push({
        path: '/projects/' + _id
      });
    },
    dateFormatVal: function dateFormatVal(val, format) {
      return moment__WEBPACK_IMPORTED_MODULE_0___default()(val).format(format);
    },
    goToTask: function goToTask(_id) {
      var id = btoa(_id);
      this.$router.push({
        path: '/tasks/' + id
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/Dashboard.vue?vue&type=template&id=78f2734c&":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/Dashboard.vue?vue&type=template&id=78f2734c& ***!
  \******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "dashboard-section" },
    [
      _c(
        "CRow",
        { staticClass: "remove-extrarow-padding" },
        [
          _c(
            "CCol",
            { attrs: { col: "8", lg: "8" } },
            [
              _vm.has_permission
                ? _c(
                    "CCard",
                    { staticClass: "feed-field-section" },
                    [
                      _c("CCardBody", [
                        _c(
                          "div",
                          { staticClass: "feed-textfield" },
                          [
                            _c(
                              "CRow",
                              [
                                _vm.show_editor
                                  ? _c(
                                      "CCol",
                                      { attrs: { col: "12" } },
                                      [
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "editor-module dashboard-feed-editor"
                                          },
                                          [
                                            _c(
                                              "DxHtmlEditor",
                                              {
                                                ref: "textEditor",
                                                attrs: {
                                                  mentions: _vm.mentions,
                                                  height: "300px"
                                                },
                                                model: {
                                                  value: _vm.feed,
                                                  callback: function($$v) {
                                                    _vm.feed = $$v
                                                  },
                                                  expression: "feed"
                                                }
                                              },
                                              [
                                                _c(
                                                  "DxToolbar",
                                                  [
                                                    _c("DxItem", {
                                                      attrs: {
                                                        "format-name": "bold"
                                                      }
                                                    }),
                                                    _vm._v(" "),
                                                    _c("DxItem", {
                                                      attrs: {
                                                        "format-name": "link"
                                                      }
                                                    }),
                                                    _vm._v(" "),
                                                    _c("DxItem", {
                                                      attrs: {
                                                        "format-name": "image"
                                                      }
                                                    }),
                                                    _vm._v(" "),
                                                    _c("DxItem", {
                                                      attrs: {
                                                        "format-name":
                                                          "blockquote"
                                                      }
                                                    })
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              { staticClass: "editor-icons" },
                                              [
                                                _c(
                                                  "a",
                                                  {
                                                    staticClass:
                                                      "attachment-img-ico",
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.showDropbox(
                                                          $event
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c("img", {
                                                      attrs: {
                                                        src:
                                                          "/img/attachment.png",
                                                        alt: ""
                                                      }
                                                    })
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "a",
                                                  {
                                                    staticClass: "bold-img-ico",
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.selectOption(
                                                          $event,
                                                          "bold"
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c("img", {
                                                      attrs: {
                                                        src: "/img/bold.png",
                                                        alt: ""
                                                      }
                                                    })
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "a",
                                                  {
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.selectOption(
                                                          $event,
                                                          "link"
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c("CIcon", {
                                                      attrs: {
                                                        name: "cil-link"
                                                      }
                                                    })
                                                  ],
                                                  1
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "a",
                                                  {
                                                    staticClass:
                                                      "quoted-img-ico",
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.selectOption(
                                                          $event,
                                                          "blockquote"
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c("img", {
                                                      attrs: {
                                                        src: "/img/quote.png",
                                                        alt: ""
                                                      }
                                                    })
                                                  ]
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _vm.edit_name.length > 0
                                              ? _c(
                                                  "div",
                                                  {
                                                    staticClass: "files-names"
                                                  },
                                                  _vm._l(
                                                    _vm.edit_name,
                                                    function(name) {
                                                      return _c("p", [
                                                        _vm._v(
                                                          _vm._s(name.name) +
                                                            " "
                                                        ),
                                                        _c(
                                                          "a",
                                                          {
                                                            staticClass:
                                                              "float-right",
                                                            on: {
                                                              click: function(
                                                                $event
                                                              ) {
                                                                return _vm.removeIndex(
                                                                  name.file
                                                                )
                                                              }
                                                            }
                                                          },
                                                          [
                                                            _c("CIcon", {
                                                              attrs: {
                                                                name: "cil-x"
                                                              }
                                                            })
                                                          ],
                                                          1
                                                        )
                                                      ])
                                                    }
                                                  ),
                                                  0
                                                )
                                              : _vm._e(),
                                            _vm._v(" "),
                                            _vm.is_shown
                                              ? _c("vue-dropzone", {
                                                  ref: "imgDropZone",
                                                  attrs: {
                                                    id: "customdropzone",
                                                    options:
                                                      _vm.dropzoneOptions,
                                                    data_id: ""
                                                  },
                                                  on: {
                                                    "vdropzone-complete":
                                                      _vm.afterComplete,
                                                    "vdropzone-removed-file":
                                                      _vm.removeThisFile
                                                  }
                                                })
                                              : _vm._e()
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _vm.is_edit
                                          ? _c(
                                              "CButton",
                                              {
                                                attrs: { color: "primary" },
                                                on: { click: _vm.editFeedVal }
                                              },
                                              [_vm._v("Edit")]
                                            )
                                          : _c(
                                              "CButton",
                                              {
                                                attrs: { color: "primary" },
                                                on: { click: _vm.addFeed }
                                              },
                                              [_vm._v("Send")]
                                            ),
                                        _vm._v(" "),
                                        _c(
                                          "CButton",
                                          {
                                            attrs: { color: "secondary" },
                                            on: { click: _vm.showEditor }
                                          },
                                          [_vm._v("Cancel")]
                                        )
                                      ],
                                      1
                                    )
                                  : _c("CCol", { attrs: { col: "12" } }, [
                                      _c(
                                        "div",
                                        {
                                          staticClass: "dash-texting",
                                          on: { click: _vm.showEditor }
                                        },
                                        [_vm._v("Send Message ...")]
                                      )
                                    ])
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ])
                    ],
                    1
                  )
                : _vm._e(),
              _vm._v(" "),
              _c("div", { staticClass: "dashboard-heading" }, [
                _c("h3", [_vm._v("Feeds")]),
                _vm._v(" "),
                _c("div", { staticClass: "input-group" }, [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.search,
                        expression: "search"
                      }
                    ],
                    staticClass: "form-control",
                    attrs: {
                      type: "text",
                      placeholder: "Filter & Search",
                      "aria-label": "Filter & Search"
                    },
                    domProps: { value: _vm.search },
                    on: {
                      input: [
                        function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.search = $event.target.value
                        },
                        function($event) {
                          return _vm.makeSearch("search")
                        }
                      ]
                    }
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "input-group-append" }, [
                    _c(
                      "span",
                      {
                        staticClass: "input-group-text",
                        on: {
                          click: function($event) {
                            return _vm.makeSearch("search")
                          }
                        }
                      },
                      [
                        _c("CIcon", { attrs: { name: "cil-magnifying-glass" } })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "span",
                      {
                        staticClass: "input-group-text",
                        on: { click: _vm.emptySearch }
                      },
                      [_c("CIcon", { attrs: { name: "cil-x" } })],
                      1
                    )
                  ])
                ])
              ]),
              _vm._v(" "),
              _c(
                "CCard",
                [
                  _c("CCardBody", [
                    _c(
                      "div",
                      { staticClass: "feed-listing-area" },
                      [
                        _vm._l(_vm.feeds, function(item, key) {
                          return _c(
                            "div",
                            {
                              staticClass: "listing-section mt-3",
                              attrs: { data: key }
                            },
                            [
                              _c(
                                "div",
                                {
                                  key: key,
                                  class: "outer-sec make-quote-" + key
                                },
                                [
                                  _c(
                                    "CRow",
                                    [
                                      _c("CCol", { attrs: { col: "1" } }, [
                                        _c(
                                          "div",
                                          { staticClass: "small-rounded-img" },
                                          [
                                            item.user_profile
                                              ? _c("img", {
                                                  attrs: {
                                                    src: item.user_profile
                                                  }
                                                })
                                              : _c("img", {
                                                  attrs: { src: _vm.avtar }
                                                })
                                          ]
                                        )
                                      ]),
                                      _vm._v(" "),
                                      _c("CCol", { attrs: { col: "11" } }, [
                                        _c(
                                          "div",
                                          { staticClass: "feeds-listing" },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "feed-user-det" },
                                              [
                                                _c(
                                                  "div",
                                                  { staticClass: "inner-div" },
                                                  [
                                                    _c("h4", [
                                                      _vm._v(
                                                        _vm._s(item.user_name)
                                                      ),
                                                      _c("span", [
                                                        _vm._v(
                                                          " > To All Employees"
                                                        )
                                                      ])
                                                    ]),
                                                    _vm._v(" "),
                                                    _c("p", [
                                                      _vm._v(
                                                        _vm._s(
                                                          _vm.dateFormat(
                                                            item.created_at
                                                          )
                                                        )
                                                      )
                                                    ])
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "div",
                                                  { staticClass: "inner-div" },
                                                  [
                                                    _vm.shouldDisplay(item)
                                                      ? _c(
                                                          "CDropdown",
                                                          {
                                                            staticClass:
                                                              "custom-menu-btn",
                                                            attrs: {
                                                              "toggler-text":
                                                                "..."
                                                            }
                                                          },
                                                          [
                                                            _c(
                                                              "CDropdownItem",
                                                              {
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    return _vm.editFeed(
                                                                      item
                                                                    )
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _c("CIcon", {
                                                                  attrs: {
                                                                    name:
                                                                      "cil-pencil"
                                                                  }
                                                                }),
                                                                _vm._v(" Edit")
                                                              ],
                                                              1
                                                            ),
                                                            _vm._v(" "),
                                                            _c(
                                                              "CDropdownItem",
                                                              {
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    return _vm.deleteFeed(
                                                                      item.id
                                                                    )
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _c("CIcon", {
                                                                  attrs: {
                                                                    name:
                                                                      "cil-trash"
                                                                  }
                                                                }),
                                                                _vm._v(
                                                                  " Delete"
                                                                )
                                                              ],
                                                              1
                                                            )
                                                          ],
                                                          1
                                                        )
                                                      : _vm._e()
                                                  ],
                                                  1
                                                )
                                              ]
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              {
                                                directives: [
                                                  {
                                                    name: "viewer",
                                                    rawName: "v-viewer"
                                                  }
                                                ],
                                                staticClass:
                                                  "description-sec images show-img-full"
                                              },
                                              [
                                                _c("p", {
                                                  domProps: {
                                                    innerHTML: _vm._s(
                                                      item.description
                                                    )
                                                  }
                                                })
                                              ]
                                            ),
                                            _vm._v(" "),
                                            JSON.parse(item.files)
                                              ? _c(
                                                  "div",
                                                  {
                                                    staticClass:
                                                      "images-viewer main-files-listing mt-3"
                                                  },
                                                  [
                                                    _vm._l(
                                                      JSON.parse(item.files),
                                                      function(src) {
                                                        return !_vm.isValidImageURL(
                                                          src
                                                        )
                                                          ? _c(
                                                              "div",
                                                              {
                                                                staticClass:
                                                                  "images-listing pr-3 files-names"
                                                              },
                                                              [
                                                                _c(
                                                                  "a",
                                                                  {
                                                                    attrs: {
                                                                      href: src,
                                                                      download:
                                                                        "",
                                                                      target:
                                                                        "_blank"
                                                                    }
                                                                  },
                                                                  [
                                                                    _vm._v(
                                                                      _vm._s(
                                                                        _vm.getValidName(
                                                                          src
                                                                        )
                                                                      )
                                                                    )
                                                                  ]
                                                                )
                                                              ]
                                                            )
                                                          : _vm._e()
                                                      }
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "viewer",
                                                      {
                                                        staticClass:
                                                          "d-flex viewer-outer show-img-full",
                                                        attrs: {
                                                          images: JSON.parse(
                                                            item.files
                                                          )
                                                        }
                                                      },
                                                      _vm._l(
                                                        JSON.parse(item.files),
                                                        function(src, a) {
                                                          return _vm.isValidImageURL(
                                                            src
                                                          )
                                                            ? _c(
                                                                "div",
                                                                {
                                                                  staticClass:
                                                                    "images-listing pr-3"
                                                                },
                                                                [
                                                                  _c("img", {
                                                                    key: a,
                                                                    attrs: {
                                                                      src: src
                                                                    }
                                                                  })
                                                                ]
                                                              )
                                                            : _vm._e()
                                                        }
                                                      ),
                                                      0
                                                    )
                                                  ],
                                                  2
                                                )
                                              : _vm._e(),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "more-icons d-flex align-items-center"
                                              },
                                              [
                                                _c(
                                                  "popper",
                                                  {
                                                    attrs: {
                                                      trigger: "hover",
                                                      options: {
                                                        placement: "top"
                                                      }
                                                    }
                                                  },
                                                  [
                                                    _c(
                                                      "div",
                                                      { staticClass: "popper" },
                                                      [
                                                        _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "like-box"
                                                          },
                                                          [
                                                            _c(
                                                              "div",
                                                              {
                                                                staticClass:
                                                                  "like-inner"
                                                              },
                                                              [
                                                                _c(
                                                                  "div",
                                                                  {
                                                                    staticClass:
                                                                      "header-box-like"
                                                                  },
                                                                  [
                                                                    _c(
                                                                      "CIcon",
                                                                      {
                                                                        attrs: {
                                                                          name:
                                                                            "cil-thumb-up"
                                                                        }
                                                                      }
                                                                    ),
                                                                    _c(
                                                                      "span",
                                                                      {
                                                                        staticClass:
                                                                          "like_box_count"
                                                                      },
                                                                      [
                                                                        _vm._v(
                                                                          _vm._s(
                                                                            item.likes_count
                                                                          )
                                                                        )
                                                                      ]
                                                                    )
                                                                  ],
                                                                  1
                                                                ),
                                                                _vm._v(" "),
                                                                _c(
                                                                  "div",
                                                                  {
                                                                    staticClass:
                                                                      "likeuser-section-values likeuser-feed-list"
                                                                  },
                                                                  _vm._l(
                                                                    item.likeusers,
                                                                    function(
                                                                      luser
                                                                    ) {
                                                                      return _c(
                                                                        "div",
                                                                        [
                                                                          _c(
                                                                            "div",
                                                                            {
                                                                              staticClass:
                                                                                "popup-left"
                                                                            },
                                                                            [
                                                                              _c(
                                                                                "div",
                                                                                {
                                                                                  staticClass:
                                                                                    "small-rounded-img"
                                                                                },
                                                                                [
                                                                                  luser.profile
                                                                                    ? _c(
                                                                                        "img",
                                                                                        {
                                                                                          staticClass:
                                                                                            "pro-img",
                                                                                          attrs: {
                                                                                            src:
                                                                                              luser.profile
                                                                                          }
                                                                                        }
                                                                                      )
                                                                                    : _c(
                                                                                        "img",
                                                                                        {
                                                                                          staticClass:
                                                                                            "pro-img",
                                                                                          attrs: {
                                                                                            src:
                                                                                              _vm.avtar
                                                                                          }
                                                                                        }
                                                                                      )
                                                                                ]
                                                                              )
                                                                            ]
                                                                          ),
                                                                          _vm._v(
                                                                            " "
                                                                          ),
                                                                          _c(
                                                                            "div",
                                                                            {
                                                                              staticClass:
                                                                                "popup-right"
                                                                            },
                                                                            [
                                                                              _c(
                                                                                "b",
                                                                                [
                                                                                  _vm._v(
                                                                                    _vm._s(
                                                                                      luser.name
                                                                                    )
                                                                                  )
                                                                                ]
                                                                              )
                                                                            ]
                                                                          )
                                                                        ]
                                                                      )
                                                                    }
                                                                  ),
                                                                  0
                                                                )
                                                              ]
                                                            )
                                                          ]
                                                        )
                                                      ]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "a",
                                                      {
                                                        class: item.liked_by
                                                          ? "like-btn top active"
                                                          : "like-btn top",
                                                        attrs: {
                                                          slot: "reference"
                                                        },
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            return _vm.makeLike(
                                                              item.id,
                                                              "feed"
                                                            )
                                                          }
                                                        },
                                                        slot: "reference"
                                                      },
                                                      [
                                                        _c("span", [
                                                          _vm._v(
                                                            _vm._s(
                                                              item.likes_count
                                                            )
                                                          )
                                                        ]),
                                                        _vm._v(" "),
                                                        _c("CIcon", {
                                                          attrs: {
                                                            name: "cil-thumb-up"
                                                          }
                                                        })
                                                      ],
                                                      1
                                                    )
                                                  ]
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "div",
                                              {
                                                staticClass:
                                                  "comment-section-values comment-feed-list"
                                              },
                                              [
                                                _c(
                                                  "div",
                                                  {
                                                    staticClass:
                                                      "pervious-comm-btn"
                                                  },
                                                  [
                                                    item.comment_count
                                                      ? _c(
                                                          "a",
                                                          {
                                                            attrs: {
                                                              "data-count": _vm.totalComment(
                                                                item.comment_count
                                                              ),
                                                              "data-paginator":
                                                                "1"
                                                            },
                                                            on: {
                                                              click: function(
                                                                $event
                                                              ) {
                                                                return _vm.viewMore(
                                                                  item.id
                                                                )
                                                              }
                                                            }
                                                          },
                                                          [
                                                            _vm._v(
                                                              "Previous Comment "
                                                            ),
                                                            _c("span", [
                                                              _vm._v(
                                                                "(" +
                                                                  _vm._s(
                                                                    _vm.totalComment(
                                                                      item.comment_count
                                                                    )
                                                                  ) +
                                                                  ")"
                                                              )
                                                            ])
                                                          ]
                                                        )
                                                      : _c("a", [
                                                          _vm._v(
                                                            "Previous Comment "
                                                          ),
                                                          _c("span", [
                                                            _vm._v("(0)")
                                                          ])
                                                        ]),
                                                    _vm._v(" "),
                                                    _vm.loading
                                                      ? _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "pervious-comm_loader"
                                                          },
                                                          [
                                                            _c("img", {
                                                              attrs: {
                                                                src: _vm.loader
                                                              }
                                                            })
                                                          ]
                                                        )
                                                      : _vm._e()
                                                  ]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "div",
                                                  { staticClass: "view-more" },
                                                  [
                                                    _c(
                                                      "div",
                                                      {
                                                        staticClass:
                                                          "starting-comment"
                                                      },
                                                      _vm._l(
                                                        item.comment_list
                                                          .slice()
                                                          .reverse(),
                                                        function(comment, j) {
                                                          return _c(
                                                            "CRow",
                                                            _vm._b(
                                                              {
                                                                key: j,
                                                                staticClass:
                                                                  "pt-3"
                                                              },
                                                              "CRow",
                                                              item.comment_list,
                                                              false
                                                            ),
                                                            [
                                                              _c(
                                                                "CCol",
                                                                {
                                                                  attrs: {
                                                                    col: "1"
                                                                  }
                                                                },
                                                                [
                                                                  _c(
                                                                    "div",
                                                                    {
                                                                      staticClass:
                                                                        "small-rounded-img"
                                                                    },
                                                                    [
                                                                      comment.profile
                                                                        ? _c(
                                                                            "img",
                                                                            {
                                                                              attrs: {
                                                                                src:
                                                                                  comment.profile
                                                                              }
                                                                            }
                                                                          )
                                                                        : _c(
                                                                            "img",
                                                                            {
                                                                              attrs: {
                                                                                src:
                                                                                  _vm.avtar
                                                                              }
                                                                            }
                                                                          )
                                                                    ]
                                                                  )
                                                                ]
                                                              ),
                                                              _vm._v(" "),
                                                              _c(
                                                                "CCol",
                                                                {
                                                                  attrs: {
                                                                    col: "11"
                                                                  }
                                                                },
                                                                [
                                                                  _c(
                                                                    "div",
                                                                    {
                                                                      staticClass:
                                                                        "wrapper-comment"
                                                                    },
                                                                    [
                                                                      _c("b", [
                                                                        _vm._v(
                                                                          _vm._s(
                                                                            comment.user_name
                                                                          ) +
                                                                            " "
                                                                        ),
                                                                        _c(
                                                                          "span",
                                                                          {
                                                                            staticClass:
                                                                              "comment-date"
                                                                          },
                                                                          [
                                                                            _vm._v(
                                                                              _vm._s(
                                                                                _vm.dateFormat(
                                                                                  comment.created_at
                                                                                )
                                                                              )
                                                                            )
                                                                          ]
                                                                        )
                                                                      ]),
                                                                      _vm._v(
                                                                        " "
                                                                      ),
                                                                      _c(
                                                                        "div",
                                                                        {
                                                                          directives: [
                                                                            {
                                                                              name:
                                                                                "viewer",
                                                                              rawName:
                                                                                "v-viewer"
                                                                            }
                                                                          ],
                                                                          staticClass:
                                                                            "comment-section-detail images show-img-full"
                                                                        },
                                                                        [
                                                                          _c(
                                                                            "p",
                                                                            {
                                                                              domProps: {
                                                                                innerHTML: _vm._s(
                                                                                  JSON.parse(
                                                                                    comment.comment
                                                                                  )
                                                                                )
                                                                              }
                                                                            }
                                                                          )
                                                                        ]
                                                                      ),
                                                                      _vm._v(
                                                                        " "
                                                                      ),
                                                                      _c(
                                                                        "div",
                                                                        {
                                                                          staticClass:
                                                                            "images-viewer"
                                                                        },
                                                                        [
                                                                          _vm._l(
                                                                            JSON.parse(
                                                                              comment.files
                                                                            ),
                                                                            function(
                                                                              src
                                                                            ) {
                                                                              return !_vm.isValidImageURL(
                                                                                src
                                                                              )
                                                                                ? _c(
                                                                                    "div",
                                                                                    {
                                                                                      staticClass:
                                                                                        "images-listing pr-3 files-names"
                                                                                    },
                                                                                    [
                                                                                      _c(
                                                                                        "a",
                                                                                        {
                                                                                          attrs: {
                                                                                            href: src,
                                                                                            download:
                                                                                              "",
                                                                                            target:
                                                                                              "_blank"
                                                                                          }
                                                                                        },
                                                                                        [
                                                                                          _vm._v(
                                                                                            _vm._s(
                                                                                              _vm.getValidName(
                                                                                                src
                                                                                              )
                                                                                            )
                                                                                          )
                                                                                        ]
                                                                                      )
                                                                                    ]
                                                                                  )
                                                                                : _vm._e()
                                                                            }
                                                                          ),
                                                                          _vm._v(
                                                                            " "
                                                                          ),
                                                                          _c(
                                                                            "viewer",
                                                                            {
                                                                              staticClass:
                                                                                "d-flex viewer-outer show-img-full",
                                                                              attrs: {
                                                                                images: JSON.parse(
                                                                                  comment.files
                                                                                )
                                                                              }
                                                                            },
                                                                            _vm._l(
                                                                              JSON.parse(
                                                                                comment.files
                                                                              ),
                                                                              function(
                                                                                src
                                                                              ) {
                                                                                return _vm.isValidImageURL(
                                                                                  src
                                                                                )
                                                                                  ? _c(
                                                                                      "div",
                                                                                      {
                                                                                        staticClass:
                                                                                          "images-listing pr-3"
                                                                                      },
                                                                                      [
                                                                                        _c(
                                                                                          "img",
                                                                                          {
                                                                                            attrs: {
                                                                                              src: src
                                                                                            }
                                                                                          }
                                                                                        )
                                                                                      ]
                                                                                    )
                                                                                  : _vm._e()
                                                                              }
                                                                            ),
                                                                            0
                                                                          )
                                                                        ],
                                                                        2
                                                                      )
                                                                    ]
                                                                  ),
                                                                  _vm._v(" "),
                                                                  _c(
                                                                    "div",
                                                                    {
                                                                      staticClass:
                                                                        "wrapper-opt"
                                                                    },
                                                                    [
                                                                      _vm.shouldDisplayComm(
                                                                        comment
                                                                      )
                                                                        ? _c(
                                                                            "CDropdown",
                                                                            {
                                                                              staticClass:
                                                                                "custom-menu-btn",
                                                                              attrs: {
                                                                                "toggler-text":
                                                                                  "More"
                                                                              }
                                                                            },
                                                                            [
                                                                              _c(
                                                                                "CDropdownItem",
                                                                                {
                                                                                  on: {
                                                                                    click: function(
                                                                                      $event
                                                                                    ) {
                                                                                      return _vm.deleteComment(
                                                                                        comment.id,
                                                                                        item.id,
                                                                                        key
                                                                                      )
                                                                                    }
                                                                                  }
                                                                                },
                                                                                [
                                                                                  _c(
                                                                                    "CIcon",
                                                                                    {
                                                                                      attrs: {
                                                                                        name:
                                                                                          "cil-trash"
                                                                                      }
                                                                                    }
                                                                                  ),
                                                                                  _vm._v(
                                                                                    " Delete"
                                                                                  )
                                                                                ],
                                                                                1
                                                                              )
                                                                            ],
                                                                            1
                                                                          )
                                                                        : _vm._e()
                                                                    ],
                                                                    1
                                                                  )
                                                                ]
                                                              )
                                                            ],
                                                            1
                                                          )
                                                        }
                                                      ),
                                                      1
                                                    )
                                                  ]
                                                )
                                              ]
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "feed-comment-section pt-3"
                                          },
                                          [
                                            _c(
                                              "div",
                                              { staticClass: "comment-field" },
                                              [
                                                _c(
                                                  "a",
                                                  {
                                                    class:
                                                      "commentfiels-" + key,
                                                    on: {
                                                      click: function($event) {
                                                        return _vm.showCommentEditor(
                                                          key
                                                        )
                                                      }
                                                    }
                                                  },
                                                  [_vm._v("Add Comment")]
                                                ),
                                                _vm._v(" "),
                                                _c(
                                                  "div",
                                                  {
                                                    class:
                                                      "hide commenteditor-" +
                                                      key
                                                  },
                                                  [
                                                    _c(
                                                      "div",
                                                      {
                                                        class:
                                                          "editor-module editor-mod-" +
                                                          key
                                                      },
                                                      [
                                                        _c(
                                                          "DxHtmlEditor",
                                                          {
                                                            ref:
                                                              "commentEditor",
                                                            refInFor: true,
                                                            attrs: {
                                                              mentions:
                                                                _vm.mentions,
                                                              height: "100px"
                                                            },
                                                            model: {
                                                              value:
                                                                _vm
                                                                  .comment_editor[
                                                                  key
                                                                ],
                                                              callback: function(
                                                                $$v
                                                              ) {
                                                                _vm.$set(
                                                                  _vm.comment_editor,
                                                                  key,
                                                                  $$v
                                                                )
                                                              },
                                                              expression:
                                                                "comment_editor[key]"
                                                            }
                                                          },
                                                          [
                                                            _c(
                                                              "DxToolbar",
                                                              [
                                                                _c("DxItem", {
                                                                  attrs: {
                                                                    "format-name":
                                                                      "bold"
                                                                  }
                                                                }),
                                                                _vm._v(" "),
                                                                _c("DxItem", {
                                                                  attrs: {
                                                                    "format-name":
                                                                      "link"
                                                                  }
                                                                }),
                                                                _vm._v(" "),
                                                                _c("DxItem", {
                                                                  attrs: {
                                                                    "format-name":
                                                                      "image"
                                                                  }
                                                                }),
                                                                _vm._v(" "),
                                                                _c("DxItem", {
                                                                  attrs: {
                                                                    "format-name":
                                                                      "blockquote"
                                                                  }
                                                                })
                                                              ],
                                                              1
                                                            )
                                                          ],
                                                          1
                                                        ),
                                                        _vm._v(" "),
                                                        _c(
                                                          "div",
                                                          {
                                                            staticClass:
                                                              "editor-icons"
                                                          },
                                                          [
                                                            _c(
                                                              "a",
                                                              {
                                                                staticClass:
                                                                  "attachment-img-ico",
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    return _vm.showCommDropbox(
                                                                      key
                                                                    )
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _c("img", {
                                                                  attrs: {
                                                                    src:
                                                                      "/img/attachment.png",
                                                                    alt: ""
                                                                  }
                                                                })
                                                              ]
                                                            ),
                                                            _vm._v(" "),
                                                            _c(
                                                              "a",
                                                              {
                                                                staticClass:
                                                                  "bold-img-ico",
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    return _vm.selectCommOption(
                                                                      key,
                                                                      "bold"
                                                                    )
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _c("img", {
                                                                  attrs: {
                                                                    src:
                                                                      "/img/bold.png",
                                                                    alt: ""
                                                                  }
                                                                })
                                                              ]
                                                            ),
                                                            _vm._v(" "),
                                                            _c(
                                                              "a",
                                                              {
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    return _vm.selectCommOption(
                                                                      key,
                                                                      "link"
                                                                    )
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _c("CIcon", {
                                                                  attrs: {
                                                                    name:
                                                                      "cil-link"
                                                                  }
                                                                })
                                                              ],
                                                              1
                                                            ),
                                                            _vm._v(" "),
                                                            _c(
                                                              "a",
                                                              {
                                                                staticClass:
                                                                  "quoted-img-ico",
                                                                on: {
                                                                  click: function(
                                                                    $event
                                                                  ) {
                                                                    return _vm.selectCommOption(
                                                                      key,
                                                                      "blockquote"
                                                                    )
                                                                  }
                                                                }
                                                              },
                                                              [
                                                                _c("img", {
                                                                  attrs: {
                                                                    src:
                                                                      "/img/quote.png",
                                                                    alt: ""
                                                                  }
                                                                })
                                                              ]
                                                            )
                                                          ]
                                                        ),
                                                        _vm._v(" "),
                                                        _c("vue-dropzone", {
                                                          ref: "commDropZone",
                                                          refInFor: true,
                                                          class:
                                                            "hide cust-dropzone-" +
                                                            key,
                                                          attrs: {
                                                            id:
                                                              "customdropzone" +
                                                              key,
                                                            options:
                                                              _vm.dropzoneOptions,
                                                            data_id: item.id
                                                          },
                                                          on: {
                                                            "vdropzone-complete":
                                                              _vm.afterComplete,
                                                            "vdropzone-removed-file":
                                                              _vm.removeThisFile
                                                          }
                                                        })
                                                      ],
                                                      1
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "CButton",
                                                      {
                                                        attrs: {
                                                          color: "primary"
                                                        },
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            return _vm.addComment(
                                                              key,
                                                              item.id
                                                            )
                                                          }
                                                        }
                                                      },
                                                      [_vm._v("Send")]
                                                    ),
                                                    _vm._v(" "),
                                                    _c(
                                                      "CButton",
                                                      {
                                                        attrs: {
                                                          color: "secondary"
                                                        },
                                                        on: {
                                                          click: function(
                                                            $event
                                                          ) {
                                                            return _vm.hideCommentEditor(
                                                              key
                                                            )
                                                          }
                                                        }
                                                      },
                                                      [_vm._v("Cancel")]
                                                    )
                                                  ],
                                                  1
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      ])
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ]
                          )
                        }),
                        _vm._v(" "),
                        _vm.feed_ct > 0
                          ? _c(
                              "div",
                              {
                                staticClass: "load-more pt-3 text-center pb-3"
                              },
                              [
                                _c(
                                  "CButton",
                                  {
                                    attrs: { color: "secondary" },
                                    on: { click: _vm.loadMore }
                                  },
                                  [_vm._v("Load More...")]
                                )
                              ],
                              1
                            )
                          : _vm._e(),
                        _vm._v(" "),
                        _vm.feeds.length <= 0
                          ? _c("div", { staticClass: "no-feeds-sec" }, [
                              _c("a", { staticClass: "error-msg-ico" }, [
                                _c("img", { attrs: { src: "/img/error.png" } })
                              ]),
                              _vm._v(
                                " Currently there are no feeds.\n\t      \t\t\t\t\t"
                              )
                            ])
                          : _vm._e()
                      ],
                      2
                    )
                  ])
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "CCol",
            { attrs: { col: "4", lg: "4" } },
            [
              _c("DashboardSidebar", {
                attrs: {
                  data: _vm.sidebar_data,
                  permissions: _vm.has_permission,
                  birthdays: _vm.birthdays,
                  avtar: _vm.avtar,
                  projects: _vm.projects,
                  tasks: _vm.tasks
                }
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _vm.latestComment
        ? _c(
            "div",
            {
              staticStyle: { display: "none" },
              attrs: { id: "record-latest-comment" }
            },
            [
              _c("div", { staticClass: "record-latest-comment" }, [
                _c(
                  "div",
                  { staticClass: "latest-comment" },
                  [
                    _c(
                      "CRow",
                      _vm._b(
                        { staticClass: "pt-3" },
                        "CRow",
                        _vm.latestComment,
                        false
                      ),
                      [
                        _c("CCol", { attrs: { col: "1" } }, [
                          _c("div", { staticClass: "small-rounded-img" }, [
                            _vm.latestComment.profile
                              ? _c("img", {
                                  attrs: { src: _vm.latestComment.profile }
                                })
                              : _c("img", { attrs: { src: _vm.avtar } })
                          ])
                        ]),
                        _vm._v(" "),
                        _c("CCol", { attrs: { col: "11" } }, [
                          _c("div", { staticClass: "wrapper-comment" }, [
                            _c("b", [
                              _vm._v(_vm._s(_vm.latestComment.user_name) + " "),
                              _c("span", { staticClass: "comment-date" }, [
                                _vm._v(
                                  _vm._s(
                                    _vm.dateFormat(_vm.latestComment.created_at)
                                  )
                                )
                              ])
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              {
                                directives: [
                                  { name: "viewer", rawName: "v-viewer" }
                                ],
                                staticClass:
                                  "comment-section-detail images show-img-full"
                              },
                              [
                                _c("p", {
                                  domProps: {
                                    innerHTML: _vm._s(
                                      JSON.parse(_vm.latestComment.comment)
                                    )
                                  }
                                })
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "images-viewer" },
                              [
                                _vm._l(
                                  JSON.parse(_vm.latestComment.files),
                                  function(src) {
                                    return !_vm.isValidImageURL(src)
                                      ? _c(
                                          "div",
                                          {
                                            staticClass:
                                              "images-listing pr-3 files-names"
                                          },
                                          [
                                            _c(
                                              "a",
                                              {
                                                attrs: {
                                                  href: src,
                                                  download: "",
                                                  target: "_blank"
                                                }
                                              },
                                              [
                                                _vm._v(
                                                  _vm._s(_vm.getValidName(src))
                                                )
                                              ]
                                            )
                                          ]
                                        )
                                      : _vm._e()
                                  }
                                ),
                                _vm._v(" "),
                                _c(
                                  "viewer",
                                  {
                                    staticClass:
                                      "d-flex viewer-outer show-img-full",
                                    attrs: {
                                      images: JSON.parse(
                                        _vm.latestComment.files
                                      )
                                    }
                                  },
                                  _vm._l(
                                    JSON.parse(_vm.latestComment.files),
                                    function(src) {
                                      return _vm.isValidImageURL(src)
                                        ? _c(
                                            "div",
                                            {
                                              staticClass: "images-listing pr-3"
                                            },
                                            [_c("img", { attrs: { src: src } })]
                                          )
                                        : _vm._e()
                                    }
                                  ),
                                  0
                                )
                              ],
                              2
                            )
                          ]),
                          _vm._v(" "),
                          _c("div", { staticClass: "wrapper-opt" }, [
                            _c("div", { staticClass: "dropdown" }, [
                              _c(
                                "p",
                                {
                                  staticClass:
                                    "dropdown-toggle pointer text-center",
                                  attrs: {
                                    id: "listDroper" + _vm.latestComment.id,
                                    "data-toggle": "dropdown",
                                    "aria-haspopup": "true",
                                    "aria-expanded": "false"
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n\t\t\t\t\t\t\t\t\tMore\n\t\t\t\t\t\t\t\t"
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass: "dropdown-menu",
                                  attrs: {
                                    "aria-labelledby":
                                      "listDroper" + _vm.latestComment.id
                                  }
                                },
                                [
                                  _c(
                                    "a",
                                    {
                                      staticClass:
                                        "driver-item-icon dropdown-item text-info",
                                      attrs: {
                                        data: _vm.latestComment.id,
                                        "data-component":
                                          _vm.latestComment.component_id
                                      }
                                    },
                                    [
                                      _c(
                                        "svg",
                                        {
                                          staticClass: "c-icon",
                                          attrs: {
                                            xmlns: "http://www.w3.org/2000/svg",
                                            viewBox: "0 0 24 24",
                                            role: "img"
                                          }
                                        },
                                        [
                                          _c("path", {
                                            attrs: {
                                              d:
                                                "M4.5 22.125c-0 0.003-0 0.006-0 0.008 0 0.613 0.494 1.11 1.105 1.116h12.79c0.612-0.006 1.105-0.504 1.105-1.117 0-0.003-0-0.006-0-0.009v0-15h-15zM6 8.625h12v13.125h-12z"
                                            }
                                          }),
                                          _c("path", {
                                            attrs: {
                                              d:
                                                "M7.875 10.125h1.5v9.375h-1.5v-9.375z"
                                            }
                                          }),
                                          _c("path", {
                                            attrs: {
                                              d:
                                                "M11.25 10.125h1.5v9.375h-1.5v-9.375z"
                                            }
                                          }),
                                          _c("path", {
                                            attrs: {
                                              d:
                                                "M14.625 10.125h1.5v9.375h-1.5v-9.375z"
                                            }
                                          }),
                                          _c("path", {
                                            attrs: {
                                              d:
                                                "M15.375 4.125v-2.25c0-0.631-0.445-1.125-1.013-1.125h-4.725c-0.568 0-1.013 0.494-1.013 1.125v2.25h-5.625v1.5h18v-1.5zM10.125 2.25h3.75v1.875h-3.75z"
                                            }
                                          })
                                        ]
                                      ),
                                      _vm._v(" Delete\n\t\t\t\t\t\t\t\t\t")
                                    ]
                                  )
                                ]
                              )
                            ])
                          ])
                        ])
                      ],
                      1
                    )
                  ],
                  1
                )
              ])
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.moreComment
        ? _c(
            "div",
            {
              staticStyle: { display: "none" },
              attrs: { id: "previous-comment" }
            },
            [
              _c(
                "div",
                { staticClass: "more-comment" },
                _vm._l(_vm.targetComment.slice().reverse(), function(
                  tcomment,
                  j
                ) {
                  return _c(
                    "CRow",
                    { key: j, staticClass: "pt-3" },
                    [
                      _c("CCol", { attrs: { col: "1" } }, [
                        _c("div", { staticClass: "small-rounded-img" }, [
                          tcomment.user_profile
                            ? _c("img", {
                                attrs: { src: tcomment.user_profile }
                              })
                            : _c("img", { attrs: { src: _vm.avtar } })
                        ])
                      ]),
                      _vm._v(" "),
                      _c("CCol", { attrs: { col: "11" } }, [
                        _c("div", { staticClass: "wrapper-comment" }, [
                          _c("b", [
                            _vm._v(_vm._s(tcomment.user_name) + " "),
                            _c("span", { staticClass: "comment-date" }, [
                              _vm._v(
                                _vm._s(_vm.dateFormat(tcomment.created_at))
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c(
                            "div",
                            {
                              directives: [
                                { name: "viewer", rawName: "v-viewer" }
                              ],
                              staticClass:
                                "comment-section-detail images show-img-full"
                            },
                            [
                              _c("p", {
                                domProps: {
                                  innerHTML: _vm._s(
                                    JSON.parse(tcomment.comment)
                                  )
                                }
                              })
                            ]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "images-viewer" },
                            [
                              _vm._l(JSON.parse(tcomment.files), function(src) {
                                return !_vm.isValidImageURL(src)
                                  ? _c(
                                      "div",
                                      {
                                        staticClass:
                                          "images-listing pr-3 files-names"
                                      },
                                      [
                                        _c(
                                          "a",
                                          {
                                            attrs: {
                                              href: src,
                                              download: "",
                                              target: "_blank"
                                            }
                                          },
                                          [
                                            _vm._v(
                                              _vm._s(_vm.getValidName(src))
                                            )
                                          ]
                                        )
                                      ]
                                    )
                                  : _vm._e()
                              }),
                              _vm._v(" "),
                              _c(
                                "viewer",
                                {
                                  staticClass:
                                    "d-flex viewer-outer show-img-full",
                                  attrs: { images: JSON.parse(tcomment.files) }
                                },
                                _vm._l(JSON.parse(tcomment.files), function(
                                  src
                                ) {
                                  return _vm.isValidImageURL(src)
                                    ? _c(
                                        "div",
                                        { staticClass: "images-listing pr-3" },
                                        [_c("img", { attrs: { src: src } })]
                                      )
                                    : _vm._e()
                                }),
                                0
                              )
                            ],
                            2
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "wrapper-opt" },
                          [
                            _vm.shouldDisplayComm(tcomment)
                              ? _c(
                                  "CDropdown",
                                  {
                                    staticClass: "custom-menu-btn",
                                    attrs: { "toggler-text": "More" }
                                  },
                                  [
                                    _c(
                                      "CDropdownItem",
                                      {
                                        on: {
                                          click: function($event) {
                                            return _vm.deleteComment(
                                              tcomment.id,
                                              tcomment.component_id
                                            )
                                          }
                                        }
                                      },
                                      [
                                        _c("CIcon", {
                                          attrs: { name: "cil-trash" }
                                        }),
                                        _vm._v(" Delete")
                                      ],
                                      1
                                    )
                                  ],
                                  1
                                )
                              : _vm._e()
                          ],
                          1
                        )
                      ])
                    ],
                    1
                  )
                }),
                1
              )
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.is_loading
        ? _c("div", { staticClass: "loader-image" }, [
            _c("img", { attrs: { src: _vm.loader } })
          ])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/dashboard/DashboardSidebar.vue?vue&type=template&id=ad75a826&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/dashboard/DashboardSidebar.vue?vue&type=template&id=ad75a826& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "sidebar-section dashboard-sidebar" },
    [
      _vm.permissions
        ? _c(
            "CCard",
            { staticClass: "mb-1" },
            [
              _c("CCardBody", [
                _c("div", { staticClass: "info-section" }, [
                  _c("div", { staticClass: "info-header green-color" }, [
                    _c("b", [_vm._v("Users")])
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "info-div", on: { click: _vm.userUrl } },
                    [
                      _c("div", { staticClass: "left-sec" }, [
                        _vm._v("Total Users")
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "right-sec" }, [
                        _vm._v(_vm._s(this.data.total_users))
                      ])
                    ]
                  )
                ])
              ])
            ],
            1
          )
        : _vm._e(),
      _vm._v(" "),
      _c(
        "CCard",
        { staticClass: "mb-1" },
        [
          _c("CCardBody", [
            _c(
              "div",
              {
                staticClass:
                  "info-section birthday_section latest-project-section"
              },
              [
                _c("div", { staticClass: "info-header blue-color" }, [
                  _c("b", [_vm._v("Latest Project")])
                ]),
                _vm._v(" "),
                _vm._l(_vm.projects, function(project, key) {
                  return _c("div", { staticClass: "info-div" }, [
                    _c(
                      "div",
                      {
                        staticClass: "project-sec",
                        on: {
                          click: function($event) {
                            return _vm.goToProject(project.id)
                          }
                        }
                      },
                      [_vm._v(_vm._s(project.name))]
                    )
                  ])
                })
              ],
              2
            )
          ])
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "CCard",
        { staticClass: "mb-1" },
        [
          _c("CCardBody", [
            _c(
              "div",
              {
                staticClass:
                  "info-section birthday_section latest-project-section"
              },
              [
                _c("div", { staticClass: "info-header blue-color" }, [
                  _c("b", [_vm._v("Recent Updated Task")])
                ]),
                _vm._v(" "),
                _vm._l(_vm.tasks, function(task, key) {
                  return _c("div", { staticClass: "info-div" }, [
                    _c(
                      "div",
                      {
                        staticClass: "project-sec",
                        on: {
                          click: function($event) {
                            return _vm.goToTask(task.id)
                          }
                        }
                      },
                      [_vm._v(_vm._s(task.name))]
                    )
                  ])
                })
              ],
              2
            )
          ])
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "CCard",
        { staticClass: "mb-1" },
        [
          _c("CCardBody", [
            _c(
              "div",
              { staticClass: "info-section birthday_section" },
              [
                _c("div", { staticClass: "info-header blue-color" }, [
                  _c("b", [_vm._v("Birthdays")])
                ]),
                _vm._v(" "),
                _vm._l(_vm.birthdays, function(birthday) {
                  return _c(
                    "div",
                    {
                      staticClass: "info-div",
                      class: birthday.birth_date == "Today!" ? "active" : ""
                    },
                    [
                      _c("div", { staticClass: "left-sec" }, [
                        _c("div", { staticClass: "small-rounded-img" }, [
                          birthday.thumbnail
                            ? _c("img", {
                                staticClass: "c-avatar-img",
                                attrs: { src: birthday.thumbnail }
                              })
                            : _c("img", { attrs: { src: _vm.avtar } })
                        ])
                      ]),
                      _vm._v(" "),
                      birthday.birth_date == "Today!"
                        ? _c(
                            "div",
                            { staticClass: "left-sec" },
                            [
                              _c("span", { staticClass: "birthday-name" }, [
                                _vm._v(_vm._s(birthday.name))
                              ]),
                              _c("br"),
                              _c("CIcon", {
                                attrs: { name: "cil-birthday-cake" }
                              }),
                              _c("span", { staticClass: "span-birth" }, [
                                _vm._v(_vm._s(birthday.birth_date))
                              ])
                            ],
                            1
                          )
                        : _c("div", { staticClass: "left-sec" }, [
                            _c("span", { staticClass: "birthday-name" }, [
                              _vm._v(_vm._s(birthday.name))
                            ]),
                            _c("br"),
                            _c("span", { staticClass: "birthday-date" }, [
                              _vm._v(_vm._s(birthday.birth_date))
                            ])
                          ])
                    ]
                  )
                })
              ],
              2
            )
          ])
        ],
        1
      ),
      _vm._v(" "),
      _vm.permissions && _vm.data.appraisals.length
        ? _c(
            "CCard",
            { staticClass: "mb-1" },
            [
              _c("CCardBody", [
                _c(
                  "div",
                  { staticClass: "info-section birthday_section" },
                  [
                    _c("div", { staticClass: "info-header blue-color" }, [
                      _c("b", [_vm._v("Appraisals")])
                    ]),
                    _vm._v(" "),
                    _vm._l(_vm.data.appraisals, function(appraisal) {
                      return _c(
                        "div",
                        {
                          staticClass: "info-div",
                          class: appraisal.date == "Today!" ? "active" : ""
                        },
                        [
                          _c("div", { staticClass: "left-sec" }, [
                            _c("div", { staticClass: "small-rounded-img" }, [
                              appraisal.profile
                                ? _c("img", {
                                    staticClass: "c-avatar-img",
                                    attrs: { src: appraisal.profile }
                                  })
                                : _c("img", { attrs: { src: _vm.avtar } })
                            ])
                          ]),
                          _vm._v(" "),
                          appraisal.date == "Today!"
                            ? _c("div", { staticClass: "left-sec" }, [
                                _c("span", { staticClass: "birthday-name" }, [
                                  _vm._v(_vm._s(appraisal.name))
                                ]),
                                _c("br"),
                                _vm._v(" "),
                                _c("span", { staticClass: "span-birth" }, [
                                  _vm._v(_vm._s(appraisal.date))
                                ])
                              ])
                            : _c("div", { staticClass: "left-sec" }, [
                                _c("span", { staticClass: "birthday-name" }, [
                                  _vm._v(_vm._s(appraisal.name))
                                ]),
                                _c("br"),
                                _vm._v(" "),
                                _c("span", { staticClass: "birthday-date" }, [
                                  _vm._v(
                                    _vm._s(
                                      _vm.dateFormatVal(
                                        appraisal.appraisel_date,
                                        appraisal.date
                                      )
                                    )
                                  )
                                ])
                              ])
                        ]
                      )
                    })
                  ],
                  2
                )
              ])
            ],
            1
          )
        : _vm._e(),
      _vm._v(" "),
      _c(
        "CCard",
        { staticClass: "mb-1" },
        [
          _c("CCardBody", [
            _c("div", { staticClass: "info-section" }, [
              _c("div", { staticClass: "info-header blue-color" }, [
                _c("b", [_vm._v("Tasks")])
              ]),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "info-div",
                  on: {
                    click: function($event) {
                      return _vm.taskUrl("ongoing")
                    }
                  }
                },
                [
                  _c("div", { staticClass: "left-sec" }, [_vm._v("Ongoing")]),
                  _vm._v(" "),
                  _c("div", { staticClass: "right-sec" }, [
                    _vm._v(_vm._s(this.data.ongoing))
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "info-div",
                  on: {
                    click: function($event) {
                      return _vm.taskUrl("assisting")
                    }
                  }
                },
                [
                  _c("div", { staticClass: "left-sec" }, [_vm._v("Assisting")]),
                  _vm._v(" "),
                  _c("div", { staticClass: "right-sec" }, [
                    _vm._v(_vm._s(this.data.assisting))
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "info-div",
                  on: {
                    click: function($event) {
                      return _vm.taskUrl("creator")
                    }
                  }
                },
                [
                  _c("div", { staticClass: "left-sec" }, [_vm._v("Set By Me")]),
                  _vm._v(" "),
                  _c("div", { staticClass: "right-sec" }, [
                    _vm._v(_vm._s(this.data.creator))
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "info-div",
                  on: {
                    click: function($event) {
                      return _vm.taskUrl("following")
                    }
                  }
                },
                [
                  _c("div", { staticClass: "left-sec" }, [_vm._v("Following")]),
                  _vm._v(" "),
                  _c("div", { staticClass: "right-sec" }, [
                    _vm._v(_vm._s(this.data.following))
                  ])
                ]
              )
            ])
          ])
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);