(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[54],{

/***/ "../coreui/src/views/settings/GlobalConfig.vue":
/*!*****************************************************!*\
  !*** ../coreui/src/views/settings/GlobalConfig.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _GlobalConfig_vue_vue_type_template_id_64117092___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./GlobalConfig.vue?vue&type=template&id=64117092& */ "../coreui/src/views/settings/GlobalConfig.vue?vue&type=template&id=64117092&");
/* harmony import */ var _GlobalConfig_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./GlobalConfig.vue?vue&type=script&lang=js& */ "../coreui/src/views/settings/GlobalConfig.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _GlobalConfig_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _GlobalConfig_vue_vue_type_template_id_64117092___WEBPACK_IMPORTED_MODULE_0__["render"],
  _GlobalConfig_vue_vue_type_template_id_64117092___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/settings/GlobalConfig.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/settings/GlobalConfig.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ../coreui/src/views/settings/GlobalConfig.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_GlobalConfig_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./GlobalConfig.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/settings/GlobalConfig.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_GlobalConfig_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/settings/GlobalConfig.vue?vue&type=template&id=64117092&":
/*!************************************************************************************!*\
  !*** ../coreui/src/views/settings/GlobalConfig.vue?vue&type=template&id=64117092& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_GlobalConfig_vue_vue_type_template_id_64117092___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./GlobalConfig.vue?vue&type=template&id=64117092& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/settings/GlobalConfig.vue?vue&type=template&id=64117092&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_GlobalConfig_vue_vue_type_template_id_64117092___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_GlobalConfig_vue_vue_type_template_id_64117092___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/settings/GlobalConfig.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/settings/GlobalConfig.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "../coreui/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! devextreme-vue/html-editor */ "../coreui/node_modules/devextreme-vue/html-editor.js");
/* harmony import */ var devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_2__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'GlobalConfigurations',
  data: function data() {
    return {
      zones: [],
      dateVals: [],
      timeVals: [],
      timezone: '',
      logo: '',
      emails: '',
      viewLogo: '',
      dateformat: '',
      timeformat: '',
      message: '',
      template_subject: '',
      template_title: '',
      template_body: '',
      alertClass: false,
      dismissSecs: 7,
      dismissCountDown: 0,
      showDismissibleAlert: false
    };
  },
  components: {
    DxHtmlEditor: devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_2__["DxHtmlEditor"],
    DxToolbar: devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_2__["DxToolbar"],
    DxItem: devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_2__["DxItem"]
  },
  methods: {
    goBack: function goBack() {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this.$router.go(-1);

              case 1:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    store: function store() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var that, _type, formdata;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                that = _this2, _type = event.target.elements._type.value;
                formdata = new FormData();

                if (_type === "Date Time") {
                  formdata.append('timezone', _this2.timezone);
                  formdata.append('date_format', _this2.dateformat);
                  formdata.append('time_format', _this2.timeformat);
                }

                if (_type === "Configration") {
                  if (_this2.logo) {
                    formdata.append('logo', _this2.logo);
                  }

                  formdata.append('emails', _this2.emails);
                }

                if (_type === "Template") {
                  formdata.append('subject', _this2.template_subject);
                  formdata.append('title', _this2.template_title);
                  formdata.append('body', _this2.template_body);
                }

                formdata.append('_type', _type);
                axios__WEBPACK_IMPORTED_MODULE_1___default.a.post('/api/settings/store?token=' + localStorage.getItem("api_token"), formdata).then(function (response) {
                  var val = response.data,
                      values = response.data.values;
                  that.message = val.message;
                  that.alertClass = true;
                  that.showAlert();

                  if (_type === "Configration") {
                    that.$refs.logoInput.state = '';
                    that.logo = "";
                    that.viewLogo = values.values.logo.value;
                  }
                })["catch"](function (error) {
                  that.message = "Unable To Proceed Your Request.";
                  that.alertClass = false;
                  that.showAlert();
                });

              case 7:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    showAlert: function showAlert() {
      var _this3 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _this3.dismissCountDown = _this3.dismissSecs;

              case 1:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    getFile: function getFile(name) {
      var _this4 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                if (name == "logo") {
                  _this4.logo = event.target.files[0];
                }

              case 1:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }))();
    }
  },
  mounted: function mounted() {
    var that = this;
    axios__WEBPACK_IMPORTED_MODULE_1___default.a.get('/api/settings?token=' + localStorage.getItem("api_token")).then(function (response) {
      var values = response.data.values;
      that.zones = values.formats.zones;
      that.dateVals = values.formats.dates;
      that.timeVals = values.formats.times;
      that.timezone = values.zone.value;
      that.dateformat = values.date.value;
      that.timeformat = values.time.value;
      that.viewLogo = values.logo.value;
      that.emails = values.emails.value;

      if (values.template.value) {
        var val = JSON.parse(values.template.value);
        that.template_body = val.body;
        that.template_subject = val.subject;
        that.template_title = val.title;
      }
    })["catch"](function (error) {
      console.log('unable to proceed');
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/settings/GlobalConfig.vue?vue&type=template&id=64117092&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/settings/GlobalConfig.vue?vue&type=template&id=64117092& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", lg: "12" } },
        [
          _c(
            "CCard",
            { attrs: { "no-header": "" } },
            [
              _c(
                "CCardBody",
                [
                  _c("div", { staticClass: "task-heading" }, [
                    _c(
                      "h3",
                      {
                        staticClass:
                          "d-flex justify-content-between align-items-center"
                      },
                      [
                        _vm._v(" Global Configurations "),
                        _c(
                          "CButton",
                          {
                            staticClass: "float-right",
                            attrs: { color: "secondary" },
                            on: { click: _vm.goBack }
                          },
                          [_vm._v("Back")]
                        )
                      ],
                      1
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "CAlert",
                    {
                      attrs: {
                        show: _vm.dismissCountDown,
                        color: _vm.alertClass ? "success" : "danger",
                        fade: ""
                      },
                      on: {
                        "update:show": function($event) {
                          _vm.dismissCountDown = $event
                        }
                      }
                    },
                    [
                      _vm._v(
                        "\n\t\t            \t" +
                          _vm._s(_vm.message) +
                          "\n\t\t          \t"
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "CTabs",
                    {
                      staticClass: "custom-tabs-sec",
                      attrs: { variant: "pills", "active-tab": 0 }
                    },
                    [
                      _c(
                        "CTab",
                        { attrs: { title: "Configurations" } },
                        [
                          _c(
                            "CForm",
                            {
                              attrs: { method: "POST" },
                              on: {
                                submit: function($event) {
                                  $event.preventDefault()
                                  return _vm.store($event)
                                }
                              }
                            },
                            [
                              _c(
                                "CRow",
                                [
                                  _c("CCol", [
                                    _c(
                                      "div",
                                      { staticClass: "form-group" },
                                      [
                                        _c("input", {
                                          attrs: {
                                            type: "hidden",
                                            name: "_type",
                                            value: "Configration"
                                          }
                                        }),
                                        _vm._v(" "),
                                        _c("CInput", {
                                          ref: "logoInput",
                                          attrs: {
                                            label: "Logo",
                                            type: "file",
                                            accept: "image/*"
                                          },
                                          on: {
                                            change: function($event) {
                                              return _vm.getFile("logo")
                                            }
                                          }
                                        })
                                      ],
                                      1
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c("CCol", [
                                    _vm.viewLogo
                                      ? _c("img", {
                                          staticStyle: { width: "100px" },
                                          attrs: {
                                            src: _vm.viewLogo,
                                            alt: "Logo"
                                          }
                                        })
                                      : _vm._e()
                                  ])
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "CRow",
                                [
                                  _c("CCol", [
                                    _c(
                                      "div",
                                      { staticClass: "form-group" },
                                      [
                                        _c("CInput", {
                                          attrs: {
                                            label: "Admin Email's",
                                            type: "email",
                                            multiple: ""
                                          },
                                          model: {
                                            value: _vm.emails,
                                            callback: function($$v) {
                                              _vm.emails = $$v
                                            },
                                            expression: "emails"
                                          }
                                        })
                                      ],
                                      1
                                    )
                                  ])
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "CButton",
                                {
                                  staticClass: "mt-3",
                                  attrs: { color: "primary", type: "submit" }
                                },
                                [_vm._v("Save")]
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "CTab",
                        { attrs: { title: "Date/Time" } },
                        [
                          _c(
                            "CForm",
                            {
                              attrs: { method: "POST" },
                              on: {
                                submit: function($event) {
                                  $event.preventDefault()
                                  return _vm.store($event)
                                }
                              }
                            },
                            [
                              _c(
                                "CRow",
                                [
                                  _c("CCol", [
                                    _c("div", { staticClass: "form-group" }, [
                                      _c("input", {
                                        attrs: {
                                          type: "hidden",
                                          name: "_type",
                                          value: "Date Time"
                                        }
                                      }),
                                      _vm._v(" "),
                                      _c("label", [_vm._v("TimeZone")]),
                                      _vm._v(" "),
                                      _c(
                                        "select",
                                        {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: _vm.timezone,
                                              expression: "timezone"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          attrs: { required: "" },
                                          on: {
                                            change: function($event) {
                                              var $$selectedVal = Array.prototype.filter
                                                .call(
                                                  $event.target.options,
                                                  function(o) {
                                                    return o.selected
                                                  }
                                                )
                                                .map(function(o) {
                                                  var val =
                                                    "_value" in o
                                                      ? o._value
                                                      : o.value
                                                  return val
                                                })
                                              _vm.timezone = $event.target
                                                .multiple
                                                ? $$selectedVal
                                                : $$selectedVal[0]
                                            }
                                          }
                                        },
                                        [
                                          _c(
                                            "option",
                                            { attrs: { value: "" } },
                                            [_vm._v("--Select Timezone--")]
                                          ),
                                          _vm._v(" "),
                                          _vm._l(_vm.zones, function(zone) {
                                            return _c(
                                              "option",
                                              {
                                                domProps: { value: zone.value }
                                              },
                                              [_vm._v(_vm._s(zone.name))]
                                            )
                                          })
                                        ],
                                        2
                                      )
                                    ])
                                  ]),
                                  _vm._v(" "),
                                  _c("CCol", [
                                    _c("div", { staticClass: "form-group" }, [
                                      _c("label", [_vm._v("Date Format")]),
                                      _vm._v(" "),
                                      _c(
                                        "select",
                                        {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: _vm.dateformat,
                                              expression: "dateformat"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          attrs: { required: "" },
                                          on: {
                                            change: function($event) {
                                              var $$selectedVal = Array.prototype.filter
                                                .call(
                                                  $event.target.options,
                                                  function(o) {
                                                    return o.selected
                                                  }
                                                )
                                                .map(function(o) {
                                                  var val =
                                                    "_value" in o
                                                      ? o._value
                                                      : o.value
                                                  return val
                                                })
                                              _vm.dateformat = $event.target
                                                .multiple
                                                ? $$selectedVal
                                                : $$selectedVal[0]
                                            }
                                          }
                                        },
                                        [
                                          _c(
                                            "option",
                                            { attrs: { value: "" } },
                                            [_vm._v("--Select Date Format--")]
                                          ),
                                          _vm._v(" "),
                                          _vm._l(_vm.dateVals, function(
                                            dateVal
                                          ) {
                                            return _c(
                                              "option",
                                              {
                                                domProps: {
                                                  value: dateVal.value
                                                }
                                              },
                                              [_vm._v(_vm._s(dateVal.name))]
                                            )
                                          })
                                        ],
                                        2
                                      )
                                    ])
                                  ])
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "CRow",
                                [
                                  _c("CCol", { attrs: { col: "6" } }, [
                                    _c("div", { staticClass: "form-group" }, [
                                      _c("label", [_vm._v("Time Format")]),
                                      _vm._v(" "),
                                      _c(
                                        "select",
                                        {
                                          directives: [
                                            {
                                              name: "model",
                                              rawName: "v-model",
                                              value: _vm.timeformat,
                                              expression: "timeformat"
                                            }
                                          ],
                                          staticClass: "form-control",
                                          attrs: { required: "" },
                                          on: {
                                            change: function($event) {
                                              var $$selectedVal = Array.prototype.filter
                                                .call(
                                                  $event.target.options,
                                                  function(o) {
                                                    return o.selected
                                                  }
                                                )
                                                .map(function(o) {
                                                  var val =
                                                    "_value" in o
                                                      ? o._value
                                                      : o.value
                                                  return val
                                                })
                                              _vm.timeformat = $event.target
                                                .multiple
                                                ? $$selectedVal
                                                : $$selectedVal[0]
                                            }
                                          }
                                        },
                                        [
                                          _c(
                                            "option",
                                            { attrs: { value: "" } },
                                            [_vm._v("--Select Time Format--")]
                                          ),
                                          _vm._v(" "),
                                          _vm._l(_vm.timeVals, function(
                                            timeVal
                                          ) {
                                            return _c(
                                              "option",
                                              {
                                                domProps: {
                                                  value: timeVal.value
                                                }
                                              },
                                              [_vm._v(_vm._s(timeVal.name))]
                                            )
                                          })
                                        ],
                                        2
                                      )
                                    ])
                                  ])
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "CButton",
                                {
                                  staticClass: "mt-3",
                                  attrs: { color: "primary", type: "submit" }
                                },
                                [_vm._v("Save")]
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "CTab",
                        { attrs: { title: "Email Templates" } },
                        [
                          _c(
                            "CForm",
                            {
                              attrs: { method: "POST" },
                              on: {
                                submit: function($event) {
                                  $event.preventDefault()
                                  return _vm.store($event)
                                }
                              }
                            },
                            [
                              _c(
                                "CRow",
                                [
                                  _c("input", {
                                    attrs: {
                                      type: "hidden",
                                      name: "_type",
                                      value: "Template"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("CCol", { attrs: { col: "12" } }, [
                                    _c(
                                      "div",
                                      { staticClass: "form-group" },
                                      [
                                        _c("CInput", {
                                          attrs: {
                                            label: "Subject",
                                            type: "text"
                                          },
                                          model: {
                                            value: _vm.template_subject,
                                            callback: function($$v) {
                                              _vm.template_subject = $$v
                                            },
                                            expression: "template_subject"
                                          }
                                        })
                                      ],
                                      1
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c("CCol", { attrs: { col: "12" } }, [
                                    _c(
                                      "div",
                                      { staticClass: "form-group" },
                                      [
                                        _c("CInput", {
                                          attrs: {
                                            label: "Title",
                                            type: "text"
                                          },
                                          model: {
                                            value: _vm.template_title,
                                            callback: function($$v) {
                                              _vm.template_title = $$v
                                            },
                                            expression: "template_title"
                                          }
                                        })
                                      ],
                                      1
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c("CCol", { attrs: { col: "12" } }, [
                                    _c(
                                      "div",
                                      { staticClass: "form-group" },
                                      [
                                        _c("label", [_vm._v("Body")]),
                                        _vm._v(" "),
                                        _c(
                                          "DxHtmlEditor",
                                          {
                                            ref: "textEditor",
                                            attrs: { height: "300px" },
                                            model: {
                                              value: _vm.template_body,
                                              callback: function($$v) {
                                                _vm.template_body = $$v
                                              },
                                              expression: "template_body"
                                            }
                                          },
                                          [
                                            _c(
                                              "DxToolbar",
                                              [
                                                _c("DxItem", {
                                                  attrs: {
                                                    "format-name": "header"
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c("DxItem", {
                                                  attrs: {
                                                    "format-name": "bold"
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c("DxItem", {
                                                  attrs: {
                                                    "format-name": "italic"
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c("DxItem", {
                                                  attrs: {
                                                    "format-name": "underline"
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c("DxItem", {
                                                  attrs: {
                                                    "format-name": "separator"
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c("DxItem", {
                                                  attrs: {
                                                    "format-name": "bulletList"
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c("DxItem", {
                                                  attrs: {
                                                    "format-name": "link"
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c("DxItem", {
                                                  attrs: {
                                                    "format-name": "blockquote"
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c("DxItem", {
                                                  attrs: {
                                                    "format-name": "alignLeft"
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c("DxItem", {
                                                  attrs: {
                                                    "format-name": "alignCenter"
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c("DxItem", {
                                                  attrs: {
                                                    "format-name": "alignRight"
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c("DxItem", {
                                                  attrs: {
                                                    "format-name":
                                                      "alignJustify"
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c("DxItem", {
                                                  attrs: {
                                                    "format-name": "separator"
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c("DxItem", {
                                                  attrs: {
                                                    "format-name": "color"
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c("DxItem", {
                                                  attrs: {
                                                    "format-name": "background"
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c("DxItem", {
                                                  attrs: {
                                                    "format-name": "undo"
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c("DxItem", {
                                                  attrs: {
                                                    "format-name": "redo"
                                                  }
                                                })
                                              ],
                                              1
                                            )
                                          ],
                                          1
                                        )
                                      ],
                                      1
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c("CCol", { attrs: { col: "12" } }, [
                                    _c(
                                      "div",
                                      { staticClass: "template-footer" },
                                      [
                                        _c("span", [
                                          _vm._v(
                                            "Mail body tags: {from_name}, {from_email}, {task_link}"
                                          )
                                        ])
                                      ]
                                    )
                                  ])
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "CButton",
                                {
                                  staticClass: "mt-3",
                                  attrs: { color: "primary", type: "submit" }
                                },
                                [_vm._v("Save")]
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);