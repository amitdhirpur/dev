(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[115],{

/***/ "../coreui/node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**********************************************************!*\
  !*** ../coreui/node_modules/moment/locale sync ^\.\/.*$ ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "../coreui/node_modules/moment/locale/af.js",
	"./af.js": "../coreui/node_modules/moment/locale/af.js",
	"./ar": "../coreui/node_modules/moment/locale/ar.js",
	"./ar-dz": "../coreui/node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "../coreui/node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "../coreui/node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "../coreui/node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "../coreui/node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "../coreui/node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "../coreui/node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "../coreui/node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "../coreui/node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "../coreui/node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "../coreui/node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "../coreui/node_modules/moment/locale/ar-tn.js",
	"./ar.js": "../coreui/node_modules/moment/locale/ar.js",
	"./az": "../coreui/node_modules/moment/locale/az.js",
	"./az.js": "../coreui/node_modules/moment/locale/az.js",
	"./be": "../coreui/node_modules/moment/locale/be.js",
	"./be.js": "../coreui/node_modules/moment/locale/be.js",
	"./bg": "../coreui/node_modules/moment/locale/bg.js",
	"./bg.js": "../coreui/node_modules/moment/locale/bg.js",
	"./bm": "../coreui/node_modules/moment/locale/bm.js",
	"./bm.js": "../coreui/node_modules/moment/locale/bm.js",
	"./bn": "../coreui/node_modules/moment/locale/bn.js",
	"./bn-bd": "../coreui/node_modules/moment/locale/bn-bd.js",
	"./bn-bd.js": "../coreui/node_modules/moment/locale/bn-bd.js",
	"./bn.js": "../coreui/node_modules/moment/locale/bn.js",
	"./bo": "../coreui/node_modules/moment/locale/bo.js",
	"./bo.js": "../coreui/node_modules/moment/locale/bo.js",
	"./br": "../coreui/node_modules/moment/locale/br.js",
	"./br.js": "../coreui/node_modules/moment/locale/br.js",
	"./bs": "../coreui/node_modules/moment/locale/bs.js",
	"./bs.js": "../coreui/node_modules/moment/locale/bs.js",
	"./ca": "../coreui/node_modules/moment/locale/ca.js",
	"./ca.js": "../coreui/node_modules/moment/locale/ca.js",
	"./cs": "../coreui/node_modules/moment/locale/cs.js",
	"./cs.js": "../coreui/node_modules/moment/locale/cs.js",
	"./cv": "../coreui/node_modules/moment/locale/cv.js",
	"./cv.js": "../coreui/node_modules/moment/locale/cv.js",
	"./cy": "../coreui/node_modules/moment/locale/cy.js",
	"./cy.js": "../coreui/node_modules/moment/locale/cy.js",
	"./da": "../coreui/node_modules/moment/locale/da.js",
	"./da.js": "../coreui/node_modules/moment/locale/da.js",
	"./de": "../coreui/node_modules/moment/locale/de.js",
	"./de-at": "../coreui/node_modules/moment/locale/de-at.js",
	"./de-at.js": "../coreui/node_modules/moment/locale/de-at.js",
	"./de-ch": "../coreui/node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "../coreui/node_modules/moment/locale/de-ch.js",
	"./de.js": "../coreui/node_modules/moment/locale/de.js",
	"./dv": "../coreui/node_modules/moment/locale/dv.js",
	"./dv.js": "../coreui/node_modules/moment/locale/dv.js",
	"./el": "../coreui/node_modules/moment/locale/el.js",
	"./el.js": "../coreui/node_modules/moment/locale/el.js",
	"./en-au": "../coreui/node_modules/moment/locale/en-au.js",
	"./en-au.js": "../coreui/node_modules/moment/locale/en-au.js",
	"./en-ca": "../coreui/node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "../coreui/node_modules/moment/locale/en-ca.js",
	"./en-gb": "../coreui/node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "../coreui/node_modules/moment/locale/en-gb.js",
	"./en-ie": "../coreui/node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "../coreui/node_modules/moment/locale/en-ie.js",
	"./en-il": "../coreui/node_modules/moment/locale/en-il.js",
	"./en-il.js": "../coreui/node_modules/moment/locale/en-il.js",
	"./en-in": "../coreui/node_modules/moment/locale/en-in.js",
	"./en-in.js": "../coreui/node_modules/moment/locale/en-in.js",
	"./en-nz": "../coreui/node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "../coreui/node_modules/moment/locale/en-nz.js",
	"./en-sg": "../coreui/node_modules/moment/locale/en-sg.js",
	"./en-sg.js": "../coreui/node_modules/moment/locale/en-sg.js",
	"./eo": "../coreui/node_modules/moment/locale/eo.js",
	"./eo.js": "../coreui/node_modules/moment/locale/eo.js",
	"./es": "../coreui/node_modules/moment/locale/es.js",
	"./es-do": "../coreui/node_modules/moment/locale/es-do.js",
	"./es-do.js": "../coreui/node_modules/moment/locale/es-do.js",
	"./es-mx": "../coreui/node_modules/moment/locale/es-mx.js",
	"./es-mx.js": "../coreui/node_modules/moment/locale/es-mx.js",
	"./es-us": "../coreui/node_modules/moment/locale/es-us.js",
	"./es-us.js": "../coreui/node_modules/moment/locale/es-us.js",
	"./es.js": "../coreui/node_modules/moment/locale/es.js",
	"./et": "../coreui/node_modules/moment/locale/et.js",
	"./et.js": "../coreui/node_modules/moment/locale/et.js",
	"./eu": "../coreui/node_modules/moment/locale/eu.js",
	"./eu.js": "../coreui/node_modules/moment/locale/eu.js",
	"./fa": "../coreui/node_modules/moment/locale/fa.js",
	"./fa.js": "../coreui/node_modules/moment/locale/fa.js",
	"./fi": "../coreui/node_modules/moment/locale/fi.js",
	"./fi.js": "../coreui/node_modules/moment/locale/fi.js",
	"./fil": "../coreui/node_modules/moment/locale/fil.js",
	"./fil.js": "../coreui/node_modules/moment/locale/fil.js",
	"./fo": "../coreui/node_modules/moment/locale/fo.js",
	"./fo.js": "../coreui/node_modules/moment/locale/fo.js",
	"./fr": "../coreui/node_modules/moment/locale/fr.js",
	"./fr-ca": "../coreui/node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "../coreui/node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "../coreui/node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "../coreui/node_modules/moment/locale/fr-ch.js",
	"./fr.js": "../coreui/node_modules/moment/locale/fr.js",
	"./fy": "../coreui/node_modules/moment/locale/fy.js",
	"./fy.js": "../coreui/node_modules/moment/locale/fy.js",
	"./ga": "../coreui/node_modules/moment/locale/ga.js",
	"./ga.js": "../coreui/node_modules/moment/locale/ga.js",
	"./gd": "../coreui/node_modules/moment/locale/gd.js",
	"./gd.js": "../coreui/node_modules/moment/locale/gd.js",
	"./gl": "../coreui/node_modules/moment/locale/gl.js",
	"./gl.js": "../coreui/node_modules/moment/locale/gl.js",
	"./gom-deva": "../coreui/node_modules/moment/locale/gom-deva.js",
	"./gom-deva.js": "../coreui/node_modules/moment/locale/gom-deva.js",
	"./gom-latn": "../coreui/node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "../coreui/node_modules/moment/locale/gom-latn.js",
	"./gu": "../coreui/node_modules/moment/locale/gu.js",
	"./gu.js": "../coreui/node_modules/moment/locale/gu.js",
	"./he": "../coreui/node_modules/moment/locale/he.js",
	"./he.js": "../coreui/node_modules/moment/locale/he.js",
	"./hi": "../coreui/node_modules/moment/locale/hi.js",
	"./hi.js": "../coreui/node_modules/moment/locale/hi.js",
	"./hr": "../coreui/node_modules/moment/locale/hr.js",
	"./hr.js": "../coreui/node_modules/moment/locale/hr.js",
	"./hu": "../coreui/node_modules/moment/locale/hu.js",
	"./hu.js": "../coreui/node_modules/moment/locale/hu.js",
	"./hy-am": "../coreui/node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "../coreui/node_modules/moment/locale/hy-am.js",
	"./id": "../coreui/node_modules/moment/locale/id.js",
	"./id.js": "../coreui/node_modules/moment/locale/id.js",
	"./is": "../coreui/node_modules/moment/locale/is.js",
	"./is.js": "../coreui/node_modules/moment/locale/is.js",
	"./it": "../coreui/node_modules/moment/locale/it.js",
	"./it-ch": "../coreui/node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "../coreui/node_modules/moment/locale/it-ch.js",
	"./it.js": "../coreui/node_modules/moment/locale/it.js",
	"./ja": "../coreui/node_modules/moment/locale/ja.js",
	"./ja.js": "../coreui/node_modules/moment/locale/ja.js",
	"./jv": "../coreui/node_modules/moment/locale/jv.js",
	"./jv.js": "../coreui/node_modules/moment/locale/jv.js",
	"./ka": "../coreui/node_modules/moment/locale/ka.js",
	"./ka.js": "../coreui/node_modules/moment/locale/ka.js",
	"./kk": "../coreui/node_modules/moment/locale/kk.js",
	"./kk.js": "../coreui/node_modules/moment/locale/kk.js",
	"./km": "../coreui/node_modules/moment/locale/km.js",
	"./km.js": "../coreui/node_modules/moment/locale/km.js",
	"./kn": "../coreui/node_modules/moment/locale/kn.js",
	"./kn.js": "../coreui/node_modules/moment/locale/kn.js",
	"./ko": "../coreui/node_modules/moment/locale/ko.js",
	"./ko.js": "../coreui/node_modules/moment/locale/ko.js",
	"./ku": "../coreui/node_modules/moment/locale/ku.js",
	"./ku.js": "../coreui/node_modules/moment/locale/ku.js",
	"./ky": "../coreui/node_modules/moment/locale/ky.js",
	"./ky.js": "../coreui/node_modules/moment/locale/ky.js",
	"./lb": "../coreui/node_modules/moment/locale/lb.js",
	"./lb.js": "../coreui/node_modules/moment/locale/lb.js",
	"./lo": "../coreui/node_modules/moment/locale/lo.js",
	"./lo.js": "../coreui/node_modules/moment/locale/lo.js",
	"./lt": "../coreui/node_modules/moment/locale/lt.js",
	"./lt.js": "../coreui/node_modules/moment/locale/lt.js",
	"./lv": "../coreui/node_modules/moment/locale/lv.js",
	"./lv.js": "../coreui/node_modules/moment/locale/lv.js",
	"./me": "../coreui/node_modules/moment/locale/me.js",
	"./me.js": "../coreui/node_modules/moment/locale/me.js",
	"./mi": "../coreui/node_modules/moment/locale/mi.js",
	"./mi.js": "../coreui/node_modules/moment/locale/mi.js",
	"./mk": "../coreui/node_modules/moment/locale/mk.js",
	"./mk.js": "../coreui/node_modules/moment/locale/mk.js",
	"./ml": "../coreui/node_modules/moment/locale/ml.js",
	"./ml.js": "../coreui/node_modules/moment/locale/ml.js",
	"./mn": "../coreui/node_modules/moment/locale/mn.js",
	"./mn.js": "../coreui/node_modules/moment/locale/mn.js",
	"./mr": "../coreui/node_modules/moment/locale/mr.js",
	"./mr.js": "../coreui/node_modules/moment/locale/mr.js",
	"./ms": "../coreui/node_modules/moment/locale/ms.js",
	"./ms-my": "../coreui/node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "../coreui/node_modules/moment/locale/ms-my.js",
	"./ms.js": "../coreui/node_modules/moment/locale/ms.js",
	"./mt": "../coreui/node_modules/moment/locale/mt.js",
	"./mt.js": "../coreui/node_modules/moment/locale/mt.js",
	"./my": "../coreui/node_modules/moment/locale/my.js",
	"./my.js": "../coreui/node_modules/moment/locale/my.js",
	"./nb": "../coreui/node_modules/moment/locale/nb.js",
	"./nb.js": "../coreui/node_modules/moment/locale/nb.js",
	"./ne": "../coreui/node_modules/moment/locale/ne.js",
	"./ne.js": "../coreui/node_modules/moment/locale/ne.js",
	"./nl": "../coreui/node_modules/moment/locale/nl.js",
	"./nl-be": "../coreui/node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "../coreui/node_modules/moment/locale/nl-be.js",
	"./nl.js": "../coreui/node_modules/moment/locale/nl.js",
	"./nn": "../coreui/node_modules/moment/locale/nn.js",
	"./nn.js": "../coreui/node_modules/moment/locale/nn.js",
	"./oc-lnc": "../coreui/node_modules/moment/locale/oc-lnc.js",
	"./oc-lnc.js": "../coreui/node_modules/moment/locale/oc-lnc.js",
	"./pa-in": "../coreui/node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "../coreui/node_modules/moment/locale/pa-in.js",
	"./pl": "../coreui/node_modules/moment/locale/pl.js",
	"./pl.js": "../coreui/node_modules/moment/locale/pl.js",
	"./pt": "../coreui/node_modules/moment/locale/pt.js",
	"./pt-br": "../coreui/node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "../coreui/node_modules/moment/locale/pt-br.js",
	"./pt.js": "../coreui/node_modules/moment/locale/pt.js",
	"./ro": "../coreui/node_modules/moment/locale/ro.js",
	"./ro.js": "../coreui/node_modules/moment/locale/ro.js",
	"./ru": "../coreui/node_modules/moment/locale/ru.js",
	"./ru.js": "../coreui/node_modules/moment/locale/ru.js",
	"./sd": "../coreui/node_modules/moment/locale/sd.js",
	"./sd.js": "../coreui/node_modules/moment/locale/sd.js",
	"./se": "../coreui/node_modules/moment/locale/se.js",
	"./se.js": "../coreui/node_modules/moment/locale/se.js",
	"./si": "../coreui/node_modules/moment/locale/si.js",
	"./si.js": "../coreui/node_modules/moment/locale/si.js",
	"./sk": "../coreui/node_modules/moment/locale/sk.js",
	"./sk.js": "../coreui/node_modules/moment/locale/sk.js",
	"./sl": "../coreui/node_modules/moment/locale/sl.js",
	"./sl.js": "../coreui/node_modules/moment/locale/sl.js",
	"./sq": "../coreui/node_modules/moment/locale/sq.js",
	"./sq.js": "../coreui/node_modules/moment/locale/sq.js",
	"./sr": "../coreui/node_modules/moment/locale/sr.js",
	"./sr-cyrl": "../coreui/node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "../coreui/node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "../coreui/node_modules/moment/locale/sr.js",
	"./ss": "../coreui/node_modules/moment/locale/ss.js",
	"./ss.js": "../coreui/node_modules/moment/locale/ss.js",
	"./sv": "../coreui/node_modules/moment/locale/sv.js",
	"./sv.js": "../coreui/node_modules/moment/locale/sv.js",
	"./sw": "../coreui/node_modules/moment/locale/sw.js",
	"./sw.js": "../coreui/node_modules/moment/locale/sw.js",
	"./ta": "../coreui/node_modules/moment/locale/ta.js",
	"./ta.js": "../coreui/node_modules/moment/locale/ta.js",
	"./te": "../coreui/node_modules/moment/locale/te.js",
	"./te.js": "../coreui/node_modules/moment/locale/te.js",
	"./tet": "../coreui/node_modules/moment/locale/tet.js",
	"./tet.js": "../coreui/node_modules/moment/locale/tet.js",
	"./tg": "../coreui/node_modules/moment/locale/tg.js",
	"./tg.js": "../coreui/node_modules/moment/locale/tg.js",
	"./th": "../coreui/node_modules/moment/locale/th.js",
	"./th.js": "../coreui/node_modules/moment/locale/th.js",
	"./tk": "../coreui/node_modules/moment/locale/tk.js",
	"./tk.js": "../coreui/node_modules/moment/locale/tk.js",
	"./tl-ph": "../coreui/node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "../coreui/node_modules/moment/locale/tl-ph.js",
	"./tlh": "../coreui/node_modules/moment/locale/tlh.js",
	"./tlh.js": "../coreui/node_modules/moment/locale/tlh.js",
	"./tr": "../coreui/node_modules/moment/locale/tr.js",
	"./tr.js": "../coreui/node_modules/moment/locale/tr.js",
	"./tzl": "../coreui/node_modules/moment/locale/tzl.js",
	"./tzl.js": "../coreui/node_modules/moment/locale/tzl.js",
	"./tzm": "../coreui/node_modules/moment/locale/tzm.js",
	"./tzm-latn": "../coreui/node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "../coreui/node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "../coreui/node_modules/moment/locale/tzm.js",
	"./ug-cn": "../coreui/node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "../coreui/node_modules/moment/locale/ug-cn.js",
	"./uk": "../coreui/node_modules/moment/locale/uk.js",
	"./uk.js": "../coreui/node_modules/moment/locale/uk.js",
	"./ur": "../coreui/node_modules/moment/locale/ur.js",
	"./ur.js": "../coreui/node_modules/moment/locale/ur.js",
	"./uz": "../coreui/node_modules/moment/locale/uz.js",
	"./uz-latn": "../coreui/node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "../coreui/node_modules/moment/locale/uz-latn.js",
	"./uz.js": "../coreui/node_modules/moment/locale/uz.js",
	"./vi": "../coreui/node_modules/moment/locale/vi.js",
	"./vi.js": "../coreui/node_modules/moment/locale/vi.js",
	"./x-pseudo": "../coreui/node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "../coreui/node_modules/moment/locale/x-pseudo.js",
	"./yo": "../coreui/node_modules/moment/locale/yo.js",
	"./yo.js": "../coreui/node_modules/moment/locale/yo.js",
	"./zh-cn": "../coreui/node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "../coreui/node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "../coreui/node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "../coreui/node_modules/moment/locale/zh-hk.js",
	"./zh-mo": "../coreui/node_modules/moment/locale/zh-mo.js",
	"./zh-mo.js": "../coreui/node_modules/moment/locale/zh-mo.js",
	"./zh-tw": "../coreui/node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "../coreui/node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "../coreui/node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "../coreui/src/views/tasks/Editask.vue":
/*!*********************************************!*\
  !*** ../coreui/src/views/tasks/Editask.vue ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Editask_vue_vue_type_template_id_79648274___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Editask.vue?vue&type=template&id=79648274& */ "../coreui/src/views/tasks/Editask.vue?vue&type=template&id=79648274&");
/* harmony import */ var _Editask_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Editask.vue?vue&type=script&lang=js& */ "../coreui/src/views/tasks/Editask.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var vue_multiselect_dist_vue_multiselect_min_css_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-multiselect/dist/vue-multiselect.min.css?vue&type=style&index=0&lang=css& */ "../coreui/node_modules/vue-multiselect/dist/vue-multiselect.min.css?vue&type=style&index=0&lang=css&");
/* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Editask_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Editask_vue_vue_type_template_id_79648274___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Editask_vue_vue_type_template_id_79648274___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/tasks/Editask.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/tasks/Editask.vue?vue&type=script&lang=js&":
/*!**********************************************************************!*\
  !*** ../coreui/src/views/tasks/Editask.vue?vue&type=script&lang=js& ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Editask_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Editask.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/tasks/Editask.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Editask_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/tasks/Editask.vue?vue&type=template&id=79648274&":
/*!****************************************************************************!*\
  !*** ../coreui/src/views/tasks/Editask.vue?vue&type=template&id=79648274& ***!
  \****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Editask_vue_vue_type_template_id_79648274___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./Editask.vue?vue&type=template&id=79648274& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/tasks/Editask.vue?vue&type=template&id=79648274&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Editask_vue_vue_type_template_id_79648274___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_Editask_vue_vue_type_template_id_79648274___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/tasks/Editask.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/tasks/Editask.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuejs_datepicker__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuejs-datepicker */ "../coreui/node_modules/vuejs-datepicker/dist/vuejs-datepicker.esm.js");
/* harmony import */ var vue_multiselect__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue-multiselect */ "../coreui/node_modules/vue-multiselect/dist/vue-multiselect.min.js");
/* harmony import */ var vue_multiselect__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(vue_multiselect__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var vue2_editor__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue2-editor */ "../coreui/node_modules/vue2-editor/dist/vue2-editor.esm.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "../coreui/node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Edittask',
  data: function data() {
    return {
      projects: [],
      users: [],
      values: '',
      value: '',
      alertClass: false,
      task_id: '',
      name: '',
      project: '',
      priority: '',
      status: '',
      responsible: '',
      observer: [],
      participants: [],
      description: '',
      deadline: new Date(),
      message: '',
      dismissSecs: 7,
      dismissCountDown: 0,
      showDismissibleAlert: false
    };
  },
  components: {
    Datepicker: vuejs_datepicker__WEBPACK_IMPORTED_MODULE_1__["default"],
    VueEditor: vue2_editor__WEBPACK_IMPORTED_MODULE_3__["VueEditor"],
    Multiselect: vue_multiselect__WEBPACK_IMPORTED_MODULE_2___default.a
  },
  methods: {
    goBack: function goBack() {
      this.$router.go(-1);
    },
    showAlert: function showAlert() {
      this.dismissCountDown = this.dismissSecs;
    },
    customFormatter: function customFormatter(date) {
      return moment__WEBPACK_IMPORTED_MODULE_4___default()(date).format('YYYY-MM-DD HH:mm:ss');
    },
    store: function store() {
      for (var i = 0; i < this.value.length; i++) {
        this.observer.push(this.value[i].id);
      }

      for (var i = 0; i < this.values.length; i++) {
        this.participants.push(this.values[i].id);
      }

      var extras = {};
      extras['observer'] = this.value;
      extras['participants'] = this.values;
      var formdata = new FormData();
      formdata.append('id', this.task_id);
      formdata.append('name', this.name);
      formdata.append('project', this.project);
      formdata.append('priority', this.priority);
      formdata.append('responsible', this.responsible);
      formdata.append('observer', JSON.stringify(this.observer));
      formdata.append('participants', JSON.stringify(this.participants));
      formdata.append('extras', JSON.stringify(extras));
      formdata.append('description', this.description);
      formdata.append('deadline', this.customFormatter(this.deadline));
      formdata.append('status', this.status);
      var that = this; // api to save task.

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/tasks/update?token=' + localStorage.getItem("api_token"), formdata).then(function (response) {
        if (response.data.status == 'success') {
          that.alertClass = true;
        } else {
          that.alertClass = false;
        }

        that.message = response.data.message;
        that.showAlert();
      })["catch"](function (error) {
        that.alertClass = false;
        that.message = 'Unable to complete process';
        that.showAlert();
      });
    }
  },
  mounted: function mounted() {
    var that = this;
    axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/tasks/edit/' + this.$route.params.id + '/?token=' + localStorage.getItem("api_token")).then(function (response) {
      var task = response.data.task;
      that.projects = response.data.projects;
      that.users = response.data.users;
      that.task_id = task.id;
      that.name = task.name;
      that.project = task.project;
      that.priority = task.priority;
      that.status = task.status;
      that.responsible = task.assigned_to;
      var extras = JSON.parse(task.extras);

      if (extras.observer) {
        that.value = extras.observer;
      }

      if (task.participants) {
        that.values = extras.participants;
      }

      that.description = task.description;
      that.deadline = task.deadline;
      that.created_at = task.created_at;
    })["catch"](function (error) {// that.$router.push({ path: '/dashboard' });
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/tasks/Editask.vue?vue&type=template&id=79648274&":
/*!**********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/tasks/Editask.vue?vue&type=template&id=79648274& ***!
  \**********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", lg: "12" } },
        [
          _c(
            "CCard",
            { attrs: { "no-header": "" } },
            [
              _c(
                "CCardBody",
                [
                  _c(
                    "h3",
                    [
                      _vm._v(" Edit Task - " + _vm._s(this.name) + " "),
                      _c(
                        "CButton",
                        {
                          staticClass: "float-right",
                          attrs: { color: "primary" },
                          on: { click: _vm.goBack }
                        },
                        [_vm._v("Back")]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "CAlert",
                    {
                      attrs: {
                        show: _vm.dismissCountDown,
                        color: _vm.alertClass ? "success" : "danger",
                        fade: ""
                      },
                      on: {
                        "update:show": function($event) {
                          _vm.dismissCountDown = $event
                        }
                      }
                    },
                    [
                      _vm._v(
                        "\n\t\t            \t(" +
                          _vm._s(_vm.dismissCountDown) +
                          ") " +
                          _vm._s(_vm.message) +
                          "\n\t\t          \t"
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "CForm",
                    {
                      attrs: { method: "POST" },
                      on: {
                        submit: function($event) {
                          $event.preventDefault()
                          return _vm.store($event)
                        }
                      }
                    },
                    [
                      _c("div", { staticClass: "row" }, [
                        _c(
                          "div",
                          { staticClass: "col" },
                          [
                            _c("CInput", {
                              attrs: {
                                label: "Name",
                                type: "text",
                                placeholder: "Name",
                                required: ""
                              },
                              model: {
                                value: _vm.name,
                                callback: function($$v) {
                                  _vm.name = $$v
                                },
                                expression: "name"
                              }
                            }),
                            _vm._v(" "),
                            _c("input", {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.task_id,
                                  expression: "task_id"
                                }
                              ],
                              attrs: { type: "hidden" },
                              domProps: { value: _vm.task_id },
                              on: {
                                input: function($event) {
                                  if ($event.target.composing) {
                                    return
                                  }
                                  _vm.task_id = $event.target.value
                                }
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "col" }, [
                          _c("div", { staticClass: "form-group" }, [
                            _c("label", [_vm._v("Project")]),
                            _vm._v(" "),
                            _c(
                              "select",
                              {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.project,
                                    expression: "project"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { required: "" },
                                on: {
                                  change: function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.project = $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  }
                                }
                              },
                              [
                                _c("option", { attrs: { value: "" } }, [
                                  _vm._v("--Select Project--")
                                ]),
                                _vm._v(" "),
                                _vm._l(_vm.projects, function(project) {
                                  return _c(
                                    "option",
                                    { domProps: { value: project.id } },
                                    [_vm._v(_vm._s(project.name))]
                                  )
                                })
                              ],
                              2
                            )
                          ])
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "col" }, [
                          _c("div", { staticClass: "form-group" }, [
                            _c("label", [_vm._v("Responsible Person")]),
                            _vm._v(" "),
                            _c(
                              "select",
                              {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.responsible,
                                    expression: "responsible"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: { required: "" },
                                on: {
                                  change: function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.responsible = $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  }
                                }
                              },
                              [
                                _c("option", { attrs: { value: "" } }, [
                                  _vm._v("--Select Person--")
                                ]),
                                _vm._v(" "),
                                _vm._l(_vm.users, function(user) {
                                  return _c(
                                    "option",
                                    { domProps: { value: user.id } },
                                    [_vm._v(_vm._s(user.name))]
                                  )
                                })
                              ],
                              2
                            )
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col" }, [
                          _c("div", { staticClass: "form-group" }, [
                            _c("label", [_vm._v("Priority")]),
                            _vm._v(" "),
                            _c(
                              "select",
                              {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.priority,
                                    expression: "priority"
                                  }
                                ],
                                staticClass: "form-control",
                                on: {
                                  change: function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.priority = $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  }
                                }
                              },
                              [
                                _c("option", { attrs: { value: "" } }, [
                                  _vm._v("--Select Priority--")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "0" } }, [
                                  _vm._v("Low")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "1" } }, [
                                  _vm._v("Medium")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "2" } }, [
                                  _vm._v("High")
                                ])
                              ]
                            )
                          ])
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "col" }, [
                          _c(
                            "div",
                            { staticClass: "form-group" },
                            [
                              _c("label", [_vm._v("Participants")]),
                              _vm._v(" "),
                              _c("multiselect", {
                                attrs: {
                                  "track-by": "id",
                                  label: "name",
                                  searchable: "",
                                  multiple: "",
                                  options: _vm.users
                                },
                                model: {
                                  value: _vm.values,
                                  callback: function($$v) {
                                    _vm.values = $$v
                                  },
                                  expression: "values"
                                }
                              })
                            ],
                            1
                          )
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col" }, [
                          _c(
                            "div",
                            { staticClass: "form-group" },
                            [
                              _c("label", [_vm._v("Observers")]),
                              _vm._v(" "),
                              _c("multiselect", {
                                attrs: {
                                  "track-by": "id",
                                  label: "name",
                                  searchable: "",
                                  multiple: "",
                                  options: _vm.users
                                },
                                model: {
                                  value: _vm.value,
                                  callback: function($$v) {
                                    _vm.value = $$v
                                  },
                                  expression: "value"
                                }
                              })
                            ],
                            1
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("div", { staticClass: "row" }, [
                        _c("div", { staticClass: "col" }, [
                          _c("div", { staticClass: "form-group" }, [
                            _c("label", [_vm._v("Status")]),
                            _vm._v(" "),
                            _c(
                              "select",
                              {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.status,
                                    expression: "status"
                                  }
                                ],
                                staticClass: "form-control",
                                on: {
                                  change: function($event) {
                                    var $$selectedVal = Array.prototype.filter
                                      .call($event.target.options, function(o) {
                                        return o.selected
                                      })
                                      .map(function(o) {
                                        var val =
                                          "_value" in o ? o._value : o.value
                                        return val
                                      })
                                    _vm.status = $event.target.multiple
                                      ? $$selectedVal
                                      : $$selectedVal[0]
                                  }
                                }
                              },
                              [
                                _c("option", { attrs: { value: "" } }, [
                                  _vm._v("--Select Status--")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "0" } }, [
                                  _vm._v("Pending")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "1" } }, [
                                  _vm._v("Complete")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "2" } }, [
                                  _vm._v("On Going")
                                ]),
                                _vm._v(" "),
                                _c("option", { attrs: { value: "3" } }, [
                                  _vm._v("Due")
                                ])
                              ]
                            )
                          ])
                        ]),
                        _vm._v(" "),
                        _c("div", { staticClass: "col" }, [
                          _c(
                            "div",
                            { staticClass: "form-group" },
                            [
                              _c("label", [_vm._v("Deadline")]),
                              _vm._v(" "),
                              _c("datepicker", {
                                attrs: {
                                  format: _vm.customFormatter,
                                  placeholder: "Deadline",
                                  "input-class": "form-control"
                                },
                                model: {
                                  value: _vm.deadline,
                                  callback: function($$v) {
                                    _vm.deadline = $$v
                                  },
                                  expression: "deadline"
                                }
                              })
                            ],
                            1
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("vue-editor", {
                        model: {
                          value: _vm.description,
                          callback: function($$v) {
                            _vm.description = $$v
                          },
                          expression: "description"
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        { attrs: { color: "primary", type: "submit" } },
                        [_vm._v("Update")]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);