(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[12],{

/***/ "../coreui/src/views/employees/EditEmployee.vue":
/*!******************************************************!*\
  !*** ../coreui/src/views/employees/EditEmployee.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EditEmployee_vue_vue_type_template_id_abd41b68___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EditEmployee.vue?vue&type=template&id=abd41b68& */ "../coreui/src/views/employees/EditEmployee.vue?vue&type=template&id=abd41b68&");
/* harmony import */ var _EditEmployee_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EditEmployee.vue?vue&type=script&lang=js& */ "../coreui/src/views/employees/EditEmployee.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EditEmployee_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EditEmployee_vue_vue_type_template_id_abd41b68___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EditEmployee_vue_vue_type_template_id_abd41b68___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/employees/EditEmployee.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/employees/EditEmployee.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ../coreui/src/views/employees/EditEmployee.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_EditEmployee_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./EditEmployee.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/EditEmployee.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_EditEmployee_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/employees/EditEmployee.vue?vue&type=template&id=abd41b68&":
/*!*************************************************************************************!*\
  !*** ../coreui/src/views/employees/EditEmployee.vue?vue&type=template&id=abd41b68& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_EditEmployee_vue_vue_type_template_id_abd41b68___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./EditEmployee.vue?vue&type=template&id=abd41b68& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/EditEmployee.vue?vue&type=template&id=abd41b68&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_EditEmployee_vue_vue_type_template_id_abd41b68___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_EditEmployee_vue_vue_type_template_id_abd41b68___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "../coreui/src/views/employees/validation-errors.vue":
/*!***********************************************************!*\
  !*** ../coreui/src/views/employees/validation-errors.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _validation_errors_vue_vue_type_template_id_2961bf1b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./validation-errors.vue?vue&type=template&id=2961bf1b& */ "../coreui/src/views/employees/validation-errors.vue?vue&type=template&id=2961bf1b&");
/* harmony import */ var _validation_errors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./validation-errors.vue?vue&type=script&lang=js& */ "../coreui/src/views/employees/validation-errors.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _validation_errors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _validation_errors_vue_vue_type_template_id_2961bf1b___WEBPACK_IMPORTED_MODULE_0__["render"],
  _validation_errors_vue_vue_type_template_id_2961bf1b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/employees/validation-errors.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/employees/validation-errors.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ../coreui/src/views/employees/validation-errors.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./validation-errors.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/validation-errors.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/employees/validation-errors.vue?vue&type=template&id=2961bf1b&":
/*!******************************************************************************************!*\
  !*** ../coreui/src/views/employees/validation-errors.vue?vue&type=template&id=2961bf1b& ***!
  \******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_template_id_2961bf1b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./validation-errors.vue?vue&type=template&id=2961bf1b& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/validation-errors.vue?vue&type=template&id=2961bf1b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_template_id_2961bf1b___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_validation_errors_vue_vue_type_template_id_2961bf1b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/EditEmployee.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/employees/EditEmployee.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _validation_errors_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./validation-errors.vue */ "../coreui/src/views/employees/validation-errors.vue");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "../coreui/node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! devextreme-vue/html-editor */ "../coreui/node_modules/devextreme-vue/html-editor.js");
/* harmony import */ var devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_3__);
var _methods;

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'EditEmployee',
  props: {
    caption: {
      type: String,
      "default": 'User id'
    }
  },
  data: function data() {
    return {
      tabs: ['Calculator', 'Shopping cart', 'Charts'],
      items: [],
      activeTab: 1,
      employee_id: '',
      name: '',
      email: '',
      is_active: 0,
      contact_number: '',
      alternate_number: '',
      position: '',
      department: '',
      joining_date: '',
      appraisel_date: '',
      additional_info: '',
      dof: '',
      pos: '',
      image: '',
      profile: '',
      pan_card: '',
      adhar_card: '',
      other_docs: '',
      official_docs: '',
      other_docss: [],
      officialDocs: [],
      otherDocs: [],
      panCard: '',
      adharCard: [],
      current_address: '',
      permanent_address: '',
      roles: [],
      avtar: '/img/user-avtar.jpg',
      loader: '/img/loader.gif',
      is_loading: false,
      showMessage: false,
      message: '',
      dismissSecs: 4,
      dismissCountDown: 0,
      new_password: '',
      confirm_password: '',
      showDismissibleAlert: false
    };
  },
  components: {
    validationError: _validation_errors_vue__WEBPACK_IMPORTED_MODULE_1__["default"],
    DxHtmlEditor: devextreme_vue_html_editor__WEBPACK_IMPORTED_MODULE_3__["DxHtmlEditor"]
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapGetters"])(["errors"])),
  methods: (_methods = {
    checked_active: function checked_active() {
      if (event.target.checked) {
        this.is_active = "1";
      } else {
        this.is_active = "0";
      }
    },
    goBack: function goBack() {
      this.$router.go(-1); // this.$router.replace({path: '/users'})
    },
    validationError: function validationError() {
      var self = this;
      var error = {};

      if (!self.name) {
        error.name = [];
        error.name[0] = "This Field is required!";
      }

      if (!self.employee_id) {
        error.employee_id = [];
        error.employee_id[0] = "This Field is required!";
      }

      if (!self.contact_number) {
        error.contact_number = [];
        error.contact_number[0] = "This Field is required!";
      }

      if (!self.position) {
        error.position = [];
        error.position[0] = "This Field is required!";
      }

      if (!self.pos) {
        error.pos = [];
        error.pos[0] = "This Field is required!";
      }

      if (!self.joining_date) {
        error.joining_date = [];
        error.joining_date[0] = "This Field is required!";
      }

      if (!self.dof) {
        error.dof = [];
        error.dof[0] = "This Field is required!";
      }

      if (Object.keys(error).length) {
        self.$store.commit("setErrors", error);
        return true;
      }

      self.$store.commit("setErrors", {});
      return false;
    },
    update: function update() {
      var self = this;
      var error = {};

      if (self.validationError()) {
        return;
      }

      this.is_loading = true;
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/users/' + self.$route.params.id + '?token=' + localStorage.getItem("api_token"), {
        _method: 'PUT',
        name: self.name,
        employee_id: self.employee_id,
        email: self.email,
        is_active: self.is_active,
        contact_no: self.contact_number,
        alternate_no: self.alternate_number,
        position: self.position,
        role: self.pos,
        department: self.department,
        birth_date: self.dof,
        profile: self.profile,
        joining_date: self.joining_date,
        appraisel_date: self.appraisel_date,
        additional_info: self.additional_info,
        current_address: self.current_address,
        permanent_address: self.permanent_address,
        headers: {
          'Content-Type': 'multipart/form-data'
        }
      }).then(function (response) {
        self.is_loading = false;

        if (response.data.status == 'success') {
          self.message = 'Successfully updated user.';
        } else if (response.data.status == 'error') {
          self.message = response.data.message;

          if (response.data.type == 'employee_id') {
            error.employee_id = [];
            error.employee_id[0] = response.data.message;
            self.$store.commit("setErrors", error);
          }
        }

        self.showAlert();
        self.$store.dispatch('topFunction');
      });
    },
    countDownChanged: function countDownChanged(dismissCountDown) {
      this.dismissCountDown = dismissCountDown;
    },
    showAlert: function showAlert() {
      this.dismissCountDown = this.dismissSecs;
    },
    getDesignations: function getDesignations() {
      var self = this;
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/designations?token=' + localStorage.getItem("api_token")).then(function (response) {
        self.items = response.data;
        self.$store.dispatch('topFunction');
      });
    },
    selectFile: function selectFile(event) {
      var _this = this;

      var self = this;
      var reader = new FileReader();
      var file = event.target.files[0];

      if (event.target.getAttribute('name') == 'pancard') {
        reader.onloadend = function () {
          self.pan_card = reader.result;
          self.panCard = event.target.files[0];
        };
      } else if (event.target.getAttribute('name') == 'adharcard') {
        reader.onloadend = function () {
          self.adhar_card = reader.result;
          self.adharCard = event.target.files[0];
        };
      } else {
        reader.onloadend = function () {
          _this.profile = reader.result;
        };
      }

      this.image = event.target.files[0];
      reader.readAsDataURL(file);
    },
    selectDocs: function selectDocs(event) {
      var _this2 = this;

      var files = event.target.files;
      this.otherDocs = event.target.files;

      for (var i = 0; i < files.length; i++) {
        var file = files[i];
        var reader = new FileReader();

        reader.onload = function (e) {
          _this2.other_docss.push(e.target.result);
        };

        reader.readAsDataURL(file);
      }
    }
  }, _defineProperty(_methods, "selectDocs", function selectDocs(event) {
    this.otherDocs = event.target.files;
  }), _defineProperty(_methods, "selectAdharDocs", function selectAdharDocs(event) {
    this.adharCard = event.target.files;
  }), _defineProperty(_methods, "selectOficialDocs", function selectOficialDocs(event) {
    this.officialDocs = event.target.files;
  }), _defineProperty(_methods, "formSubmit", function formSubmit(e) {
    e.preventDefault();
    var self = this;
    var formData = new FormData();
    formData.append('file', self.image);
    axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/users/' + self.$route.params.id + '/save?token=' + localStorage.getItem("api_token"), formData).then(function (response) {
      self.message = 'Successfully Picture Updated.';
      self.showAlert();
      self.$store.dispatch('topFunction');
    })["catch"](function (error) {
      console.log(error.response.errors);
    });
  }), _defineProperty(_methods, "docsSubmit", function docsSubmit(e) {
    e.preventDefault();
    var self = this;
    var config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    };
    var formData = new FormData();
    formData.append('pan_card', self.panCard);
    formData.append('adhar_card', self.adharCard);

    for (var i = 0; i < this.adharCard.length; i++) {
      var file = this.adharCard[i];
      formData.append('adhar_card[' + i + ']', file);
    }

    for (var j = 0; j < this.officialDocs.length; j++) {
      var _file = this.officialDocs[j];
      formData.append('officialDocs[' + j + ']', _file);
    }

    for (var k = 0; k < this.otherDocs.length; k++) {
      var _file2 = this.otherDocs[k];
      formData.append('otherDocs[' + k + ']', _file2);
    }

    axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/users/' + self.$route.params.id + '/save?token=' + localStorage.getItem("api_token"), formData, config).then(function (response) {
      self.pan_card = response.data.pan_card;

      if (response.data.adhar_card) {
        self.adhar_card = JSON.parse(response.data.adhar_card);
      }

      if (response.data.official_docs) {
        self.official_docs = JSON.parse(response.data.official_docs);
      }

      if (response.data.other_docs) {
        self.other_docs = JSON.parse(response.data.other_docs);
      }

      self.message = 'Successfully Docs Updated.';
      self.showAlert();
      self.$store.dispatch('topFunction');
    })["catch"](function (error) {
      console.log(error.response.errors);
    });
  }), _defineProperty(_methods, "passwordUpdateSubmit", function passwordUpdateSubmit(e) {
    e.preventDefault();
    var self = this;
    var formData = new FormData();
    formData.append('new_password', self.new_password);
    formData.append('confirm_password', self.confirm_password);
    this.$store.commit("setErrors", {});
    axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/users/' + self.$route.params.id + '/save?token=' + localStorage.getItem("api_token"), formData).then(function (response) {
      if (response.data.error) {
        self.message = response.data.error;
      } else {
        self.message = 'Successfully Password Updated.';
      }

      self.showAlert();
      self.$store.dispatch('topFunction');
    })["catch"](function (error) {
      console.log(error.response.errors);
    });
  }), _defineProperty(_methods, "updatePassword", function updatePassword() {
    var self = this;
  }), _methods),
  mounted: function mounted() {
    var self = this;
    axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/users/' + self.$route.params.id + '/edit?token=' + localStorage.getItem("api_token")).then(function (response) {
      console.log(response.data);
      self.name = response.data.name;
      self.employee_id = response.data.employee_id;
      self.email = response.data.email;
      self.is_active = response.data.is_active;
      self.contact_number = response.data.contact_no;
      self.profile = response.data.profile;
      self.alternate_number = response.data.alternate_no;
      self.position = response.data.position;
      self.current_address = response.data.current_address;
      self.permanent_address = response.data.permanent_address;
      self.pos = response.data.roles;
      self.department = response.data.department;

      if (response.data.birth_date) {
        self.dof = response.data.birth_date.split(' ')[0];
      }

      if (response.data.joining_date) {
        self.joining_date = response.data.joining_date.split(' ')[0];
      }

      if (response.data.appraisel_date) {
        self.appraisel_date = response.data.appraisel_date.split(' ')[0];
      }

      self.additional_info = response.data.additional_info;
      self.pan_card = response.data.pan_card;

      if (response.data.adhar_card) {
        self.adhar_card = JSON.parse(response.data.adhar_card);
      }

      if (response.data.official_docs) {
        self.official_docs = JSON.parse(response.data.official_docs);
      }

      if (response.data.other_docs) {
        self.other_docs = JSON.parse(response.data.other_docs);
      }
    });
    this.getDesignations();
    this.$store.commit("setErrors", {});
    axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/roles?token=' + localStorage.getItem("api_token")).then(function (response) {
      self.roles = response.data;
    });
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/validation-errors.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/employees/validation-errors.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    error: {
      type: [Number, String, Array, Object],
      "default": 'Unknown'
    }
  },
  data: function data() {
    return {
      test: 'helo'
    };
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/EditEmployee.vue?vue&type=template&id=abd41b68&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/employees/EditEmployee.vue?vue&type=template&id=abd41b68& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { xs: "12", lg: "12" } },
        [
          _c(
            "CCard",
            [
              _c("CCardHeader", { staticClass: "task-heading mb-1" }, [
                _vm.name
                  ? _c(
                      "h3",
                      {
                        staticClass:
                          "d-flex justify-content-between align-items-center"
                      },
                      [
                        _vm._v(
                          "\n          Edit Employee : " +
                            _vm._s(_vm.name) +
                            "\n          "
                        ),
                        _c(
                          "div",
                          { staticClass: "btns-header" },
                          [
                            _c(
                              "CButton",
                              {
                                attrs: { color: "secondary" },
                                on: { click: _vm.goBack }
                              },
                              [_vm._v("Back")]
                            )
                          ],
                          1
                        )
                      ]
                    )
                  : _vm._e()
              ]),
              _vm._v(" "),
              _c("CCardBody", [
                _c(
                  "div",
                  { staticClass: "custom-tabs-sec" },
                  [
                    _c(
                      "CTabs",
                      { attrs: { variant: "pills", horizontal: "" } },
                      [
                        _c(
                          "CTab",
                          { attrs: { title: "Basic Info", active: "" } },
                          [
                            _c(
                              "CForm",
                              [
                                _c(
                                  "CAlert",
                                  {
                                    attrs: {
                                      show: _vm.dismissCountDown,
                                      color: "primary",
                                      fade: ""
                                    },
                                    on: {
                                      "update:show": function($event) {
                                        _vm.dismissCountDown = $event
                                      }
                                    }
                                  },
                                  [
                                    _vm._v(
                                      "\n                    " +
                                        _vm._s(_vm.message) +
                                        "\n                  "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c("CInput", {
                                  attrs: {
                                    label: "Employee Id *",
                                    type: "number",
                                    placeholder: "Employee Id"
                                  },
                                  model: {
                                    value: _vm.employee_id,
                                    callback: function($$v) {
                                      _vm.employee_id = $$v
                                    },
                                    expression: "employee_id"
                                  }
                                }),
                                _vm._v(" "),
                                _vm.errors.employee_id
                                  ? _c("validationError", {
                                      attrs: {
                                        error: _vm.errors.employee_id[0]
                                      }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("CInput", {
                                  attrs: {
                                    type: "text",
                                    required: "",
                                    label: "Name *",
                                    placeholder: "Name"
                                  },
                                  model: {
                                    value: _vm.name,
                                    callback: function($$v) {
                                      _vm.name = $$v
                                    },
                                    expression: "name"
                                  }
                                }),
                                _vm._v(" "),
                                _vm.errors.name
                                  ? _c("validation-error", {
                                      attrs: { error: _vm.errors.name[0] }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("CInput", {
                                  attrs: {
                                    type: "text",
                                    required: "",
                                    label: "Email",
                                    placeholder: "Email",
                                    disabled: ""
                                  },
                                  model: {
                                    value: _vm.email,
                                    callback: function($$v) {
                                      _vm.email = $$v
                                    },
                                    expression: "email"
                                  }
                                }),
                                _vm._v(" "),
                                _vm.errors.email
                                  ? _c("validation-error", {
                                      attrs: { error: _vm.errors.email[0] }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("CInput", {
                                  attrs: {
                                    type: "number",
                                    required: "",
                                    label: "Contact Number *",
                                    placeholder: "Contact Number"
                                  },
                                  model: {
                                    value: _vm.contact_number,
                                    callback: function($$v) {
                                      _vm.contact_number = $$v
                                    },
                                    expression: "contact_number"
                                  }
                                }),
                                _vm._v(" "),
                                _vm.errors.contact_number
                                  ? _c("validation-error", {
                                      attrs: {
                                        error: _vm.errors.contact_number[0]
                                      }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("CInput", {
                                  attrs: {
                                    type: "number",
                                    label: "Alternate Number",
                                    placeholder: "Alternate Number"
                                  },
                                  model: {
                                    value: _vm.alternate_number,
                                    callback: function($$v) {
                                      _vm.alternate_number = $$v
                                    },
                                    expression: "alternate_number"
                                  }
                                }),
                                _vm._v(" "),
                                _c("div", { staticClass: "form-group" }, [
                                  _c("label", [_vm._v(" Select Position *")]),
                                  _vm._v(" "),
                                  _c(
                                    "select",
                                    {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.position,
                                          expression: "position"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      on: {
                                        change: function($event) {
                                          var $$selectedVal = Array.prototype.filter
                                            .call(
                                              $event.target.options,
                                              function(o) {
                                                return o.selected
                                              }
                                            )
                                            .map(function(o) {
                                              var val =
                                                "_value" in o
                                                  ? o._value
                                                  : o.value
                                              return val
                                            })
                                          _vm.position = $event.target.multiple
                                            ? $$selectedVal
                                            : $$selectedVal[0]
                                        }
                                      }
                                    },
                                    [
                                      _c(
                                        "option",
                                        {
                                          attrs: { disabled: "", value: "null" }
                                        },
                                        [_vm._v("Select a Position")]
                                      ),
                                      _vm._v(" "),
                                      _vm._l(_vm.items, function(item) {
                                        return _c(
                                          "option",
                                          { domProps: { value: item.id } },
                                          [_vm._v(_vm._s(item.name))]
                                        )
                                      })
                                    ],
                                    2
                                  )
                                ]),
                                _vm._v(" "),
                                _vm.errors.position
                                  ? _c("validation-error", {
                                      attrs: { error: _vm.errors.position[0] }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("div", { staticClass: "form-group" }, [
                                  _c("label", [_vm._v(" Select role *")]),
                                  _vm._v(" "),
                                  _c(
                                    "select",
                                    {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.pos,
                                          expression: "pos"
                                        }
                                      ],
                                      staticClass: "form-control",
                                      on: {
                                        change: function($event) {
                                          var $$selectedVal = Array.prototype.filter
                                            .call(
                                              $event.target.options,
                                              function(o) {
                                                return o.selected
                                              }
                                            )
                                            .map(function(o) {
                                              var val =
                                                "_value" in o
                                                  ? o._value
                                                  : o.value
                                              return val
                                            })
                                          _vm.pos = $event.target.multiple
                                            ? $$selectedVal
                                            : $$selectedVal[0]
                                        }
                                      }
                                    },
                                    [
                                      _c(
                                        "option",
                                        {
                                          attrs: { disabled: "", value: "null" }
                                        },
                                        [_vm._v("Select a Role")]
                                      ),
                                      _vm._v(" "),
                                      _vm._l(_vm.roles, function(role) {
                                        return _c(
                                          "option",
                                          { domProps: { value: role.name } },
                                          [_vm._v(_vm._s(role.name))]
                                        )
                                      })
                                    ],
                                    2
                                  )
                                ]),
                                _vm._v(" "),
                                _vm.errors.pos
                                  ? _c("validation-error", {
                                      attrs: { error: _vm.errors.pos[0] }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("CInput", {
                                  attrs: {
                                    type: "text",
                                    label: "Department",
                                    placeholder: "Department"
                                  },
                                  model: {
                                    value: _vm.department,
                                    callback: function($$v) {
                                      _vm.department = $$v
                                    },
                                    expression: "department"
                                  }
                                }),
                                _vm._v(" "),
                                _vm.errors.department
                                  ? _c("validation-error", {
                                      attrs: { error: _vm.errors.department[0] }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("CInput", {
                                  attrs: {
                                    label: "Joining Date *",
                                    placeholder: "Joining Date",
                                    type: "date"
                                  },
                                  model: {
                                    value: _vm.joining_date,
                                    callback: function($$v) {
                                      _vm.joining_date = $$v
                                    },
                                    expression: "joining_date"
                                  }
                                }),
                                _vm._v(" "),
                                _vm.errors.joining_date
                                  ? _c("validation-error", {
                                      attrs: {
                                        error: _vm.errors.joining_date[0]
                                      }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("CInput", {
                                  attrs: {
                                    label: "Next Appraisal Date",
                                    placeholder: "Next Appraisal Date",
                                    type: "date"
                                  },
                                  model: {
                                    value: _vm.appraisel_date,
                                    callback: function($$v) {
                                      _vm.appraisel_date = $$v
                                    },
                                    expression: "appraisel_date"
                                  }
                                }),
                                _vm._v(" "),
                                _c("CInput", {
                                  attrs: {
                                    label: "Date of Birth *",
                                    placeholder: "Department",
                                    type: "date"
                                  },
                                  model: {
                                    value: _vm.dof,
                                    callback: function($$v) {
                                      _vm.dof = $$v
                                    },
                                    expression: "dof"
                                  }
                                }),
                                _vm._v(" "),
                                _vm.errors.dof
                                  ? _c("validation-error", {
                                      attrs: { error: _vm.errors.dof[0] }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("CTextarea", {
                                  attrs: {
                                    type: "text",
                                    label: "Current Address",
                                    rows: "5"
                                  },
                                  model: {
                                    value: _vm.current_address,
                                    callback: function($$v) {
                                      _vm.current_address = $$v
                                    },
                                    expression: "current_address"
                                  }
                                }),
                                _vm._v(" "),
                                _c("CTextarea", {
                                  attrs: {
                                    type: "text",
                                    label: "Permanent Address",
                                    rows: "5"
                                  },
                                  model: {
                                    value: _vm.permanent_address,
                                    callback: function($$v) {
                                      _vm.permanent_address = $$v
                                    },
                                    expression: "permanent_address"
                                  }
                                }),
                                _vm._v(" "),
                                _c("CTextarea", {
                                  attrs: {
                                    type: "text",
                                    label: "Additional Information",
                                    rows: "5"
                                  },
                                  model: {
                                    value: _vm.additional_info,
                                    callback: function($$v) {
                                      _vm.additional_info = $$v
                                    },
                                    expression: "additional_info"
                                  }
                                }),
                                _vm._v(" "),
                                _c(
                                  "div",
                                  { staticClass: "form-group is_active_div" },
                                  [
                                    _c("label", [_vm._v("Is active? ")]),
                                    _vm._v(" "),
                                    _c("CInput", {
                                      attrs: {
                                        type: "checkbox",
                                        checked: parseInt(_vm.is_active)
                                      },
                                      on: { change: _vm.checked_active },
                                      model: {
                                        value: _vm.is_active,
                                        callback: function($$v) {
                                          _vm.is_active = $$v
                                        },
                                        expression: "is_active"
                                      }
                                    })
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _c(
                                  "CButton",
                                  {
                                    attrs: { color: "primary" },
                                    on: {
                                      click: function($event) {
                                        return _vm.update()
                                      }
                                    }
                                  },
                                  [_vm._v("Save")]
                                )
                              ],
                              1
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("CTab", { attrs: { title: "Profile Pic" } }, [
                          _c(
                            "form",
                            {
                              attrs: { enctype: "multipart/form-data" },
                              on: { submit: _vm.formSubmit }
                            },
                            [
                              _c(
                                "CAlert",
                                {
                                  attrs: {
                                    show: _vm.dismissCountDown,
                                    color: "primary",
                                    fade: ""
                                  },
                                  on: {
                                    "update:show": function($event) {
                                      _vm.dismissCountDown = $event
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                    " +
                                      _vm._s(_vm.message) +
                                      "\n                  "
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _vm.profile
                                ? _c("img", {
                                    staticClass: "add-profile",
                                    attrs: { src: _vm.profile }
                                  })
                                : _c("img", {
                                    staticClass: "add-profile",
                                    attrs: { src: _vm.avtar }
                                  }),
                              _vm._v(" "),
                              _c(
                                "div",
                                {
                                  staticClass: "form-group",
                                  attrs: { role: "group" }
                                },
                                [
                                  _c("label", [_vm._v("Profile Picture")]),
                                  _vm._v(" "),
                                  _c("input", {
                                    staticClass: "form-control",
                                    attrs: { type: "file" },
                                    on: { change: _vm.selectFile }
                                  })
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "CButton",
                                { attrs: { color: "primary", type: "submit" } },
                                [_vm._v("Save")]
                              )
                            ],
                            1
                          )
                        ]),
                        _vm._v(" "),
                        _c("CTab", { attrs: { title: "Document" } }, [
                          _c(
                            "form",
                            {
                              staticClass: "document-form",
                              attrs: { enctype: "multipart/form-data" },
                              on: { submit: _vm.docsSubmit }
                            },
                            [
                              _c(
                                "CAlert",
                                {
                                  attrs: {
                                    show: _vm.dismissCountDown,
                                    color: "primary",
                                    fade: ""
                                  },
                                  on: {
                                    "update:show": function($event) {
                                      _vm.dismissCountDown = $event
                                    }
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                    " +
                                      _vm._s(_vm.message) +
                                      "\n                  "
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "employee-docs-tab" },
                                [
                                  _c(
                                    "CTabs",
                                    { attrs: { variant: "pills" } },
                                    [
                                      _c(
                                        "CTab",
                                        {
                                          attrs: {
                                            title: "Pan Card",
                                            active: ""
                                          }
                                        },
                                        [
                                          _vm.pan_card
                                            ? _c(
                                                "div",
                                                { staticClass: "outer-box" },
                                                [
                                                  _vm.pan_card
                                                    .split(".")
                                                    .pop() == "pdf" ||
                                                  _vm.pan_card
                                                    .split(".")
                                                    .pop() == "docx"
                                                    ? _c("iframe", {
                                                        attrs: {
                                                          src: _vm.pan_card
                                                        }
                                                      })
                                                    : _c("img", {
                                                        attrs: {
                                                          src: _vm.pan_card
                                                        }
                                                      }),
                                                  _vm._v(" "),
                                                  _vm.pan_card
                                                    ? _c(
                                                        "a",
                                                        {
                                                          staticClass:
                                                            "btn-primary btn",
                                                          attrs: {
                                                            href: _vm.pan_card,
                                                            download:
                                                              _vm.name +
                                                              "-pan-card"
                                                          }
                                                        },
                                                        [_vm._v("Download")]
                                                      )
                                                    : _vm._e()
                                                ]
                                              )
                                            : _vm._e(),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            {
                                              staticClass: "form-group",
                                              attrs: { role: "group" }
                                            },
                                            [
                                              _c("input", {
                                                staticClass: "form-control",
                                                attrs: {
                                                  type: "file",
                                                  name: "pancard"
                                                },
                                                on: { change: _vm.selectFile }
                                              })
                                            ]
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "CTab",
                                        { attrs: { title: "Adhar Card" } },
                                        [
                                          _vm.adhar_card
                                            ? _c(
                                                "div",
                                                { staticClass: "outer-box" },
                                                _vm._l(_vm.adhar_card, function(
                                                  aadhar,
                                                  key
                                                ) {
                                                  return _c("div", [
                                                    aadhar.split(".").pop() ==
                                                      "pdf" ||
                                                    aadhar.split(".").pop() ==
                                                      "docx"
                                                      ? _c("iframe", {
                                                          attrs: { src: aadhar }
                                                        })
                                                      : _c("img", {
                                                          attrs: { src: aadhar }
                                                        }),
                                                    _vm._v(" "),
                                                    _c(
                                                      "a",
                                                      {
                                                        staticClass:
                                                          "btn-primary btn",
                                                        attrs: {
                                                          href: aadhar,
                                                          download:
                                                            _vm.name +
                                                            key +
                                                            "-other-docs"
                                                        }
                                                      },
                                                      [_vm._v("Download")]
                                                    )
                                                  ])
                                                }),
                                                0
                                              )
                                            : _vm._e(),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            {
                                              staticClass: "form-group",
                                              attrs: { role: "group" }
                                            },
                                            [
                                              _c("input", {
                                                staticClass: "form-control",
                                                attrs: {
                                                  type: "file",
                                                  name: "adharcard",
                                                  multiple: ""
                                                },
                                                on: {
                                                  change: _vm.selectAdharDocs
                                                }
                                              })
                                            ]
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "CTab",
                                        { attrs: { title: "Official Docs" } },
                                        [
                                          _vm.official_docs
                                            ? _c(
                                                "div",
                                                { staticClass: "outer-box" },
                                                _vm._l(
                                                  _vm.official_docs,
                                                  function(office_doc, key) {
                                                    return _c("div", [
                                                      office_doc
                                                        .split(".")
                                                        .pop() == "pdf" ||
                                                      office_doc
                                                        .split(".")
                                                        .pop() == "docx"
                                                        ? _c("iframe", {
                                                            attrs: {
                                                              src: office_doc
                                                            }
                                                          })
                                                        : _c("img", {
                                                            attrs: {
                                                              src: office_doc
                                                            }
                                                          }),
                                                      _vm._v(" "),
                                                      _c(
                                                        "a",
                                                        {
                                                          staticClass:
                                                            "btn-primary btn",
                                                          attrs: {
                                                            href: office_doc,
                                                            download:
                                                              _vm.name +
                                                              key +
                                                              "-other-docs"
                                                          }
                                                        },
                                                        [_vm._v("Download")]
                                                      )
                                                    ])
                                                  }
                                                ),
                                                0
                                              )
                                            : _vm._e(),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            {
                                              staticClass: "form-group",
                                              attrs: { role: "group" }
                                            },
                                            [
                                              _c("input", {
                                                staticClass: "form-control",
                                                attrs: {
                                                  type: "file",
                                                  name: "officialDocs",
                                                  multiple: ""
                                                },
                                                on: {
                                                  change: _vm.selectOficialDocs
                                                }
                                              })
                                            ]
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "CTab",
                                        {
                                          attrs: {
                                            title:
                                              "Qualification/Experience Docs"
                                          }
                                        },
                                        [
                                          _vm.other_docs
                                            ? _c(
                                                "div",
                                                { staticClass: "outer-box" },
                                                _vm._l(_vm.other_docs, function(
                                                  other_doc,
                                                  key
                                                ) {
                                                  return _c("div", [
                                                    other_doc
                                                      .split(".")
                                                      .pop() == "pdf" ||
                                                    other_doc
                                                      .split(".")
                                                      .pop() == "docx"
                                                      ? _c("iframe", {
                                                          attrs: {
                                                            src: other_doc
                                                          }
                                                        })
                                                      : _c("img", {
                                                          attrs: {
                                                            src: other_doc
                                                          }
                                                        }),
                                                    _vm._v(" "),
                                                    _c(
                                                      "a",
                                                      {
                                                        staticClass:
                                                          "btn-primary btn",
                                                        attrs: {
                                                          href: other_doc,
                                                          download:
                                                            _vm.name +
                                                            key +
                                                            "-other-docs"
                                                        }
                                                      },
                                                      [_vm._v("Download")]
                                                    )
                                                  ])
                                                }),
                                                0
                                              )
                                            : _vm._e(),
                                          _vm._v(" "),
                                          _c(
                                            "div",
                                            {
                                              staticClass: "form-group",
                                              attrs: { role: "group" }
                                            },
                                            [
                                              _c("input", {
                                                staticClass: "form-control",
                                                attrs: {
                                                  type: "file",
                                                  name: "otherdocs",
                                                  multiple: ""
                                                },
                                                on: { change: _vm.selectDocs }
                                              })
                                            ]
                                          )
                                        ]
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "CButton",
                                { attrs: { color: "primary", type: "submit" } },
                                [_vm._v("Save")]
                              )
                            ],
                            1
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "CTab",
                          { attrs: { title: "Password" } },
                          [
                            _c(
                              "CForm",
                              { on: { submit: _vm.passwordUpdateSubmit } },
                              [
                                _c(
                                  "CAlert",
                                  {
                                    attrs: {
                                      show: _vm.dismissCountDown,
                                      color: "primary",
                                      fade: ""
                                    },
                                    on: {
                                      "update:show": function($event) {
                                        _vm.dismissCountDown = $event
                                      }
                                    }
                                  },
                                  [
                                    _vm._v(
                                      "\n                    " +
                                        _vm._s(_vm.message) +
                                        "\n                  "
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c("CInput", {
                                  attrs: {
                                    type: "password",
                                    label: "New Password",
                                    placeholder: "New Password",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.new_password,
                                    callback: function($$v) {
                                      _vm.new_password = $$v
                                    },
                                    expression: "new_password"
                                  }
                                }),
                                _vm._v(" "),
                                _vm.errors.new_password
                                  ? _c("validation-error", {
                                      attrs: {
                                        error: _vm.errors.new_password[0]
                                      }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c("CInput", {
                                  attrs: {
                                    type: "password",
                                    label: "Confirm Password",
                                    placeholder: "Confirm Password",
                                    required: ""
                                  },
                                  model: {
                                    value: _vm.confirm_password,
                                    callback: function($$v) {
                                      _vm.confirm_password = $$v
                                    },
                                    expression: "confirm_password"
                                  }
                                }),
                                _vm._v(" "),
                                _vm.errors.confirm_password
                                  ? _c("validation-error", {
                                      attrs: {
                                        error: _vm.errors.confirm_password[0]
                                      }
                                    })
                                  : _vm._e(),
                                _vm._v(" "),
                                _c(
                                  "CButton",
                                  {
                                    attrs: { color: "primary", type: "submit" }
                                  },
                                  [_vm._v("Save")]
                                )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              ])
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _vm.is_loading
        ? _c("div", { staticClass: "loader-image" }, [
            _c("img", { attrs: { src: _vm.loader } })
          ])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/employees/validation-errors.vue?vue&type=template&id=2961bf1b&":
/*!************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/employees/validation-errors.vue?vue&type=template&id=2961bf1b& ***!
  \************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "form-group", attrs: { role: "group" } }, [
    _vm.error
      ? _c("span", { staticClass: "error" }, [_vm._v(_vm._s(_vm.error))])
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);