<?php 
namespace App\Repositories\Resource;

use Illuminate\Database\Eloquent\Model;
use App\Repositories\Resource\ResourceInterface as ResourceInterface;
use Illuminate\Support\Facades\DB;

class ResourceRepository implements ResourceInterface
{
    // model property on class instances
    protected $model;

    // Constructor to bind model to repo
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    // Get all instances of model
    public function all($softDelete=null,$id=null)
    {
        $model = $this->model;
        if($softDelete) {
            $model = $model->withTrashed();
        } 
        if($id){
            $model = $model->where('user_id',$id);
        } 
      return $model->orderBy('id', 'DESC')->get();      
    }

    // create a new record in the database
    public function create(array $data)
    {
        return $this->model->create($data);
    }

    // update record in the database
    public function update(array $data, $id)
    {
        $record = $this->find($id);
        return $record->update($data);
    }

    // remove record from the database
    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    // show the record with the given id
    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    // Get the associated model
    public function getModel()
    {
        return $this->model;
    }

    // Set the associated model
    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    // Eager load database relationships
    public function with($relations)
    {
        return $this->model->with($relations);
    }
}