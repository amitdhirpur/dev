<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Task;
use Auth;

class Project extends Model
{
	protected $hidden = ["created_at", "updated_at"];
    use SoftDeletes;
    protected $guarded = [];

    /**
     * Get the phone record associated with the user.
     */
    public function tasks()
    {
        return $this->hasMany('App\Task', 'project');
    }
    static function taksRelatedProjects($id) {
        $projects = Project::leftJoin('tasks', 'projects.id', '=', 'tasks.project')
                            ->leftJoin('folders', 'folders.project_id', '=', 'projects.id');
        if(Auth::user()->menuroles != 'admin') {
            $projects = $projects->where(function($query) use ($id) {
               $query->where('tasks.assigned_to', $id)
               ->orWhere('tasks.user', $id)
               ->orWhereRaw('json_contains(tasks.observers, \'['.$id.']\')')
               ->orWhereRaw('json_contains(tasks.participants, \'['.$id.']\')');
            })->orWhere('projects.user_id', $id);
        } else {
          $projects->withTrashed();
        } 
        $projects = $projects->select('projects.*','folders.id as folder_id','folders.name as folder_name')           
            ->groupBy('projects.id')
            ->orderBy('projects.id', 'DESC')
            ->get();
        return $projects ?? [];
   }
    static function latestProjects($id) {
        $projects = Project::leftJoin('tasks', 'projects.id', '=', 'tasks.project')
                            ->leftJoin('folders', 'folders.project_id', '=', 'projects.id');
        if(Auth::user()->menuroles != 'admin') {
            $projects = $projects->where(function($query) use ($id) {
               $query->where('tasks.assigned_to', $id)
               ->orWhere('tasks.user', $id)
               ->orWhereRaw('json_contains(tasks.observers, \'['.$id.']\')')
               ->orWhereRaw('json_contains(tasks.participants, \'['.$id.']\')');
            })->orWhere('projects.user_id', $id);
        } else {
          $projects->withTrashed();
        } 
        $projects = $projects->select('projects.*','folders.id as folder_id','folders.name as folder_name')           
            ->groupBy('projects.id')
            ->orderBy('projects.id', 'DESC')
            ->limit(5)
            ->get();
        return $projects ?? [];
   }
   static function taksRelatedArchiveProjects($id) {
        $projects = Project::leftJoin('tasks', 'projects.id', '=', 'tasks.project')
                            ->leftJoin('folders', 'folders.project_id', '=', 'projects.id');
        $projects = $projects->onlyTrashed()->select('projects.*','folders.id as folder_id','folders.name as folder_name') ->groupBy('projects.id')->orderBy('projects.id', 'DESC')->get();
        
        return $projects ?? [];
   }
}
