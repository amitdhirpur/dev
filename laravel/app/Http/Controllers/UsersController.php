<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;
use App\Task;
use App\Models\Project;
use App\DriveFolder;
use Auth;
use App\UserInformation;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\UserPostRequest;
use App\Rules\MatchOldPassword;
use Hash;
use App\Drive;
use App\Models\Feed;
use Image,File, Storage;
use ZipArchive, DataTables;
class UsersController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Datatable controller.
     *
    */
    protected function generateDatatable($listing) {
        return DataTables::of($listing)
            // ->addIndexColumn()
            ->addColumn('employee_id', function($row){  
                return $row->employee_id;
            })
            ->addColumn('name', function($row){  
                return $row->name;
            })
            ->addColumn('registered', function($row){  
                return $row->registered;
            })
            ->addColumn('roles', function($row){  
                return $row->roles;
            })
            ->addColumn('status', function($row){  
                if($row->is_active == '1') {
                  $status = '<span class="badge badge-success">Active</span>';
                } else {
                  $status = '<span class="badge badge-danger">Deactive</span>';
                }
                return $status;
            }) 
            ->setRowAttr(['style' => function($row){
                    return $row->trashed() ? 'background-color: #f5c1c1;' : '';
                }
            ])
            ->rawColumns(['employee_id', 'name', 'registered', 'roles', 'status'])
            ->make(true);
    }
     protected function generateArchiveDatatable($listing) {
        return DataTables::of($listing)
            // ->addIndexColumn()
            ->addColumn('employee_id', function($row){  
                return $row->employee_id;
            })
            ->addColumn('name', function($row){  
                return $row->name;
            })
            ->addColumn('registered', function($row){  
                return $row->registered;
            })
            ->addColumn('roles', function($row){  
                return $row->roles;
            })
            ->addColumn('status', function($row){  
                if($row->is_active == '1') {
                  $status = '<span class="badge badge-success">Active</span>';
                } else {
                  $status = '<span class="badge badge-danger">Deactive</span>';
                }
                return $status;
            })
            ->rawColumns(['employee_id', 'name', 'registered', 'roles', 'status'])
            ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $role = $user->menuroles;
        $you = $user->id;
        $users = User::select('users.id','users.employee_id', 'users.name', 'users.is_active', 'users.email', 'users.menuroles as roles', 'users.status', 'users.created_at as registered', 'users.deleted_at')
        ->where('menuroles', '!=', 'admin');
        if($role == 'HR') {
            $users->where('menuroles', '!=', 'HR');
        }
        if($role == 'admin') {
            $users->withTrashed();
        }

        $users = $users->orderBy('id','DESC')->get();
        $users = $users->each(function($item, $key){ $item['you'] = Auth::user()->id; });
        return $this->generateDatatable($users); 
        // return response()->json( compact('users', 'you') );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::leftJoin('user_information', 'users.id', '=', 'user_information.user_id')
            ->leftJoin('user_positions', 'user_information.position', '=', 'user_positions.id')
            ->select('users.*', 'user_information.profile', 'user_information.contact_no', 'user_information.alternate_no', 'user_positions.name as user_position', 'user_information.department', 'user_information.birth_date', 'user_information.joining_date', 'user_information.appraisel_date', 'user_information.additional_info','user_information.current_address','user_information.permanent_address')
            ->where('users.id', '=', $id)
            ->first();

        return response()->json( $user );
    }
/**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        return response()->json( ['status' => 'success'] );  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'name'        => 'required',
            'employee_id' => 'required|min:1|max:6|',
            'email'       => 'required|email|unique:users',
            'password'    => 'required|min:4|confirmed',
        ]);
        if ($validate->fails()){
            return response()->json([
                'status' => 'error',
                'errors' => $validate->errors()
            ], 422);
        }

        $user = new User;
        $user->name = $request->name;
        $user->employee_id = $request->employee_id;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->menuroles = $request->role;
        $user->status = 'Active';
        if($user->save()) {
            $userInfo = new UserInformation;
            $userInfo->position = $request->position;
            $userInfo->joining_date = $request->joining_date;
            $userInfo->user_id = $user->id;
            $userInfo->save();
        }
        

        $drive = New Drive;
        $drive->name = 'drive_'.$user->id;

        $drive->save();

        return response()->json(['status' => 'success'], 200);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::leftJoin('user_information', 'users.id', '=', 'user_information.user_id')
            ->select('users.id as userId', 'users.employee_id', 'users.is_active','users.name', 'users.email', 'users.menuroles as roles', 'users.status', 'user_information.*')
            ->where('users.id', $id)
            ->first();
        return response()->json( $user );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserPostRequest $request, $id)
    {
    	if(User::where('id', '!=', $id)->where('employee_id', $request['employee_id'])->count() > 0) {
    		return response()->json([ 'status' => 'error', 'message' => 'Employee ID is already taken.', 'type' => 'employee_id' ]);
    	}

        if(User::where('id', '!=', $id)->where('email', $request['email'])->count() > 0) {
            return response()->json([ 'status' => 'error', 'message' => 'Email is already taken.', 'type' => 'email' ]);
        }
        
        $user = User::find($id);
        $user->employee_id       = $request->input('employee_id');
        $user->name       = $request->input('name');
        $user->email      = $request->input('email');
        $user->is_active      = $request->input('is_active');
        $user->menuroles      = $request->input('role');
        $user->save();

        $userInformation = UserInformation::updateOrCreate([
            'user_id'   => $id
        ],[
            'contact_no' => $request->input('contact_no'),
            'alternate_no' => $request->input('alternate_no'),
            'position' => $request->input('position'),
            'department' => $request->input('department'),
            'birth_date' => $request->input('birth_date'),
            'joining_date' => $request->input('joining_date'),
            'appraisel_date' => $request->input('appraisel_date'),
            'additional_info' => $request->input('additional_info'),
            'current_address' => $request->input('current_address'),
            'permanent_address' => $request->input('permanent_address'),
        ]
        );
        //$request->session()->flash('message', 'Successfully updated user');
        return response()->json( ['status' => 'success'] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if($user){
            $user->delete();
        }
        return response()->json( ['status' => 'success'] );
    }
    public function save(Request $request,$id)
    {             
        $path = '/users/'.$id;
        $doc = UserInformation::where('user_id',$id)->first();    
        if(!Storage::exists($path)){ 
            Storage::makeDirectory($path,0777, true, true);
        }
        if(isset($request->new_password) && isset($request->confirm_password))  {
            $validatedData = $request->validate([
                'new_password'       => 'required|min:1|max:256',
                'confirm_password'       => 'required|min:1|max:256',
            ]);
            if($request->new_password != $request->confirm_password){
                return response()->json( ['error' => "Password not match."] );
            }else{
                $userInformation = User::withTrashed()->updateOrCreate(
                    ['id'   => $id ],
                    [
                      'password' => \Hash::make($request->new_password)
                    ]
                );
            }
        } 
    
        if ($request->hasFile('file')) {
            $folderPath='/img/avatars';
            $file = $request->file('file');
            $ext = $file->getClientOriginalExtension();
            $image = $this->upload_image($file,$ext,$folderPath);
            $img = $this->upload_thumbnail_image(public_path($image->path));
            $userInformation = UserInformation::updateOrCreate(
                ['user_id'   => $id ],
                [
                    'profile' => $image->path,
                    'thumbnail' => $img
                ]
            );
        }
        if ($request->hasFile('pan_card')) {         
            if(isset($doc->pan_card) && $doc->pan_card) {               
                if(Storage::exists($doc->pan_card)) {
                    Storage::delete($doc->pan_card); //delete from storage
                }
            }
            $file = $request->file('pan_card');
            $ext = $file->getClientOriginalExtension();
            $image = $this->upload_image($file,$ext,$path);
            $userInformation = UserInformation::updateOrCreate(
                ['user_id'   => $id ],['pan_card' => $image->path]
            );
        }
        
        $adharr = []; $docs = []; $official_val = [];
        if ($request->hasFile('adhar_card')) {
            if(isset($doc->adhar_card) && $doc->adhar_card) {
                $adharr = json_decode($doc->adhar_card);
                if(gettype($adharr) == 'array' || gettype($adharr) == 'object') {
                    foreach ($adharr as $key => $doc) {
                        if(Storage::exists($doc)) {
                            Storage::delete($doc); //delete from storage
                        }
                    }
                }
            }
            
            $adhar_card = $request->file('adhar_card');
            foreach($adhar_card as $key => $aadhar) {
                $ext = $aadhar->getClientOriginalExtension();
                $image = $this->upload_image($aadhar,$ext,$path);
                $adharr[$key]=$image->path;
            }
            $adhar_card = json_encode($adharr);
            $userInformation = UserInformation::updateOrCreate(
                ['user_id'   => $id ],['adhar_card' => $adhar_card]
            );
        } 

        if ($request->hasFile('officialDocs')) {
            if(isset($doc->official_docs) && $doc->official_docs) {
                $official = json_decode($doc->official_docs);
                foreach ($official as $key => $fil) {
                    if(Storage::exists($fil)) {
                        Storage::delete($fil); //delete from storage
                    }
                }
            }
            
            $official_docs = $request->file('officialDocs');
            foreach($official_docs as $key => $official) {
                $ext = $official->getClientOriginalExtension();
                $image = $this->upload_image($official,$ext,$path);
                $official_val[$key]=$image->path;
            }
            $official_val = json_encode($official_val);
            $userInformation = UserInformation::updateOrCreate(
                ['user_id'   => $id ],['official_docs' => $official_val]
            );
        } 

        if ($request->hasFile('otherDocs')) {
            if(isset($doc->other_docs) && $doc->other_docs) {
                $docs = json_decode($doc->other_docs);
                foreach ($docs as $key => $doc) {
                    if(Storage::exists($doc)) {
                        Storage::delete($doc); //delete from storage
                    }
                }
            }
            
            $otherDocs = $request->file('otherDocs');
            foreach($otherDocs as $key => $otherDoc) {
                $ext = $otherDoc->getClientOriginalExtension();
                $image = $this->upload_image($otherDoc,$ext,$path);
                $docs[$key]=$image->path;
            }
            $otherDocs = json_encode($docs);
            $userInformation = UserInformation::updateOrCreate(
                ['user_id'   => $id ],['other_docs' => $otherDocs]
            );
        }    

        $user = DB::table('users')
            ->leftJoin('user_information', 'users.id', '=', 'user_information.user_id')
            ->select('users.id', 'users.name', 'users.email', 'users.menuroles as roles', 'users.status', 'user_information.*')
            ->where('users.id', '=', $id)
            ->first();

        return response()->json($user);
    }
    //thumbnail
    protected function upload_thumbnail_image($image) {
        $name = 'thumb-'.rand(10000, 99999).'.png';
        $path ='/img/avatars/thumbnails/'.$name;
        $destinationPath = public_path($path);
        $image_resize = Image::make($image); 
        $image_resize->resize(37, 37);  
        $image_resize->save($destinationPath);

        return $path;
    }
     // upload  image 
    public function upload_image($file,$ext,$folderPath) {
        $filename = $file->getClientOriginalName();
        $name = pathinfo($filename,PATHINFO_FILENAME);
        
        $name = $name.time().'.'.$ext;

        $destinationPath = public_path($folderPath);        
        $file->move($destinationPath, $name);

        $path = $folderPath."/".$name;
        $return = (object)[];
        $return->path = $path;
        $return->name = $name;

        return $return;
    }
    public function user()
    {
        $id =auth()->user()->id;
         $user = DB::table('users')
                ->leftJoin('user_information', 'users.id', '=', 'user_information.user_id')
                ->select('users.id', 'users.name', 'users.email', 'users.menuroles as roles', 'users.status', 'user_information.*')
                ->where('users.id', '=', $id)
                ->first();
         return response()->json( ['user' => $user, 'id' => $id] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateUserProfile(Request $request, $id)
    {
        
        $validatedData = $request->validate([
            'name'       => 'required|min:1|max:256',
            'email'      => 'required|email|max:256',
            'contact_no' => 'required|min:11|numeric',
            'birth_date' => 'required'
        ]);
        $user = User::find($id);
        $user->name       = $request->input('name');
        $user->email      = $request->input('email');
        $user->save();

        $userInformation = UserInformation::updateOrCreate([
            'user_id'   => $id
        ],[
            'contact_no' => $request->input('contact_no'),
            'alternate_no' => $request->input('alternate_no'),
            'department' => $request->input('department'),
            'birth_date' => $request->input('birth_date'),
            'additional_info' => $request->input('additional_info'),
            'current_address' => $request->input('current_address'),
            'permanent_address' => $request->input('permanent_address'),
        ]
        );
        $user = DB::table('users')
                ->leftJoin('user_information', 'users.id', '=', 'user_information.user_id')
                ->select('users.id', 'users.name', 'users.email', 'users.menuroles as roles', 'users.status', 'user_information.*')
                ->where('users.id', '=', $id)
                ->first();
        //$request->session()->flash('message', 'Successfully updated user');
        return response()->json($user);
    }
    public function updatePassword(Request $request)
    {

          $validate = Validator::make($request->all(), [
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required','min:8'],
            'new_confirm_password' => ['same:new_password'],
        ]);
          if ($validate->fails()){
            return response()->json([
                'status' => 'error',
                'errors' => $validate->errors()
            ], 422);
        }
          User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);

          return response()->json( ['status' => 'success','msg'=>'Password has been updated!!'] );
    }

    /**
     * get users for tagging.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function tagUsers() {
        $users = User::select('id', 'name', 'name as text')->get();
        
        return response()->json([ 'status' => 'success', 'users' => $users ]);
    }

    /**
     * Global search module for all users and roles..
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function globalSearch(Request $request) {
        $_search = $request['search']; $_id = Auth::user()->id;

        $tasks = Task::where('name', 'LIKE', "%".$_search."%")->orWhere('description', 'LIKE', "%".$_search."%");
        $projects = Project::leftJoin('tasks', 'projects.id', '=', 'tasks.project')->where('projects.name', 'LIKE', "%".$_search."%")->orWhere('projects.client', 'LIKE', "%".$_search."%");

        $drives = DriveFolder::where('name', 'LIKE', "%".$_search."%")->where('isFolder', 'true')->whereNull('parent_id');

        if(Auth::user()->menuroles == 'admin' || Auth::user()->menuroles == 'HR') {
            $users = User::leftJoin('user_information', 'users.id', '=', 'user_information.user_id')->where('users.name', 'LIKE', "%".$_search."%");
            if(Auth::user()->menuroles == 'admin') {
                $users = $users->where('menuroles', '!=', 'admin');
            }
            if(Auth::user()->menuroles == 'HR') {
                $users = $users->where('menuroles', '!=', 'admin')->where('menuroles', '!=', 'HR');
            }
            // list of the members.
            $data['members'] = $users->groupBy('users.id')->take(5)->select('users.id', 'users.name', 'user_information.profile')->get();
        }

        if(Auth::user()->menuroles != 'admin') {
            $tasks = $tasks->where(function($query) use ($_id) {
                $query->where('assigned_to', $_id)
                ->orWhere('user', $_id)
                ->orWhereRaw('json_contains(observers, \'['.$_id.']\')')
                ->orWhereRaw('json_contains(participants, \'['.$_id.']\')');
            });

            $projects = $projects->where(function($query) use ($_id) {
                $query->where('tasks.assigned_to', $_id)
                ->orWhere('tasks.user', $_id)
                ->orWhereRaw('json_contains(tasks.observers, \'['.$_id.']\')')
                ->orWhereRaw('json_contains(tasks.participants, \'['.$_id.']\')');
            });

            $drives = $drives->whereRaw('json_contains(drive, \'["' . 'drive_'.$_id . '"]\')');
        }

        // search data.    
        $data['_search'] = $_search;
        $data['tasks'] = $tasks->select('id', 'name')->groupBy('tasks.id')->take(10)->get();
        $data['drives'] = $drives->select('id', 'name')->groupBy('id')->take(5)->get();
        $data['projects'] = $projects->select('projects.id', 'projects.name', 'tasks.project', 'tasks.assigned_to', 'tasks.user', 'tasks.observers', 'tasks.participants', 'projects.client', 'projects.profile')->groupBy('projects.id')->take(10)->get();

        return response()->json($data);
    }
    public function is_dir_empty($dir) {
      if (!is_readable($dir)) return NULL; 
      return (count(scandir($dir)) == 2);
    }
     public function downloadZipFolder(Request $request)
    {
        $sourcePath = public_path('users/'.$request->id);
        $destinationPath = '/downloadArchive/user-document'.$request->id.'.zip';
        $outZipPath = public_path($destinationPath);
         if ($this->is_dir_empty($sourcePath)) {
          return response()->json(['error' => 'Folder empty']);
        }
        if (!extension_loaded('zip') || !file_exists($sourcePath)) {
            return response()->json(['error' => 'Folder empty']);
        }

        $zip = new ZipArchive();
        if (!$zip->open($outZipPath, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE)) {
            return response()->json(['error' => 'Folder empty']);
        }

        $sourcePath = str_replace('\\', '/', realpath($sourcePath));

        if (is_dir($sourcePath) === true) {
            $files = new  \RecursiveIteratorIterator(new  \RecursiveDirectoryIterator($sourcePath),  \RecursiveIteratorIterator::SELF_FIRST);

            foreach ($files as $file) {
                $file = str_replace('\\', '/', $file);

                // Ignore "." and ".." folders
                if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                    continue;

                $file = realpath($file);

                if (is_dir($file) === true) {
                    $zip->addEmptyDir(str_replace($sourcePath . '/', '', $file . '/'));
                } else if (is_file($file) === true) {
                    $zip->addFromString(str_replace($sourcePath . '/', '', $file), file_get_contents($file));
                }
            }
        } else if (is_file($sourcePath) === true) {
            $zip->addFromString(basename($sourcePath), file_get_contents($sourcePath));
        }

        $zip->close();
        if(file_exists($outZipPath)) {
          return response($destinationPath);            
        }
        return response()->json(['error' => 'Folder empty']);
        
    }
    public function add_pic(Request $request){

        $image = $this->upload_comp_image($request->data_file, $request->_id, $request->_comp);
        $src = $image;

        return response()->json(['src' => $src,"format"=>$request->format, "file_name"=>$request->_file_name]);


    }
    /**
     * Archive users.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function archiveUsers()
    {
        $user = auth()->user();
        $role = $user->menuroles;
        $you = $user->id;
        $users = User::select('users.id','users.employee_id', 'users.name', 'users.is_active', 'users.email', 'users.menuroles as roles', 'users.status', 'users.created_at as registered', 'users.deleted_at')
        ->where('menuroles', '!=', 'admin');
        $users->onlyTrashed();

        $users = $users->orderBy('id','DESC')->get();
        $users = $users->each(function($item, $key){ $item['you'] = Auth::user()->id; });
        return $this->generateArchiveDatatable($users); 
    }
    /**
     *Restore Archive users.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
     public function archiveRestore(Request $request)
    {
      $id = $request->id;
      $project = User::withTrashed()->find($id);
      $project->restore();
      return response()->json('success');
    }
    /**
     * upload files.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    protected function upload_comp_image($file, $_id, $_comp) {
        $ext = $file->getClientOriginalExtension(); 
        $filename = $file->getClientOriginalName();
        $name = pathinfo($filename,PATHINFO_FILENAME);
       
        $name = $name.'-'.time().rand(100, 999).'.'.$ext;
        if($_comp == "feed" && $_id == ""){

            $_id = Feed::orderBy('id', 'DESC')->first()->id + 1;
            $path = "/images/feeds/".$_id;

        }if($_comp == "addtask" && $_id == ""){

            $_id = Task::orderBy('id', 'DESC')->first()->id + 1;
            $path = "/images/tasks/".$_id;

        }elseif($_comp == "edittask"){

            $_id = Feed::orderBy('id', 'DESC')->first()->id + 1;
            $path = "/images/tasks/".$_id;

        }else{
          $path = "/images/comments/".$_comp.'/'.$_id;
        }
        
        if(!File::isDirectory($path)){
            File::makeDirectory(public_path($path), 0777, true, true);
        }

        $destinationPath = public_path($path);      
        $file->move($destinationPath, $name);

        return $path."/".$name;
    }
}
