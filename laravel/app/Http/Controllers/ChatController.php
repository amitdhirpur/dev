<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;
use App\User;
use App\Chat;
use Auth, DB, File,Carbon\Carbon;

class ChatController extends Controller
{
	/**
     * upload files.
     */
    protected function upload_image($file) {
        $ext = $file->getClientOriginalExtension(); 
        $filename = $file->getClientOriginalName();
        $name = pathinfo($filename,PATHINFO_FILENAME);
       
        $name = $name.'-'.time().rand(100, 999).'.'.$ext;
        $path = "/images/chat/".Auth::user()->id;

        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }

        $destinationPath = public_path('/images/chat/'.Auth::user()->id);
        $file->move($destinationPath, $name);

        return $path."/".$name;
    }

    protected function listChat($user) {
        $val = $user; $_id = Auth::user()->id;
        $ct = 50;
        if(array_key_exists('page', $_GET)) {
            $ct = $ct * (int) $_GET['page'];
        }

        $chats = Chat::query();
        $chats = $chats->where(function($query) use ($_id) {
            $query->where('user_id', $_id)
                ->orWhere('to', $_id);
        });
        $chats = $chats->where(function($query) use ($val) {
            $query->where('user_id', $val)
                ->orWhere('to', $val);
        });

        // return $chats->latest()->skip(0)->take($ct)->get();
        return $chats->latest()->get();
    }

    /**
     * send chrome notifications.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    protected function notifyUsers($user, $body, $url) {
        $u_info = DB::table('user_information')->where('user_id', Auth::user()->id)->select('thumbnail')->first();
        $tokens = [];

        $to = User::find($user);
        $tokens[] = $to->device_token;
        
        if(count($tokens) > 0) { 
            User::send_notification($tokens, ucwords(Auth::user()->name), $body, $u_info->thumbnail ?? null, $url);
        }

        return true;
    }

    /**
     * index controller.
     *
     * @param  \Illuminate\Http\Request  $request
    */
    public function index() {

       
    	$time = Setting::where('name', 'time_format')->first();

        $counts = Chat::where('to', Auth::user()->id)->where('user_id', $_GET['user'])->where('status', '0')->get();
        foreach ($counts as $key => $value) {
            Chat::where('id', $value->id)->update(['status' => '1']);
        }

    	$data['chats'] = $this->listChat($_GET['user']);
    	$data['time'] = $time->value;
    	return response()->json($data);
    }

    protected function setNotiStatus() {
            $_id = Auth::user()->id;
            $chat = Chat::where("to",$_id)->update(["noti_status"=> "1"]); 
            return $chatCount = $this->getNotiCounters()->count;
    }
    protected function getNotiCounters() {
        $_id = Auth::user()->id;
        $counts = Chat::select('user_id', DB::raw('count(id) as count'))
            ->where('to', $_id)->where('status', '0')->first();

        return $counts;
    }

    protected function getCounters() {
        $_id = Auth::user()->id;
        $counts = Chat::select('user_id', DB::raw('count(id) as count'))
            ->where('to', $_id)->where('status', '0')->groupBy('user_id')->get();

        return $counts;
    }

    protected function listAllUsers() {
        
        $_id = Auth::user()->id;


        $current_dataTime = date("Y-m-d h:i:s");

        $user =  User::find(Auth::user()->id);
        $user->is_logged = $current_dataTime;
        $user->save();

        if(array_key_exists('search', $_GET)) {
            $users = User::leftJoin('user_information as info', 'users.id', '=', 'info.user_id')
                ->select('users.id', 'users.name', 'users.is_logged', 'users.menuroles', 'info.profile')
                ->where('users.id', '!=', $_id)
                ->where('users.is_active', '1')
                ->where('users.name', 'LIKE', "%".$_GET['search']."%")->orderBy('users.name', 'ASC')->limit(10)->get();

        } else {
            // list users according to chat.
            $chats = Chat::select(['user_id', 'to', DB::raw('MAX(created_at) AS created_date')])
                ->where(function($query) use ($_id) {
                    $query->where('user_id', $_id)
                        ->orWhere('to', $_id);
                })
                ->orderBy('created_date', 'desc')->groupBy('user_id', 'to')->get();

            $users = []; $exists = []; $a = 1;
            foreach ($chats as $key => $value) {
                if($value->user_id != $_id && !in_array($value->user_id, $exists)) {
                    if($key >= 10) {  break; }
                    $exists[] = $value->user_id;
                    $user = User::leftJoin('user_information as info', 'users.id', '=', 'info.user_id')
                        ->select('users.id', 'users.name', 'users.is_logged', 'users.menuroles', 'info.profile')
                        ->where('users.id', $value->user_id)->first();
                    $users[$key] = $user;
                } else {
                    if($value->to != $_id && !in_array($value->to, $exists)) {
                        if($key >= 10) {  break; }
                        $exists[] = $value->to;
                        $user = User::leftJoin('user_information as info', 'users.id', '=', 'info.user_id')
                            ->select('users.id', 'users.name', 'users.is_logged', 'users.menuroles', 'info.profile')
                            ->where('users.id', $value->to)->first();
                        if($user) {
                            $users[$key] = $user;
                        }
                    }
                }
                $a++;
            }
        }

        $user_logged_minute = 0;
        foreach ($users as $key => $user_data) {

            if($user_data->is_logged != ""){
                    $actual_start_at = Carbon::parse($current_dataTime);
                    $actual_end_at = Carbon::parse($user_data->is_logged);
                    $user_logged_minute = $actual_end_at->diffInMinutes($actual_start_at, true);
                    if($user_logged_minute < 10){
                        $user_data->user_loggedIn = true;
                    }else{
                        $user_data->user_loggedIn = false;
                    } 
            }else{
                $user_data->user_loggedIn = false;
            }
            
        }


        return $users;
    }

    /**
     * realtime chat controller.
     *
     * @param  \Illuminate\Http\Request  $request
    */
    public function realtime() {

        $users = $this->listAllUsers();

        
        $data['notify'] = $this->getCounters();
        $data['users'] = $users;
        $data['chats'] = $this->listChat($_GET['user']);
        return response()->json($data);
    }

    /**
     * upload files.
     */
    public function getUser() {

        $users = $this->listAllUsers();
    	
        
        $data['notify'] = $this->getCounters();
        $data['chats'] = [];
        if(array_key_exists('user', $_GET)) {
            $data['chats'] = $this->listChat($_GET['user']);
        }
    	$data['users'] = $users;
        $data['type'] = gettype($users);
        
    	return response()->json($data);
    }

    /**
     * save chat message.
     */
    public function save(Request $request) {
        $counts = Chat::where('to', Auth::user()->id)->where('user_id', $request['user'])->where('status', '0')->get();
        $url = url('inbox?user='.$request['user']);

        foreach ($counts as $key => $value) {
            Chat::where('id', $value->id)->update(['status' => '1']);
        }

        if(array_key_exists('files', $request->all())) {
            $send_message = '';
            $message = [];
            foreach($request->file('files') as $key => $value) {
                $image = $this->upload_image($value);
                $chat = new Chat;

                $chat->user_id = Auth::user()->id;
                $chat->to = $request['user'];
                $chat->message = $image;
                $chat->type = 'file';
                $chat->save();
            }
        } else {
            $send_message = strip_tags($request['message']);
            $more = ''; if(strlen($send_message) > 25) { $more = '...'; }
            $send_message = substr($send_message, 0, 25). $more;

            $chat = new Chat;

            $chat->user_id = Auth::user()->id;
            $chat->to = $request['user'];
            $chat->message = $request['message'];
            $chat->type = 'message';
            $chat->save();
        }

        $this->notifyUsers($request['user'], $send_message, $url);

        $data['status'] = 'success';
        $data['chats'] = $this->listChat($request['user']);

        return response()->json($data);
    }

    /**
     * update chat message.
     */
    public function update(Request $request) {
        $counts = Chat::where('to', Auth::user()->id)->where('user_id', $request['user'])->where('status', '0')->get();
        foreach ($counts as $key => $value) {
            Chat::where('id', $value->id)->update(['status' => '1']);
        }

        $status = 'success';
        $message = $request['message'];

        Chat::where('id', $request['id'])->update([
            'message' => $message,
            'type' => 'message'
        ]);

        $data['status'] = $status;
        $data['chats'] = $this->listChat($request['user']);

        return response()->json($data);
    }

    /**
     * delete chat message.
     */
    public function delete($id, $user) {
        $chat = Chat::find($id);
        if($chat->type == 'file') {
            $file = public_path($chat->message);
            unlink($file);
        }
        Chat::where('id', $id)->delete();

        $data['status'] = 'success';
        $data['chats'] = $this->listChat($user);

        return response()->json($data);
    }
}
