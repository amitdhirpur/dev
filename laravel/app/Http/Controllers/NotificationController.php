<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserNotification;
use App\Chat;
use Auth, DB;


class NotificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}

    /**
     * get Notification.
     *
     * @return void
     */
    public function index(Request $request) { 
        $targetMonth = date("Y-m-d", strtotime("-1 month"));
        $targetNotification = UserNotification::whereDate('created_at', '=', $targetMonth);
        $targetChat = Chat::whereDate('created_at', '=', $targetMonth);
        if($targetNotification->get()) { $targetNotification->delete(); }
        if($targetChat->get()) { $targetChat->delete(); }
        $skip = $request->countVal;  
        $userNotification = UserNotification::where("user_notifications.user_id",Auth::id())
                            ->where("user_notifications.type",'sidebar')
                            ->leftJoin('users', 'users.id', '=', 'user_notifications.created_by')
                            ->leftJoin('user_information', 'user_information.user_id', '=', 'users.id')
                            ->orderBy('user_notifications.id', 'DESC')
                            ->select('user_notifications.*','user_information.profile','users.name')
                            ->skip($skip)->take(15)->get();

        $totalUnchecked = UserNotification::where("user_id",Auth::id())
                          ->where("type",'sidebar')->where('read','N')->count();     
        $totalCount = UserNotification::where("user_id",Auth::id())
                          ->where("type",'sidebar')->count();
        $feedscount = UserNotification::where("user_id",Auth::id())
                          ->where("type",'feed')->where('read','N')->count();

        $chatCount = $this->getCounters()->count;
         
        return response()->json(['userNotification'=> $userNotification,'unCheck' => $totalUnchecked,  'chatCount' => $chatCount, 'totalCount' => $totalCount, 'feedscount' => $feedscount]);
    }

    protected function getCounters() {
        $_id = Auth::user()->id;
        $counts = Chat::select( DB::raw('count(id) as count'))
            ->where('to', $_id)->where('noti_status', '0')->first();
        return $counts;
    }

    public function updateUncheck()
    {
        UserNotification::where("user_id",Auth::id())->where("type","sidebar")->where('read','N')->update(['read' => "Y"]);

       return response()->json([ 'success' => 'success']);
        
    }

    public function feedUncheck()
    {
        UserNotification::where("user_id",Auth::id())->where("type","feed")->where('read','N')->update(['read' => "Y"]);
        
       $totalCount= UserNotification::where("user_id",Auth::id())->where("type",'feed')->where('read','N')->count();
       return response()->json([ 'success' => 'success','totalCount' => $totalCount ]);
        
    }
}
