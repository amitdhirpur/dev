<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Project;
use App\Drive;
use App\DriveFolder;
use App\User;
use App\Models\UserNotification;
use File, Storage, DB;
class PortalApiController extends Controller
{
     public function handleSalesPortalRequest(Request $request)
    {
        $proposalId = $request->input('proposalId');
        $clientName = $request->input('clientName');
        $portal = $request->input('portal');
        $profile = $request->input('profile');
        $title = $request->input('title');
        $userName = $request->input('userName');
        $createdUserEmail = $request->input('userEmail');
        $jobCategory = $request->input('jobCategory');
        $notificationProject = Project::where('proposal_id', $proposalId)->first();
        $createdUser = User::where('email', $createdUserEmail)->first();
         if(!$createdUser) {
            $createdUser = User::where('menuroles', 'admin')->first();
         }
        $url = url('/projects');
        $body='New Project Hired By '.$userName.' '.$url; 
        
        $project = Project::where('proposal_id',$proposalId)->first();
        $users = User::where('menuroles', 'admin')->pluck('id')->toArray();       

       
        $name="";
        if($clientName) {
            $name .= $clientName.'-';
        }
        if($title) {
            $name .= json_decode($request->input('title')).'-';
        }
        if($portal) {
            $name .= $portal.'-';
        }
        if($jobCategory) {
            $name .= $jobCategory.'-';            
        }
        if($profile) {
            $name .= $profile;
        }
        $path = "/files/".$name;
        if(!$project) {
            Storage::makeDirectory($path,0777, true, true);
            $drive = $this->createJson(array_unique($users));
        }else{
            $old_project_name = $project->name;
            $old_project_path = "/files/".$project->name;
            $body='Update Project Hired By '.$userName.' '.$url;
            $targetFolder = DriveFolder::where('project_id', $project->id)->first();
            $drive = $targetFolder->drive;
        }

        $project = Project::updateOrCreate(
            ['proposal_id' => $proposalId],
            ['client' => $clientName, 'portal' => $portal, 'profile' => $profile, 'name' => $name, 'user_id' => $createdUser->id]
        );

            
            //  echo $project_name; die;
            if(isset($old_project_name) && Storage::exists($old_project_path)) {

                if(!Storage::exists($path)){
                    Storage::rename($old_project_path, $path);
                    $folders =  DriveFolder::where("project_id",$project->id)->first();
                    $rootFolder = DriveFolder::findOrFail($folders->id);
                    $folder = DB::select("select id, name,parent_id from (select * from folders order by parent_id, id) products_sorted, (select @pv := '".$folders->id."') initialisation where find_in_set(parent_id, @pv)and length(@pv := concat(@pv, ',', id))");

                    foreach ($folder as $key => $value) {
                       $DriveFolder =  DriveFolder::where("id",$value->id);
                       $get_drivefolders = $DriveFolder->first();
                       $new_path = str_replace($old_project_name, $project->name, $get_drivefolders->path);
                       $DriveFolder->update(["path"=>$new_path]);
                    }
                } 

            }
        
        $users[end($users)+1] = $project->user_id;
       

        if(!$notificationProject) {
            $status="Added a new Project";            
            $destinationUsers = array_unique($users);
            $this->UserNotification($destinationUsers,$project->id,$status,$createdUser);
        }
        
        $driveFolder = DriveFolder::updateOrCreate(
            [
                'project_id' => $project->id
            ],
            [
                'drive' => $drive,
                'name' => $project->name,
                'isFolder' => true,
                'path' => $path,
                'user_id' => $createdUser->id
            ]
        );

        $driveFolder->link = 'drives/'.base64_encode($driveFolder->id);
        $driveFolder->save();


        $users = User::where('menuroles','admin')->get();
        $tokens = [];
        foreach ($users as $key => $user) {            
            if($user && $user->device_token) {
              $tokens[] = $user->device_token;                   
            }            
        }
        if(count($tokens) > 0) { 
                      
            $user = User::send_notification($tokens, $userName, $body, null, $url);
        }
        
        return response()->json('success');
    }
    /**
     * User  Notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    private function UserNotification($users,$projectId,$status,$createdUser)
    {
        $project = Project::find($projectId);
        $text = $status." <a class='notification-project' data-id='".$project->id."'>[#".$project->id."]".$project->name."</a>";
        foreach ($users as $key => $user) {
           $userNotification =  New UserNotification;
           $userNotification->user_id = $user;
           $userNotification->text = $text;
           $userNotification->read = "N";
           $userNotification->type = "sidebar";
           $userNotification->created_by = $createdUser->id;
           $userNotification->save();
        }
        
    }
     /**
    *json data with quotes
    */
    public function createJson($users)
    {
        $count = count($users);
        $destUser="";
        if($count) {
            $i=1;
            $destUser="[";
            foreach ($users as $key => $value) {
            if($count == $i) {
            $destUser .='"drive_'.$value.'"';
            } else {
            $destUser .='"drive_'.$value.'",';
            }

            $i++;
            }
            $destUser.="]";
        }
        
        return $destUser;
    }
}
