<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;
use Auth, File;

class SettingsController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Upload file or folder.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    protected function upload_image($image, $_name) {
        $name = $_name.'-'.time().'.'.$image->getClientOriginalExtension();
        $path = '/assets/images';
        $destinationPath = public_path('/assets/images');
        if(!File::isDirectory($path)) {
            File::makeDirectory($path, 0777, true, true);
        }
        $image->move($destinationPath, $name);

        return $path.'/'.$name;
    }

    protected function getData() {
        $data['formats'] = $this->zonesData();
        $data['zone'] = Setting::where('name', 'timezone')->where('status', '0')->first();
        $data['date'] = Setting::where('name', 'date_format')->where('status', '0')->first();
        $data['time'] = Setting::where('name', 'time_format')->where('status', '0')->first();
        $data['logo'] = Setting::where('name', 'logo')->where('status', '0')->first();
        $data['emails'] = Setting::where('name', 'admin_emails')->where('status', '0')->first();
        $data['template'] = Setting::where('name', 'emailTemplate')->where('status', '0')->first();

        return $data;
    }

    /**
     * Index function of Settings.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function index() {
    	$status = true; 
        $data = $this->getData();

    	return response()->json([ 'status' => 'success', 'values' => $data, 'date' => date("Y/m/d h:i:s") ]);
    }

    protected function addValues($value, $name) {
    	$status = 'no';
    	if(Setting::where('name', $name)->count() > 0) {
    		Setting::where('name', $name)->update([ 'value' => $value ]);
    		$status = 'update';
    	} else {
    		$setting = new Setting;

    		$setting->name = $name;
    		$setting->user_id = Auth::user()->id;
    		$setting->value = $value;

    		if($setting->save()) {
    			$status = 'save';
    		}
    	}
    	return $status;
    }

    /**
     * store values to the database.
     *
     * @return void
    */
    public function store(Request $request) {
    	$message = "Something went wrong."; $status = false;
    	
    	if(array_key_exists('timezone', $request->all())) {
    		$this->addValues($request->timezone, 'timezone');
    	}
    	if(array_key_exists('date_format', $request->all())) {
    		$this->addValues($request->date_format, 'date_format');
    	}
    	if(array_key_exists('time_format', $request->all())) {
    		$this->addValues($request->time_format, 'time_format');
    	}

    	if(array_key_exists('logo', $request->all())) {
    		$image = $this->upload_image($request->file('logo'), 'logo');
    		$this->addValues($image, 'logo');
    	}
        if(array_key_exists('emails', $request->all())) {
            $this->addValues($request->emails, 'admin_emails');
        }

        if($request['_type'] == 'Template') {
            $template = array(
                'subject' => $request['subject'],
                'title' => $request['title'],
                'body' => $request['body']
            );
            $this->addValues(json_encode($template), 'emailTemplate');
        }

        $data = $this->getData();

    	return response()->json([ 'status' => 'success', 'message' => $request->_type." Values Updated Sucessfully.", 'values' => $data ]);
    }

    /**
     * get data from json file.
     *
     * @return void
    */
    protected function zonesData() {
        $path = public_path().'/assets/zones.json';
        $content = json_decode(file_get_contents($path), true);

        return $content;
    }
}
