<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Feed;
use App\Models\Setting;
use App\Models\Comment;
use App\Models\Like;
use App\Task;
use App\User;
use App\Models\Project;
use App\UserInformation;
use Auth, File, DB, Carbon\Carbon;
use App\Models\UserNotification;

class FeedController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $time_for;
    public $date_for;
    public function __construct() {
        $_time = Setting::where('name', 'time_format')->first();
        $_date = Setting::where('name', 'date_format')->first();
        $this->time_for = $_time->value;
        $this->date_for = $_date->value;
    }

    /**
     * upload files.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    protected function upload_image($file, $_id) {
        $ext = $file->getClientOriginalExtension(); 
        $filename = $file->getClientOriginalName();
        $name = pathinfo($filename,PATHINFO_FILENAME);
       
        $name = $name.'-'.time().rand(100, 999).'.'.$ext;
        $path = "/images/feeds/".$_id;
        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }

        $destinationPath = public_path('/images/feeds/'.$_id);      
        $file->move($destinationPath, $name);

        return $path."/".$name;
    }

    /**
     * List of all the feeds.
     *
     * @return void
     */
    protected function getList() {
        $current_dataTime = date("Y-m-d h:i:s");

        $user =  User::find(Auth::user()->id);
        $user->is_logged = $current_dataTime;
        $user->save();
        
    	$feeds = Feed::leftJoin('users', 'feeds.user_id', '=', 'users.id')
    		->leftJoin('user_information', 'feeds.user_id', '=', 'user_information.user_id')
            ->leftJoin("likes",function($join) {
                $join->on("likes.component_id","=","feeds.id")
                    ->where("likes.component","=","feed");
            })
            ->leftJoin("likes as l",function($join) {
                $join->on("l.component_id","=","feeds.id")
                    ->where("l.component","=","feed")
                    ->where("l.user_id","=",Auth::user()->id);
            })
          
    		->select('feeds.*', 'users.name as user_name', 'user_information.profile as user_profile', 'likes.id as like_id', 'likes.component', DB::raw('COUNT(likes.id) as likes_count'), DB::raw('COUNT(l.id) as liked_by'))
            ->groupBy('feeds.id');

    	$feeds = $feeds->orderBy('feeds.id', 'DESC');

    	return $feeds;
    }

    /**
     * List of all the feeds with comments.
     *
     * @return void
     */
    protected function feedsWithComment($ct) {
        $feeds = $this->getList()->limit($ct)->get()->each(function($feed){
            $feed->comment_count=Comment::where('component_id',$feed->id)->where('component','feed')->count();
        });
        foreach ($feeds as $key => $value) {
            $value['comment_list'] = Comment::where('component', 'feed')->where('component_id', $value->id)
                ->leftJoin('users', 'comments.user', '=', 'users.id')
                ->leftJoin('user_information', 'comments.user', '=', 'user_information.user_id')
                ->select('comments.*', 'users.name as user_name', 'user_information.profile')
                ->orderBy('id', 'DESC')->take(3)->get();
            $value['likeusers'] = Like::leftJoin('users', 'likes.user_id', '=', 'users.id')->leftJoin('user_information', 'users.id', '=', 'user_information.user_id')->select("users.name","user_information.profile")->where('likes.component_id', $value->id)->where("likes.component","=","feed")->get();    
        }
        return $feeds;
    }

    /**
     * List of birthdays..
     *
     * @return void
     */
    protected function birthdayLists() {
        $get_currunt_month_birtdays = User::leftJoin('user_information', 'users.id', '=', 'user_information.user_id')->whereMonth('user_information.birth_date','=',Carbon::today()->month)->whereDay("user_information.birth_date", ">=", Carbon::today())->orderByRaw("DAYOFMONTH(user_information.birth_date)",'ASC')->get();
        $currunt_month_birthdays = [];
        foreach ($get_currunt_month_birtdays as $key => $get_currunt_month_birtday) {
                if(isset($get_currunt_month_birtday->birth_date) && $get_currunt_month_birtday->birth_date != ""){
                    $currunt_month_birthdays[$key]["name"] = $get_currunt_month_birtday->name;
                    if(Carbon::today()->format("m-d") == date('m-d', strtotime($get_currunt_month_birtday->birth_date))){
                        $currunt_month_birthdays[$key]["birth_date"] = "Today!";
                    }else{
                       $currunt_month_birthdays[$key]["birth_date"] = date('F d', strtotime($get_currunt_month_birtday->birth_date)); 
                    }
                    $currunt_month_birthdays[$key]["thumbnail"] = $get_currunt_month_birtday->profile;
                }
        }

        // Get Next month birthdays
        $next_month_birthdays = [];

        $currunt_month = Carbon::today()->month + 1;
       
        for ($i=0; $i < 8; $i++) { 
            
            if($currunt_month > 12){
               $currunt_month = 1;
            }
            $get_next_month_birtdays = User::leftJoin('user_information', 'users.id', '=', 'user_information.user_id')->whereMonth('user_information.birth_date' , $currunt_month)->orderByRaw("DAYOFMONTH(user_information.birth_date)",'ASC')->get();
            foreach ($get_next_month_birtdays as $key => $get_next_month_birtday) {
                if(isset($get_next_month_birtday->birth_date) && $get_next_month_birtday->birth_date != ""){
                    $next_month_birthdays[$i][$key]["name"] = $get_next_month_birtday->name;
                    $next_month_birthdays[$i][$key]["birth_date"] = date('F d', strtotime($get_next_month_birtday->birth_date));
                    $next_month_birthdays[$i][$key]["thumbnail"] = $get_next_month_birtday->profile;
                }
            }
            $currunt_month++;
        }

        // Get Next Months all birthdays
        $next_all_birth = array();
        $birth_inc = 0;
        foreach ($next_month_birthdays as $key => $next_month_birthday) {
            if(!empty($next_month_birthday)){
                foreach ($next_month_birthday as $key => $birth) {
                    $next_all_birth[$birth_inc]["name"] = $birth["name"];
                    $next_all_birth[$birth_inc]["birth_date"] = date('F d', strtotime($birth["birth_date"]));
                    $next_all_birth[$birth_inc]["thumbnail"] = $birth["thumbnail"];
                    $birth_inc++;
                }
            }
        }
        // Marge birthdays
        if(count($currunt_month_birthdays) < 4){
            $birthdays = array_merge($currunt_month_birthdays,$next_all_birth);
            $birthdays = array_slice($birthdays,0,4);
        }else{
            $birthdays = $currunt_month_birthdays;
        }

        return $birthdays;
    }

    /**
     * List of appraisals..
     *
     * @return void
     */
    protected function appraisalLists() {
        $appraisals = [];

        $current = UserInformation::leftJoin('users', 'user_information.user_id', '=', 'users.id')
            ->select('user_information.id', 'user_information.appraisel_date', 'user_information.profile', 'users.name')
            ->whereNotNull('appraisel_date')->where("appraisel_date", ">=", Carbon::today())
            ->orderBy("appraisel_date" , 'ASC')->get();

        foreach ($current as $key => $value) {
            $date = Carbon::parse($value->appraisel_date);

            if($date->isCurrentMonth()) {
                $appraisals[$key] = $value;
                if($date->isToday()) {
                    $appraisals[$key]["date"] = "Today!";
                } else {
                    $appraisals[$key]["date"] = $this->date_for;
                }
            } else {
                if(count($appraisals) < 4) {
                    $appraisals[$key] = $value;
                    $appraisals[$key]["date"] = $this->date_for;
                }
            }  
        }

        return $appraisals;
    }

    /**
     * index of feeds.
     *
     * @return void
     */
    public function index() {
    	$permission = false; $_count = 0; $sidebar = array(); $_id = Auth::user()->id;
        $on_going = Task::where('status', '!=', '1')->where('assigned_to', $_id);
    	if(Auth::user()->menuroles == "admin" || Auth::user()->menuroles == "HR") {
    		$permission = true;
    	}
        if(Auth::user()->menuroles == "admin") {
            $_count = User::where('menuroles', '!=', 'admin')->where('is_active', '1')->count();
        } 
        
        if(Auth::user()->menuroles == "HR") {
            $_count = User::where('menuroles', '!=', 'admin')->where('menuroles', '!=', 'HR')->where('is_active', '1')->count();
        }
        
        $birthdays = $this->birthdayLists();
        $appraisals = $this->appraisalLists();
        $vals=[];
        $data['tasks'] = $this->listTasks($vals)->limit(5)->get();
        // adding values to the sidebar array.
        $sidebar['total_users'] = $_count;
        $sidebar['ongoing'] = $on_going->count();
        $sidebar['assisting'] = Task::whereRaw('json_contains(participants, \'['.$_id.']\')')->count();
        $sidebar['creator'] = Task::where('user', $_id)->where('status', '!=', '1')->count();
        $sidebar['following'] = Task::whereRaw('json_contains(observers, \'['.$_id.']\')')->count();
        $sidebar['birthdays'] = $birthdays;
        $sidebar['appraisals'] = $appraisals;

        // data values.
    	$data['feeds'] = $this->feedsWithComment(5);
        $data['feed_ct'] = Feed::count() - 5;
    	$data['permission'] = $permission;
        $data['time'] = $this->time_for;
    	$data['users'] = User::select('id', 'name as text')->get();
        $data['user_name'] = Auth::user()->name;
        $data['profile'] = DB::table('user_information')->where('user_id', $_id)->select('profile')->first();
        $data['sidebar_data'] = $sidebar;
        $data['projects'] = Project::latestProjects(Auth::id());
    	return response()->json($data);
    }
    protected function listTasks($request) {
        $__id = Auth::user()->id;

        $tasks = Task::leftJoin('users as from', 'tasks.user', '=', 'from.id')
            ->leftJoin('users as to', 'tasks.assigned_to', '=', 'to.id')
            ->leftJoin('projects', 'tasks.project', '=', 'projects.id')
            ->leftJoin('folders', 'folders.project_id', '=', 'tasks.project')
            ->select('tasks.*', 'from.name as created_by', 'to.name as responsible_person', 'projects.name as project_name','projects.id as project_id','folders.id as folder_id','folders.name as folder_name');

        if(empty($request)) {
            $tasks = $tasks->where('tasks.status', '!=', '1');
        }

        if(!empty($request)) {
            if(array_key_exists('status', $request)) {
                if($request['status'] || $request['status'] == '0') {
                    $tasks = $tasks->where('tasks.status', $request['status']);
                } else {
                    $tasks = $tasks->where('tasks.status', '!=', '1');
                }
            } else {
                $tasks = $tasks->where('tasks.status', '!=', '1');
            }
            if(array_key_exists('priority', $request)) {
                if($request['priority'] || $request['priority'] == '0') {
                    $tasks = $tasks->where('tasks.priority', $request['priority']);
                }
            }
            if(array_key_exists('users', $request)) {
                $val = $request['users'];
                $tasks = $tasks->where(function($query) use ($val) {
                    $query->where('tasks.assigned_to', $val)
                    ->orWhere('tasks.user', $val)
                    ->orWhereRaw('json_contains(tasks.observers, \'['.$val.']\')')
                    ->orWhereRaw('json_contains(tasks.participants, \'['.$val.']\')');
                });
            }
            if(array_key_exists('project', $request)) {
                $tasks = $tasks->where('tasks.project', $request['project']);
            }

            if(array_key_exists('_type', $request)) {
                if($request['_type'] == 'creator') {
                    $tasks = $tasks->where('tasks.user', $__id);
                } else if($request['_type'] == 'assisting') {
                    $tasks = $tasks->whereRaw('json_contains(tasks.participants, \'['.$__id.']\')');
                } else if($request['_type'] == 'ongoing') {
                    $tasks = $tasks->where('tasks.assigned_to', $__id);
                } else if($request['_type'] == 'following') {
                    $tasks = $tasks->whereRaw('json_contains(tasks.observers, \'['.$__id.']\')');
                }  
            }
        }

        if(Auth::user()->menuroles == 'admin') {
            $tasks = $tasks->withTrashed();
        } else {
            $tasks = $tasks->where(function($query) use ($__id) {
                $query->where('tasks.assigned_to', $__id)
                ->orWhere('tasks.user', $__id)
                ->orWhereRaw('json_contains(tasks.observers, \'['.$__id.']\')')
                ->orWhereRaw('json_contains(tasks.participants, \'['.$__id.']\')');
            });
        }


        $tasks = $tasks->groupBy('tasks.id')->orderBy('tasks.updated_at','DESC');
        return $tasks;
    }

    /**
     * add new feed.
     *
     * @return void
     */
    public function add(Request $request) { 
    	$status = 'error'; $images = []; $ct = (int) $request['page'] * 5;
    	$feed = new Feed;
    	$feed->description = $request->feed;
    	$feed->user_id = Auth::user()->id;
        // $feed->files = json_encode($images);
        $feed->files = json_encode($request['files']);

    	if($feed->save()) {
            // if(array_key_exists('files', $request->all())) {
            //     foreach($request->file('files') as $key => $value) {
            //         $image = $this->upload_image($value, $feed->id);
            //         $images[$key] = $image;
            //     }
            // }
            // Feed::where('id', $feed->id)->update([ 'files' => json_encode($images) ]);
    		$status = "success";
    	}

    	$data['feeds'] = $this->feedsWithComment($ct);
        $data['feed_ct'] = Feed::count() - $ct;

        $notify_users = User::where("is_active","1")->get();
        $user_ids = array();
        foreach ($notify_users as $key => $users) {
           $user_ids[] = $users->id;
        }
        $notify_users = array_unique(array_diff($user_ids, [Auth::user()->id]), SORT_REGULAR);
        $status="Create New feed";
        $type="feed";
        $this->feedNotification($notify_users,$feed->id,$status,$type);
        $url = url('/');
        $this->notifyUsers($notify_users, Auth::user()->name.' Added a new feed!', $url);

    	return response()->json([ 'status' => $status, 'values' => $data ]);
    }
     /**
     * feed  Notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    private function feedNotification($users,$feedId,$status,$type)
    {
        $feed = Feed::find($feedId);
        $text = " <p class='notification-task' data-id='".$feed->id."'>".$status."->".$feed->description."</p>";
        foreach ($users as $key => $user) {
           $userNotification =  New UserNotification;
           $userNotification->user_id = $user;
           $userNotification->text = $text;
           $userNotification->read = "N";
           $userNotification->type = $type;
           $userNotification->created_by = Auth::id();
           $userNotification->save();
        }
        
    }
    /**
     * update feed.
     *
     * @return void
     */
    public function update(Request $request, $id) {
        $ct = (int) $request['page'] * 5; $images = []; $k = 0;
        if(array_key_exists('old_files', $request->all())) {
            foreach($request['old_files'] as $old) {
                $images[$k] = $old; $k++;
            }
        }
        if(array_key_exists('files', $request->all())) {
            foreach($request->file('files') as $value) {
                $image = $this->upload_image($value, $id);
                $images[$k] = $image; $k++;
            }
        }

        Feed::where('id', $id)->update([ 
            'description' => $request->feed,
            'files' => json_encode($images)
        ]);

        $data['feeds'] = $this->feedsWithComment($ct);
        $data['feed_ct'] = Feed::count() - $ct;

        return response()->json([ 'status' => 'success', 'values' => $data ]);
    }

    /**
     * search/load more feed.
     *
     * @return void
     */
    public function search(Request $request) {
        $ct = (int) $request['page'] * 5;

        $query = Feed::leftJoin('users', 'feeds.user_id', '=', 'users.id')
            ->leftJoin('user_information', 'feeds.user_id', '=', 'user_information.user_id')
            ->leftJoin("likes",function($join) {
                $join->on("likes.component_id","=","feeds.id")
                    ->where("likes.component","=","feed");
            })
            ->leftJoin("likes as l",function($join) {
                $join->on("l.component_id","=","feeds.id")
                    ->where("l.component","=","feed")
                    ->where("l.user_id","=",Auth::user()->id);
            })
            ->select('feeds.*', 'users.name as user_name', 'user_information.profile as user_profile', 'likes.id as like_id', 'likes.component', DB::raw('COUNT(likes.id) as likes_count'), DB::raw('COUNT(l.id) as liked_by'))
            ->groupBy('feeds.id');

        $query = $query->where('feeds.description', 'LIKE', "%".$request['search']."%")
            ->orWhere('users.name', 'LIKE', "%".$request['search']."%");
        $query = $query->orderBy('feeds.id', 'DESC');

        // getting feeds with comments.
        $feeds = $query->take($ct)->get();
        foreach ($feeds as $key => $value) {
            $value['comment_list'] = Comment::where('component', 'feed')->where('component_id', $value->id)
                ->leftJoin('users', 'comments.user', '=', 'users.id')
                ->leftJoin('user_information', 'comments.user', '=', 'user_information.user_id')
                ->select('comments.*', 'users.name as user_name', 'user_information.profile')
                ->orderBy('id', 'DESC')->get();
            $value['likeusers'] = Like::leftJoin('users', 'likes.user_id', '=', 'users.id')->leftJoin('user_information', 'users.id', '=', 'user_information.user_id')->select("users.name","user_information.profile")->where('likes.component_id', $value->id)->where("likes.component","=","feed")->get();    
        }

        $data['feeds'] = $feeds;
        $data['feed_ct'] = Feed::count() - $ct;

        return response()->json($data);
    }

    /**
     * delete feed.
     *
     * @return void
     */
    public function delete(Request $request) {
        $ct = (int) $request['page'] * 5;
        Feed::where('id', $request['id'])->delete();

        $data['feeds'] = $this->feedsWithComment($ct);
        $data['feed_ct'] = Feed::count() - $ct;
        return response()->json($data);
    }
    /**
     * send chrome notifications.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    protected function notifyUsers($users, $body, $url) {
        $u_infor = DB::table('user_information')->where('user_id', Auth::user()->id)->select('thumbnail')->first();
        $tokens = [];
        foreach ($users as $key => $value) {
            if($value) {
                $user = User::select('id', 'device_token')->where('id', $value)->first();
                if($user) {
                    if($user->device_token) {
                        $tokens[] = $user->device_token; 
                    }
                } 
            }
        }
        if(count($tokens) > 0) { 
            User::send_notification($tokens, ucwords(Auth::user()->name), $body, $u_infor->thumbnail ?? null, $url);
        }

        return true;
    }
}
