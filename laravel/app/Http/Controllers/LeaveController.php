<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth, Storage, Mail, DataTables;
use App\Models\Leave;
use App\Models\Setting;
use App\User;
use App\Models\Notification;
use App\Models\UserNotification;
use DateTime;

class LeaveController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}

    /*
    *get reports
    */
    /**
        * Datatable controller.
        *
    */
    protected function generateDatatable($listing) {
        return DataTables::of($listing)
            // ->addIndexColumn()
            ->addColumn('name', function($row) {  
                return $row->name;
            })
            ->rawColumns(['name', 'subject', 'reason'])
            ->make(true);
    }

    /**
     * get leaves listing.
     *
     * @return void
     */
    public function index() {
        $leaves = Leave::leftJoin('users', 'leaves.user_id', '=', 'users.id')
            ->select('leaves.*', 'users.name', 'users.menuroles');

        if(Auth::user()->menuroles == 'admin') {} else if(Auth::user()->menuroles == 'Manager') {
            $leaves = $leaves->where('users.menuroles', '!=', 'admin')->where('users.menuroles', '!=', 'HR');
        } else if(Auth::user()->menuroles == 'HR') {
            $leaves = $leaves->where('users.menuroles', '!=', 'admin');
        } else if(Auth::user()->menuroles == 'Team Lead') {
            $leaves = $leaves->where('users.menuroles', '!=', 'admin')->where('users.menuroles', '!=', 'HR')->where('users.menuroles', '!=', 'Manager');
        } else {
            $leaves = $leaves->where('user_id', Auth::user()->id);
        }
        if(array_key_exists('start_date', $_GET) && array_key_exists('end_date', $_GET)) {
            $dt = new DateTime($_GET['start_date']);
            $startDate = $dt->format('Y-m-d');
            $dt = new DateTime($_GET['end_date']);
            $endDate = $dt->format('Y-m-d');
            $leaves = $leaves->whereDate('start_date', '>=', $startDate)->whereDate('end_date', '<=', $endDate);
        }
        if(array_key_exists('status', $_GET)) {
            $leaves = $leaves->where('leaves.status', $_GET['status']);
        }
        if(array_key_exists('users', $_GET)) {
            $leaves = $leaves->where('leaves.user_id', $_GET['users']);
        }

        // echo $leaves->toSql(); die;

        $leaves = $leaves->orderBy('id', 'DESC')->get();
        return $this->generateDatatable($leaves);
    }

    /**
     * get users for filtering.
     *
     * @return void
     */
    public function getUsers() {
        $permission = true;
        $users = User::select('id', 'name')->active()->orderBy('name', 'ASC');

        if(Auth::user()->menuroles == 'admin') {
            $users = $users->get();
        } else if(Auth::user()->menuroles == 'Manager') {
            $users = $users->where('menuroles', '!=', 'admin')->where('menuroles', '!=', 'HR')->get();
        } else if(Auth::user()->menuroles == 'HR') {
            $users = $users->where('menuroles', '!=', 'admin')->get();
        } else if(Auth::user()->menuroles == 'Team Lead') {
            $users = $users->where('menuroles', '!=', 'admin')->where('menuroles', '!=', 'HR')->where('menuroles', '!=', 'Manager')->get();
        } else {
            $users = []; $permission = false;
        }

        $data['users'] = $users;
        $data['permission'] = $permission;

        return response()->json($data);
    }

    /**
     * Show leave.
     *
     * @return void
     */
    public function show($id) {
        $permission = false;
        $leave = Leave::leftJoin('users', 'leaves.user_id', '=', 'users.id')
            ->leftJoin('user_information', 'leaves.user_id', '=', 'user_information.user_id')
            ->select('leaves.*', 'users.name', 'user_information.profile')
            ->where('leaves.id', $id)
            ->first();

        if(Auth::user()->menuroles == 'admin' || Auth::user()->menuroles == 'HR') {
            $permission = true;
        }

        $data['leave'] = $leave;
        $data['permission'] = $permission;
        $data['date_format'] = Setting::where('name', 'date_format')->first();

        return response()->json($data);
    }

    /**
     * add new leave.
     *
     * @return void
     */
    public function save(Request $request) {
        $validate = Validator::make($request->all(), [
            'start_date'        => 'required',
            'end_date'          => 'required',
            'reason'            => 'required',
        ]);
        if ($validate->fails()){
            return response()->json([
                'status' => 'error',
                'errors' => $validate->errors()
            ], 422);
        }
        $path = '/leave/'.Auth::id();
        $data=[];
        $startDate= $request->start_date;
        $endDate= $request->end_date;
        $subject= $request->subject;
        $userName = Auth::user()->name;
        $reason = $request->reason;

        $leave = new Leave;
        $leave->start_date   = $startDate;
        $leave->end_date     = $endDate;
        $leave->subject      = $subject;
        $leave->description  = $reason;
        $leave->user_id      = Auth::id();

        if($leave->save()) {
            $attachs = [];
            if ($request->hasFile('attachements')) {
                $attachements = $request->file('attachements');
                foreach($attachements as $key => $attachement) {
                    $ext = $attachement->getClientOriginalExtension();
                    $attachs[$key] = $this->upload_image($attachement,$ext,$path);
                }
                $attach_file = json_encode($attachs);
                $userInformation = Leave::updateOrCreate(
                    ['id'   => $leave->id ],
                    ['files' => $attach_file]
                );
            } 
            $roles = ['admin','Manager','Team Lead'];
            $users = User::whereIn('menuroles',$roles)->active()->select('id','email','device_token')->get();
            $i=0; 
            $ccEmails=[];
            foreach ($users as $value) {
                $ccEmails[$i] = $value->email; 
                $i++;
            }
            $emails = $ccEmails;
            $hr = User::where('menuroles','HR')->active()->first();

            $template = 'mail.leave_mail';
            $data['start_date'] = $startDate;
            $data['end_date'] = $endDate;
            $data['subject'] = $subject;
            $data['user_name'] = $userName;
            $data['from_email'] = Auth::user()->email;
            $data['emails'] = $emails;
            $data['hr_email'] = $hr->email;
            $data['reason'] = $reason;
            $data['attachment'] = $attachs;
            $data['url'] = url("/leaves")."/".$leave->id;

            Mail::send($template, $data, function($message) use ($data) {
                $message->from($data['from_email'], 'Teqtop')->to($data['hr_email'])->subject('Leave Request from '.$data['user_name'])->cc($data['emails']);
                foreach($data["attachment"] as $filePath){
                    $message->attach(public_path("/").$filePath);
                }
            });

            $tokens = [];
            $j=0;
            foreach ($users as $key => $user) {
                    if($user->device_token) {   
                        $tokens[$j] = $user->device_token; 
                   
                    $j++;
                     }
            }
            if($hr->device_token) {
               array_push($tokens, $hr->device_token);
            }
            if(count($tokens) > 0) {
                $status="Added Leave Request";
                $destUser = $users->pluck('id')->toArray();
                array_push($destUser,$hr->id);
                $this->UserNotification($destUser,$leave->id,$status);
                $userName = Auth::user()->name;
                $url = url('/leaves');
                $body='Leave Request Send By '.$userName.' '.$url;
                $user = User::send_notification($tokens, $userName, $body, null, $url);
            }
        }  
        return response()->json(['status' =>'success', 'emails' => $emails]);
    }
    /**
     * User  Notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    private function UserNotification($users,$leaveId,$status)
    {
        $leave = Leave::find($leaveId);
        $text = $status." <a class='notification-leave' data-id='".$leave->id."'>[#".$leave->id."]".$leave->subject."</a>";
        foreach ($users as $key => $user) {
           $userNotification =  New UserNotification;
           $userNotification->user_id = $user;
           $userNotification->text = $text;
           $userNotification->read = "N";
           $userNotification->type = "sidebar";
           $userNotification->created_by = Auth::id();
           $userNotification->save();
        }
        
    }
    public function update(Request $request) {
        $leave = Leave::find($request['id']);
        $roles = ['admin','Manager','Team Lead'];
        $status= "Pending";
        if($request['status'] == 1) {
            $status= "Approved";
        }
        if($request['status'] == 2) {
            $status= "Declined";
        }
       
        $users = User::whereIn('menuroles',$roles)->where('id','!=', Auth::id())->active()->select('id','device_token','email')->get();
        $hr = User::where('menuroles','HR')->where('id','!=', Auth::id())->active()->first();
        $leaveUser = User::where('id', $leave->user_id)->active()->first();
        $userName = $leaveUser->name;
        $i=0; 
        $j=0; 
        $ccEmails=[];
        foreach ($users as $value) {
            $ccEmails[$i] = $value->email; 
            $i++;
        }
        if($hr && isset($hr->email)) {
            array_push($ccEmails, $hr->email);
        }
        $emails = $ccEmails;
        
        $template = 'mail.admin_leave_mail';
        $data['emails'] = $emails;
        $data['reason'] = $request->reason ?? '';
        $data['status'] = $status;
        $data['user_name'] = $userName;
        $data['from_email'] = Auth::user()->email;
        $data['to_email'] = $leaveUser->email;
        $data['subject'] = $leaveUser->subject;
        $data['url'] = url("/leaves")."/".$leave->id;
        Mail::send($template, $data, function($message) use ($data) {
            $message->from($data['from_email'], 'Teqtop')->to($data['to_email'])->subject('Leave Request from '.$data['user_name'])->cc($data['emails']);
        });

        foreach ($users as $key => $user) {
                if($user->device_token) {   
                    $tokens[$j] = $user->device_token; 
               
                $j++;
                 }
        }
        if($hr && isset($hr->device_token)) {
           array_push($tokens, $hr->device_token);
        }
        if($leaveUser->device_token) {
           array_push($tokens, $leaveUser->device_token);
        }
        if(count($tokens) > 0) {
            $status="Response Leave Request";
            $destUser = $users->pluck('id')->toArray();
            if($hr && isset($hr->id)) { array_push($destUser,$hr->id); }
            array_push($destUser,$leaveUser->id);
            $this->UserNotification($destUser,$leave->id,$status);
            $userName = Auth::user()->name;
            $url = url('/leaves');
            $body='Leave Request Related feedback'.$url;
            $user = User::send_notification($tokens, $userName, $body, null, $url);
        }

        Leave::where('id', $request['id'])->update([
            'status' => $request['status'],
            'if_reason' => $request['reason']
        ]);

        return response()->json(['status' =>'success']);
    }
    private function mailUsers() {

    }
    // update
    public function responde(Request $request)
    {
        print_r($request->all()); die;
    }


    // upload  image 
    public function upload_image($file,$ext,$path) {
        if(!Storage::exists($path)){
            Storage::makeDirectory($path,0777, true, true);
        }
        $filename = $file->getClientOriginalName();
        $name = pathinfo($filename,PATHINFO_FILENAME);
        
        $name = $name.time().'.'.$ext;

        $destinationPath = public_path($path);        
        $file->move($destinationPath, $name);

        $path = $path."/".$name;

        return $path;
    }
    // Create Json
    public function createJson($users,$hr,$user=null)
    {
        $count = count($users);
        $destUser="";
        if($count) {
            $i=1;
            $destUser="[";
            foreach ($users as $key => $value) {
                $destUser .='"'.$value->id.'",';

            $i++;
            }            
            if($user) {
                $destUser .= '"'.$user.'",';
            }
            $destUser .= '"'.$hr->id.'"';
            $destUser.="]";
        }
        
        return $destUser;
    }
}    