<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;
use App\Models\Setting;
use App\Models\Feed;
use App\User;
use App\Task;
use Auth, File, DB;
use App\Models\UserNotification;
use App\Jobs\ProcessPodcast;

class CommentController extends Controller
{
    public $time_for;
    public function __construct() {
        $_time = Setting::where('name', 'time_format')->first();
        $this->time_for = $_time->value;
    }
    /**
     * Query listing comments.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    protected function getListing($request) {
        $comments = Comment::leftJoin('user_information', 'comments.user', '=', 'user_information.user_id')->leftJoin('users', 'comments.user', '=', 'users.id')->select('comments.*', 'users.name as user_name', 'user_information.profile  as user_profile')->where('comments.component', $request['component'])->where('comments.component_id', $request['component_id'])->latest();

        return $comments;
    }
     /**
     * Query latest comment.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    protected function getLatestComment($id) {
        $comments = Comment::leftJoin('user_information', 'comments.user', '=', 'user_information.user_id')->leftJoin('users', 'comments.user', '=', 'users.id')->select('comments.*', 'users.name as user_name', 'user_information.profile  as user_profile')->where('comments.id', $id)->first();

        return $comments;
    }


    /**
     * List of all the feeds.
     *
     * @return void
     */
    protected function getFeedList($ct) {
        $feeds = Feed::leftJoin('users', 'feeds.user_id', '=', 'users.id')
            ->leftJoin('user_information', 'feeds.user_id', '=', 'user_information.user_id')
            ->leftJoin("likes",function($join) {
                $join->on("likes.component_id","=","feeds.id")
                    ->where("likes.component","=","feed");
            })
            ->leftJoin("likes as l",function($join) {
                $join->on("l.component_id","=","feeds.id")
                    ->where("l.component","=","feed")
                    ->where("l.user_id","=",Auth::user()->id);
            })
            ->select('feeds.*', 'users.name as user_name', 'user_information.profile as user_profile', 'likes.id as like_id', 'likes.component', DB::raw('COUNT(likes.id) as likes_count'), DB::raw('COUNT(l.id) as liked_by'))
            ->groupBy('feeds.id');

        $feeds = $feeds->orderBy('feeds.id', 'DESC');

        $feeds = $feeds->take($ct)->get();
        $data['counts'] = $feeds->count() - $ct;
        foreach ($feeds as $key => $value) {
            $value['comment_list'] = Comment::where('component', 'feed')->where('component_id', $value->id)
                ->leftJoin('users', 'comments.user', '=', 'users.id')
                ->leftJoin('user_information', 'comments.user', '=', 'user_information.user_id')
                ->select('comments.*', 'users.name as user_name', 'user_information.profile')
                ->orderBy('id', 'DESC')->get();
        }

        $data['feeds'] = $feeds;

        return $data;
    }

    /**
     * send chrome notifications.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    protected function notifyUsers($users, $body, $url) {
       // echo "<pre>"; print_r($users); die;
        $u_infor = DB::table('user_information')->where('user_id', Auth::user()->id)->select('thumbnail')->first();
        $tokens = [];
        foreach ($users as $key => $value) {
            if($value) {
                $user = User::select('id', 'device_token')->where('id', $value)->first();
                if($user) {
                    if($user->device_token) {
                        $tokens[] = $user->device_token; 
                    }
                } 
            }
        }

        if(count($tokens) > 0) { 
            User::send_notification($tokens, ucwords(Auth::user()->name), $body, $u_infor->thumbnail ?? null, $url);
        }

        return true;
    }

    /**
     * upload files.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    protected function upload_image($file, $_id, $_comp) {
        $ext = $file->getClientOriginalExtension(); 
        $filename = $file->getClientOriginalName();
        $name = pathinfo($filename,PATHINFO_FILENAME);
       
        $name = $name.'-'.time().rand(100, 999).'.'.$ext;
        $path = "/images/comments/".$_comp.'/'.$_id;
        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }

        $destinationPath = public_path('/images/comments/'.$_comp.'/'.$_id);      
        $file->move($destinationPath, $name);

        return $path."/".$name;
    }
    public function linkify($value, $protocols = array('http', 'mail'), array $attributes = array())
    {
        // Link attributes
        $attr = '';
        foreach ($attributes as $key => $val) {
            $attr .= ' ' . $key . '="' . htmlentities($val) . '"';
        }
        
        $links = array();
        
        // Extract existing links and tags
        $value = preg_replace_callback('~(<a .*?>.*?</a>|<.*?>)~i', function ($match) use (&$links) { return '<' . array_push($links, $match[1]) . '>'; }, $value);


        
        // Extract text links for each protocol
        foreach ((array)$protocols as $protocol) {
            switch ($protocol) {
                case 'http':
                case 'https':   $value = preg_replace_callback('~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) { if ($match[1]) $protocol = $match[1]; $link = $match[2] ?: $match[3]; return '<' . array_push($links, '<a '.$attr.' href=\"'.$protocol.'://'.$link.'\">'.$link.'</a>') . '>'; }, $value); break;
                case 'mail':    $value = preg_replace_callback('~([^\s<]+?@[^\s<]+?\.[^\s<]+)(?<![\.,:])~', function ($match) use (&$links, $attr) { return '<' . array_push($links, "<a $attr href=\"mailto:{$match[1]}\">{$match[1]}</a>") . '>'; }, $value); break;
                case 'twitter': $value = preg_replace_callback('~(?<!\w)[@#](\w++)~', function ($match) use (&$links, $attr) { return '<' . array_push($links, "<a $attr href=\"https://twitter.com/" . ($match[0][0] == '@' ? '' : 'search/%23') . $match[1]  . "\">{$match[0]}</a>") . '>'; }, $value); break;
                default:        $value = preg_replace_callback('~' . preg_quote($protocol, '~') . '://([^\s<]+?)(?<![\.,:])~i', function ($match) use ($protocol, &$links, $attr) { return '<' . array_push($links, "<a $attr href=\"$protocol://{$match[1]}\">{$match[1]}</a>") . '>'; }, $value); break;
            }
        }
        
        // Insert all link
        return preg_replace_callback('/<(\d+)>/', function ($match) use (&$links) { return $links[$match[1] - 1]; }, $value);
    }

    /**
     * add new comment.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function add(Request $request) {  
 //   echo "<pre>"; print_r($request->all()); die;       
        $ct = (int) $request['pager'] * 3; $feeds = []; $comments = []; $count = 0;
        $message = "Something went wrong."; $status = "false";
        /*$url = '<a[\S\s]+?<\/a>(*SKIP)(*FAIL)|\bTERM\b';
        $request['comment'] = preg_replace($url, '<a href=\"$0\" target=\"_blank\" title=\"$0\">$0</a>', $request['comment']);
        $request['comment'] = str_replace('href=\"www', 'href=\"http://www', $request['comment']); */

        $request['comment'] = $this->linkify($request['comment'],array("https"),array()); 
        $request['comment'] = str_replace('href=\"www', 'href=\"https://www', $request['comment']); 

       // echo $request['comment']; die;
        $comment = new Comment;

        $comment->user = Auth::user()->id;
        $comment->component_id = $request['component_id'];
        $comment->component = $request['component'];
        $comment->comment = $request['comment'];
        


        $files = $request['files'];
        // if(array_key_exists('files', $request->all())) {
        //     foreach($request->file('files') as $key => $value) {
        //         $image = $this->upload_image($value, $request['component_id'], $request['component']);
        //         $files[$key] = $image;
        //     }
        // }
        $comment->files = json_encode($files);
        $latestComment="";
        if($comment->save()) {
            $message = "Message added successfully."; $status = "true";
            $latestComment = $this->getLatestComment($comment->id);
        }

        if($request['component'] == 'feed') {
            $a= (int) $request['pager'] * 5;
            $feeds = $this->getFeedList($a);

            $notify_users = User::where("is_active","1")->get();
            $user_ids = array();
            foreach ($notify_users as $key => $users) {
               $user_ids[] = $users->id;
            }
            $notify_users = array_unique(array_diff($user_ids, [Auth::user()->id]), SORT_REGULAR);
            $status="Added a comment on feed";
            $type="feed";
            $this->feedNotification($notify_users,$request['component_id'],$status,$type);
            $url = url('/');
            $this->notifyUsers($notify_users, Auth::user()->name.' Added a comment on feed!', $url);
        } else {
            $comments = $this->getListing($request->all())->take($ct)->get();
            $count = $this->getListing($request->all())->count() - $ct;
        }

        if($request['component'] == 'task') {   
            $componentId = $request['component_id']; 
            $project_name = "";        
            $task = Task::where('id',$componentId)->first();

            if(isset($task->projectModel->name)){
                $project_name = $task->projectModel->name;
            }


            $destinationUser[] = $task->user; $extras = [];

            if($task->observers && $task->participants) {
                $extras = array_merge(json_decode($task->observers), json_decode($task->participants));
            } elseif($task->observers || $task->participants) {
                $extras = json_decode($task->observers) || json_decode($task->participants);
            } 

            $destinationUser = array_merge($destinationUser, $extras);
            $notify = explode(',', $request['mentioned']);
            $destinationUser = array_merge($notify, $destinationUser);
            $nextKey = array_key_last($destinationUser)+1;
            $destinationUser[$nextKey] = $task->assigned_to; 
            $destinationUser = array_unique(array_diff($destinationUser, [Auth::user()->id]), SORT_REGULAR);

            $allObs = []; $convArr = [];
            if($task->observers) {
                $allObs = array_unique(array_merge(json_decode($task->observers), $notify), SORT_REGULAR);
            } else {
                $allObs = $notify;
            }
            $allObs = array_diff($allObs, [$task->user]);
            $allObs = array_diff($allObs, [$task->assigned_to]);
            $allObs = array_diff($allObs, [""]);
            if($task->participants) {
                $allObs = array_diff($allObs, json_decode($task->participants));
            }

            $allObs = array_values($allObs);
            foreach ($allObs as $key => $vals) {
                $convArr[] = (int) $vals;
            }

            Task::where('id', $task->id)->update([ 'observers' => json_encode($convArr) ]);
            
            $url = url('/tasks').'/'.base64_encode($request['component_id']);

            $status="Added a comment to task";
            $type="sidebar";
            $destinationUser = array_diff($destinationUser, [""]);
            $commentText=$request['comment'];
            // send notifications.
            $this->userNotification($destinationUser,$task->id,$status,$type,$commentText);
            $this->notifyUsers($destinationUser, 'Added a comment to task '.$project_name." - ".$url, $url);
            
            $task = Task::where('id', $task->id)->first();
            $obs = json_decode($task->observers);
            $observers = array();
            if(count($obs) > 0) {
                    foreach ($obs as $key => $value) {
                        if($value) {
                            $user = User::leftJoin('user_information', 'users.id', '=', 'user_information.user_id')->select('users.id', 'users.name', 'user_information.profile', 'users.menuroles')->where('users.id', $value)->first();
                            if($user && $user->id) { 
                                $observers[$key]['name'] = $user->name;
                                $observers[$key]['profile'] = $user->profile;
                                $observers[$key]['role'] = $user->menuroles;
                            }
                        }
                    }
                }
        }

        return response()->json([ 'status' => $status, 'message' => $message, "comments" => $comments,"latestComment" => $latestComment, 'count' => $count, 'feeds' => $feeds,'observers' => $observers ?? [] ]);
    }
     /**
     * feed  Notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    private function feedNotification($users,$feedId,$status,$type)
    {
        $feed = Feed::find($feedId);
        $send_message = strip_tags($feed->description);
        $more = ''; if(strlen($send_message) > 25) { $more = '...'; }
        $send_message = substr($send_message, 0, 25). $more;
        $text = " <p class='feed-task' data-id='".$feed->id."'>".$status."->".$send_message."</p>";
        foreach ($users as $key => $user) {
           $userNotification =  New UserNotification;
           $userNotification->user_id = $user;
           $userNotification->text = $text;
           $userNotification->read = "N";
           $userNotification->type = $type;
           $userNotification->created_by = Auth::id();
           $userNotification->save();
        }
        
    }
     /**
     * User  Notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    private function userNotification($users,$taskId,$status,$type,$commentText)
    {
        $task = Task::find($taskId);
        $send_message = strip_tags($commentText);
        $more = ''; if(strlen($send_message) > 50) { $more = '...'; }
        $send_message = substr($send_message, 0, 50). $more;
        $text = $status." <a class='notification-task' data-id='".$task->id."'>[#".$task->number."]".$task->name."(".$task->projectModel->name.")</a>The comment text is:".$send_message;
        foreach ($users as $key => $user) {
           $userNotification =  New UserNotification;
           $userNotification->user_id = $user;
           $userNotification->text = $text;
           $userNotification->read = "N";
           $userNotification->type = $type;
           $userNotification->created_by = Auth::id();
           $userNotification->save();
        }
        
    }
    /**
     * edit listed comment.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function update(Request $request) {
        $ct = (int) $request['pager'] * 3; $feeds = []; $comments = []; $count = 0;
        $files = array(); $k = 0;
        /*$url = '~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])|(<a.*?</a>(*SKIP)(*F)|http://\S+)~i';
        $request['comment'] = preg_replace($url, '<a href=\"$0\" target=\"_blank\" title=\"$0\">$0</a>', $request['comment']);
        $request['comment'] = str_replace('href=\"www', 'href=\"http://www', $request['comment']); */

        $request['comment'] = $this->linkify($request['comment'],array("https"),array()); 
        $request['comment'] = str_replace('href=\"www', 'href=\"https://www', $request['comment']); 

        if(array_key_exists('old_files', $request->all())) {
            foreach($request['old_files'] as $old) {
                $files[$k] = $old; $k++;
            }
        }
        if(array_key_exists('files', $request->all())) {
            foreach($request['files'] as $key => $value) {
                $files[$k] = $value; $k++;
            }
            // foreach($request->file('files') as $key => $value) {
            //     $image = $this->upload_image($value, $request['component_id'], $request['component']);
            //     $files[$k] = $image; $k++;
            // }
        }
        $files = json_encode($files);
        Comment::where('id', $request['id'])->update([ 'comment' => $request['comment'], 'files' => $files ]);
        if($request['component'] == 'feed') {
            $a= (int) $request['pager'] * 5;
            $feeds = $this->getFeedList($a);
        } else {
            $comments = $this->getListing($request->all())->take($ct)->get();
            $count = $this->getListing($request->all())->count() - $ct;
        }

        if($request['component'] == 'task') {   
            $componentId = $request['component_id'];         
            $task = Task::where('id',$componentId)->first();
            $destinationUser[] = $task->user; $extras = [];

            if($task->observers && $task->participants) {
                $extras = array_merge(json_decode($task->observers), json_decode($task->participants));
            } elseif($task->observers || $task->participants) {
                $extras = json_decode($task->observers) || json_decode($task->participants);
            } 

            $destinationUser = array_merge($destinationUser, $extras);
            $notify = explode(',', $request['mentioned']);
            $destinationUser = array_merge($notify, $destinationUser);
            $nextKey = array_key_last($destinationUser)+1;
            $destinationUser[$nextKey] = $task->assigned_to; 
            $destinationUser = array_unique(array_diff($destinationUser, [Auth::user()->id]), SORT_REGULAR); 

            $allObs = []; $convArr = [];
            if($task->observers) {
                $allObs = array_unique(array_merge(json_decode($task->observers), $notify), SORT_REGULAR);
            } else {
                $allObs = $notify;
            }
            $allObs = array_diff($allObs, [$task->user]);
            $allObs = array_diff($allObs, [$task->assigned_to]);
            $allObs = array_diff($allObs, [""]);
            if($task->participants) {
                $allObs = array_diff($allObs, json_decode($task->participants));
            }

            $allObs = array_values($allObs);
            foreach ($allObs as $key => $vals) {
                $convArr[] = (int) $vals;
            }
            Task::where('id', $task->id)->update([ 'observers' => json_encode($convArr) ]);

            $url = url('/tasks').'/'.base64_encode($request['component_id']);

            $destinationUser = array_diff($destinationUser, [""]);
            $status="Updated a comment to task";
            $type="sidebar";
             $commentText=$request['comment'];
            // send notifications.
            $this->userNotification($destinationUser,$task->id,$status,$type,$commentText);
            $this->notifyUsers($destinationUser, 'Updated a comment to task '.$url, $url);
            $task = Task::where('id', $task->id)->first();
            $obs = json_decode($task->observers);
            $observers = array();
            if(count($obs) > 0) {
                foreach ($obs as $key => $value) {
                    if($value) {
                        $user = User::leftJoin('user_information', 'users.id', '=', 'user_information.user_id')->select('users.id', 'users.name', 'user_information.profile', 'users.menuroles')->where('users.id', $value)->first();
                        if($user && $user->id) { 
                            $observers[$key]['name'] = $user->name;
                            $observers[$key]['profile'] = $user->profile;
                            $observers[$key]['role'] = $user->menuroles;
                        }
                    }
                }
            }
        }
        return response()->json([ 'status' => 'true', 'message' => "Comment updated sucessfully.", "comments" => $comments, 'count' => $count, 'feeds' => $feeds,'observers' => $observers ]);
    }

    /**
     * delete comment.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function delete(Request $request) {
        $ct = (int) $request['pager'] * 3; $feeds = []; $comments = []; $count = 0;
        $comm = Comment::where('id', $request['id'])->first();
        if(json_decode($comm->files)) {
             foreach (json_decode($comm->files) as $key => $value) {
                unlink(public_path($value));
            }
        }
              
         Comment::where('id', $request['id'])->delete();
        if($request['component'] == 'feed') {
            $a= (int) $request['pager'] * 5;
            $feeds = $this->getFeedList($a);
        } else {
            $comments = $this->getListing($request->all())->take($ct )->get();
            $count = $this->getListing($request->all())->count() - $ct;
        }
        
        return response()->json([ 'status' => 'success', 'comments' => $comments, 'count' => $count, 'feeds' => $feeds ]);
    }

    /**
     * List comments.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function list(Request $request) {
        $comments = $this->getListing($request->all())->take(3)->get();
        $count = $this->getListing($request->all())->count() - 3;
        
        return response()->json([ 'status' => 'success', 'comments' => $comments, 'count' => $count, 'time' => $this->time_for ]);
    }

    /**
     * List comments.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function countMore(Request $request) {
        $ct = (int) $request['pager'] * 3;
        $comments = $this->getListing($request->all())->take($ct)->get();
        $count = $this->getListing($request->all())->count() - $ct;
        return response()->json([ 'status' => 'success', 'comments' => $comments, 'count' => $count ]);
    }
     /**
     * List comments.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */
    public function feedsCountMore(Request $request) {
        $ct = (int) $request['pager'] * 3;
        $comments = $this->getListing($request->all())->take($ct)->get();
        $count = $this->getListing($request->all())->count() - $ct;
        return response()->json([ 'status' => 'success', 'comments' => $comments, 'count' => $count ]);
    }
      /**
     * add the Notification.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     */

    private function addNotification($componentId,$destinationUser,$componentName,$action,$status)
    {
        $count = count($destinationUser);
        if($count) {
            $i=1;
            $destUser="[";
            foreach ($destinationUser as $key => $value) {
                if($count == $i) {
                    $destUser .='"'.$value.'"';
                } else {
                    $destUser .='"'.$value.'",';
                }

                $i++;
            }
            $destUser.="]";
            $notitfication = New Notification;
            $notitfication->user_id          = Auth::id();
            $notitfication->destination_user = $destUser;
            $notitfication->component        = $componentName;
            $notitfication->component_id     = $componentId;
            $notitfication->action           = $action;
            $notitfication->status           = $status;

            $notitfication->save();
        }
    }
}
