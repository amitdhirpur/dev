<?php

namespace App\Http\Controllers;
use App\User;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use Crypt;
class SocialAuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public $date_format;
    public function __construct()
    {
        $date = Setting::where("name","date_format")->first();
        $time = Setting::where("name","time_format")->first();
        $this->date_format = $date->value.' '.$time->value;
        $this->middleware('auth:api', ['except' => ['socialSignup','handleGoogleCallback']]);
    }


    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        $type = false;
        if(Auth::user()->menuroles == 'admin') { $type = true; }
        return response()->json([
            'access_token' => $token,
            'user' => Auth::user()->id,
            'type' => $type,
            'date_format' => $this->date_format,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
    public function socialSignup($provider)
   {
       // Socialite will pick response data automatic
       $url =  Socialite::driver('google')->redirect()->getTargetUrl();
         return response()->json([
           'url' => $url,
         ]);
   }
   public function handleGoogleCallback()
   {
      $socialUser = Socialite::driver('google')->stateless()->user();
      if(!$socialUser->token){
          return response()->json([
            'error' => 'error',
          ]);
      }
      $appUser = User::whereEmail($socialUser->email)->first();
      $user = $appUser;

      if(!$user){
          return response()->json([
            'error' => 'error',
          ]);
      }

     if (! $token = auth()->login($user)) {
          return response()->json(['error' => 'Unauthorized'], 401);
      }

      return $this->respondWithToken($token);

    }

}
