<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Setting;
use App\User;
use App\Chat;
use Auth, DB, File;

class ChatController extends Controller
{
	/**
     * upload files.
     */
    protected function upload_image($file) {
        $ext = $file->getClientOriginalExtension(); 
        $filename = $file->getClientOriginalName();
        $name = pathinfo($filename,PATHINFO_FILENAME);
       
        $name = $name.'-'.time().rand(100, 999).'.'.$ext;
        $path = "/images/chat/".Auth::user()->id;

        if(!File::isDirectory($path)){
            File::makeDirectory($path, 0777, true, true);
        }

        $destinationPath = public_path('/images/chat/'.Auth::user()->id);
        $file->move($destinationPath, $name);

        return $path."/".$name;
    }

    protected function listChat($user) {
        $val = $user; $_id = Auth::user()->id;
        $ct = 50;
        if(array_key_exists('page', $_GET)) {
            $ct = $ct * (int) $_GET['page'];
        }

        $chats = Chat::query();
        $chats = $chats->where(function($query) use ($_id) {
            $query->where('user_id', $_id)
                ->orWhere('to', $_id);
        });
        $chats = $chats->where(function($query) use ($val) {
            $query->where('user_id', $val)
                ->orWhere('to', $val);
        });

        return $chats->latest()->skip(0)->take($ct)->get();
    }

    /**
     * index controller.
     *
     * @param  \Illuminate\Http\Request  $request
    */
    public function index() {
    	$time = Setting::where('name', 'time_format')->first();

        $counts = Chat::where('to', Auth::user()->id)->where('user_id', $_GET['user'])->where('status', '0')->get();
        foreach ($counts as $key => $value) {
            Chat::where('id', $value->id)->update(['status' => '1']);
        }

    	$data['chats'] = $this->listChat($_GET['user']);
    	$data['time'] = $time->value;
    	return response()->json($data);
    }

    protected function getCounters() {
        $_id = Auth::user()->id;
        $counts = Chat::select('user_id', DB::raw('count(id) as count'))
            ->where('to', $_id)->where('status', '0')->groupBy('user_id')->get();

        return $counts;
    }

    /**
     * realtime chat controller.
     *
     * @param  \Illuminate\Http\Request  $request
    */
    public function realtime() {
        $data['notify'] = $this->getCounters();
        $data['chats'] = $this->listChat($_GET['user']);
        return response()->json($data);
    }

    /**
     * upload files.
     */
    public function getUser() {
    	$_id = Auth::user()->id;

    	if(array_key_exists('search', $_GET)) {
            $users = User::leftJoin('user_information as info', 'users.id', '=', 'info.user_id')
                ->select('users.id', 'users.name', 'users.menuroles', 'info.profile')
                ->where('users.id', '!=', $_id)
                ->where('users.is_active', '1')
    		    ->where('users.name', 'LIKE', "%".$_GET['search']."%")->orderBy('users.name', 'ASC')->limit(10)->get();
    	} else {
            // list users according to chat.
            $chats = Chat::select(['user_id', 'to', DB::raw('MAX(created_at) AS created_date')])
                ->where(function($query) use ($_id) {
                    $query->where('user_id', $_id)
                        ->orWhere('to', $_id);
                })
                ->orderBy('created_date', 'desc')->groupBy('user_id', 'to')->get();

            $users = []; $exists = []; $a = 1;
            foreach ($chats as $key => $value) {
                if($value->user_id != $_id && !in_array($value->user_id, $exists)) {
                    if($key >= 10) {  break; }
                    $exists[] = $value->user_id;
                    $user = User::leftJoin('user_information as info', 'users.id', '=', 'info.user_id')
                        ->select('users.id', 'users.name', 'users.menuroles', 'info.profile')
                        ->where('users.id', $value->user_id)->first();
                    $users[$key] = $user;
                } else {
                    if($value->to != $_id && !in_array($value->to, $exists)) {
                        if($key >= 10) {  break; }
                        $exists[] = $value->to;
                        $user = User::leftJoin('user_information as info', 'users.id', '=', 'info.user_id')
                            ->select('users.id', 'users.name', 'users.menuroles', 'info.profile')
                            ->where('users.id', $value->to)->first();
                        $users[$key] = $user;
                    }
                }
                $a++;
            }
    	}

        $data['notify'] = $this->getCounters();
    	$data['users'] = $users;
        $data['type'] = gettype($users);
        
    	return response()->json($data);
    }

    /**
     * save chat message.
     */
    public function save(Request $request) {
        $counts = Chat::where('to', Auth::user()->id)->where('user_id', $request['user'])->where('status', '0')->get();
        foreach ($counts as $key => $value) {
            Chat::where('id', $value->id)->update(['status' => '1']);
        }

        if(array_key_exists('files', $request->all())) {
            $message = [];
            foreach($request->file('files') as $key => $value) {
                $image = $this->upload_image($value);
                $chat = new Chat;

                $chat->user_id = Auth::user()->id;
                $chat->to = $request['user'];
                $chat->message = $image;
                $chat->type = 'file';
                $chat->save();
            }
        } else {
            $chat = new Chat;

            $chat->user_id = Auth::user()->id;
            $chat->to = $request['user'];
            $chat->message = $request['message'];
            $chat->type = 'message';
            $chat->save();
        }

        $data['status'] = 'success';
        $data['chats'] = $this->listChat($request['user']);

        return response()->json($data);
    }

    /**
     * update chat message.
     */
    public function update(Request $request) {
        $counts = Chat::where('to', Auth::user()->id)->where('user_id', $request['user'])->where('status', '0')->get();
        foreach ($counts as $key => $value) {
            Chat::where('id', $value->id)->update(['status' => '1']);
        }

        $status = 'success';
        $message = $request['message'];

        Chat::where('id', $request['id'])->update([
            'message' => $message,
            'type' => 'message'
        ]);

        $data['status'] = $status;
        $data['chats'] = $this->listChat($request['user']);

        return response()->json($data);
    }

    /**
     * delete chat message.
     */
    public function delete($id, $user) {
        $chat = Chat::find($id);
        if($chat->type == 'file') {
            $file = public_path($chat->message);
            unlink($file);
        }
        Chat::where('id', $id)->delete();

        $data['status'] = 'success';
        $data['chats'] = $this->listChat($user);

        return response()->json($data);
    }
}
