<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Like;
use Auth;

class LikesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {}

    /**
     * add/remove like.
     *
     * @return void
     */
    public function like(Request $request) { 
    	$status = 'error';

    	if(Like::where('user_id', Auth::user()->id)->where('component', $request->component)->where('component_id', $request->component_id)->count() > 0) {
    		Like::where('user_id', Auth::user()->id)->where('component', $request->component)->where('component_id', $request->component_id)->delete();
    		$status = "removed";
    	} else {
	    	$like = new Like;
	    	
	    	$like->user_id = Auth::user()->id;
	    	$like->component = $request->component;
	    	$like->component_id = $request->component_id;

	    	if($like->save()) {
	    		$status = "success";
	    	}
	    }

	    $count = Like::where('component', $request->component)->where('component_id', $request->component_id)->count();

        $likefeed_users = Like::leftJoin('users', 'likes.user_id', '=', 'users.id')->leftJoin('user_information', 'users.id', '=', 'user_information.user_id')->select("users.name","user_information.profile")->where('likes.component', $request->component)->where('likes.component_id', $request->component_id)->get();

            
            $likes_html = '';
            foreach ($likefeed_users as $key => $likeuser) {
                if($likeuser->profile != ""){
                    $pro_img = '<img src="'.$likeuser->profile.'" class="pro-img">';
                }else{
                    $pro_img = '<img src="img/user-avtar.jpg" class="pro-img">';
                }
                $likes_html .= '<div>
                                   <div class="popup-left">
                                       <div class="small-rounded-img">
                                          '.$pro_img.'
                                       </div>
                                    </div> 
                                   <div class="popup-right"><b>'.$likeuser->name.'</b></div>
                                </div>';
            }

    	return response()->json([ 'status' => $status, 'counts' => $count , 'likefeed_users' => $likes_html]);
    }

    public function getlikelist(Request $request) { 
        $status = 'error';

        $get_like = Like::leftJoin('users', 'likes.user_id', '=', 'users.id')->where('component', $request->component)->where('likes.component_id', $request->component_id)->leftJoin('user_information', 'users.id', '=', 'user_information.user_id')->select("users.name","user_information.thumbnail")->get();
       
        return response()->json([ 'status' => $status, 'likelist' => $get_like ]);
    }
}
