<?php

namespace App\Http\Middleware;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\Setting;
use Closure;

class VerifyJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	if (!JWTAuth::parseToken()->authenticate()) {
            return response()->json([ 'status' => 401, 'errors' => $errors ], 401);
    	}

        return $next($request);
    }
}
