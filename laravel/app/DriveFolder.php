<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DriveFolder extends Model
{
	 protected $guarded = [];
     protected $table = 'folders';
}
