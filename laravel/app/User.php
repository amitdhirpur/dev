<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Spatie\Permission\Traits\HasRoles;
use Auth;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;
    use SoftDeletes;
    use HasRoles;
 
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = [
        'deleted_at'
    ];
    
    protected $guard_name = 'api';

    protected $attributes = [ 
        'menuroles' => 'user',
    ];

     public function userInfo()
    {
        return $this->hasOne('App\UserInformation');
    }
    public function isAdmin() {
      return $this->menuroles == 'admin';
    }

    // Notifu user public function.
    public static function send_notification($device_token, $title ,$body, $icon = null, $action = null) {
        if($icon == null || $icon == '') {
            $icon = "images/avtar-small.png";
        }

        // notification pussher data.
        $json_data = array(
            "title" => $title,
            "body" => $body,
            "click_action" => $action,
            "icon" => url($icon),
            "AuthId" => Auth::id()
        );

        $arrayToSend = array('registration_ids' => $device_token, 'data' => $json_data, 'priority'=>'high');
        $json = json_encode($arrayToSend);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);

        $headers = array();
        $headers[] = 'Authorization: key=AAAALOrk12g:APA91bGQY2jBdSng36OYMUZlv1mzioxGtRTfMBXVmRc7bX-B6eOoV1allo0HhHKoM8eebdL3aqbKCoZFxlPbL3a0_gBrr13skxX5Dx9F3GBNW0vHFNKCpLzRku-qHMTlJHTwahJDUrBA';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }

        curl_close($ch);
      //  echo "<pre>"; print_r($result); die;
        return true;
    }
     /**
     * Scope a query to only include active users.     
     */
    public function scopeActive($query)
    {
        return $query->where('is_active','1');
    }
}
