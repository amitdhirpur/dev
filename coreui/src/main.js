import 'devextreme/dist/css/dx.common.css';
import 'devextreme/dist/css/dx.light.css';
import 'core-js/stable'
import 'sweetalert2/dist/sweetalert2.min.css';
import Vue from 'vue'
import App from './App'
import router from './router'
import CoreuiVue from '@coreui/vue'
import { iconsSet as icons } from './assets/icons/icons.js'
import store from './store'
import PHPUnserialize from 'php-unserialize'
import VueSweetalert2 from 'vue-sweetalert2';
import VueLazyLoad from 'vue-lazyload'
import axios from 'axios';
import firebase from "firebase/app";
import "firebase/messaging";


const options = {
    confirmButtonColor: '#41b882',
    cancelButtonColor: '#ff7674'
}

axios.interceptors.request.use(function(config) {
    config.headers.common = {
        Authorization: `Bearer ${localStorage.getItem("api_token")}`,
        "Content-Type": "application/json",
        Accept: "application/json"
    };

    return config;
}); 

axios.interceptors.response.use(response => response, error => {
    if (error.response.status === 422) {
        store.commit('setErrors', error.response.data.errors);
    } else if (error.response.status === 401) {
        store.commit("auth/setUser", null);
        localStorage.clear();
        router.push({ name: "Login" });
    } 
    else if (error.response.status === 500) {
        store.commit("auth/setUser", null);
        localStorage.clear();
        router.push({ name: "Login" });
    } else {
        return Promise.reject(error);
    }
});

// firebase configurations & initialization.
const configOptions = {
    apiKey: "AIzaSyCl58SJ3W0VdPAjjOin0gfnthcmxqW4Wyg",
    authDomain: "teqtopportal.firebaseapp.com",
    databaseURL: "https://teqtopportal-default-rtdb.firebaseio.com",
    projectId: "teqtopportal",
    storageBucket: "teqtopportal.appspot.com",
    messagingSenderId: "192919426920",
    appId: "1:192919426920:web:8e8c4b9cde2502912ae8dd",
    measurementId: "G-6PENXZGLPM"
};

if (firebase.messaging.isSupported()) {
    firebase.initializeApp(configOptions);
    const messaging = firebase.messaging();
    localStorage.setItem("notify","true");
    messaging.onMessage(function(payload) {
         setTimeout(function() {
            let _url = '/api/notifications?countVal=0';
            axios.get(_url).then(function (response) { 
                 store.commit("settotalInboxNotification",  response.data.chatCount);
                 store.commit("setUncheckNotification",  response.data.unCheck);
                 store.commit("setNotifcationItems",  response.data);
                 var span_div = document.createElement('span');
                 span_div.className = 'feed_countt';
                 span_div.innerHTML = response.data.feedscount;
                 if(response.data.feedscount != 0){
                   if(document.getElementsByClassName("feed_countt").length > 0){
                     document.getElementsByClassName("feed_countt")[0].remove();
                   }
                   document.getElementsByClassName("c-sidebar-nav-item")[0].childNodes[0].appendChild(span_div);
                 }
            });


            
            // console.log("Message received. ", payload);
            if(localStorage.getItem("notify") == "true"){
              var notificationTitle = payload.data.title,
              notificationOptions = {
                  body: payload.data.body,
                  icon: payload.data.icon,
                  image:  payload.data.image,
                  data: {
                        url: payload.data.click_action
                  } 
              };
              var notification = new Notification(notificationTitle, notificationOptions);
              notification.onclick = function (e) {
                window.open(notificationOptions.data.url);      
              };
            };
            localStorage.setItem("notify","false");
            setTimeout(function() {
              localStorage.setItem("notify","true");
            }, 2000);
         }, 400);
      
        
    });
}

Vue.config.performance = true;
Vue.use(CoreuiVue)
Vue.use(VueSweetalert2, options)
Vue.use(VueLazyLoad);

new Vue({
  	el: '#app',
    render: h => h(App),
  	router,
  	store,  
  	icons,
  	PHPUnserialize,
  	
  	template: '<App/>',
  	components: {
    	App
  	},
});