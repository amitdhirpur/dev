import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
// Containers
const TheContainer = () => import('@/containers/TheContainer')

// Views
const Dashboard = () => import('@/views/Dashboard')

// Views - Pages
const Page404 = () => import('@/views/pages/Page404')
const Page500 = () => import('@/views/pages/Page500')
const Login = () => import('@/views/pages/Login')
const Register = () => import('@/views/pages/Register')

// Users
const Employees = () => import('@/views/employees/Employees')
const Employee = () => import('@/views/employees/Employee')
const EditEmployee = () => import('@/views/employees/EditEmployee')
const CreateEmployee = () => import('@/views/employees/CreateEmployee')

// User
const UserProfile = () => import('@/views/user/User')

//Roles
const Roles = () => import('@/views/roles/Roles')
const Role = () => import('@/views/roles/Role')
// const EditRole = () => import('@/views/roles/EditRole')
// const CreateRole = () => import('@/views/roles/CreateRole')

//Destinations
const Designations = () => import('@/views/designations/Designations')
const Designation = () => import('@/views/designations/Designation')
const EditDesignation = () => import('@/views/designations/EditDesignation')
const CreateDesignation = () => import('@/views/designations/CreateDesignation')

// Tasks routes.
const Tasks = () => import('@/views/tasks/Tasks')
const Viewtask = () => import('@/views/tasks/Viewtask')
const Editask = () => import('@/views/tasks/Editask')
const Addtask = () => import('@/views/tasks/Addtask')

// menus routes
const Menus       = () => import('@/views/menu/MenuIndex')
const CreateMenu  = () => import('@/views/menu/CreateMenu')
const EditMenu    = () => import('@/views/menu/EditMenu')
const DeleteMenu  = () => import('@/views/menu/DeleteMenu')

const MenuElements = () => import('@/views/menuElements/ElementsIndex')
const CreateMenuElement = () => import('@/views/menuElements/CreateMenuElement')
const EditMenuElement = () => import('@/views/menuElements/EditMenuElement')
const ShowMenuElement = () => import('@/views/menuElements/ShowMenuElement')
const DeleteMenuElement = () => import('@/views/menuElements/DeleteMenuElement')

const Media = () => import('@/views/media/Media')

// Social login
const Sociallogin = () => import('@/views/pages/Sociallogin')

// Projects 
const Projects = () => import('@/views/projects/Projects')
const Project = () =>  import('@/views/projects/Project')
const EditProject = () => import('@/views/projects/EditProject')
const CreateProject = () => import('@/views/projects/CreateProject')

// Logs 
const Logs = () => import('@/views/logs/Logs')
//Drives
const Drives = () => import('@/views/drive/Drives')
const DriveFiles = () => import('@/views/drive/Files')

// Settings module
const GlobalConfig = () => import('@/views/settings/GlobalConfig')

//Reports
const Reports = () => import('@/views/reports/Reports')
const Report = () => import('@/views/reports/Report')

//Leave
const Leaves = () => import('@/views/leaves/Leaves')
const Leave = () => import('@/views/leaves/Leave')
const CreateLeave = () => import('@/views/leaves/CreateLeave')
const EditLeave = () => import('@/views/leaves/EditLeave')

//Archive
const ArchiveProjects = () => import('@/views/archives/Projects')
const ArchiveEmployees = () => import('@/views/archives/Employees')

//Chat
const Chat = () => import('@/views/chat/Chat')

Vue.use(Router)

const router = new Router({
  mode: 'history', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'active',
  scrollBehavior: () => ({ y: 0 }),
  routes: configRoutes()
})

router.beforeEach((to, from, next) => {  
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (store.state.auth.isAuthenticated) {
      next();
      return;
    }
   next("/login");
  } else {
    next();
  }
});

// router.beforeEach((to, from, next) => {
//   if (to.matched.some((record) => record.meta.guest)) {
//     if (store.getters.isAuthenticated) {
//       next("/posts");
//       return;
//     }
//     next();
//   } else {
//     next();
//   }
// });

function configRoutes () {
  return [
    {
      path: '/',
      redirect: '/dashboard',
      name: 'Home',
      component: TheContainer,
      children: [
        {
          path: 'media',
          name: 'Media',
          component: Media
        },
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard,
          meta: { requiresAuth: true }
        },
        {
          path: 'menu',
          meta: { label: 'Menu'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: '',
              component: Menus,
            },
            {
              path: 'create',
              meta: { label: 'Create Menu' },
              name: 'CreateMenu',
              component: CreateMenu
            },
            {
              path: ':id/edit',
              meta: { label: 'Edit Menu' },
              name: 'EditMenu',
              component: EditMenu
            },
            {
              path: ':id/delete',
              meta: { label: 'Delete Menu' },
              name: 'DeleteMenu',
              component: DeleteMenu
            },
          ]
        },
        {
          path: 'menuelement',
          redirect: '/menu',
          meta: { label: 'MenuElement'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: ':menu/menuelement',
              component: MenuElements,
            },
            {
              path: ':menu/menuelement/create',
              meta: { label: 'Create Menu Element' },
              name: 'Create Menu Element',
              component: CreateMenuElement
            },
            {
              path: ':menu/menuelement/:id',
              meta: { label: 'Menu Element Details'},
              name: 'Menu Element',
              component: ShowMenuElement,
            },
            {
              path: ':menu/menuelement/:id/edit',
              meta: { label: 'Edit Menu Element' },
              name: 'Edit Menu Element',
              component: EditMenuElement
            },
            {
              path: ':menu/menuelement/:id/delete',
              meta: { label: 'Delete Menu Element' },
              name: 'Delete Menu Element',
              component: DeleteMenuElement
            },
          ]
        },
        {
          path: 'employees',
          meta: { label: 'Employees'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: '',
              component: Employees,
            },
            {
              path: 'create',
              meta: { label: 'Create Employee' },
              name: 'Create Employee',
              component: CreateEmployee
            },
            {
              path: ':id',
              meta: { label: 'Employee Details'},
              name: 'Employee',
              component: Employee,
            },
            {
              path: ':id/edit',
              meta: { label: 'Edit Employee' },
              name: 'Edit Employee',
              component: EditEmployee
            },
          ]
        },
        {
          path: 'user-profile',
          redirect: '/dashboard',
          meta: { label: 'User'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              name: 'UserProfile',
              path: ':id/edit',
              component: UserProfile,
              meta: { requiresAuth: true }
            },
          ]
        },
        {
          path: 'roles',
          meta: { label: 'Roles'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: '',
              component: Roles,
            },
            // {
            //   path: 'create',
            //   meta: { label: 'Create Role' },
            //   name: 'Create Role',
            //   component: CreateRole
            // },
            {
              path: ':id',
              meta: { label: 'Role Details'},
              name: 'Role',
              component: Role,
            },
            // {
            //   path: ':id/edit',
            //   meta: { label: 'Edit Role' },
            //   name: 'Edit Role',
            //   component: EditRole
            // },
          ]
        },
        {
          path: 'reports',
          meta: { label: 'Reports'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: '',
              component: Reports,
            },
            {
              path: ':id',
              meta: { label: 'Report'},
              name: 'Report',
              component: Report,
            }
          ]
        },
        {
          path: 'designations',
          meta: { label: 'Designations'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: '',
              component: Designations,
            },
            {
              path: 'create',
              meta: { label: 'Create Role' },
              name: 'Create Designation',
              component: CreateDesignation,
              meta: { requiresAuth: true }
            },
            {
              path: ':id',
              meta: { label: 'Designation Details'},
              name: 'Designation',
              component: Designation,
              meta: { requiresAuth: true }
            },
            {
              path: ':id/edit',
              meta: { label: 'Edit Designation' },
              name: 'Edit Designation',
              component: EditDesignation,
              meta: { requiresAuth: true }
            },
          ]
        },
        {
          path: 'tasks',
          meta: { label: 'Tasks'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: '/',
              component: Tasks,
            },
            {
              path: 'add',
              meta: { label: 'Add New Task' },
              name: 'Addtask',
              component: Addtask
            },
            {
              path: ':id',
              meta: { label: 'View Task' },
              name: 'Viewtask',
              component: Viewtask
            },
            {
              path: ':id/edit',
              meta: { label: 'Edit Task' },
              name: 'Edittask',
              component: Editask
            }
          ]
        }, 
        {
          path: 'leaves',
          meta: { label: 'Leaves'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: '',
              component: Leaves,
            },
            {
              path: 'create',
              meta: { label: 'Create Leave' },
              name: 'Create Leave',
              component: CreateLeave
            },
            {
              path: ':id',
              meta: { label: 'Leave Details'},
              name: 'Leave',
              component: Leave,
            },
            {
              path: ':id/edit',
              meta: { label: 'Edit Leave'},
              name: 'EditLeave',
              component: EditLeave,
            },
          ]
        },
        {
          path: 'inbox',
          meta: { label: 'Inbox'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: '',
              component: Chat,
            }
          ]
        },
        {
          path: 'settings',
          meta: { label: 'Settings'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: '/',
              name: 'Settings',
              redirect: '/settings/global'
            },
            {
              path: 'global',
              meta: { label: 'Global Configurations' },
              name: 'GlobalConfigurations',
              component: GlobalConfig
            }
          ]
        },       
        {
          path: 'projects',
          meta: { label: 'Projects'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: '',
              component: Projects,
              meta: { requiresAuth: true }
            },
            {
              path: 'create',
              meta: { label: 'Create Project' },
              name: 'Create Project',
              component: CreateProject,
              meta: { requiresAuth: true }
            },
            {
              path: ':id',
              meta: { label: 'Project Details'},
              name: 'Project',
              component: Project,
              meta: { requiresAuth: true }
            },
            {
              path: ':id/edit',
              meta: { label: 'Edit Project' },
              name: 'Edit Project',
              component: EditProject,
              meta: { requiresAuth: true }
            },
          ]
        },
        {
          path: 'archive',
          meta: { label: 'Archives'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: 'projects',
              name: 'ArchiveProjects',              
              meta: { label: 'Archives Projects'},              
              component: ArchiveProjects,
              meta: { requiresAuth: true }
            },
            {
              path: 'employees',
              meta: { label: 'Archives Employees'},
              name: 'ArchiveEmployees',
              component: ArchiveEmployees,
              meta: { requiresAuth: true }
            },
          ]
        },
         {
          path: 'logs',
          meta: { label: 'Logs'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: '',
              component: Logs,
            },            
          ]
        },
         {
          path: 'drives',
          meta: { label: 'Drives'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [
          {
              path: '',
              component: Drives,
              meta: { requiresAuth: true, label: 'Drive Folders' },
            },  
             {
              path: '*',
              meta: { requiresAuth: true, label: 'Drive Files' },
              component: DriveFiles,
              props:true,
              name: 'DriveFiles',
            },            
          ]
        },
      ]
    },
    {
      path: '/pages',
      redirect: '/pages/404',
      name: 'Pages',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
        {
          path: '404',
          name: 'Page404',
          component: Page404
        },
        {
          path: '500',
          name: 'Page500',
          component: Page500
        },
      ]
    },
    {
      path: '/',
      redirect: '/login',
      name: 'Auth',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
        {
          path: 'login',
          name: 'Login',
          component: Login
        },
        {
          path: 'register',
          name: 'Register',
          component: Register
        },
        {
          path: 'sociallogin/:provider/callback',
          name: 'Sociallogin',
          component: Sociallogin
        },
      ]
    },
    {
      path: '*',
      name: '404',
      component: Page404
    }
  ]
}
export default router;