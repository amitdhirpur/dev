import Vue from 'vue'
import Vuex from 'vuex'
import auth from "./store/auth/auth";
import project from "./store/project/project";

Vue.use(Vuex)
const state = {
  sidebarShow: 'responsive',
  sidebarMinimize: false,
  isAuthenticated: localStorage.getItem("api_token"),
  errors:[],
  user:{},
  authProfile:'/img/user-avtar.jpg',
  authName:"",
  authId:"",
  successmodal:false,
  reportmodal:false,
  taskModal:false,
  downloadBtn:false,
  totalUncheckNotification:0,
  totalInboxNotification:0,
  notifcationItems:[],
  feedscount:0,
  userLoggedin:0,
  userReport:"",
  progressbarmodal:false,
  downloadProgressBarModal:false,
  downloadPercentComplete:0,
  percentComplete:0,
  kbps: 0
}

const mutations = {
  toggleSidebarDesktop (state) {
    const sidebarOpened = [true, 'responsive'].includes(state.sidebarShow)
    state.sidebarShow = sidebarOpened ? false : 'responsive'
  },
  toggleSidebarMobile (state) {
    const sidebarClosed = [false, 'responsive'].includes(state.sidebarShow)
    state.sidebarShow = sidebarClosed ? true : 'responsive'
  },
  set (state, [variable, value]) {
    state[variable] = value
  },
  setErrors(state, errors) {
      state.errors = errors;
    },
  setUser(state, data) {
      state.user.profile =data.profile ? data.profile :'/img/user-avtar.jpg';
      state.user.name =data.name;
      state.user.id =data.id;
      state.authProfile =data.profile ? data.profile : '/img/user-avtar.jpg';
      state.authName =data.name;
      state.authId =data.id;
   },
   setUserReport(state, report) {
      state.userReport =report;
   },
   setFeedNotifcationCount(state, feedcount) {
      state.feedscount = feedcount;
   },
   setUserloggedin(state, userLoggedin) {
      state.userLoggedin = userLoggedin;
   },
   setNotifcationItems(state, data) {
      state.notifcationItems =data.userNotification;
   },
   setSuccessModal(state, value) {
      state.successmodal =value;
   },
   setReportModal(state, value) {
      state.reportmodal =value;
   },
    setTaskModal(state, value) {
      state.taskModal =value;
   },
   setDownloadBtn(state, value) {
      state.downloadBtn =value;
   },
   setUncheckNotification(state, value) {
      state.totalUncheckNotification =value;
   },
   settotalInboxNotification(state, value) {
      state.totalInboxNotification =value;
   },
   setPushMoreNotification(state, data) {
      let oldNotification = state.notifcationItems;
      let newNotification =data.userNotification;
      let updateNotification = oldNotification.concat(newNotification);
      
      state.notifcationItems = updateNotification;
   },
   setProgressBarModal(state, value) {
     state.progressbarmodal =value;
   },
   setPercentComplete(state, value) {
     state.percentComplete =value;
   },
   setDownloadPercentComplete(state, value) {
      state.downloadPercentComplete = value.percentCompleted;
      state.kbps                    = value.kbps
   },
    setDownloadProgressBarModal(state, value) {
     state.downloadProgressBarModal =value;
   },
   
}
const actions = {
  async topFunction () {
    document.querySelector('.c-body').scrollTo({top: 0, behavior: 'smooth'}); 
  }
   
}
const getters= {
    errors: state => state.errors, 
    loginUser: state => state.user,
    successModal: state => state.successmodal,
    reportmodal: state => state.reportmodal,
    userReport: state => state.userReport,
    taskModal: state => state.taskModal,
    downloadBtn: state => state.downloadBtn,
    totalUncheckNotification: state => state.totalUncheckNotification,
    totalInboxNotification: state => state.totalInboxNotification,
    notifcationItems: state => state.notifcationItems,
    feedscount: state => state.feedscount,
    userLoggedin: state => state.userLoggedin
}
export default new Vuex.Store({
  state,
  mutations,
  actions,
  getters,
  modules: {
    auth,
    project
  }
})